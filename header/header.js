$(document).ready(function(){
	   $('#imgdrawer').hide();
       responsive();
        $('#respondcover').css("display","none");
//responsive
    $(window).on('resize', function(){
                responsive();
                $('#respondcover').css("display","none");
    });

    function responsive(){
        var win = $(this); //this = window

            //desktop
            if (win.width() > 900) { 
                    $('#imgsearch').show();
                    $('#txt-search').show();
                    $('#nav ul li').show();
                    $('#maincategory').show();
                    $('#respondcateg').hide();
                    $('#imgdrawer').hide();
            }

            //mobile & tab
            if (win.width() <= 900) { 
                    $('#imgsearch').hide();
                    $('#txt-search').hide();
                    $('#nav ul li').hide();
                    $('#maincategory').hide();
                    $('#imgdrawer').show();
            }
    }

    $("#imgdrawer").click(function(){
        $('#respondcategbg').css("width","300px");
        $('#respondcateg').show();
        $("#respondcover").show();
    });

    $("#respondcover").click(function(){
               $('#respondcategbg').css("width","0px");
                $('#respondcateg').hide();
                $(this).hide();
    });



//animation 
    
    $('#menu a').mouseover(function() { 
            $(this).css("border-bottom","2px solid #b3161d");
    });
    
    $('#menu a').mouseout(function() {      
            $(this).css("border-bottom","none");
    });
    
    $(".help-content").hide();
    $(".login-content").hide();
    $(".account-content").hide();
    $(".cart-content").hide();
    /*-----------help slide-----------*/
    $(".help-dropdown").click(function(){
        $(".help-content").slideToggle(500);
        $(".login-content").slideUp(500);
        $(".account-content").slideUp(500);
        $(".cart-content").slideUp(500);
    });
    /*-----------login slide-----------*/
    $(".login-dropdown").click(function(){
        $(".login-content").slideToggle(500);
        $(".help-content").slideUp(500);
    });
    
    /*-----------account slide-----------*/
    $(".account-dropdown").click(function(){
        $(".account-content").slideToggle(500);
        $(".help-content").slideUp(500);
        $(".cart-content").slideUp(500);
    });
    
        
    /*-----------cart slide-----------*/
    $(".cart-dropdown").click(function(){
        $(".cart-content").slideToggle(500);
        $(".help-content").slideUp(500);
        $(".account-content").slideUp(500);
    });
    //main categ

    
    $(document).on({
    mouseenter: function () {
            $("#ul-main li").removeClass("active");
            $(".sub-category").css("display","none");
            $("#subcategory").css("display","block");
            $("#submen").css("display","block");
            $("#page-cover").show().css("opacity",0.6);
            $("#li-men").addClass("active");
    },

  /*  mouseleave: function () {
            $("#subcategory").css("display","none");
            $(".sub-category").css("display","none");
    }*/
    }, '#mainmen,#li-men');
    
    $(document).on({
    mouseenter: function () {
            $("#ul-main li").removeClass("active");
            $(".sub-category").css("display","none");
            $("#subcategory").css("display","block");
            $("#subwomen").css("display","block");
            $("#page-cover").show().css("opacity",0.6);
            $("#li-women").addClass("active");
            
    },

  /*  mouseleave: function () {
            $("#subcategory").css("display","none");
            $(".sub-category").css("display","none");
    }*/
    }, '#mainwomen,#li-women');
    
     $(document).on({
    mouseenter: function () {
            $("#ul-main li").removeClass("active");
            $(".sub-category").css("display","none");
            $("#subcategory").css("display","block");
            $("#subboy").css("display","block");
            $("#page-cover").show().css("opacity",0.6);
            $("#li-boy").addClass("active");
    },

   /* mouseleave: function () {
            $("#subcategory").css("display","none");
            $(".sub-category").css("display","none");
    }*/
    }, '#mainboy,#li-boy');

    $(document).on({
    mouseenter: function () {
            $("#ul-main li").removeClass("active");
            $(".sub-category").css("display","none");
            $("#subcategory").css("display","block");
            $("#subgirl").css("display","block");
            $("#page-cover").show().css("opacity",0.6);
            $("#li-girl").addClass("active");
    },

   /* mouseleave: function () {
            $("#subcategory").css("display","none");
            $(".sub-category").css("display","none");
    }*/
    }, '#maingirl,#li-girl');
    
    
    $(document).on({
    mouseenter: function () {
            $("#page-cover").show().css("opacity",0.6);
    },

  /*  mouseleave: function () {
            $("#page-cover").hide();
    }*/
    }, '#ul-main li a');
    
    $(document).on({
   /* mouseenter: function () {
            $("#page-cover").hide().css("opacity",0.6);
    },*/

    mouseleave: function () {
        $("#ul-main li").removeClass("active");
        $("#subcategory").css("display","none");
        $("#page-cover").hide();
    }
    }, '#subcategory');
    
    $(document).on({
    mouseenter: function () {
           $("#ul-main li").removeClass("active");
        $("#subcategory").css("display","none");
        $("#page-cover").hide();
    },

   /* mouseleave: function () {
           
    }*/
    }, '#header');
    
    //prod
    $(document).on("mouseover", "#prod", function() {
        $(this).css("border","1px solid lightgray");
    });
    
    $(document).on("mouseout", "#prod", function() {
        $(this).css("border","none");
    });
	

    

    //use to load //www.mixlarts.com/MixlArts/customer/cart/mirror_cart.php to be attach on div 
    $("#cartContainer").load("//www.mixlarts.com/MixlArts/customer/cart/mirror_cart.php", function(data){
        $("#cartCount").html($("#cartCountSub").val());
    });

	
    //trigger when button delete cart was fired

    if($("#tblprodcart").length){                       
                            var count_tr = $("#tblprodcart tr").length -eval(1);
                            if(count_tr <= 0){
                                count_tr = "";
                                $("#div_cart").html("Cart Empty!");
                            }
                        }
                        
                      
                        

                        $("body").find("#cartCount").html(count_tr);
    $("body").click(function(e){
		
        if($(event.target).attr('class')=== "delete btn-delete-cart"){
			
            var cart = $(event.target).attr("cart-id");
            
            if(cart == ""){
                alert("Something went wrong!");
            }else{
                $.post("//www.mixlarts.com/MixlArts/customer/cart/cart.php", {cartId:cart}, function(data){			
                    if(data === "Something went wrong!"){
                        alert(data);
                    }else if(data === "Cart Not Successfully Deleted!"){
                        alert(data);
                    }else if(data === "Cart Successfully Deleted!"){
	           alert(data);
                        $("body").find('#sc_'+cart).remove();
                        var quantity = [];
                        var price = [];
                        var total = 0, vat = 0, vatPrice = 0;
                        $(".quantity").each(function(){
                            quantity.push($(this).html());
                        });

                        $(".price").each(function(){
                            price.push($(this).html());
                        });

                        for(var x= 0; x < price.length; x++){
                            total += (quantity[x]*price[x]);
                        }
                        vat = total * 0.12;
                        vatPrice = vat + total;
                        $("body").find("#total").html(total.toFixed(2));
                        $("body").find("#vat").html(vat.toFixed(2));
                        $("body").find("#vatPrice").html(vatPrice.toFixed(2));
                        if(total.toFixed(2) <= 0){
		count_tr = "";
                            $("#cartContainer").html("Cart Empty!");
                        }
                        if($("#tblCart").length){
                            var count_tr = $("#tblCart tr").length -eval(1);
                            if(count_tr == 0)
                                count_tr = "";
                        }
	           if($("#tblprodcart").length){						
                            var count_tr = $("#tblprodcart tr").length -eval(1);
                            if(count_tr <= 0){
                                count_tr = "";
                                $("#div_cart").html("Cart Empty!");
                            }
                        }
                        $("body").find("#cartCount").html(count_tr);
                        $("#selColor").trigger("change");
                    }
                });
            }
        }
        
    }); 

    $("#txt-search").on("input", function(){
    setInterval(mirror_search(), 5000);

    });

    function mirror_search(){
        $("#cont-search").html("");
        var search = $("#txt-search").val();
        console.log(search);
        if(search !== ""){

            $.post("//www.mixlarts.com/MixlArts/header/search_result.php", {search:search}, function(data){
                if(data != "empty"){
                    $("#cont-search").html(data);
                    $("#trsearch").click(function(){
                        var search_pick = $(this).find(".p-name").html();
                        $("#txt-search").val(search_pick);
                        $("#cont-search").html("");
                        window.location.replace("//www.mixlarts.com/MixlArts/customer/search/?search="+$("#txt-search").val());
                        console.log("Search " +search_pick);
                    });
                    $("#cont-search").slideDown(500);
                }
            });
            
        }else{
            $("#cont-search").slideUp(500);
        }
    }   
});

