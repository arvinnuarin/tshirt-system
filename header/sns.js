// initialize and setup facebook js sdk
window.fbAsyncInit = function() {
	FB.init({
	appId      : '1080944878608496',
	xfbml      : true,
	version    : 'v2.5'
});

FB.getLoginStatus(function(response) {
	if (response.status === 'connected') {
		
	} else if (response.status === 'not_authorized') {
		
	} else {
		
	}
	});
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// login with facebook with extra permissions
	function login() {
	FB.login(function(response) {
	if (response.status === 'connected') {
		getInfo();
	} else if (response.status === 'not_authorized') {
		
	} else {
		alert('You are not logged into Facebook.');
	}
	}, {scope: 'email'});
}

// getting basic user info
function getInfo() {
	FB.api('/me', 'GET', {fields: 'id,first_name,last_name,email,gender'}, function(r) {

		$.post("//www.mixlarts.com/MixlArts/header/reg.php", {email: r.email, lname: r.last_name,
			fname: r.first_name, type: "facebook"}, function(data){

				FB.getLoginStatus(function(response) {
			        if (response && response.status === 'connected') {
			            FB.logout(function(response) {
			               window.location.href = "//www.mixlarts.com/MixlArts";
			            });
			        }
			    });
		});
	});

}

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();

  	$.post("//www.mixlarts.com/MixlArts/header/reg.php", {email: profile.getEmail(), lname: profile.getFamilyName(),
			fname: profile.getGivenName(), type: "google"}, function(data){

				window.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://www.mixlarts.com/MixlArts";
	});
}