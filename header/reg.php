<?php
session_start();

include $_SERVER['DOCUMENT_ROOT']."/MixlArts/php/config.php";

$email = $_POST['email'];
$fname = $_POST['fname'];
$lname = $_POST['lname'];
$type = $_POST['type'];

$stmt= $dbc->prepare("SELECT customer_id, fname, email FROM customer WHERE email=? and type=?");
$stmt->bind_param("ss", $email, $type);
$stmt->execute();	
$stmt->store_result();
$data = $stmt->num_rows();

if ($data==0) {

	$ps = ""; $status = "Fully Verified"; $email_ver="";

	$stmt= $dbc->prepare("INSERT INTO customer(email, password, lname, fname, type, status, email_ver) VALUES(?,?,?,?,?,?,?)");
	$stmt->bind_param("sssssss", $email, $ps, $lname, $fname, $type, $status, $email_ver);
	$stmt->execute();

	if ($stmt->affected_rows == 1) {

		$_SESSION['customer_name'] = $fname;
		$_SESSION['customer_email'] = $email;
		$_SESSION['customer_id'] = $dbc->insert_id;

		echo "created";
	} else {

		echo "something wrong";
	}

} else {

	$stmt->bind_result($i, $f, $e);
	
	while($stmt->fetch()) {
		
		$_SESSION['customer_id'] = $i;
		$_SESSION['customer_name'] = $f;
		$_SESSION['customer_email'] = $e;
	}
	echo "registered";
}
?>