<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="//www.mixlarts.com/MixlArts/header/header.css">
</head>
<body>

<div id = 'footer' class="col-12 col-m-12">
    <div id = "footwrapper">
        
        <div id = "foot1">
            <div id = "foot-1">
                <p id = "foot-label">CONTACT US</p>
                <div id = "foot-1-container"><img id = "footericon" src = "//www.mixlarts.com/MixlArts/header/resources/phone.png"/> <label>+63 925 652 4521</label></div>
                <div id = "foot-1-container"><img id = "footericon" src = "//www.mixlarts.com/MixlArts/header/resources/email.png"/> <label>support@mixlarts.com</label></div>
            </div>

             <div id = "foot-1">
                <p id = "foot-label">FOLLOW US</p>
                 <div id = "foot-1-container"><a href = ""><img id = "footericon2" src = "//www.mixlarts.com/MixlArts/header/resources/facebook.png"/></a> <a href = ""><img id = "footericon2" src = "//www.mixlarts.com/MixlArts/header/resources/ig.png"/></a> <a href = ""><img id = "footericon2" src = "//www.mixlarts.com/MixlArts/header/resources/gmail.png"/></a> </div>
            </div>

             <div id = "foot-1">
                <p id = "foot-label">CUSTOMER CARE</p>
                <div id = "foot-1-container"><a href = "">Help</a></div>
                <div id = "foot-1-container"><a href = "">Shipping Policy</a></div>
                <div id = "foot-1-container"><a href = "">Return Policy</a></div>
            </div>

            <div id = "foot-1">
                <p id = "foot-label">ABOUT US</p>
                <div id = "foot-1-container"><a href = "">About</a></div>
                <div id = "foot-1-container"><a href = "">Privacy Policy</a></div>
                <div id = "foot-1-container"><a href = "">Terms and Conditions</a></div>
            </div>
            
            <div id = "foot-1">
                <div id = 'divssl'><a href = 'http://www.whois.com/whois/mixlarts.com'><img id = 'imgssl' src = '//www.mixlarts.com/MixlArts/header/resources/ssl.png'></a></div>
            </div>
        </div>
        
        <div>
            <p id = 'foot'>Mixl Arts will not be held responsible for any liabilities arising in connection with any transactions between buyers and seller on this website.
            <br>Copyright ©Mixl Arts., All rights reserved.</p>
        </div>
    
    </div>
</div>
</body>
</html>