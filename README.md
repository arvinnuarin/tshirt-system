MixlArts Tshirt Customization System
===================


This README contains the **MIXLARTS Tshirt Customization System ** with Store Front Management System. This README is designed to help you get **started contributing in this project**.

----------


Let's Get Started
-------------

To start developing you need a **Sublime Text 3 text editor, XAMPP, Git Bash, internet connection, Windows PC and a lot of coffee.**

 1. Clone this git repository to your htdocs directory **C:/xampp/htdocs/**: 

    >git clone https://arvinnuarin@bitbucket.org/arvinnuarin/tshirt-system.git
 

 2. Edit Windows hosts file.
 > Open your windows host configration file. It is located in **C:/Windows/System32/drivers/etc/hosts** add paste this code on the end of the line.    
	
	> **127.0.0.1 www.mixlarts.com**

 3. Create a XAMPP Virtual Host.
>  Open your xampp virtual host configuration file. **C:/xampp/apache/extra/conf/httpd-vhosts.conf** and paste this code:
     ```<VirtualHost *:80>
    	DocumentRoot "C:/xampp/htdocs/"
    	ServerName www.mixlarts.com
    	ServerAlias www.mixlarts.com
    	<Directory "C:/xampp/htdocs/">
    		AllowOverride All
    		Require all Granted
    	</Directory>
    </VirtualHost>```

 4. Restart computer.
 5. Launch your HTML5 compatible browser and navigate to [http://www.mixlarts.com](http://www.mixlarts.com)
 
 How to contribute
-------------

 6. You may edit all codes using a Sublime Text Editor. If you have any changes, please commit on this repository.
	 >git status
	 git add (all untracked files)
	 git commit -m 'Your message here'
	 git push origin master

 7. If you found any bugs, please create an issue and pull request so that you can submit the necessary fix.


 I'm lost. Who am I gonna call?
-------------

> If you don't know what do. Please message me on [Facebook](https://facebook.com/arvinnuarin16) or throw an issue on this repository. I'll do what I can do but I can't promise that I can fix every bugs or issues.