$(document).ready(function(){
  $("#divLogIn").hide();
  $("#btnLog").click(function(){
    var txtEm = $('#txtEm').val();
    var txtPw = $('#txtPw').val();
    var ctr = 0;

    if (txtEm == "") {
      $("label#lblWarnEmail").html("Empty Email Address!");
    }else if(!isValidEmailAddress(txtEm)){
      $("label#lblWarnEmail").html("Invalid Email Address!");
    }else {
      $("label#lblWarnEmail").html("");
      ctr += 1;
    }
    if (txtPw == "") {
      $("label#lblWarnPass").html("Empty Password!");
    } else {
      $("label#lblWarnPass").html("");
      ctr += 1;
    }

    if(ctr == 2){
      $.post("../login/login.php",{txtEm:txtEm, txtPw:txtPw},
      function(data){
        alert(data);
        if(data == "Account not already Verified!" || data == "Verification Email Already Sent!"){
          window.location.replace('acc_em_auth');
        }else if(data == "Email is only Verified!"){
          window.location.replace('acc_cn_auth');
        }else if(data == "Successfully Log In!"){
          window.location.replace(window.location.href);        
        }else{
          popupOk("Log In", data);
        }
      });
    }
  });

  $("#btnOpenLogIn").click(function(){
    $(".popup").fadeIn(350);
  });

  $('.popup-close').click(function(){   
    $(".popup").fadeOut(350);
  });

  $('#btn-search').click(function(){
    if($("#txt-search").val() !== ""){
      window.location.href = "search/?search="+$("#txt-search").val();
    }
  });
});
