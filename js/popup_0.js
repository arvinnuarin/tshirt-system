function popup(content){
	$('.popup').remove();
	var div = $.parseHTML("<div class='popup' data-popup='popup-text'><div class='popup-inner'>"+content+" <button class='popup-close' data-popup-close='popup-text'>x</button></div></div>");
	$("body").append(div);
	$(".popup").fadeIn(350);



	$('.popup-close').click(function(){		
		$(".popup").fadeOut(350);
	});

}

function popupOk(header, content){
	var div = $.parseHTML("<div class='popup'><div class='popup-inner'><h2>"+header+"</h2>"+content+"<hr> <button class='popup-ok'>OK</button></div></div>");
	$("body").append(div);
	$(".popup").fadeIn(350);

	$('.popup-ok').click(function(){
		$(".popup").fadeOut(350);
		$('.popup').remove();
	});
}

function popupOkCancel(header, content){
	$('.popup').remove();
	var div = $.parseHTML("<div class='popup'><div class='popup-inner'><h2>"+header+"</h2>"+content+"<hr><div class = 'yes-no-cancel'><button class='popup-ok'>Ok</button><button class='popup-cancel'>Cancel</button><button class='popup-cancel'>Cancel</button></div></div></div>");
	$("body").append(div);
	$(".popup").fadeIn(350);


	$('.popup-yes').click(function(){		
		$(".popup").fadeOut(350);
	});

	$('.popup-no').click(function(){		
		$(".popup").fadeOut(350);
	});

	$('.popup-cancel').click(function(){		
		$(".popup").fadeOut(350);
	});
}




