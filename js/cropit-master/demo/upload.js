$(document).ready(function(){
	var start = 0;
	var obj = [];
	loadSlot();
	
	$('#export').click(function() {
		var imageData = "";
		for(var x = 0; x < obj.length; x++){
			var idpic = "#"+obj[x][0];
			imageData += $(idpic).cropit('export')+"<3";
		}
		
		//var imageData2 = $('#pic2').cropit('export');
		//var data = imageData1+"<3"+imageData2;
        //window.open(imageData);
		$.post("upload.php",{imageData:imageData},
		function(data){
			alert(obj.length);
		});
    });

	function loadSlot(){
		obj.push(["pic"+start.toString(), "rccw"+start.toString(), "rcw"+start.toString(), "not filled"]);
		$('body').append($($.parseHTML('<div></div>')).attr({ id : obj[start][0] }).addClass("image-editor").
		append($($.parseHTML('<input>')).attr({type:"file"}).addClass("cropit-image-input")).
		append($($.parseHTML('<div></div>')).addClass("cropit-preview")).
		append($($.parseHTML('<div></div>')).addClass("image-size-label").append($($.parseHTML('Resize Image')))).
		append($($.parseHTML('<input>')).attr({type:"range"}).addClass("cropit-image-zoom-input")).
		append($($.parseHTML('<button>')).attr({id:obj[start][1]}).addClass("rotate-ccw").append($($.parseHTML('Rotate counterclockwise')))).
		append($($.parseHTML('<button>')).attr({id:obj[start][2]}).addClass("rotate-cw").append($($.parseHTML('Rotate clockwise'))))
		);
	
		$("#"+obj[start][0]).cropit({
			
			exportZoom: 1.00,
			allowDragNDrop: true,
			onImageLoaded: function(){
				if(start < 4){
					loadSlot();
				}
				
			},
			onImageError: function() {
				alert("Please use an image that's at least 250px in width and 450px in height.");
			},
		});
		
		var idpic = "#"+obj[start][0];
		var idrotate = "";
		idrotate = "#"+obj[start][2];
		$(idrotate).click(function() {
			$(idpic).cropit('rotateCW');
		});
		idrotate = "#"+obj[start][1];
		$(idrotate).click(function() {
			$(idpic).cropit('rotateCCW');
		});
		start += 1;
	}
});