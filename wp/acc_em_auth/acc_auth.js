$(document).ready(function(){
	$("#spanChangeEmail").hide();
	$("#changeEmail").click(function(){
		$(this).hide();
		$("#spanChangeEmail").show();
	});

	$("#cancelEmailChange").click(function(){
		$("#spanChangeEmail").hide();
		$("#changeEmail").show();
		$("#txtChangeEmail").val("");
		$("#lblWarnChangeEmail").html("");
	});

	$("#btnVerifyEmail").click(function(){

		if($("#spanChangeEmail").css('display') === 'block'){
			if($("#txtChangeEmail").val() == ""){
				$("#lblWarnChangeEmail").html("Empty Email Address!");
			}else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#txtChangeEmail').val())){
				$("#lblWarnChangeEmail").html("Invalid Email Address!");
			}
			else{
				$.post("acc_auth.php",{txtChangeEmail: $("#txtChangeEmail").val(), txtVerCode:$.trim($('#txtVerCode').val())},
		  		function(data){
		  			popupOk("Email Verification", "Verification message is now send to your email! Please proceed to your Email.");
		  			$(".popup-ok").click(function(){
			  			if(data == 1){
				        	window.location.replace("../acc_em_auth");
				        }
		  			});

		  		});
			}

		}else if($("#spanChangeEmail").css('display') === 'none'){
			$.post("acc_auth.php",{txtVerCode:$.trim($('#txtVerCode').val())},
	  		function(data){
	  			popupOk("Email Verification", "Verification message is now send to your email! Please proceed to your Email.");
		  			$(".popup-ok").click(function(){
			  			if(data == 1){
				        	window.location.replace("../acc_em_auth");
				        }
		  			});
	  		});
		}

	});
});



