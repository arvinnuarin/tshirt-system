<html>
	<head>
		<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
		<link rel = 'stylesheet' href = "acc_auth.css">
		<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
		<script type = "text/javascript" src = "/MixlArts/js/mixlarts.js"></script>
		<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
		<script type = "text/javascript" src = "acc_auth.js"></script>
	</head>
	<body style = "background-color:#d8cfc0;">
		<div id = "bg1"></div>
		<img id = "IconEmail" src = "Icons/IconEmail.png">
		<label id = "lblEmail">Email Verification</label> <br />


		<?php
			require_once('../../php/mixlarts.php');
			session_start();
			$mixlarts = new MixlArts();
			$user = "employee";
			if($mixlarts -> get_user_stat($_SESSION['id'], "Email Send", $user) || $mixlarts -> get_user_stat($_SESSION['id'], "Email Verified", $user))
				{
					$btnVerifyEmail = "Resend Verification Email";
					echo "<label id = 'lblOrder'>Your Message was already Sent. Check our message to your Email for more info.</label> <br />";
				}
			else{
					$btnVerifyEmail = "Verify via Email";
					echo "<label id = 'lblOrder'>In order to use your account, you must verify your account via Email you provided.</label> <br />";
				}
		?>
		<label id = "lblSave1"> Your saved email is: </label> 
		<label id = "lblSave"> <?php $email = $mixlarts -> get_email($_SESSION['id'], $user); echo $email[0]['email']?> </label>
		<button id = 'changeEmail'>Change your email</button><br />
		<span id = 'spanChangeEmail'><input type = 'text' id = 'txtChangeEmail' placeholder = "Enter new email">
		<label id = 'lblWarnChangeEmail'></label><button id = 'cancelEmailChange'>Cancel</button></span><br />
		<button id = 'btnVerifyEmail'><?php echo $btnVerifyEmail;?></button><br />
	</body>
</html>