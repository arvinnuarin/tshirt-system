<?php
	session_start();
	require_once('../../php/mixlarts.php');
	require_once('../../php/send_message.php');
	$user = "employee";
	$mixlarts = new MixlArts();
	$verId = $_SESSION['id'];	
	$verCode = md5(uniqid(rand(), true));
	if(isset($_POST['txtChangeEmail'])){
		if(!empty($_POST['txtChangeEmail'])){
			$txtChangeEmail = $_POST['txtChangeEmail'];
			$mixlarts -> update_email($_SESSION['id'], $txtChangeEmail, $user);
		}
	}else{
		$txtChangeEmail = $_SESSION['email'];
	}
	if($mixlarts -> update_ver_email_code($_SESSION['id'], $verCode, $user)){
		if($send_email = new SendEmail($txtChangeEmail, "Verify Email", "Click this to activate your account https://localhost/MixlArts/wp/verify_email/verify_email_form.php?verId=$verId&verCode=$verCode")){
			$mixlarts -> update_user_status($verId, "Email Send", $user);
			echo 1;
		}
	}


?>