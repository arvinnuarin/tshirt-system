<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			echo "<head>";
			?>
			<title>Raw Material</title>
			<link rel="stylesheet" type="text/css" href="raw_material.css">
			<link rel="stylesheet" type="text/css" href="../../css/loading.css">
			<link rel="stylesheet" type="text/css" href="../../css/popup.css">
			<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type = "text/javascript" src = "../../js/popup_0.js"></script>
			<script type = "text/javascript" src = "../../js/loading.js"></script>
			<script type = "text/javascript" src = "raw_material.js"></script>
			<?php
			require_once('../setup/setup_form.php');
			echo "</head>";
			echo "<body>";
			$setup = new SetUp($_SESSION['position']);
			require_once("../../php/awp.php");
			$awp = new Awp();

			if(isset($_GET['start'])){
				if(!empty($_GET['start'])){
					if(is_numeric($_GET['start'])){
						$start = intval($_GET['start']);
					}else{
						$start = 0;
					}
				}else{
					$start = 0;
				}
			}else{
				$start = 0;
			}
			$per_page = 10;
			if(isset($_GET['search']) && isset($_GET['filter'])){
				if($_GET['search'] !== "" && $_GET['filter'] !== ""){
					$search = $_GET['search'];
					$filter_id = $_GET['filter'];
					if($filter_id >= 0 && $filter_id <= 4){
						if($filter_id == 0){
							$filter = "raw_material.name";
						}else if($filter_id == 1){
							$filter = "raw_material.description";
						}
					}else{
						$search = "";
						$filter = "raw_material.name";
						$filter_id = -1;
					}
				}else{
					$search = "";
					$filter = "raw_material.name";
					$filter_id = -1;
				}
			}else{
				$search = "";
				$filter = "raw_material.name";
				$filter_id = -1;
			}

			$sort_trans = array(array("id_asc", "rm_id", "asc"), array("id_desc", "rm_id", "desc"),array("name_asc", "name", "asc"), array("name_desc", "name", "desc"), array("desc_asc", "rm_desc", "asc"), array("desc_desc", "rm_desc", "desc"), array("stock_asc", "stocks", "asc"), array("stock_desc", "stocks", "desc"), array("price_asc", "price", "asc"), array("price_desc", "price", "desc"));

			foreach ($sort_trans as $st) {
				if(isset($_GET['sort'])){
					if(!empty($_GET['sort'])){
						$ctr = 0;
						if($st[0] == $_GET['sort']){
							$sort = $_GET['sort'];
							$col_order = $st[1];
							$order = $st[2];
							break;
						}else{
							$ctr += 1;
						}
						
						if(count($sort_trans) == $ctr){
							$sort = "";
							$col_order = "name";
							$order = "asc";
						}
					}else{
						$sort = "";
						$col_order = "name";
							$order = "asc";
					}
				}else{
					$sort = "";
					$col_order = "name";
					$order = "asc";
				}
			}

			if(isset($_GET['CRITICAL'])){
				$status = "CRITICAL";
			}else
				$status = "";


			$per_page = 10;
			$raw_materials = $awp -> get_raw_materials($start, $per_page, $col_order, $order, $filter, $search, $status);
			$record_count = $awp -> get_raw_materials_count($filter, $search, $status);

			echo "<ul class = 'topMenu'>";
			$count = $awp -> get_raw_materials_count("name", "", "CRITICAL");
			echo "<li><a href = 'raw_material/?CRITICAL'>Critical Stocks<label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
			echo "<li><a class = 'active' href = 'raw_material'>Raw Materials</a></li>";
			echo "</ul>";
			
			echo "<h1>Raw Material Management</h1>";

			echo "<input type = 'hidden' value = '$status' id = 'status'>";
			
			echo "<a id = 'btnAddRaw' href = 'raw_material/add_raw_material_form.php'>Add Raw Material</a>";
			echo "<div id = 'divRaw'>";
			$arr_sort = array(array("ID (Lowest to Highest)", "id_asc"), array("ID (Highest to Lowest)", "id_desc"),array("Name (A-Z)", "name_asc"), array("Name (Z-A)", "name_desc"), array("Description (A-Z)", "desc_asc"), array("Description (Z-A)", "desc_desc"), array("Stock (Lowest to Highest)", "stock_asc"), array("Stock (Highest to Lowest)", "stock_desc"), array("Price (Lowest to Highest)", "price_asc"), array("Price (Highest to Lowest)", "price_desc"));
			
			echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search via ";
			$select = array("Name", "Description");
			echo "<select id = 'selSearch'>";
			for($x = 0; $x < count($select); $x++) {
				echo "<option value = '$x'"; 
				if($x == $filter_id){
					echo " selected ";
				}
				echo ">".$select[$x]."</option>";
			}
			echo "</select> ";
			echo "<input type = 'text' id = 'txtSearch' value = '$search'/> <input type = 'image' value = 'Search' id = 'btnSearch' src = 'raw_material/icon/imgSearch.png'>";
			
			echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: <select id = 'selSort'>";	
			foreach($arr_sort as $s){
				$filter_url = "/wp/raw_material/?";
				if($search != ""){
					$filter_url .= "search=$search&";
				}
				if($start !== ""){
					$filter_url .= "start=$start&";
				}
				if($status !== "")
					$filter_url .= "$status&";
				$filter_url .= "sort=".$s[1];
				echo "<option ";
				if($sort == $s[1]){
					echo "selected";
				}
				echo " value = '".$filter_url."'>".$s[0]."</option>";
			}
			echo "</select>";

			if(count($raw_materials) > 0){
				$header = array("ID", "Name", "Description", "Date", "Stock", "Price", "Option");
				echo "<table>";
				foreach ($header as $h) {
					echo "<th>$h</th>";
				}
				foreach ($raw_materials as $rm){
					echo "<tr rm-id = '".$rm['rm_id']."'>";
					echo "<td>RAWMAT-".$rm['rm_id']."</td>";
					echo "<td class = 'name'>".$rm['name']."</td>";
					echo "<td>".$rm['description']."</td>";
					$datetime = new DateTime($rm['time']);
					echo "<td class = 'slot-date'>".$datetime->format('F j, Y g:ia')."</td>";
					echo "<td class = 'stock'><label class = 'lblStocks'>".$rm['stock']."</label><input type = 'text' style = 'display:none;' class = 'txtStocks' value = '".$rm['stock']."' base = '".$rm['stock']."'></td>";
					echo "<td class = 'price'><label class = 'lblPrice'>Php ".number_format($rm['price'],2)."</label><input type = 'text' style = 'display:none;' class = 'txtPrice' value = '".$rm['price']."' base = '".$rm['price']."'></td>";
					echo "<td><a href = '#' class = 'update'btn-name = 'update'>Update Information</a><a href = '#' style = 'display:none;' class = 'cancel'>Cancel</a></td>";
					echo "</tr>";
				}
				echo "</table>";
			}else{
				echo "Empty Raw Materials!";
			}
			
			if($record_count[0]['count'] > 10){
				$page_url = "";

				if($search != ""){
					$page_url .= "&search=$search";
				}

				if($sort != ""){
					$page_url .= "&sort=$sort";
				}

				if($status != ""){
					$page_url .= "&status";
				}

				if($page_url != ""){
					$page_url .= "&";
				}

				$page_url = "wp/raw_material/?".$page_url."&";

				$prev = $start - $per_page;
				if($start > 0){
					echo "<a href = '".$page_url."start=$prev'>Prev</a>";
				}

				$i = 1;
				for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
					if($start != $x){
						echo " <a href = '".$page_url."start=$x'>$i</a> ";
					}else{
						echo " <a href = '".$page_url."start=$x'><b>$i</b></a> ";
					}
					$i++;
				}

				$next = $start + $per_page;
				if($start < $record_count[0]['count'] - $per_page ){
					echo " <a href = '".$page_url."start=$next'>Next</a>";
				}
			}
			echo "</div>";
			echo "</body>";
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}

?>