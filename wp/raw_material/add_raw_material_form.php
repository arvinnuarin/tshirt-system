<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			echo "<head>";
			?>
			<link rel="stylesheet" type="text/css" href="add_raw_material.css">
			<link rel="stylesheet" type="text/css" href="../../css/popup.css">
			<link rel="stylesheet" type="text/css" href="../../css/loading.css">
			<script type="text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type="text/javascript" src = "../../js/popup_0.js"></script>
			<script type="text/javascript" src = "../../js/loading.js"></script>
			<script type="text/javascript" src = "add_raw_material.js"></script>
			<title>Raw Material</title>
			<?php
								require_once('../setup/setup_form.php');

			echo "</head>";
			
			echo "<body>";
			$setup = new SetUp($_SESSION['position']);
			echo "<h1>Add Raw Material</h1>";
			echo "<div id ='divRaw'>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name: <input type = 'text' id = 'rawMatName'/>";
			echo "<br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style = 'color:red; font-size:13px;' id = 'lblRawMatName'></label>";
			echo "<br />";
			echo "Description: <textarea id = 'rawMatDesc' style = 'width:300px; height:100px;'></textarea>";
			echo "<br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style = 'color:red; font-size:13px;' id = 'lblRawMatDesc'></label>";
			echo "<br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Stocks: <input type = 'number' id = 'stocks' />";
			echo "<br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style = 'color:red; font-size:13px;' id = 'lblStocks'></label>";
			echo "<br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price: <input type = 'text' id = 'price' />";
			echo "<br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style = 'color:red; font-size:13px;'  id = 'lblPrice'></label>";
			echo "<br />";
			echo "<input class = 'btnAdd' type = 'button' value = 'Add Raw Material' id = 'add' />";
			echo "</div>";
			echo "</body>"; 
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}
?>