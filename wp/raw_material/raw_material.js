$(document).ready(function(){
	$(".tabRaw").css("background-color", "#2c3b41");
    $(".tabRaw").css("color","white");
    $(".tabRaw").css("border-left","2px solid #e04545");
	
	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	$("#btnSearch").click(function(){
		if($("#selSearch").val() >= 0 && $("#selSearch").val() <= 1){
			window.location.href = "raw_material/?start=0&search="+$("#txtSearch").val()+"&filter="+$("#selSearch").val()+"&"+$("#status").val();
		}
	});

	$(".update").click(function(e){
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var tr = $(this).parent().parent();
		if($(this).attr('btn-name') == "update"){
			$(this).attr('btn-name', 'proceed');
			$(this).next().css("display", "inline");
			$(this).html("Proceed");
			tr.find(".stock").find(".lblStocks").css("display", "none");
			tr.find(".stock").find(".txtStocks").css("display", "inline");
			tr.find(".price").find(".lblPrice").css("display", "none");
			tr.find(".price").find(".txtPrice").css("display", "inline");
		}else if($(this).attr('btn-name') == "proceed"){
			var price = tr.find(".price").find(".txtPrice");
			var lblPrice = tr.find(".price").find(".lblPrice");
			var stock = tr.find(".stock").find(".txtStocks");
			var lblStock = tr.find(".stock").find(".lblStocks");
			var id = tr.attr("rm-id");
			if((price.val() !== "" && price.val() !== price.attr('base')) && (stock.val() !== "" && stock.val() !== stock.attr('base'))){
				loading();
				$.post("raw_material/raw_material.php",{id:id, price:price.val(), stock:stock.val(), date:currentDate, name:tr.find(".name").html()}, function(data){
					if(data){
						$(".back").remove();
						if(data.indexOf("Success") != -1){
							var x = data.split("#");
							tr.find(".slot-date").html(x[1]);
							price.attr('base',price.val());
							lblPrice.html("Php "+eval(price.val()).toFixed(2)); 
							stock.attr('base',stock.val());
							lblStock.html(stock.val());
							popupOk("Raw Materials", "Stock and Price Successfully Updated!");
						}else{
							popupOk("Warning", "Failed to Update!");
						}
					}
				});
			}else if(price.val() !== "" && price.val() !== price.attr('base')){
				loading();
				$.post("raw_material/raw_material.php",{id:id, price:price.val(),date:currentDate, name:tr.find(".name").html()}, function(data){
					if(data){
						$(".back").remove();
						if(data.indexOf("Success") != -1){
							price.attr('base',price.val());
							lblPrice.html("Php "+eval(price.val()).toFixed(2)); 
							popupOk("Raw Materials",  "Price Successfully Updated!");
						}else{
							popupOk("Warning", "Failed to Update!");
						}
					}
				});
			}else if(stock.val() !== "" && stock.val() !== stock.attr('base')){
				loading();
				$.post("raw_material/raw_material.php",{id:id, stock:stock.val(),  date:currentDate, name:tr.find(".name").html()}, function(data){
					if(data){
						$(".back").remove();
						if(data.indexOf("Success") != -1){
							var x = data.split("#");
							tr.find(".slot-date").html(x[1]);
							stock.attr('base',stock.val());
							lblStock.html(stock.val());
							popupOk("Raw Materials", "Stocks Successfully Updated!");
						}else{
							popupOk("Warning", "Failed to Update!");
						}
					}
				});
			}
			$(this).html("Update Information");
			$(this).next().css("display", "none");
			$(this).attr('btn-name', 'update');
			lblPrice.css("display", "inline");
			price.css("display", "none");
			lblStock.css("display", "inline");
			stock.css("display", "none");
		}
		e.preventDefault();
	});

	$(".cancel").click(function(e){
		var tr = $(this).parent().parent();
		$(this).css("display", "none");
		$(this).prev().css("display", "inline");
		$(this).prev().attr('btn-name', 'update');
		$(this).prev().html("Update Information");
		tr.find(".price").find(".lblPrice").css("display", "inline");
		tr.find(".price").find(".txtPrice").css("display", "none");
		tr.find(".stock").find(".lblStocks").css("display", "inline");
		tr.find(".stock").find(".txtStocks").css("display", "none");
		e.preventDefault();
	});
});