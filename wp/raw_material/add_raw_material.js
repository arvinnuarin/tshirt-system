$(document).ready(function(){
		$(".tabRaw").css("background-color", "#2c3b41");
    $(".tabRaw").css("color","white");
    $(".tabRaw").css("border-left","2px solid #e04545");
	
	$("#stocks").on("input propertychange", function(){
		$(this).val($(this).val().replace(/[^0-9]/g,''));
	});

	$("#price").on("input propertychange", function(){
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
	});

	$("#add").click(function(){
		var ctr = 0;
		if($("#rawMatName").val() == ""){
			$("#lblRawMatName").html("Empty Raw Materials Name!");
		}else{
			ctr += 1;
		}
		if($("#rawMatDesc").val() == ""){
			$("#lblRawMatDesc").html("Empty Raw Materials Description!");
		}else{
			ctr += 1;
		}
		if($("#stocks").val() == ""){
			$("#lblStocks").html("Empty Stocks!");
		}else{
			ctr += 1;
		}
		if($("#price").val() == ""){
			$("#lblPrice").html("Empty Price!");
		}else{
			ctr += 1;
		}

		if(ctr == 4){
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			loading();
			$.post("add_raw_material.php", {name:$("#rawMatName").val(), 
				description:$("#rawMatDesc").val(), stocks:$("#stocks").val(), 
				price:$("#price").val(), date:currentDate}, function(data){
				if(data){
					$(".back").remove();
					if(data == "Success"){
						popupOk("Raw Material Successfully Added!");
						$(".popup-ok").click(function(){
							window.location.replace("../raw_material/");
						});
					}else if(data == "Failed to add raw materials"){
						popupOk("Raw Material Failed to Add!");
					}else if(data == "Failed to add raw materials log"){
						popupOk("Raw Materials Logs Failed to Add!");
					}else{
						popupOk("Something went wrong!");
					}
				}
			});
		}
	});
});