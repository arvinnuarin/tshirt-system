$(document).ready(function(){
    $(".tabCusTransaction").css("background-color", "#2c3b41");
    $(".tabCusTransaction").css("color","white");
    $(".tabCusTransaction").css("border-left","2px solid #e04545");
	$('#popup1').hide();

	$(".btnViewOrder").click(function(e){
		$.post("orders/orders.php", {oId:$(this).attr("o-id")}, function(data){
			if(data){
				if(data.indexOf("table") || data == "No Transaction to Show!"){
					$("#popup-inner-sub").html("<a href = '#' class = 'close'>x</a>"+data);
					$(".popup-ci").fadeIn(500);
					$(".close").click(function(e){
						$(".popup-ci").fadeOut(500);
						e.preventDefault();
					});
				}else{
					alert("Something went wrong!");
				}
			}
		});
		e.preventDefault();
	});

	$('.cmbStatus').on('change', function(){
		
		 var status   = $("option:selected", this).text();
		 var orderId = $(this).attr('id');
		 var custId = $(this).attr('cust');
		 var addr= $(this).attr('addr');

		if (confirm("Do you want to update order status?")){

			if (status == "SHIPPED"){

			 	$.post('orders/showAddress.php', {addrId: addr}, function(data){

		                   		$('#lblname').text(data.name);
		                   		$('#lblAddress').text(data.address);
		                   		$('#lblContact').text(data.contact);

		                   		$('#popup1').show();
		                    		 $('#popup-ok1').click(function(){
		                       		 	
		                       		 	var trck = $('#txtTracking').val(), cour = $('.selCourier :selected').html();
		                    		 	$.post('orders/updateStatus.php', {status: status, orderId : orderId, custId: custId, courier: cour, track: trck}, function(data){
		             					
		             					if (data == 'success') {
		                    		 			window.location.replace("orders");	
		             					} else {
		             						alert(data);
		             					}
		                    		 	});
		                   		});	                   		

			 		console.log("JSON " + data);
			 	},'json');
			 } else {

			 	$.post('orders/updateStatus.php', {status: status, orderId : orderId, custId: custId}, function(data){
			 		
			 		alert(data);
			 		popupOk("",  "Status Updated!");
			                    	$('.popup-ok').click(function(){
			                       		
			                       		window.location.replace("orders");
			                	 });
			 	});
			}	
		}
	});
});