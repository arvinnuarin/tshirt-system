<html>
<head>
	<link rel = 'stylesheet' href = "order.css">
	<link rel = 'stylesheet' href = "../../css/popup.css">
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
    <script type = "text/javascript" src = "orders.js"></script>
    <script type = "text/javascript" src = "../../js/popup_0.js"></script>
	<title>Customer Transaction</title>
</head>
<body>
<?php
	session_start();
	require_once('../../php/awp.php');	
	require_once('../setup/setup_form.php');
	$setup = new SetUp($_SESSION['position']);
	$awp = new Awp();
	$arr_const_size = unserialize (SIZE);

	if(isset($_GET['start'])){
		if(!empty($_GET['start'])){
			if(is_numeric($_GET['start'])){
				$start = intval($_GET['start']);
			}else{
				$start = 0;
			}
		}else{
			$start = 0;
		}
	}else{
		$start = 0;
	}

	if(isset($_GET['search']) && isset($_GET['filter'])){
		if(!empty($_GET['search']) && !empty($_GET['filter'])){
			$search = $_GET['search'];
			$filter_id = $_GET['filter'];
			if($filter_id > 0 && $filter_id <= 2){
				if($filter_id == 1){
					$filter = "product_design.name";
				}else if($filter_id == 2){
					$filter = "category.sub_cat_name";
				}
			}else{
				$search = "";
				$filter = "product_design.pd_id";
				$filter_id = -1;
			}
		}else{
			$search = "";
			$filter = "product_design.pd_id";
			$filter_id = -1;
		}
	}else{
		$search = "";
		$filter = "product_design.pd_id";
		$filter_id = -1;
	}

	echo "<h1>Customer Transactions</h1>";
	echo "<ul class = 'topMenu'>";
	echo "<li><a href = 'orders/?status=DELIVERED'>DELIVERED</a></li>";
	echo "<li><a href = 'orders/?status=SHIPPED'>SHIPPED</a></li>";
    echo "<li><a href = 'orders/?status=PROCESSED'>PROCESSED</a></li>";
  	echo "<li><a href = 'orders/?status=PAID'>PAID</a></li>";
    echo "</ul>";


    if(isset($_GET['status'])) {
    	//$record_count = $awp ->get_all_product_count($filter, $search);
		$per_page = 10;
		//$products = $awp -> view_all_products($start, $per_page, $filter, $search);
		//$header = array("Product Name", "Shirt Color", "Size", "Product Preview", "Price", "Brand", "Type", "Fabric", "Thickness", "Average Rating", "Category", "Sales");
		//$searchOptions = array("Product Name", "Sub Category");
		$header = array("Order ID", "Buyer", "Date", "Total Sales", "Payment Method", "Token", "Status", "View Order");
		$arr_stat = array();

		$order_history = $awp -> view_order_history($_GET['status']);
		
		if ($_GET['status'] == "PAID") {
			array_push($arr_stat, "PAID", "PROCESSED");
		}else if ($_GET['status'] == "PROCESSED") {
			array_push($arr_stat,  "PROCESSED", "SHIPPED");
		}  else if ($_GET['status'] == "SHIPPED") {
			array_push($arr_stat, "SHIPPED", "DELIVERED");
			array_push($header, "TRACK ORDER");
		} else if($_GET['status'] == "DELIVERED") {
			array_push($arr_stat, "DELIVERED");
		}

		echo "<table id = 'tblOrders'>";
		foreach ($header as $h) {
			echo "<th>".$h."</th>";
		}
		
		foreach ($order_history as $o) {
			echo "<tr>";
			echo "<td>".$o['order_history_id']."</td>";
			echo "<td>".$o['buyer']."</td>";
			echo "<td>".$o['date']."</td>";
			echo "<td>".$o['total_sales']."</td>";
			echo "<td>".$o['payment_method']."</td>";
			echo "<td>".$o['session']."</td>";
			
			echo "<td><select class='cmbStatus'  addr='".$o['ab_id']."' cust='".$o['customer_id']. "'   id='".$o['order_history_id']." '>";
			foreach ($arr_stat as $k) {
				echo "<option ";   if ($o['status'] == $k) echo ' selected="selected"';                       echo ">".$k."</option>";
			}

			echo "<td><a class = 'btnViewOrder' href = '#' o-id = '".$o['order_history_id']."'>View Order</a></td>";
			
			if ($o['status'] == 'SHIPPED') {

				if ($o['courier'] == "LBC") {
					echo "<td><a href='http://www.lbcexpress.com/track/?tracking_no=".$o['tracking_no']. "'>TRACK</a></td> ";
				} 
				else if ($o['courier'] == "XEND") {
					echo "<td><a href='http://tracker.xend.com.ph/?waybill=".$o['tracking_no']. "'>TRACK</a></td> ";
				}
			}

			echo "</tr>";
		}

		echo "</table>";

		/*$page_url = "index.php?";

		if($search != ""){
			$page_url .= "search=$search&";
		}
		if($filter_id > 0 && $filter_id <= 2){
			$page_url .= "filter=$filter_id&";
		}

		$prev = $start - $per_page;
		if($start > 0){
			echo "<a href = '".$page_url."start=$prev'>Prev</a>";
		}

		$i = 1;
		for($x = 0; $x < $record_count; $x = $x + $per_page){
			if($start != $x){
				echo " <a href = '".$page_url."start=$x'>$i</a> ";
			}else{
				echo " <a href = '".$page_url."start=$x'><b>$i</b></a> ";
			}
			$i++;
		}

		$next = $start + $per_page;
		if($start < $record_count - $per_page ){
			echo " <a href = '".$page_url."start=$next'>Next</a>";
		}*/
            }
	
?>
        <div id = 'popup1'>
        <div id = 'pop-up-form1'>
  
        <div id ='divHead'>
        <h3> Shipment Form </h3>
         </div>
        
        <div id = 'divOrder'>
        <label class = 'ordlbl'>Receiver Name</label><br><br>
         <label id='lblname'></label><br></br>
         <label class='ordlbl'>Contact Number</label><br><br>
        <label id = 'lblContact'></label><br><br>
        <label class='ordlbl'>Address</label><br><br>
        <label id = 'lblAddress'></label>
         </div>
        
         <div id = 'divCourier'>
         <label class = 'orderlbl'>Courier: </label><br><br>
         <select class="selCourier"><option>LBC</option> <option>XEND</option> </select>
         </div>

          <div id = 'divTrack'>
         <label class = 'orderlbl'>Tracking No. : </label><br><br>
	 <input type = 'text' id = 'txtTracking' placeholder = "Tracking "/>
         </div>
                <button id = 'popup-ok1'>Submit</button>
         </div>
        </div>

        <div class='popup-ci'><div id = 'popup-inner-sub' style = 'background-color:white;position:absolute;top:5%;width:90%;left:5%;'></div>
</body>
</html>