<?php
require_once("../../php/config.php");
require_once("../../php/send_sms/send.php");
require_once("../../php/send_message.php");

$status = $_POST['status'];
$orderId = $_POST['orderId'];
$custId = $_POST['custId'];
$cr = date("Y-m-d H:i:s");

$email=""; $contact=""; $msg="";

$stmt = $dbc->prepare("UPDATE order_history SET status = ?, update_date = ? WHERE order_history_id = ?");
$stmt->bind_param('ssi', $status, $cr, $orderId);
$stmt->execute();

if ($stmt->affected_rows >0) {

	$stmt = $dbc->prepare("SELECT customer.email,  address_book.contact_number FROM customer INNER JOIN address_book ON customer.customer_id = address_book.customer_id WHERE customer.customer_id =? AND address_book.status='PRIMARY'");
	$stmt->bind_param('i', $custId);
	$stmt->execute();
	$stmt->store_result();
	$data = $stmt->num_rows();

	if ($data>0) {
		$stmt->bind_result($e, $c);

		while($stmt->fetch()) {
			$email = $e; $contact= $c;
		}
	}

	if ($status == "SHIPPED") {

		$courier = $_POST['courier'];
		$track = $_POST['track'];

		$stmt = $dbc->prepare("INSERT INTO shipping(order_history_id, courier, tracking_no,date_send) VALUES (?,?,?,?)");
		$stmt->bind_param('isss', $orderId, $courier, $track, $cr );
		$stmt->execute();
	}

	echo "success";

	$msg = "Your order id # ".$orderId . " has been ". $status. ". Thanks for trusting MixlArts.";
	$sms = new SMS();
	print_r($sms->sendSMS($contact, $msg));

	if ($status == "SHIPPED") {

		$msg = "TRACKING # ".$track . " COURIER: ". $courier. ". Thanks for trusting MixlArts.";
		$sms = new SMS();
		print_r($sms->sendSMS($contact, $msg));
	}

	//$em = new SendEmail();
	//$em->send_em($email, "MixlArts Order Status", $msg); 

} else {
	echo "failed";
}
?>