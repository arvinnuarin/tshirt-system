<?php
require_once("../../php/config.php");

$courier = $_POST['cname'];
$fee = $_POST['fee'];
$trck = $_POST['trck'];

$stmt = $dbc->prepare("SELECT id FROM ship_courier WHERE courier_name=?");
$stmt->bind_param('s', $courier);
$stmt->execute();
$stmt->store_result();
$data = $stmt->num_rows();

if ($data == 0) {

	$stmt = $dbc->prepare("INSERT INTO ship_courier(courier_name, ship_fee, track_url) VALUES (?,?,?)");
	$stmt->bind_param('sds', $courier, $fee, $trck);
	$stmt->execute();

	if ($stmt->affected_rows == 1) {
		echo "success";
	} else {
		echo "failed";
	}
} else {

	$stmt = $dbc->prepare("UPDATE ship_courier SET ship_fee=?, track_url=? WHERE courier_name=?");
	$stmt->bind_param('dss', $fee, $trck,$courier);
	$stmt->execute();

	if ($stmt->affected_rows == 1) {
		echo "success";
	} else {
		echo "failed";
	}
}

?>