$(document).ready(function(){
    $(".tabShip").css("background-color", "#2c3b41");
    $(".tabShip").css("color","white");
    $(".tabShip").css("border-left","2px solid #e04545");
	$('#popup1').hide();

	$('#btnNew').click(function(){

		$('#courTitle').text("Add New Courier");
		$('#txtCourier').val('');
		$('#txtCourier').prop('readonly', false);
		$('#txtShip').val('');
		$('#txtTracking').val('');
		$('#popup1').show();

		$('#popup-ok1').click(function(){
        		 	
   		 	var courier= $("#txtCourier").val(), shipfee= $("#txtShip").val(), track = $("#txtTracking").val();
		 	$.post('shipping/newCourier.php', {cname: courier, fee: shipfee, trck: track}, function(data){
				
				if (data == 'success') {
					$('#popup1').hide();
					window.location.reload();
				} else {
					alert(data);
				}
		 	});
		});
	});

	$(document).on('click', '.btnUpdate', function(){

		$('#courTitle').text("Update Courier Information");
		$('#txtCourier').val($(this).attr('name'));
		$('#txtCourier').prop('readonly', true);
		$('#txtShip').val($(this).attr('fee'));
		$('#txtTracking').val($(this).attr('url'));
		$('#popup1').show();

		$('#popup-ok1').click(function(){

			var courier= $("#txtCourier").val(), shipfee= $("#txtShip").val(), track = $("#txtTracking").val();
		 	$.post('shipping/newCourier.php', {cname: courier, fee: shipfee, trck: track}, function(data){
				
				if (data == 'success') {
					$('#popup1').hide();
					window.location.reload();
				} else {
					alert(data);
				}
		 	});
		});
	});
});