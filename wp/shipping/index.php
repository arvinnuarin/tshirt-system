<html>
<head>
	 <link rel = 'stylesheet' href = "ship.css">
	<link rel = 'stylesheet' href = "../../css/popup.css">
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
    	<script type = "text/javascript" src = "ship.js"></script>
    	<script type = "text/javascript" src = "../../js/popup_0.js"></script>
	<title>Customer Transaction</title>
</head>
<body>
<?php
	session_start();
	require_once('../../php/awp.php');	
	require_once('../setup/setup_form.php');
	$setup = new SetUp($_SESSION['position']);
	$awp = new Awp();
	$arr_const_size = unserialize (SIZE);

	if(isset($_GET['start'])){
		if(!empty($_GET['start'])){
			if(is_numeric($_GET['start'])){
				$start = intval($_GET['start']);
			}else{
				$start = 0;
			}
		}else{
			$start = 0;
		}
	}else{
		$start = 0;
	}

	if(isset($_GET['search']) && isset($_GET['filter'])){
		if(!empty($_GET['search']) && !empty($_GET['filter'])){
			$search = $_GET['search'];
			$filter_id = $_GET['filter'];
			if($filter_id > 0 && $filter_id <= 2){
				if($filter_id == 1){
					$filter = "product_design.name";
				}else if($filter_id == 2){
					$filter = "category.sub_cat_name";
				}
			}else{
				$search = "";
				$filter = "product_design.pd_id";
				$filter_id = -1;
			}
		}else{
			$search = "";
			$filter = "product_design.pd_id";
			$filter_id = -1;
		}
	}else{
		$search = "";
		$filter = "product_design.pd_id";
		$filter_id = -1;
	}

	echo "<h1>Shipping Courier</h1>";
	echo "<ul class = 'topMenu'>";
	echo "<li><a id='btnNew'>Add New Courier</a></li>";
    echo "</ul>";

		$header = array("Courier Name", "Shipping Fee", "Tracking URL", "Options");
		$arr_stat = array();

		$courier = $awp -> view_courier();

		echo "<table id = 'tblOrders'>";
		foreach ($header as $h) {
			echo "<th>".$h."</th>";
		}
		
		foreach ($courier as $o) {
			echo "<tr>";
			echo "<td>".$o['courier_name']."</td>";
			echo "<td>".$o['ship_fee']."</td>";
			echo "<td><a id = 'btnTrack' href='".$o['track_url']."'>TRACKING LINK</a></td> ";
			echo "<td><button fee='".$o['ship_fee']."' name='".$o['courier_name']."' url='".$o['track_url']."' class='btnUpdate'>UPDATE</button></td>";
			echo "</tr>";
		}

		echo "</table>"; 
	
?>
        <div id = 'popup1'>
	        <div id = 'pop-up-form1'>
	  
	        <div id ='divHead'>
	        <h3 id='courTitle'>Add New Courier</h3>
	         </div>
	        
	        <div id = 'divOrder'>
	        	
	        	<div id = 'divShip'>
		         	<label class = 'orderlbl'>Courier Name: </label><br><br>
			 		<input type = 'text' id = 'txtCourier'/>
	         	</div>

	         	<div id = 'divFee'>
		         	<label class = 'orderlbl'>Shipping Fee: </label><br><br>
			 		<input type = 'number' id = 'txtShip'/>
	         	</div>

	          <div id = 'divTrack'>
	         	<label class = 'orderlbl'>Tracking URL: </label><br><br>
		 		<input type = 'text' id = 'txtTracking' placeholder = "Paste Tracking Link"/>
	         </div>
	                <button id = 'popup-ok1'>Submit</button>
					<a href = 'shipping' class = 'close'>x</a>
	         </div>
        </div>
        </div>
</body>
</html>