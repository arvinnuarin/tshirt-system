<html>
<head>
	<link rel = 'stylesheet' href = '../wp/login/login.css'>
	<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
	<script type = "text/javascript" src = "../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "../js/command.js"></script>
	<script type = "text/javascript" src = "../wp/login/login.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body style = "background-color:#eaeff2;">

	<div id = "bg1">
	<img id="logo" src="login\Icons\logo.png"/>
	<br><label id = "txtLogIn"><br>Sign in to start your session</label>
	<form id = 'frmLogIn'>
		<input type = 'text' id = 'txtEm' placeholder = "Email"/> 
		<label id = 'lblWarnEmail'></label>
		<img id="iconEmail" src="login\Icons\icon2.png"/>
		<input type = 'password' id = 'txtPw' placeholder = "Password"/>
		<label id = 'lblWarnPass'></label>
		<img id="iconPass" src="login\Icons\icon1.png"/>
		<input type = 'button' id = 'btnLog' value = 'Log In'/>
	</form>
	</div>
    <div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>
</body>
</html>