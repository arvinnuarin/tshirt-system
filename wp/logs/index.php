<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			require_once('../../php/awp.php');	
			echo "<head>";
			?>
			<link rel = 'stylesheet' href = "logs.css">
			<link rel="stylesheet" type="text/css" href="../../css/jquery.datetimepicker.css">
			<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
		    <script type = "text/javascript" src = "logs.js"></script>
		    <script type = "text/javascript" src = '../../js/jquery.datetimepicker.full.js'></script>
			<title></title>
			<?php
			require_once('../setup/setup_form.php');
			echo "</head>";
			echo "<body>";

			$setup = new SetUp($_SESSION['position']);
			$awp = new Awp();
			$arr_const_size = unserialize (SIZE);

			if(isset($_GET['start'])){
				if(!empty($_GET['start'])){
					if(is_numeric($_GET['start'])){
						$start = intval($_GET['start']);
					}else{
						$start = 0;
					}
				}else{
					$start = 0;
				}
			}else{
				$start = 0;
			}

			$arr_options = array("Category", "Clothing", "Brand", "Type", "Fabric", "Color", "Stock", "Banner", "Clothing Design", "Clothing", "Raw Material");

			if(isset($_GET['search']) && isset($_GET['filter'])){
				if(!empty($_GET['search']) && !empty($_GET['filter'])){
					$search = $_GET['search'];
					$filter_id = $_GET['filter'];
					if($filter_id >= 1 && $filter_id <= 11){
						$filter = $arr_options[$filter_id-1];
					}else{
						$search = "";
						$filter = "";
						$filter_id = -1;
					}
				}else{
					$search = "";
					$filter = "";
					$filter_id = -1;
				}
			}else{
				$search = "";
				$filter = "";
				$filter_id = -1;
			}

			$sort_trans = array(array("time_desc", "desc"), array("time_asc", "asc"));
			foreach ($sort_trans as $st) {
				if(isset($_GET['sort'])){
					if(!empty($_GET['sort'])){
						$ctr = 0;
						if($st[0] == $_GET['sort']){
							$sort = $_GET['sort'];
							$order = $st[1];
							break;
						}else{
							$ctr += 1;
						}
						
						if(count($sort_trans) == $ctr){
							$sort = "";
							$order = "";
						}
					}else{
						$sort = "";
						$order = "";
					}
				}else{
					$sort = "";
					$order = "";
				}
			}

			if(isset($_GET['from_date']) && isset($_GET['to_date'])){
				if(!empty($_GET['from_date']) && !empty($_GET['to_date'])){
					$from_date = $_GET['from_date'];
					$to_date = $_GET['to_date'];
				}else{
					$from_date = "";
					$to_date = "";
				}
			}else{
				$from_date = "";
				$to_date = "";				
			}



			echo "<h1>Activity Log</h1>";

			$per_page = 30;
			$header = array("ID", "Time", "Activity");
			$employee_logs = $awp -> view_all_employee_logs($start, $per_page, $filter, $search, $order, $from_date, $to_date);
			$record_count = $employee_logs_count = $awp -> get_employee_logs_count($filter, $search, $from_date, $to_date);
			echo "<div id = 'divLogs'>";
			echo "<br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search Activity Related to: ";
			$arr_options = array("Choose Entity", "Category", "Clothing", "Brand", "Type", "Fabric", "Color", "Stock", "Banner", "Clothing Design", "Clothing", "Raw Material");
		    echo "<select id = 'selSearch'>";
			
		    for($x = 0; $x < count($arr_options); $x++){
				echo "<option value = '".($x)."' ";
				if($filter_id == $x){echo "selected";} 
				echo ">".$arr_options[$x]."</option>";
			}
			echo "</select> ";   

			echo "<input type = 'text' id = 'txtSearch' value = '$search' placeholder = 'Search..'/>";
			echo "<input type='image' value = 'Search' id = 'btnSearch' src ='logs/icon/imgSearch.png'/>";


			$arr_sort = array(array("Time (Ascending)", "time_asc"), array("Time (Descending)", "time_desc"));

			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: <select id = 'selSort'>";	
							
			foreach($arr_sort as $s){
				$filter_url = "logs/index.php?";
				if($search != ""){
					$filter_url .= "search=$search&";
				}
				if($filter_id <= 4 && $filter_id > 0){
					$filter_url .= "filter=$filter_id&";
				}
				if($start !== ""){
					$filter_url .= "start=$start&";
				}

				$filter_url .= "sort=".$s[1];
				echo "<option ";
				if($sort == $s[1]){
					echo "selected";
				}
				echo " value = '".$filter_url."'>".$s[0]."</option>";
			}
			echo "</select> ";
			echo "<br><br>";
			echo "<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filter Activities From: </label><input placeholder = 'Click to choose date...' type = 'text' id = 'fromdate' value = '$from_date'/>";
			echo "<label> To: </label><input placeholder = 'Click to choose date...' type = 'text' id = 'todate' value = '$to_date' />";
			echo "<input type = 'button' id = 'btnFilter' value = 'Filter'>";


			echo "<table id = 'tblLogs'>";
			foreach ($header as $h) {
				echo "<th>".$h."</th>";
			}
			
			foreach ($employee_logs as $el) {
				echo "<tr>";
				echo "<td>LOG-".$el['log_id']."</td>";
				$datetime = new DateTime($el['time']);
				echo "<td>".$datetime->format('F j, Y g:ia')."</td>";
				echo "<td>".$el['description']."</td>";
				echo "</tr>";
			}
			
			echo "</table>";

			if($record_count[0]['count'] > 30){
				$page_url = "index.php?";

				if($search != ""){
					$page_url .= "search=$search&";
				}
				if($filter_id > 0 && $filter_id <= 4){
					$page_url .= "filter=$filter_id&";
				}
				if($sort != ""){
					$page_url .= "sort=$sort&";
				}

				if($from_date != "" && $to_date){
					$page_url .= "from_date=$from_date&to_date=$to_date&";
				}

		      		  echo "<div id = 'btnPagination'>";
				$prev = $start - $per_page;
				if($start > 0){
					echo "<a class = 'lblPagination' href = 'logs/".$page_url."start=$prev'>Prev</a>";
				}
				$i = 1;
				for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
					if($start != $x){
						echo " <a class = 'lblPagination' href = 'logs/".$page_url."start=$x'>$i</a> ";

					}else{
						echo " <a style='background-color: #dd4b39; color:white;'class = 'lblPagination' href = 'logs/".$page_url."start=$x'><b>$i</b></a> ";
					}
					$i++;
				}

				$next = $start + $per_page;
				if($start < $record_count[0]['count'] - $per_page ){
					echo " <a class = 'lblPagination' href = 'logs/".$page_url."start=$next'>Next</a>";
				}
		            echo "</div>";
			}
			echo "</div>";
			echo "</body>";
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}
	
?>
</html>