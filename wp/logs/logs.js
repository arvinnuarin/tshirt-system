$(document).ready(function(){
    $(".tabLogs").css("background-color", "#2c3b41");
    $(".tabLogs").css("color","white");
    $(".tabLogs").css("border-left","2px solid #e04545");

    $("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	$("#btnSearch").click(function(){
		if($("#selSearch").val() <= 11 && $("#selSearch").val() >= 1){
			if($("#txtSearch").val() !== ""){
				window.location.href = "logs/?start=0&search="+$("#txtSearch").val()+"&filter="+$("#selSearch").val();
			}
		}
	});

	var date = new Date();
	$("#fromdate").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	var date = new Date();
	$("#todate").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#fromdate").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$('#btnFilter').click(function(){
		if($("#fromdate").val() != "" || $("#todate").val() != ""){
			if($("#todate").val() >= $("#fromdate").val()){
				window.location.href = "logs/?start=0&from_date="+$("#fromdate").val()+"&to_date="+$("#todate").val(); 
			}
		}
	});
});