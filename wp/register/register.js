$(document).ready(function(){
	
    var set_prov = [];
    for(var i = 0; i < set_address.length; i++){
    	if(jQuery.inArray(set_address[i][0], set_prov) == -1){
    		set_prov.push(set_address[i][0]);
    	}
    }
    var opt_prov = "";
    for(var x = 0; x < set_prov.length; x++){
        opt_prov += "<option>"+set_prov[x]+"</option>"
    }
    $("#selProv").append(opt_prov);

	$('#txtPw').bind("cut copy paste",function(e) {
	    e.preventDefault();
	});

	$('#txtCP').bind("cut copy paste",function(e) {
	    e.preventDefault();
	});

	$("#txtCP").prop( "disabled", true );
    
	$("#btnRegister").click(function(){
		alert("hey");
	    var ctr = 0;
	    var d = new Date();
	    var datetime = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDay()+" "+d.getHours() +":"+d.getMinutes()+":"+d.getSeconds();
	    //var captcha = $("#g-recaptcha-response").val();

		if ($('#txtFN').val() == "") {
	 		$("label#lblWarnFN").html("Empty First Name!");
	  	} else {
	      $("label#lblWarnFN").html("");
	      ctr += 1;
	  	}
	    if ($('#txtLN').val() == "") {
	      $("label#lblWarnLN").html("Empty Last Name!");
	    } else {
	      $("label#lblWarnLN").html("");
	      ctr += 1;
	    }
	    if ($('#txtEm').val() == "") {
	      $("label#lblWarnEm").html("Empty Email!");
	    } else {
	      if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#txtEm').val())){
	        $("label#lblWarnEm").html("Invalid Email!");
	      }else{
	        $("label#lblWarnEm").html("");
	        ctr += 1;
	      }
	    }
	    if ($('#txtPw').val() == "") {
	      $("label#lblWarnPw").html("Empty Password!");
	    }else if ($('#txtPw').val().length < 8){
	      $("label#lblWarnPw").html("Password must be 8 or more characters long..!");
	    }else if($('#txtPw').val().match(/[^0-9a-z]/i))
		    $("label#lblWarnPw").html("Only letters and digits allowed!");
		else if(!$('#txtPw').val().match(/\d/))
		    $("label#lblWarnPw").html("At least one digit required!");
		else if(!$('#txtPw').val().match(/[a-z]/i))
	    	$("label#lblWarnPw").html("At least one letter required!");
	    else {
	    	$("label#lblWarnPw").html("");
	      	ctr += 1;
	    }
	    if ($('#txtCP').val() == "") {
	      $("label#lblWarnCP").html("Empty Confirm Password!");
	    } else if($('#txtCP').val() != $('#txtPw').val()){
	      $("label#lblWarnCP").html("Mismatch Password!");
	    }else {
	      $("label#lblWarnCP").html("");
	      ctr += 1;
	    }
	    if ($('#txtCN').val() == "") {
	      $("label#lblWarnCN").html("Empty Contact Number!");
	    } else {
	      if($('#txtCN').val.length == 10){
	        $("label#lblWarnCN").html("Invalid Cellphone Number!");
	      }else{
	        $("label#lblWarnCN").html("");
	        ctr += 1;
	      }
	    }
	    alert("hey");
	    if ($('#txtSt').val() == "" || $('#selBrgy :selected').text() == "SELECT BARANGAY" || $('#selTown :selected').text() == "SELECT TOWN/CITY" || $('#selProv :selected').text() == "SELECT PROVINCE") {
	    	$("label#lblWarnAdd").html("Address is not yet completed!");
	    } else {
	    	$("label#lblWarnAdd").html("");
	        ctr += 1;
	    }

	  	if (ctr == 7) {
	  		/*$.post("../register/register.php",{txtFN:$.trim($('#txtFN').val()), txtMN:$.trim($('#txtMN').val()), txtLN:$.trim($('#txtLN').val()), txtEm:$.trim($('#txtEm').val()), 
	  			txtPw:$.trim($('#txtPw').val()), txtCN:"+63"+$.trim($('#txtCN').val()), txtSt:$.trim($('#txtSt').val()), selBrgy:$.trim($('#selBrgy').val()), selTown:$.trim($('#selTown').val()), 
	  			selProv:$.trim($('#selProv').val()), captcha:captcha},
	  		function(data){
	  			alert(data);
		        if(data == "Successfully Registered! Please verify your account using the email you provided."){
		        	$('#frmReg')[0].reset();
		        	window.location.href = "../../wp";
		        }
	  		});*/
	  	}
	});

  $("#selProv").on('change',function(){
    $('#selTown').empty().append('<option>SELECT TOWN/CITY</option>');
    $('#selBrgy').empty().append('<option>SELECT BARANGAY</option>');
    var prov = $('#selProv :selected').text();
    var set_town = [];
    for(var i = 0; i < set_address.length; i++){
    	if(jQuery.inArray(set_address[i][1], set_town) == -1 && prov == set_address[i][0]){
    		set_town.push(set_address[i][1]);
    	}
    }
    var opt_town = "";
    for(var x = 0; x < set_town.length; x++){
        opt_town += "<option>"+set_town[x]+"</option>"
    }
    $("#selTown").append(opt_town);
  });

  $("#selTown").on('change', function(){
    $('#selBrgy').empty().append('<option>SELECT BARANGAY</option>');
    var town = $('#selTown :selected').text();
    var set_brgy = [];
    for(var i = 0; i < set_address.length; i++){
    	if(jQuery.inArray(set_address[i][2], set_brgy) == -1 && town == set_address[i][1]){
    		set_brgy.push(set_address[i][2]);
    	}
    }
    var opt_brgy = "";
    for(var x = 0; x < set_brgy.length; x++){
        opt_brgy += "<option>"+set_brgy[x]+"</option>"
    }
    $("#selBrgy").append(opt_brgy);
  });

  $('#txtPw').keyup(function(){
  	if($('#txtPw').val().length >= 8){
  		$("#txtCP").prop("disabled", false);
  	}else{
  		$("#txtCP").prop("disabled", true);
  	}
  });

  $('#txtFN').keyup(function(){
  	$('#txtFN').val(titleCase(removeDigits($('#txtFN').val())));
  });

  $('#txtMN').keyup(function(){
  	$('#txtMN').val(titleCase(removeDigits($('#txtMN').val())));
  });

  $('#txtLN').keyup(function(){
  	$('#txtLN').val(titleCase(removeDigits($('#txtLN').val())));
  });

  $('#txtCN').keyup(function(){
  	$('#txtCN').val(removeNonDigits($('#txtCN').val()));
  	$('#txtCN').val(cpChecker($('#txtCN').val()));
  });

  $('#txtSt').keyup(function(){
  	$('#txtSt').val(titleCase($('#txtSt').val()));
  })
});



