<head>
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/mixlarts.js"></script>
	<script type = "text/javascript" src = "register.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<h1> Registration</h1> <br />
	<form id = 'frmReg'>
		First Name: <input type = 'text' id = 'txtFN' /> <br />
		<label id = "lblWarnFN"></label> <br />
		Middle Name: <input type = 'text' id = 'txtMN' /> <br />
		<label id = "lblWarnMN"></label> <br />
		Last Name: <input type = 'text' id = 'txtLN' /> <br />
		<label id = "lblWarnLN"></label> <br />
		Email: <input type = 'text' id = 'txtEm' /> <br />
		<label id = "lblWarnEm"></label> <br />
		Password: <input type = 'password' id = 'txtPw' /> Must consist of 8 characters and must be Alphanumeric<br />
		<label id = "lblWarnPw"></label> <br />
		Confirm Password: <input type = 'password' id = 'txtCP' /> <br />
		<label id = "lblWarnCP"></label> <br />
		Contact Number: +63<input type = 'text' id = 'txtCN' value = ''/> <br />
		<label id = "lblWarnCN"></label> <br />
		Address: <input type = 'text' id = 'txtSt' placeholder='Home Address'>

		
		<?php
			require_once("../../php/mixlarts.php");
			$ma = new MixlArts();
			echo $ma -> user_address();
		?>

	    <select id = 'selProv'><option>SELECT PROVINCE</option></select>
		<select id = 'selTown'><option>SELECT TOWN/CITY</option></select>
		<select id = 'selBrgy'><option>SELECT BARANGAY</option></select><br />
		<label id = "lblWarnAdd"></label><br />
		<div class="g-recaptcha" data-sitekey="6LeGihgTAAAAAL-MgBsvramsVbl6FrP6qwG5w2mi"></div>
		<input type = 'button' id = 'btnAddEmp' value = 'Register'>
	</form>
</body>