<html>
<head>
    
<link rel="stylesheet" type="text/css" href="modify_category.css">
<link rel = 'stylesheet' href = "../../css/popup.css">
<link rel = 'stylesheet' href = "../../css/upload_photo.css">
<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
<script type = "text/javascript" src = "modify_category.js"></script>
<script type = "text/javascript" src = "../../js/mixlarts.js"></script>
<script type = "text/javascript" src = "../../js/popup_0.js"></script>
<script type = "text/javascript" src = '../../js/cropit-master/dist/jquery.cropit.js'></script>
	<?php 
		session_start();
		require_once('../setup/setup_form.php');
		require_once('../../php/awp.php');

		$setup = new SetUp($_SESSION['position']);
		$awp = new Awp();
		$s_cat = false;
		if(isset($_GET['catId'])){
			$cat_id = $_GET['catId'];
			if(!empty($cat_id)){
				if($cat_id !== ""){
					if($res = $awp -> get_one_category($cat_id)){
						$sub_cat_name = $res[0]['sub_cat_name'];
						$cat_name = $res[0]['cat_name'];
						$s_cat = true;
						echo "<script type = 'text/javascript'>var bannerPic = '//www.mixlarts.com/MixlArts/cms/category_banner/".$sub_cat_name.".png';</script>";
					}
				}
			}

		}
	?>
</head>
<body>
	<?php 
		if($s_cat){
			echo "<h1>Edit Sub Category</h1>";
			echo "<label id = 'lblCatId1'>ID:</label>";
			echo "<label id = 'lblCatId'>CAT-$cat_id</label>";
			echo "<label id = 'lblCatName1'>Name: </label>";
			echo "<label id = 'lblCatName'>".$sub_cat_name."</label>";
		}else{
			echo "<h1>Add Sub Category</h1>";
			echo "<label id = 'lblCatName2'>Name: </label>";
			echo "<input type = 'text' id = 'txtSubCatName' placeholder = 'Name'/>";
			echo "<label id = 'lblEmptySubCat'></label>";
		}
		echo "</br>";
        echo "<div id = 'divCatban'>";
		echo "<label id = 'lblcatban'>Category Banner:</label>";
		echo "<label id = 'lblEmptyPic'></label>";
		echo "<button class = 'btnClear'><span id ='iconClear'></span>Clear</button>";
        echo "<div class = 'image-editor'>";
		echo "<div class = 'cropit-preview'></div>";
		echo "<input type = 'image' class = 'rotate-cw' src='category/Icons/cs.png'>";
		echo "<input type = 'image' class = 'rotate-ccw' src ='category/Icons/ccs.png'>";
		echo "<input type = 'range' class = 'cropit-image-zoom-input'>";
		echo "<div class='fileUpload btn btn-primary'>";
    	echo "<span id ='iconUpload'>&nbsp;&nbsp;Choose a file...</span>";
		echo "<input type = 'file' class='cropit-image-input'/>";
        echo "</div>";
        echo "</div>";
	?>
	<label id = 'lblcat'>Category: </label>
	<?php
		if($s_cat){
			echo "<label id = 'catName'>".$cat_name."</label>";
			echo "<input type = 'button' value = 'Edit Category' id = 'btnModifyCat'>";
		}else{
			echo "<select id = 'selCatName'>";
			echo "<option value = 'Men'>Men</option>";
			echo "<option value = 'Women'>Women</option>";
			echo "<option value = 'Boys'>Boys</option>";
			echo "<option value = 'Girls'>Girls</option>";
			echo "<option value = 'Unisex'>Unisex</option>";
			echo "<option value = 'Unisex(Kids)'>Unisex(Kids)</option>";
			echo "</select>";
			echo "<input type = 'button' value = 'Add Category' id = 'btnModifyCat'>";
		}
			echo "</div>";
	?>		<br><br>
			<div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>

</body>
</html>