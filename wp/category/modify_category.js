$(document).ready(function(){
    $(".tabCat").css("background-color", "#2c3b41");
    $(".tabCat").css("color","white");
    $(".tabCat").css("border-left","2px solid #e04545");
  if(typeof bannerPic == "undefined"){
    bannerPic = "";
  }
	$("#btnModifyCat").click(function(){
		var ctr = 0;
		if ($('#txtSubCatName').val() == "") {
 			$("#lblEmptySubCat").html("Empty Category Name!");
          	} else {
              	ctr += 1;
         		$("label#lblEmptySubCat").html("");
          	}
            if($(".cropit-preview-image").attr('src')){
              if($(".cropit-preview-image")){
                ctr += 1;
                $('#lblEmptyPic').html("");
              }
            }else{
              $('#lblEmptyPic').html("Empty Category Banner!");
            }
            if($("#lblCatId").html() && ctr == 2){
                  $.post("category/edit_category.php", {catName:$("#lblCatName").html(), banner:$('.image-editor').cropit('export')},
                  function(data){
                      popupOk("Sub Category","Successfully Updated!");
                      $('.popup-ok').click(function(){
                        window.location.replace("category");
                    });
                  });
		$(".popup-cancel").click(function(){
              $(".popup").fadeOut(350);
        });


    }else if(ctr == 2){
       $.post("category/modify_category.php",{txtCatName:$.trim($('#txtSubCatName').val()), banner:$('.image-editor').cropit('export'), selCatName:$('#selCatName :selected').text()},
        function(data){
          popupOk("Add Category", data);
          $('.popup-ok').click(function(){
            window.location.replace("category");
          });
        });
      }

  	});

  $('.image-editor').cropit({
    quality: .9,
      originalSize: true,
      exportZoom: 1.00,
      allowDragNDrop: true,
    imageState: {
      src: bannerPic,
    },
      onImageError:function(){
        popupOk("Warning","Height must be 250px or higher and width must be 1000px or higher!");
      }
  });

  $('.rotate-cw').click(function() {
    $('.image-editor').cropit('rotateCW');
  });
  $('.rotate-ccw').click(function() {
    $('.image-editor').cropit('rotateCCW');
  });


    $(".btnClear").click(function(){
      if($(".cropit-preview-image")){
        $(".cropit-preview-image").removeAttr('src');
        $(".cropit-image-zoom-input").val("0.5");
        $(".cropit-preview").removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
        $(".cropit-image-input")[0].value = null;
      }
    });

});