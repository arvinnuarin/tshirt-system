<html>
<body>
	<?php
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['position'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) && isset($_SESSION['name'])) {
				require_once('../setup/setup_form.php');
				$setup = new SetUp($_SESSION['position']);
				echo "<head>";
				?>
				<title>Category Management</title>
				<link rel = 'stylesheet' href = "../css/popup.css">
				<link rel="stylesheet" type="text/css" href="category/category.css">
				<script type = "text/javascript" src = "../js/jquery-2.2.3.js"></script>
				<script type="text/javascript" src = 'category/category.js'></script>
				<?php
				echo "</head>";
				?>
				<h1>Category Management</h1>
				<a id = 'btnCategory' href = 'category/modify_category_form.php'>Add Category</a>
				<table>
					<th>ID</th>
					<th>Sub Category Name</th>
					<th>Banner</th>
					<th>Category Name</th>
					<th>Edit Category</th>		
					<th>View Design</th>
					<?php
						require_once('../../php/awp.php');
						$awp = new Awp();
						$arr_cat = $awp -> get_all_category();
						foreach($arr_cat as $ac){
							echo "<tr>";
							echo "<td>CAT-".$ac['category_id']."</td>";
							echo "<td>".$ac['sub_cat_name']."</td>";
							$src = "No Image to Show!";
							if($ac['cat_name']){
								$src = "<img style = 'height:50px;' src = '../cms/category_banner/".$ac['sub_cat_name'].".png'/>";
							}
							echo "<td>$src</td>";
							echo "<td>".$ac['cat_name']."</td>";
							echo "<td><a class = 'btnEdit' href = ' category/modify_category_form.php?catId=".$ac['category_id']."'><span id ='iconEdit'></span>Edit Category</a></td>";
							echo "<td><a class = 'btnView' href = 'shirt_design/index.php?filter=2&search=".$ac['sub_cat_name']."&start=0'><span id ='iconView'></span>View Design</a></td>";
							echo "</tr>";
						}
					?>
				</table>
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

				<div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>
				<?php
			}else{
				session_destroy();
				echo "<script>window.location.replace('../../wp/');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
		
	?>
	

</body>
</html>
