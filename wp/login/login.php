<?php
	require_once('../../php/awp.php');
		
	session_start();

	$awp = new Awp();

	$txtEm = $_POST['txtEm'];
	$txtPw = $_POST['txtPw'];

	if (!filter_var($txtEm, FILTER_VALIDATE_EMAIL)){
		echo "Invalid Email!";
	}else{
		$result = $awp -> log_in($txtEm, $txtPw);
		if ($result[0] == "Success"){
			$_SESSION['name'] = $result[2];
			$_SESSION['email'] = $result[3];
			$_SESSION['position'] = $result[4];
			$_SESSION['id'] = $result[1];
			echo "Successfully Log In!";
		}else if($result[0] == "Email doesn't match any account in our website!" || $result[0] == "Your Password is incorrect!"){
			echo $result[0];
		}else{
			$_SESSION['id'] = $result[1];
			$_SESSION['email'] = $result[2];
			if($result[0] == "Email is only Verified!"){
				//echo $result[2];
				//$awp -> send_code($result[2], $result[4]);
				$_SESSION['password'] = $result[3];
			}
			echo $result[0];
		}
	}

?>