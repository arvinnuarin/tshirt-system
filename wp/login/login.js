$(document).ready(function(){
	$("#btnLog").click(function(){
    var txtEm = $('#txtEm').val();
    var txtPw = $('#txtPw').val();
    var ctr = 0;

    if (txtEm == "") {
      $("label#lblWarnEmail").html("Empty Email Address!");
    	  document.getElementById("txtEm").style.border = '1px solid #cf2a31';
    }else if(!isValidEmailAddress(txtEm)){
      $("label#lblWarnEmail").html("Invalid Email Address!");
        	  document.getElementById("txtEm").style.border = '1px solid #cf2a31';
    }else {
      $("label#lblWarnEmail").html("");
      ctr += 1;
    }
    if (txtPw == "") {
      $("label#lblWarnPass").html("Empty Password!");
        document.getElementById("txtPw").style.border = '1px solid #cf2a31';

    } else {
      $("label#lblWarnPass").html("");
      ctr += 1;
    }

    if(ctr == 2){
      $.post("login/login.php",{txtEm:txtEm, txtPw:txtPw},
      function(data){

        if(data == "Account not already Verified!" || data == "Verification Email Already Sent!"){
          window.location.replace('acc_em_auth');
        }else if(data == "Email is only Verified!"){
          window.location.replace('acc_cn_auth');
        }else if(data == "Successfully Log In!"){
          window.location.replace("../wp/home");        
        }else{
          popupOk("Log In", data);
        }
		  alert(data);
      });
    }

	});
});
