<html>
<head>
    <link rel = 'stylesheet' href = "view_clothing_brand.css">
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = 'view_clothing_brand.js'></script>
	<title>Clothing Brand</title>
</head>
<body>
<?php
	require_once('../../php/awp.php');
	require_once('../setup/setup_form.php');
	session_start();
	$awp = new Awp();
	$setup = new SetUp($_SESSION['position']);

	if(isset($_GET['start'])){
		if(!empty($_GET['start'])){
			if(is_numeric($_GET['start'])){
				$start = intval($_GET['start']);
			}else{
				$start = 0;
			}
		}else{
			$start = 0;
		}
	}else{
		$start = 0;
	}

	$per_page = 2;
	if(isset($_GET['search'])){
		if(!empty($_GET['search'])){
			$search = $_GET['search'];
		}else{
			$search = "";
		}
	}else{
		$search = "";
	}
$sort_trans = $arr_sort = array(array("brand_desc", "brand_name", "desc"), array("brand_asc", "brand_name", "asc"));
foreach ($sort_trans as $st) {
	if(isset($_GET['sort'])){
		if(!empty($_GET['sort'])){
			
			$ctr = 0;
			
				if($st[0] == $_GET['sort']){
					$sort = $_GET['sort'];
					$col_order = $st[1];
					$order = $st[2];
					break;
				}else{
					$ctr += 1;
				}
			
			if(count($sort_trans) == $ctr){
				$sort = "";
				$col_order = $st[1];
				$order = "";
			}
		}else{
			$sort = "";
			$col_order = $st[1];
			$order = "";
		}
	}else{
		$sort = "";
		$col_order = $st[1];
		$order = "";
	}
		}

	$record_count = $awp -> get_all_clothing_brand_count($search, $col_order, $order);
	$clothing_type = $awp -> view_clothing_brand($start, $per_page, $search, $col_order, $order);
	//echo $record_count;
    echo "<ul class = 'topMenu'>";
    echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
    echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
    echo "<li><a class = 'active' href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
    echo "<li><a href = 't-shirt/index.php'>Clothing Information</a></li>";
    $count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
	echo "<li><a href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
    echo "</ul>";
    echo "<h1>Clothing Brand</h1>";
    echo "<a id = 'btnAddClothingBrand' href = 't-shirt/add_clothing_brand_form.php'>Add Clothing Brand</a>";

    echo "<div id = 'divBrand'>";
    echo "<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search: <input type = 'text' id = 'txtSearch' value = '$search'>";
    echo "<input value = 'Search' type = 'image' src ='t-shirt/icon/imgSearch.png' id = 'btnSearch'>";

	$arr_sort = array(array("Brand(A-Z)", "brand_asc"), array("Brand(Z-A)", "brand_desc"));
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: <select id = 'selSort'>";
	foreach($arr_sort as $s){
		$filter_url = "t-shirt/view_clothing_brand_form.php?";
		if($search != ""){
			$filter_url .= "search=$search";
		}
		if($start !== ""){
			$filter_url .= "start=$start";
		}
		$filter_url .= "sort=".$s[1];
		echo "<a href = '".$filter_url."'>".$s[0]."</a> | ";
		echo "<option ";
		if($sort == $s[1]){
			echo "selected";
		}
		echo " value = '".$filter_url."'>".$s[0]."</option>";
	}
	echo "</select>";

    echo "<br><br>";

	echo "<table id = 'tblBrand' ><th>ID</th><th>Brand</th>";
	foreach ($clothing_type as $ct) {
		echo "<tr>";
		echo "<td>CB-".$ct['cb_id']."</td>";
		echo "<td>".$ct['brand_name']."</td>";
		echo "</tr>";
	}
	echo "</table>";


	if($record_count > 1){
		$page_url = "t-shirt/view_clothing_brand_form.php?";

		if($search != ""){
			$page_url .= "search=$search";
		}

		if($sort != ""){
			$page_url .= "sort=$sort";
		}
        
        echo "<div id = 'btnPagination'>";
		$prev = $start - $per_page;
		if($start > 0){
			echo "<a class = 'lblPagination' href = '".$page_url."start=$prev'>Prev</a>";
		}

		$i = 1;
		for($x = 0; $x < $record_count; $x = $x + $per_page){
			if($start != $x){
				echo " <a class = 'lblPagination' href = '".$page_url."start=$x'>$i</a> ";
			}else{
				echo " <a style='background-color: #dd4b39; color:white;' class = 'lblPagination' href = '".$page_url."start=$x'><b>$i</b></a> ";
			}
			$i++;
		}
		$next = $start + $per_page;
		if($start < $record_count - $per_page ){
			echo " <a class = 'lblPagination' href = '".$page_url."start=$next'>Next</a>";
		}
        echo "</div>";
	}
        echo "</div>";
?>
    <div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved</div>
</body>
</html>