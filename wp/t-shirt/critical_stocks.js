$(document).ready(function(){
	$(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	$("#btnSearch").click(function(){
		if($("#txtSearch").val() !== ""){
			if($("#selSearch").val() >= 0 && $("#selSearch").val() <= 4){
				window.location.href = "t-shirt/critical_stocks_form.php?start=0&search="+$("#txtSearch").val()+"&filter="+$("#selSearch").val();
			}
		}
	});

	function updateStocks(e){
		var cont = $(this).parent().parent().parent().parent().parent().find(".color").children().children().find(".sub_stock");
		var tr = $(this).parent().parent();
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var stocksVal = tr.find('.slot-stock').html();
		var prevStocks = tr.find(".stocks").attr('def-val');
		if($(this).attr('btn-name') == "update"){
			$(this).next().css('visibility', 'visible');
			$(this).next().click(function(e){
				tr.css("background-color", "white");
				tr.find(".stocks").css("visibility", "hidden");
				tr.find('.slot-stock').html(stocksVal);
				$(this).prev().attr('btn-name', 'update');
				$(this).prev().html("Update Stocks");
				$(this).css("visibility", "hidden");
				e.preventDefault();
			});
			$(this).attr('btn-name', 'proceed');
			$(this).html('Proceed');
			tr.css("background-color", "orange");
			tr.find('.slot-stock').html("<input class = 'stocks' type = 'number' def-val = '"+stocksVal+"' value = '"+stocksVal+"'/>");
			$(".stocks").on("input propertychange", function(){
				$(this).val($(this).val().replace(/[^0-9]/g,''));
			});
		}else if($(this).attr('btn-name') == "proceed"){
			var clothing = tr.find(".brand").html()+"("+tr.find(".type").html()+tr.find(".fabric").html()+")";
			var size = tr.find(".slot-stock").attr('size-clone');
			var stocks = tr.find(".stocks").val();
			var stockId = tr.attr('stock-id');
			var colorId = $(this).attr('color-id');
			
			if(prevStocks != stocks){
				if(!size || !stockId || !clothing){
					popupOk("Something went wrong!");
				}else if(size != "" && stocks != "" && stockId != "" && clothing != ""){
					loading();
					$.post("t-shirt/view_clothing.php", {do:"update", stocks:stocks, stockId:stockId, size:size, clothing:clothing, date:currentDate, clothingId:tr.attr("clothingId") ,colorId:colorId, prodId:0, pmId:0}, function(data){
						if(data){
							$(".back").remove();
							if(data.indexOf("Success") != -1){
								tr.find('.slot-stock').html(stocks);
								tr.find('.btn-manipulator').find(".updateStocks").html("Update Stocks");
								var x = data.split("#");
								tr.find('.slot-date').html(x[3]);

								popupOk("Critical Stocks", "Successfully Updated!");
							}else{
								tr.find('.slot-stock').html(prevStocks);
								popupOk("Stocks Failed to Update!");
							}						
						}	
					});
				}else{
					$(this).next().css("visibility", "hidden");
					tr.find('.slot-stock').html(prevStocks);
				}
			}else{
				$(this).next().css("visibility", "hidden");
				tr.find('.slot-stock').html(prevStocks);
			}

			tr.css("background-color", "white");
			tr.find(".stocks").css("visibility", "hidden");
			tr.find(".stocks").val("");
			$(this).attr('btn-name', 'update');
			$(this).html("Update Stocks");
			$(this).next().css("visibility", "hidden");
		}
		e.preventDefault();
	}

	$(".updateStocks").click(updateStocks);
});