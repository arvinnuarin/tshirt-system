<html>
<head>
    <link rel = 'stylesheet' href = "add_clothing_type.css">
	<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "add_clothing_type.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
	<title>Clothing Type</title>
</head>
<body>
	<?php
		session_start();
		require_once('../setup/setup_form.php');
		$setup = new SetUp($_SESSION['position']);
	?>
	<h1>Add Clothing Type</h1>
    <br>
    <div id = "divAddCloth">
    <label id = lblAdd>Add Clothing Type</label>
    <div id = "divline"></div>
    <br>    <br>    <br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing Type Name: <input type='text' id = 'txtCTN' />
	<label id = 'lblWarnCTN'></label>
	<input type = 'button' value = 'Add Clothing Type' id = 'btnAddCT'/>
    </div>
    <div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>
</body>
</html>