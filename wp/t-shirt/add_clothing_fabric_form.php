<html>
<head>
    <link rel = 'stylesheet' href = "add_clothing_fabric.css">
	<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "add_clothing_fabric.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
	<title>Clothing Fabric</title>
</head>
<body>
	<?php
		session_start();
		require_once('../setup/setup_form.php');
		$setup = new SetUp($_SESSION['position']);
	?>
    
	<h1>Add Clothing Fabric</h1>
    <br>
    <div id = "divAddFabric">
    <label id = lblAdd>Add Clothing Fabric</label>
    <div id = "divline"></div>
    <br>    <br>    <br> 
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing Fabric Name: <input type='text' id = 'txtFabric' />
	<label id = 'lblWarnFabric'></label>
	<input type = 'button' value = 'Add Fabric' id = 'btnAddFabric'/>
    </div>
    <div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>
</body>
</html>