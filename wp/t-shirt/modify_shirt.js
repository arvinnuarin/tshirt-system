var numColors = 0;
$(document).ready(function(){
    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	var containerOccur = [];
	var faces = ["Front", "Back", "Left", "Right"];
	var arrSize;
	var origSize = ["XXS", "XS", "S", "M", "L", "XL", "XXL"];
	if (typeof arrSizeFilled2 !== 'undefined') {
    	arrSize = arrSizeFilled2;
	}else{
		arrSize = ["XXS", "XS", "S", "M", "L", "XL", "XXL"];
	}
	for(var x = 0; x < arrSize.length; x++){
		$("#txtHeight"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
			if($(this).val() != "" && $("#txtWidth"+$(this).attr('kp')).val() != "" && $("#txtPrice"+$(this).attr('kp')).val() != ""){
				$("input[ckp='"+$(this).attr('kp')+"']").prop('disabled', false);
			}else{
				$("input[ckp='"+$(this).attr('kp')+"']").prop('disabled', true);
			}
		});
		$("#txtWidth"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
			if($(this).val() != "" && $("#txtHeight"+$(this).attr('kp')).val() != "" && $("#txtPrice"+$(this).attr('kp')).val() != ""){
				$("input[ckp='"+$(this).attr('kp')+"']").prop('disabled', false);
			}else{
				$("input[ckp='"+$(this).attr('kp')+"']").prop('disabled', true);
			}
		});
		$("#txtPrice"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
			if($(this).val() != "" && $("#txtHeight"+$(this).attr('kp')).val() != "" && $("#txtWidth"+$(this).attr('kp')).val() != ""){
				$("input[ckp='"+$(this).attr('kp')+"']").prop('disabled', false);
			}else{
				$("input[ckp='"+$(this).attr('kp')+"']").prop('disabled', true);
			}
		});
	}

	/*$("#addColor").click(function(){
		createColorStock();
	});*/

	$("#btnAddInfo").click(function(){
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var arrSizesInfo = [], arrStocks = [], arrColorName = [], arrColorHex = [], imageData = [], arrImageName = [], arrSizeFilled = [];
		var filled = 0, ctr = 0;;
		$("#lblWarnBrand").html("");
		$("#lblWarnType").html("");
		$("#lblWarnFabric").html("");
		$("#lblWarnSizeInfo").html("");
		if($("#selBrand").val() == ""){
			$("#lblWarnBrand").html("Empty Brand Name!");
		}else{
			ctr += 1;
		}
		if($("#selType").val() == ""){
			$("#lblWarnType").html("Empty Type Name!");
		}else{
			ctr += 1;
		}
		if($("#selFabric").val() == ""){
			$("#lblWarnFabric").html("Empty Fabric Name!");
		}else{
			ctr += 1;
		}
		for(var x = 0; x < arrSize.length; x ++){
			$("#lblWarn"+arrSize[x]).html("");
			if($("#txtHeight"+arrSize[x]).attr('id') && $("#txtWidth"+arrSize[x]).attr('id') && $("#txtPrice"+arrSize[x]).attr('id') && $("#lblWarn"+arrSize[x]).attr('id')){
				if($("#txtHeight"+arrSize[x]).val() != "" || $("#txtWidth"+arrSize[x]).val() != "" || $("#txtPrice"+arrSize[x]).val() != ""){
					var warn = "";
					if($("#txtHeight"+arrSize[x]).val() == ""){
						warn += "Height ";
					}
					if($("#txtWidth"+arrSize[x]).val() == ""){
						warn += "Width ";
					}
					if($("#txtPrice"+arrSize[x]).val() == ""){
						warn += "Price ";
					}
					if(warn != ""){
						warn = "Empty "+$.trim(warn).replace(/[ ,]+/g, ", ");
						$("#lblWarn"+arrSize[x]).html(warn);
					}else{
						
						filled += 1;
						var subSizesInfo = [$("#txtHeight"+arrSize[x]).val(), $("#txtWidth"+arrSize[x]).val(), $("#txtPrice"+arrSize[x]).val(), x];
						arrSizesInfo.push(subSizesInfo);
					}
				}	
			}
		}
		if(arrSizesInfo.length == 0){
			$("#lblWarnSizeInfo").html("Please Fill Up at least one Size Information!");
		}else {
			ctr += 1;
			//alert("size info");
		}
		for(var x = 0; x < containerOccur.length; x ++){
			$("#lblWarnColorStock"+containerOccur[x].toString()).html("");
			$("#lblWarnColor"+containerOccur[x].toString()).html("");
			$("#lblWarnColorHex"+containerOccur[x].toString()).html("");
			if($.trim($("#txtColorName"+containerOccur[x].toString()).val()) == ""){
				$("#lblWarnColor"+containerOccur[x].toString()).html("Empty Color Name!");
			}else if(jQuery.inArray($.trim($("#txtColorName"+containerOccur[x].toString()).val()), arrColorName) !== -1){
				$("#lblWarnColor"+containerOccur[x].toString()).html("Color Name Already Listed!");
			}else{
				arrColorName.push($.trim($("#txtColorName"+containerOccur[x].toString()).val()));				
			}
			if(jQuery.inArray($("#color"+containerOccur[x].toString()).val(), arrColorHex) !== -1){
				$("#lblWarnColorHex"+containerOccur[x].toString()).html("Color Already Listed!");
			}else{
				arrColorHex.push($("#color"+containerOccur[x].toString()).val());
			}
			
			var subArrStocks = [], subArrSizeFilled = [];

			for(var y = 0; y < origSize.length; y++){
				if ($("#txtStock"+origSize[y]+containerOccur[x]).prop('disabled') == false) {
					if($("#txtStock"+origSize[y]+containerOccur[x]).val() != ""){
						subArrSizeFilled.push(y);
						subArrStocks.push($("#txtStock"+origSize[y]+containerOccur[x]).val());
					}
				}			
			}

			if(subArrStocks.length == 0 && subArrSizeFilled.length == 0){
				$("#lblWarnColorStock"+containerOccur[x].toString()).html("Please fill at least one size's stock!");
			}else{
				arrSizeFilled.push(subArrSizeFilled);
				arrStocks.push(subArrStocks);
			}

			var subName = [];
			var subData = [];
			for(var y = 0; y < faces.length; y++){
				$("#lblWarnTemplateColor"+faces[y]+containerOccur[x]).html("");
				var idpic = "#pic"+faces[y]+containerOccur[x];
				if($(idpic).length){
					if($(idpic+" .cropit-preview-image").attr('src')){
						$(".lblWarnTemplateColor"+containerOccur[x]).html("");
						if($(idpic).cropit('export')){
							subData.push($(idpic).cropit('export'));
							subName.push("pictures/shirt_template/" + $("#selBrand").val()+"_"+$("#selType").val()+"_"+$("#selFabric").val()+"_"+faces[y]+"_" + $("#txtColorName"+containerOccur[x].toString()).val().replace(/ /g,"_") + ".png");
						}
					}else{
						$("#lblWarnTemplateColor"+faces[y]+containerOccur[x]).html("Empty "+faces[y]+" Template.");
					}
				}
			}if(subData.length == 4 && subName.length == 4){
				imageData.push(subData);
				arrImageName.push(subName);
			}
		}
		if(imageData.length == containerOccur.length && arrImageName.length == containerOccur.length){
			ctr += 1;
		}

		if(arrColorName.length == containerOccur.length){
			ctr += 1;
		}
		if(arrStocks.length == containerOccur.length){
			ctr += 1;
		}
		if(arrColorHex.length == containerOccur.length){
			ctr += 1;
		}
		
		if(ctr == 8 && $("#lblId").html()){
			$.post("modify_shirt.php",{prodId:$("#lblId").html(), brand:$("#selBrand").val(), type:$("#selType").val(), fabric:$("#selFabric").val(), sizeFilled:arrSizeFilled,
					colorName:arrColorName, colorHex:arrColorHex, stocks:arrStocks, date:currentDate, imageData:imageData, imageName:arrImageName},function(data){
				if(data == "Success"){

					popupOk("T-Shirt Info", "T-Shirt Color Information Successfully Updated!");
					$('.popup-ok').click(function(){
						window.location.replace("../t-shirt");
					});
				}else if(data == "Failed"){
					popupOk("T-Shirt Info", "T-Shirt Color Information Updating Failed!");
				}else if(data == "Color Hex Exist!"){
					popupOk("T-Shirt Info", "T-Shirt Color Hex already exist!");
				}
				alert(data);
			});
		}else if(ctr == 9){		
			$.post("modify_shirt.php",{brand:$("#selBrand").val(), type:$("#selType").val(), fabric:$("#selFabric").val(), sizeFilled:arrSizeFilled, sizeInfo:arrSizesInfo, 
					colorName:arrColorName, colorHex:arrColorHex, stocks:arrStocks, date:currentDate, imageData:imageData, imageName:arrImageName}, function(data){
				if(data == "Success"){
					popupOk("T-Shirt Info", "T-Shirt Information Successfully Added!");
					$('.popup-ok').click(function(){
						window.location.replace("../t-shirt");
					});
				}else{
					popupOk("T-Shirt Info", "T-Shirt Information Adding Failed!");
				}
				alert(data);
			});

		}
	});

	createColorStock();

	function createColorStock(){
		containerOccur.push(numColors);
		var divColor = "<div id = 'divColor"+numColors+"'>";
		if(numColors > 0){
			divColor += "<input type = 'button' value = 'Close' id = 'btnCloseColor"+numColors+"' numcolor = '"+numColors+"'/><br />";
		}
		divColor += "Color Name: <input type = 'text' id = 'txtColorName"+numColors+"' /> <br />";
		divColor += "<label id = 'lblWarnColor"+numColors+"'></label><br />";
		divColor += "Color Representation: <input readonly class='color' value='#000' id = 'color"+numColors+"'/><br />";
		divColor += "<label id = 'lblWarnColorHex"+numColors+"'></label><br />";

		for(var x = 0; x < faces.length; x++){
			divColor += "<div id = 'pic"+faces[x]+numColors+"' class = 'image-editor'>";
			divColor += faces[x]+"<br />";
			divColor += "<button class = 'btnClear' value = '"+faces[x]+numColors+"'>Clear</button>";
			divColor += "<div class = 'cropit-preview' id = 'cp"+faces[x]+numColors+"' style = 'height:400px; width:350px'></div>";
			divColor += "<input type = 'file' class='cropit-image-input'/><br />";
			divColor += "<label id = 'lblWarnTemplateColor"+faces[x]+numColors+"'></label><br />";
			divColor += "</div>";

	    	$("#pic"+faces[x]+numColors+" .btnClear").click(function(){
				var id  = "#"+$(this).parent().prop('id');
				if($(id+" .cropit-preview-image")){
					$(id+" .cropit-preview-image").removeAttr('src');
					$(id+" .cropit-image-zoom-input").val("0.5");
					$(id+" .cropit-preview").removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
					$(id+" .cropit-image-input")[0].value = null;
				}
			});
		}

		divColor += "Stocks: <br /><table><th>Size</th><th>Stock</th>";
		if(($("#txtHeight"+arrSize[x]).attr('id') && $("#txtWidth"+arrSize[x]).attr('id') && $("#txtPrice"+arrSize[x]).attr('id') && $("#lblWarn"+arrSize[x]).attr('id'))){
			
			for(var x = 0; x < arrSize.length; x++){
				divColor += "<tr><td>"+arrSize[x]+"</td><td><input id = 'txtStock"+arrSize[x]+numColors+"' ckp = '"+arrSize[x]+"' type = 'text' ";
				if($("#txtWidth"+arrSize[x]).val() == ""){
					divColor += "disabled ";
				}
				divColor += "/></td>";
				divColor += "</tr>";
			}
		}else{
			for(var x = 0; x < arrSize.length; x++){
				divColor += "<tr><td>"+origSize[arrSize[x]]+"</td><td><input id = 'txtStock"+origSize[arrSize[x]]+numColors+"' type = 'text' ";
				divColor += "/></td>";
				divColor += "</tr>";
			}
		}

		divColor += "</table><br /><label id = 'lblWarnColorStock"+numColors+"'></label></div>";
		$("#divColor").append($.parseHTML(divColor));

		for(var x = 0; x < arrSize.length; x++){
			$("#txtColorName"+numColors).on("input propertychange",function() {
				$(this).val($(this).val().replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}));
			});
			$("#txtStock"+arrSize[x]+numColors).on("input propertychange", function(){	
	    		$(this).val($(this).val().replace(/\D/g,''));
	    	});
		}

    	$("#btnCloseColor"+numColors.toString()).click(function(){
    		$("#divColor"+ $(this).attr('numcolor')).remove();
    		containerOccur.splice(containerOccur.indexOf($(this).attr('numcolor')), 1);
    	});

    	for(var x = 0; x < faces.length; x++){
	    	$("#pic"+faces[x]+numColors+" .btnClear").click(function(){
				var id  = "#"+$(this).parent().prop('id');
				if($(id+" .cropit-preview-image")){
					$(id+" .cropit-preview-image").removeAttr('src');
					$(id+" .cropit-image-zoom-input").val("0.5");
					$(id+" .cropit-preview").removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
					$(id+" .cropit-image-input")[0].value = null;
				}
			});
		}


		$(".image-editor").cropit({
			quality: .9,
			originalSize: true,
			exportZoom: 1.00,
			allowDragNDrop: true,
			onImageLoaded:function(){
				for(var y = 0; y < containerOccur.length; y++){
					for(var x = 0; x < faces.length; x++){
						var img = $("#pic"+faces[x]+containerOccur[y]).find('.cropit-preview-image');

						if((img.outerHeight() > 400 || img.outerWidth() < 350) && img.attr('src')){
							
							$(img).removeAttr('src');
							$("#pic"+faces[x]+y).find('.cropit-preview').removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
							$("#pic"+faces[x]+y).find(" .cropit-image-input")[0].value = null;
							popupOk("Warning","The height must be equal to 400px and the width must be equal to 350px.");
						}
					}
				}
			},
			onImageError:function(){
				popupOk("Warning","The height must be equal to 400px and the width must be equal to 350px.");
			}
		});

    	$('#color'+numColors.toString()).colorPicker(options);
    	numColors ++;
	}

});