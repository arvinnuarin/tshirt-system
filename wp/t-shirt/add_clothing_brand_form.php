<html>
<head>
    <link rel = 'stylesheet' href = "add_clothing_brand.css">
	<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "add_clothing_brand.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
	<title>Clothing Brand</title>
</head>
<body>
	<?php
		session_start();
		require_once('../setup/setup_form.php');
		$setup = new SetUp($_SESSION['position']);
	?>
    
	<h1>Add Clothing Brand</h1>
    <br>
    <div id = "divAddBrand">
    <label id = lblAdd>Add Clothing Brand</label>
    <div id = "divline"></div>
    <br>    <br>    <br> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing Brand Name: <input type='text' id = 'txtBrand'/>
	<label id = 'lblWarnBrand'></label> <br />
	<input type = 'button' value = 'Add Brand' id = 'btnAddBrand'/>
    </div>
    <div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>

</body>
</html>