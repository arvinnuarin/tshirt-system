<?php
	session_start();

	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['position'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) && !empty($_SESSION['position'])){
			if(isset($_POST['do'])){
				if(!empty($_POST['do'])){
					require_once('../../php/awp.php');
					$awp = new Awp();
					if($_POST['do'] == "add"){
						if(isset($_POST['stocks']) && isset($_POST['sizeId']) && isset($_POST['colorId']) && isset($_POST['size']) && isset($_POST['clothing']) && isset($_POST['date']) && isset($_POST['do']) && isset($_POST['clothingId']) && isset($_POST['prodId']) && isset($_POST['pmId'])){
							if($_POST['stocks'] !== "" && $_POST['sizeId'] !== "" && $_POST['colorId'] !== "" && $_POST['size'] !== "" && $_POST['clothing'] !== "" && $_POST['date'] !== "" && $_POST['clothingId'] !== ""&& $_POST['do'] !== "" && $_POST['prodId'] !== "" && $_POST['pmId'] !== ""){
								if($_POST['do'] == "add"){
									$stocks = $_POST['stocks'];
									$size_id = $_POST['sizeId'];
									$color_id = $_POST['colorId'];
									$size = $_POST['size'];
									$clothing = $_POST['clothing'];
									$date = $_POST['date'];
									$clothing_id = $_POST['clothingId'];
									$employee_id = $_SESSION['id'];
									$prodId = $_POST['prodId'];
									$pmId = $_POST['pmId'];
									$result = $awp -> add_clothing_color_stock($size, $color_id, $size_id, $stocks, $clothing, $employee_id, $date, $clothing_id, $prodId, $pmId);
									echo $result;
								}
							}else{
								echo "Some data are empty";
							}
						}else{
							echo "Some data are not set";
						}
					}else if($_POST['do'] == "update"){
						if(isset($_POST['stocks']) && isset($_POST['stockId']) && isset($_POST['size']) && isset($_POST['clothing']) && isset($_POST['date']) && isset($_POST['colorId']) && isset($_POST['clothingId']) && isset($_POST['prodId']) && isset($_POST['pmId'])){
							if($_POST['stocks'] !== "" && $_POST['stockId'] !== "" && $_POST['size'] !== "" && $_POST['clothing'] !== "" && $_POST['date'] !== "" && $_POST['clothingId'] !== "" && $_POST['colorId'] && $_POST['prodId'] !== "" && $_POST['pmId'] !== ""){
								if($_POST['do'] == "update"){
									$stocks = $_POST['stocks'];
									$stock_id = $_POST['stockId'];
									$size = $_POST['size'];
									$clothing = $_POST['clothing'];
									$date = $_POST['date'];
									$color_id = $_POST['colorId'];
									$clothing_id = $_POST['clothingId'];
									$prodId = $_POST['prodId'];
									$pmId = $_POST['pmId'];
									$employee_id = $_SESSION['id'];
									$result = $awp -> update_clothing_color_stock($stock_id, $stocks, $size, $clothing, $employee_id, $date, $color_id, $clothing_id, $prodId, $pmId);
									echo $result;
								}
							}else{
								echo "Some data are empty";
							}
						}else{
							echo "Some data are not set";
						}
					}else if($_POST['do'] == "update_price"){
						if(isset($_POST['price']) && isset($_POST['priceId']) && isset($_POST['clothing']) && isset($_POST['size']) && isset($_POST['date'])){
							if($_POST['price'] !== "" && $_POST['priceId'] !== "" &&$_POST['clothing'] !== "" && $_POST['size'] !== "" && $_POST['date'] !== ""){
									$employee_id = $_SESSION['id'];
									$price = $_POST['price'];
									$price_id = $_POST['priceId'];
									$size = $_POST['size'];
									$clothing = $_POST['clothing'];
									$date = $_POST['date'];
									/*if($awp -> update_clothing_price($price_id, $price, $date, $size, $clothing))
										echo "Success";
									else
										echo "Failed";*/
									$result = $awp -> update_clothing_price($price_id, $price, $date, $size, $clothing, $employee_id);
									echo $result;
								
							}else{
								echo "Some data are empty";
							}
						}else{
							echo "Some data are not set";
						}
					}else if($_POST['do'] == "get_color_info"){
						if(isset($_POST['colorId']) && isset($_POST['pdId']) && isset($_POST['pmId'])){
							if(!empty($_POST['colorId']) && $_POST['pdId'] !== "" && $_POST['pmId'] !== ""){
								require_once("../../php/config.php");
								$arr_const_size = unserialize(SIZE);
								$color_id = $_POST['colorId'];
								$pd_id = $_POST['pdId'];
								$pm_id = $_POST['pmId'];
								$size_info = $awp -> view_shirt_color_stock($color_id, $pd_id, $pm_id);
								foreach($size_info as $si){
									echo "<tr stock-id = '".$si['scs_id']."'>";
									echo "<td class = 'size'>".$arr_const_size[$si['size']]."</td>";

									if($si['stock'] > 0)
										echo "<td class = 'slot-stock' size-clone = '".$si['stock']."'>".$si['stock']."</td>";
									else
										echo "<td size-clone = '".$si['size']."' class = 'slot-stock' ><label class = 'lblNA'>N/A</label><input size-id = '".$si['csd_id']."' color-id = '".$color_id."' class = 'stocks' type = 'number' style = 'visibility:hidden; width:0px;'/></td>";
									if($si['time']){
										$datetime = new DateTime($si['time']);
										echo "<td class = 'slot-date'>".$datetime->format('F j, Y g:ia')."</td>";
									}else
										echo "<td class = 'slot-date'>N/A</td>";
									
									if($si['stock'] > 0)
										echo "<td><a href = '#' class = 'updateStocks' btn-name = 'update' color-id = '".$color_id."'>Update Stocks</a> <a href = '#' class = 'cancel-stock' style ='visibility:hidden;'>Cancel</a></td>";
									else 
										echo "<td class = 'btn-manipulator'><a href = '#' btn-name = 'add' class = 'addStocks' >Add Stocks</a> <a href = '#' class = 'cancel-stock' style ='visibility:hidden;'>Cancel</a></td>";
									
									echo"</tr>";
									
								}	
								
								
							}else{
								echo "Some data are empty";
							}
						}else{
							echo "Some data are not set";
						}
					}
				}
			}
		}else{
			echo "User is empty";
		}
	}else {
		echo "User not set";
	}


?>