$(document).ready(function(){
	    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	var faces = ["Front", "Back"];
	$("#btnAddInfo").click(function(){
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var imageData = [], arrImageName = [], faces = ["Front", "Back"], arrStocks = [], arrSizeId = [], arrSize = [];
		var ctr = 0;

		$("#lblWarnColorStock").html("");
		$("#lblWarnColorHex").html("");
		$("#lblWarnColor").html("");
		if($.trim($("#txtColorName").val()) == ""){
			$("#lblWarnColor").html("Empty Color Name!");
		}else{
			ctr += 1;			
		}

		if($("#txtId").val() == ""){
			popupOk("Alert","Something went wrong!");
			$(".popup-ok").click(function(){
				window.location.replace("");
			});
		}else{
			ctr += 1;
		}

		if($(".color").val() == ""){
			$("#lblWarnColorHex").html("Empty Color Hex!");
		}else{
			ctr += 1;
		}

		var arrSizeForm = $(".size").map(function() {
			if($(this).val() != "" ){
		   		return $(this).val();
			}
		}).get();


		for(var x = 0; x < arrSizeForm.length; x++){
			if($("#stocks"+arrSizeForm[x].toString()).val() != "" && $("#stocks"+x.toString()).attr("size-id") != ""){
				arrStocks.push($("#stocks"+arrSizeForm[x].toString()).val());
			}
			if($("#stocks"+arrSizeForm[x].toString()).val() != "" && $("#stocks"+x.toString()).attr("size-id") != ""){
				arrSizeId.push($("#stocks"+arrSizeForm[x].toString()).attr("size-id"));
			}
			if($("#size"+arrSizeForm[x].toString()).val() != "" && $("#stocks"+arrSizeForm[x].toString()).val() != ""){
				arrSize.push($("#size"+arrSizeForm[x].toString()).val());
			}
		}

		if(arrStocks.length == 0){
			$("#lblWarnColorStock").html("Please fill at least one size's stock!");
		}else{
			ctr += 1;
		}

		if(arrSizeId.length == 0 && arrSizeForm.length == 0){
			popupOk("Something went wrong!");
		}else{
			ctr += 1;
		}

		if(arrSize.length == 0 && arrSizeForm.length == 0){
			popupOk("Something went wrong!");
		}else{
			ctr += 1;
		}

		for(var y = 0; y < faces.length; y++){
			$("#lblWarnTemplateColor"+faces[y]).html("");
			var idpic = "#pic"+faces[y];
			if($(idpic).length){
				if($(idpic+" .cropit-preview-image").attr('src')){
					$(".lblWarnTemplateColor").html("");
					if($(idpic).cropit('export')){
						imageData.push($(idpic).cropit('export'));
						arrImageName.push("pictures/shirt_template/" + $("#txtId").val()+"_"+faces[y]+"_" + $.trim($("#txtColorName").val()) + ".png");
					}
				}else{
					$("#lblWarnTemplateColor"+faces[y]).html("Empty "+faces[y]+" Template.");
				}
			}
		}

		if(arrImageName.length == 2 && imageData.length == 2){
			ctr += 1;
		}

		if($("#clothing").val() == ""){
			popupOk("Error","Something went wrong!");
			$(".popup-ok").click(function(){
				window.location.replace("");
			});
		}else{
			ctr += 1;
		}


		if(ctr == 8){
			loading();
			$.post("t-shirt/add_clothing_color.php",{clothingId:$("#txtId").val(), colorName:$.trim($("#txtColorName").val()), colorHex:$(".color").val(), sizeId:arrSizeId, stocks:arrStocks, date:currentDate, imageData:imageData, imageName:arrImageName, clothing:$("#clothing").val(), size:arrSize},
			function(data){
				if(data){
					$('.back').remove();
					if(data.indexOf("Color Name Exist") != -1 || data.indexOf("Color Hex Exist") != -1){
						var strWarn = "";
						if(data.indexOf("Color Name Exist") != -1){
							$("#lblWarnColor").html("Color Name Already Exist!");
							strWarn += "Color Name";
						}
						if(data.indexOf("Color Hex Exist") != -1){
							if(strWarn != "")
								strWarn += " and ";
							$("#lblWarnColorHex").html("Color Hex Already Exist!");
							strWarn += "Color Hex";
						}
						strWarn += " Exist!";
						popupOk("Warning", strWarn);
					}else if(data.indexOf("Success") == 0){
						var count = data.split("#");
						popupOk("Clothing Color", "New Color Successfully Added!");
						$(".popup-ok").click(function(){
							window.location.replace("t-shirt/view_clothing_color.php?clothing_id="+$("#txtId").val()+"&start="+count[1]);
						});	
					}else{
						popupOk("Error", "Something went wrong!");
					}
				}
			});
		}
	});

	$(".stocks").on("input propertychange", function(){
		$(this).val($(this).val().replace(/[^0-9]/g,''));
	});

	$(".btnClear").click(function(){
		var id  = "#"+$(this).parent().prop('id');
		if($(id+" .cropit-preview-image")){
			$(id+" .cropit-preview-image").removeAttr('src');
			$(id+" .cropit-preview").removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
			$(id+" .cropit-image-input")[0].value = null;
		}
	});

	$('.color').colorPicker(options);
	$(".image-editor").cropit({
		quality: .9,
		originalSize: true,
		exportZoom: 1.00,
		allowDragNDrop: true,
		onImageLoaded:function(){
			for(var x = 0; x < faces.length; x++){
				var img = $("#pic"+faces[x]).find('.cropit-preview-image');

				if((img.outerHeight() > 550 || img.outerWidth() < 550) && img.attr('src')){
					
					$(img).removeAttr('src');
					$("#pic"+faces[x]).find('.cropit-preview').removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
					$("#pic"+faces[x]).find(" .cropit-image-input")[0].value = null;
					popupOk("Warning","The height must be equal to 550px and the width must be equal to 550px.");
				}
			}
		},
		onImageError:function(){
			popupOk("Warning","The height must be equal to 550px and the width must be equal to 550px.");
		}
	});
});