<html>
	<?php	
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['position'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) && isset($_SESSION['name'])) {
				echo "<head>";
				?>
				<link rel="stylesheet" type="text/css" href="../../css/jquery.datetimepicker.css">
				<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
				<script type = "text/javascript" src = '../../js/jquery.datetimepicker.full.js'></script>
				<script type = "text/javascript" src = 't-shirt.js'></script>
			    <link rel = 'stylesheet' href = "t-shirt.css">
				<title>Clothing Information</title>
				<?php
					require_once('../setup/setup_form.php');
					echo "</head>";
					echo "<body>";
					require_once('../../php/awp.php');
					require_once('../../php/config.php');
					$awp = new Awp();
					if(isset($_GET['start'])){
						if(!empty($_GET['start'])){
							if(is_numeric($_GET['start'])){
								$start = intval($_GET['start']);
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}

					if(isset($_GET['add_clothing_color'])){
						$stat = "add_clothing_color";
					}else{
						$stat = "";
					}
					$per_page = 10;
					if(isset($_GET['search']) && isset($_GET['filter'])){
						if(!empty($_GET['search']) && !empty($_GET['filter'])){
							$search = $_GET['search'];
							$filter_id = $_GET['filter'];
							if($filter_id >= 1 && $filter_id <= 4){
								if($filter_id == 1){
									$filter = "clothing_brand.brand_name";
								}else if($filter_id == 2){
									$filter = "clothing_type.type";
								}else if($filter_id == 3){
									$filter = "clothing_fabric.name";
								}else if($filter_id == 4){
									$filter = "clothing.clothing_id";
								}
							}else{
								$search = "";
								$filter = "clothing.clothing_id";
								$filter_id = -1;
							}
						}else{
							$search = "";
							$filter = "clothing.clothing_id";
							$filter_id = -1;
						}
					}else{
						$search = "";
						$filter = "clothing.clothing_id";
						$filter_id = -1;
					}

					if(isset($_GET['product_design_id'])){
						if(!empty($_GET['product_design_id'])){
							$product_design_id = $_GET['product_design_id'];
						}else{
							$product_design_id = 0;
						}
					}else{
						$product_design_id = 0;
					}

					if(isset($_GET['pm_id'])){
						if(!empty($_GET['pm_id'])){
							$print_method_id = $_GET['pm_id'];
						}else{
							$print_method_id = 0;
						}
					}else{
						$print_method_id = 0;
					}

					if(isset($_GET['from_date']) && isset($_GET['to_date']) && !isset($_GET['from_date_sales']) && !isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date']) && !empty($_GET['to_date'])){
							$from_date = $_GET['from_date'];
							$to_date = $_GET['to_date'];
						}else{
							$from_date = "";
							$to_date = "";
						}
					}else{
						$from_date = "";
						$to_date = "";				
					}

					if(!isset($_GET['from_date']) && !isset($_GET['to_date']) && isset($_GET['from_date_sales']) && isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date_sales']) && !empty($_GET['to_date_sales'])){
							$from_date_sales = $_GET['from_date_sales'];
							$to_date_sales = $_GET['to_date_sales'];
						}else{
							$from_date_sales = "";
							$to_date_sales = "";
						}
					}else{
						$from_date_sales = "";
						$to_date_sales = "";				
					}
					$setup = new SetUp($_SESSION['position']);
					$sort_trans = array(array("id_desc", "clothing.clothing_id", "desc"), array("id_asc", "clothing.clothing_id", "asc"), array("brand_asc", "brand_name", "asc"), array("brand_desc", "brand_name", "desc"), array("type_desc", "type", "desc"), array("type_asc", "type", "asc"));
					foreach ($sort_trans as $st) {
						if(isset($_GET['sort'])){
							if(!empty($_GET['sort'])){
								$ctr = 0;
								if($st[0] == $_GET['sort']){
									$sort = $_GET['sort'];
									$col_order = $st[1];
									$order = $st[2];
									break;
								}else{
									$ctr += 1;
								}
								
								if(count($sort_trans) == $ctr){
									$sort = "";
									$col_order = "clothing.clothing_id";
									$order = "";
								}
							}else{
								$sort = "";
								$col_order = "clothing.clothing_id";
								$order = "";
							}
						}else{
							$sort = "";
							$col_order = "clothing.clothing_id";
							$order = "";
						}
					}

					$record_count = $awp ->get_all_shirt_count($filter, $search, $product_design_id, $print_method_id);
					$arr_shirt = $awp -> view_shirt($start, $per_page, $filter, $search, $col_order, $order, $product_design_id, $from_date, $to_date,  $from_date_sales, $to_date_sales, $print_method_id);

		            if($product_design_id == 0){
		                echo "<ul class = 'topMenu'>";
						echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
						echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
						echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
		                echo "<li><a class='active' href = 't-shirt'>Clothing Information</a></li>";
		                $count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
		                echo "<li><a href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
		                echo "</ul>";
		                echo "<h1>Clothing Information</h1>";
						echo "<a id = 'btnClothingInfo' href = 't-shirt/add_clothing_form.php?product_design_id'>Add Clothing Information</a>";
						
					}else if($product_design = $awp -> get_pd_name_with_pm($product_design_id, $print_method_id)){
						echo "<h2>Clothing Used by Product Design <b style = 'color: #d73925';>".$product_design[0]['design']."</b>";
						if($print_method_id > 0)
							echo "(".$product_design[0]['print'].")";
						echo "</h2></br></br>";
					}else{
						echo "<script type = 'text/javascript'>window.location.replace('//www.mixlarts.com/MixlArts/wp/shirt_design/');</script>";
					}
		            echo "<div id= 'divCloth'>";
		            echo "</br>";
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Search via: </label>";
					$arr_options = array("Brand Name", "Type", "Fabric");
		            echo "<select id = 'selSearch'>";
					
		            for($x = 0; $x < count($arr_options); $x++){
						echo "<option value = '".($x+1)."' ";
						if($filter_id == $x+1){echo "selected";} 
						echo ">".$arr_options[$x]."</option>";
					}
					echo "</select> ";          
					echo "<input type = 'text' id = 'txtSearch' value = '$search' placeholder = 'Search..'/>";
					echo "<input type='image' value = 'Search' id = 'btnSearch'  src ='t-shirt/icon/imgSearch.png'/>";
				
		           	echo "<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: </label><select id = 'selSort'>";	
					$arr_sort = array(array("ID: Lowest to Highest", "id_asc"), array("ID: Highest to Lowest", "id_desc"), array("Brand Name: (A-Z)", "brand_asc"), array("Brand Name: (Z-A)", "brand_desc"), array("Type: (A-Z)", "type_asc"), array("Type: (Z-A)", "type_desc"), array("Fabric: (A-Z)", "fabric_asc"), array("Fabric: (Z-A)", "fabric_desc"), array("Type: (A-Z)", "type_asc"), array("Type: (Z-A)", "type_desc"));
					foreach($arr_sort as $s){
						$filter_url = "t-shirt/index.php?";
						if($search != ""){
							$filter_url .= "search=$search&";
						}
						if($filter_id <= 4 && $filter_id > 0){
							$filter_url .= "filter=$filter_id&";
						}
						if($start !== ""){
							$filter_url .= "start=$start&";
						}
						if($product_design_id != 0){
							$filter_url .= "product_design_id=$product_design_id&";
						}
						$filter_url .= "sort=".$s[1];
						//echo "<a href = '".$filter_url."'>".$s[0]."</a> | ";
						echo "<option ";
						if($sort == $s[1]){
							echo "selected";
						}
						echo " value = '".$filter_url."'>".$s[0]."</option>";
					}
					echo "</select> ";
					
					$dt = time();
					$current_time = strftime("%Y-%m-%d %H:%M:%S",$dt);
					

					
					if($search !== ""){
						echo "<label id = 'lblResult'>";
						echo $record_count[0]['count']." result";
						if($record_count[0]['count'] > 1)
							echo "s";
						echo " for Color \"". $search."\"";
						echo "</label>";
					}
					
		            $header = array("Clothing ID", "Brand Name", "Fabric", "Type", "Options");
					echo "<table id = 'tbClothInfo'>";

		            foreach ($header as $h) {
		            	echo "<th ";
		            	if($h == "Options")
		            		echo " colspan = '4' ";
		            	echo ">$h</th>";
		            }

					$arr_const_size = unserialize (SIZE);
					$flag = 0;
					foreach ($arr_shirt as $as) {
						if($flag == 0){
							$color = '#e8e7e7';
							$flag += 1;
						}else{
							$color = 'white';
							$flag = 0;
						}
						echo "<tr>";
						echo "<td>CLOTH-".$as['ci']."</td>";
						echo "<td>".$as['brand_name']."</td>";
						echo "<td>".$as['name']."</td>";
						echo "<td>".$as['type']."</td>";
						
						if($product_design_id <= 0 && $print_method_id <= 0)
							echo "<td><a href = 't-shirt/modify_shirt_size_info_form.php?clothing_id=".$as['ci']."&product_design_id=$product_design_id'>Add Size</a></td>";
						echo "<td><a href = 't-shirt/view_clothing_size_desc.php?clothing_id=".$as['ci'];
						if($product_design_id > 0 || $print_method_id > 0)
							echo "&product_design_id=$product_design_id&pm_id=$print_method_id";
						echo "'>View Size</a></td>";
						
						if($product_design_id <= 0 && $print_method_id <= 0)
							echo "<td><a href = 't-shirt/add_clothing_color_form.php?clothing_id=".$as['ci']."'>Add Color</a></td>";
						else
							echo "<td><a href = 'shirt_design/add_clothing_assoc_form.php?product_design_id=".$product_design_id."'>Add Associated Color</a></td>";
						echo "<td><a href = 't-shirt/view_clothing_color.php?clothing_id=".$as['ci'];
						if($product_design_id > 0 || $print_method_id > 0)
							echo "&product_design_id=$product_design_id&pm_id=$print_method_id";
						echo "'>View Color</a></td>";

						echo "</tr>";
					}
					echo "</table>";

					if($record_count[0]['count'] > 10){
						$page_url = "index.php?";

						if($search != ""){
							$page_url .= "search=$search&";
						}
						if($filter_id > 0 && $filter_id <= 4){
							$page_url .= "filter=$filter_id&";
						}
						if($sort != ""){
							$page_url .= "sort=$sort&";
						}
						if($product_design_id != 0){
							$page_url .= "product_design_id=$product_design_id&";
						}
		                echo "<div id = 'btnPagination'>";
						$prev = $start - $per_page;
						if($start > 0){
							echo "<a class = 'lblPagination' href = '".$page_url."start=$prev'>Prev</a>";
						}
						$i = 1;
						for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
							if($start != $x){
								echo " <a class = 'lblPagination' href = '".$page_url."start=$x'>$i</a> ";

							}else{
								echo " <a style='background-color: #dd4b39; color:white;'class = 'lblPagination' href = '".$page_url."start=$x'><b>$i</b></a> ";
							}
							$i++;
						}

						$next = $start + $per_page;
						if($start < $record_count[0]['count'] - $per_page ){
							echo " <a class = 'lblPagination' href = '".$page_url."start=$next'>Next</a>";
						}
		                    echo "</div>";
					}

				echo "</br></br></br>";
				echo "</div>";
				echo "</br>";
		    	echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
		    	echo "<input type = 'hidden' value = '".$product_design_id."' id = 'productDesignId'>";
		    	echo "<input type = 'hidden' value = '".$print_method_id."' id = 'pmId'>";
				echo "</body>";
			}else{
				session_destroy();
				echo "<script>window.location.replace('../../wp/');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	?>

</html>
		    	