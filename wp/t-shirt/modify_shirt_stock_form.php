<html>
<head>
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "modify_shirt_stock.js"></script>
</head>
<body>
	<?php
	if(isset($_GET['clothing_id']) && isset($_GET['clothing_color_id'])){
		if(!empty($_GET['clothing_id']) && !empty($_GET['clothing_color_id'])){
			require_once('../../php/awp.php');
			$awp = new Awp();
			$clothing_id = $_GET['clothing_id'];
			$clothing_color_id = $_GET['clothing_color_id'];
			if($arr_shirt_color_stock = $awp -> view_clothing_color_stock($clothing_color_id)){
				session_start();
				require_once('../../php/config.php');
				require_once('../setup/setup_form.php');
				$arr_const_size = unserialize (SIZE);
				//$setup = new SetUp($_SESSION['position']);
				$clothing = $awp -> get_clothing($clothing_id);
				$clothing_color = $awp -> get_color_name($clothing_color_id);
				$arr_size = $awp -> view_available_shirt_size($clothing_id);
				echo "<h1>Update Stocks for ".$clothing[0]['clothing']."</h1>";

				echo "<input type = 'hidden' value = '".$clothing_id."' id = 'clothingId'/>";
				echo "<input type = 'hidden' value = '".$clothing_color_id."' id = 'clothingColorId'>";
				echo "<input type = 'hidden' value = '".$clothing[0]['clothing']."' id = 'brandName'>";

				echo "Color: ".$clothing_color[0]['color'];
				echo "<input readonly style = 'height:20px;width:20px;background-color:".$clothing_color[0]['hex']."'/>";
				echo "<br />";
				echo "<table><tr><td>Size</td><td>Current Stocks</td><td>Date Stocks Updated</td></tr>";
				foreach ($arr_const_size as $s) {
					echo "<tr>";
					echo "<td>$s</td>";
				
					$ctr = 0;
					foreach ($arr_shirt_color_stock as $as) {
						if($s == $arr_const_size[$as['size']]){
							echo "<td><input type = 'text' value = '".$as['stock']."' id = 'txt$s' default = '".$as['stock']."' shadow-id = '".$as['scs_id']."'/></td>";
							$datetime = new DateTime($as['time']);
							echo "<td>".$datetime->format('F j, Y g:ia')."</td></tr>";
							break;
						}else{
							$ctr ++;
						}
					}
					if($ctr == count($arr_shirt_color_stock)){
						$ctr_2 = 0;
						foreach ($arr_size as $asize) {
							if($arr_const_size[$asize['size']] == $s){
								echo "<td><input type = 'text' id = 'txt$s'></td>";
								echo "<td></td></tr>";
								break;
							}else
								$ctr_2 += 1;
						}
						if($ctr_2 == count($arr_size)){
							echo "<td>No Size Info</td>";
							echo "<td></td></tr>";	
						}
					}
				}
				echo "</table>";
				echo "<input type = 'button' id = 'btnEditStocks' value = 'Update Stocks'/>";
			}
		}
	}
?>
	
	
</body>
</html>