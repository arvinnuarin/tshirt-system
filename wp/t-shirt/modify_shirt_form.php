<html>
<head>	
	<?php
	session_start();
	if(isset($_GET['shirtId'])){
		if(!empty($_GET['shirtId'])){

			require_once('../../php/awp.php');
			$awp = new Awp();
			$shirt_id = $_GET['shirtId'];
			if($arr_size = $awp -> view_available_shirt_size($shirt_id)){
				$script = "<script type = 'text/javascript'>var arrSizeFilled2 = [";
				foreach ($arr_size as $as) {
					$script .="'".$as['size']."',";
				}
				$script = trim($script, ",")."];</script>";
				
			}
		}
	}
	if(isset($arr_size) || !isset($_GET['shirtId'])){
	?>
        
		<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
		<link rel = "stylesheet" href = '/MixlArts/css/upload_photo.css' type = "text/css">
		<link id="colorPickerMod" rel="stylesheet" type="text/css" href="/MixlArts/js/tinyColorPicker-master/demo/mod.css">
		<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>		
		<script type="text/javascript" src="/MixlArts/js/tinyColorPicker-master/colors.js"></script>
		<script type="text/javascript" src="/MixlArts/js/tinyColorPicker-master/jqColorPicker.js"></script>
		<script type="text/javascript" src="/MixlArts/js/tinyColorPicker-master/demo/index.js"></script>
		<script type = "text/javascript" src = '/MixlArts/js/cropit-master/dist/jquery.cropit.js'></script>
		
	<?php
	}
		if(isset($arr_size)){
			echo $script;
		}
	?>
    <link rel = 'stylesheet' href = "t-shirt/modify_shirt.css">
	<script type = "text/javascript" src = "modify_shirt.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
</head>
<body>
	<?php
		if(isset($arr_size) || !isset($_GET['shirtId'])){
			require_once('../setup/setup_form.php');
			require_once('../../php/awp.php');
			$setup = new SetUp($_SESSION['position']);
			$awp = new Awp();
            
            echo "<ul class = 'topMenu'>";
            echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
            echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
            echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
            echo "<li><a class = 'active'href = 't-shirt/index.php'>Clothing Information</a></li>";
            echo "</ul>";

			if(isset($arr_size)){
                echo "<div class = 'colorInfoMenu'>";
				echo "<h1>Add Color Information</h1>";
				echo "Brand ID: PROD - <label id = 'lblId'>$shirt_id</label><br />";
				echo "Brand Name: ".$arr_size[0]['brand_name']."<br />";
				echo "Type: ".$arr_size[0]['type']."<br />";
				echo "Fabric: ".$arr_size[0]['name']."<br />";
				echo "<input id = 'selBrand' type = 'hidden' value = '".$arr_size[0]['brand_name']."'/>";
				echo "<input id = 'selType' type = 'hidden' value = '".$arr_size[0]['type']."'/>";
				echo "<input id = 'selFabric' type = 'hidden' value = '".$arr_size[0]['name']."'/>";
                echo "</div>";

			}
			else{
				$clothing_brand = $awp -> get_all_clothing_brand();
				$clothing_type = $awp -> get_all_clothing_type();
				$clothing_fabric = $awp -> get_all_clothing_fabric();
				echo "<h1>Add Clothing Information</h1>";
                echo "<div id = 'divColorMenu'>";
                echo "<h3>Clothing Information</h3><br />";

				echo " &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand Name: ";
				echo "<select id = 'selBrand'><option value = ''>Select Brand</option>";
				foreach($clothing_brand as $cb) {
					echo "<option value = '".$cb['cb_id']."'>".$cb['brand_name']."</option>";
				}
				echo "</select>";
				echo "<label id = 'lblWarnBrand'></label>";

				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;&nbsp;Type: ";
				echo "<select id = 'selType'><option value = ''>Select Type</option>";
				foreach ($clothing_type as $ct) {
					echo "<option value = '".$ct['ct_id']."'>".$ct['type']."</option>";
				}
				echo "</select><br />";
				echo "<label id = 'lblWarnType'></label><br />";

				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fabric: ";
				echo "<select id = 'selFabric'><option value = ''>Select Fabric</option>";
				foreach ($clothing_fabric as $cf) {
					echo "<option value = '".$cf['cf_id']."'>".$cf['name']."</option>";
				}
				echo "</select>";
				echo "<label id = 'lblWarnFabric'></label>";
				
				echo "<h3>Size Information</h3>";
				echo "<table id = 'tblSizeInfo'>";
				echo "<th>Size</th>";
                echo "<th>Height</th>";
                echo "<th>Width</th";
                echo "><th>Price</th>";
                
				$size = array('XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL');
				foreach($size as $s){
					echo "<tr>";
					echo "<td>$s</td>";
					echo "<td><input type = 'text' id = 'txtHeight$s' kp = '$s'></td>";
					echo "<td><input type = 'text' id = 'txtWidth$s' kp = '$s'></td>";
					echo "<td><input type = 'text' id = 'txtPrice$s' kp = '$s'></td>";
					echo "<td><label id = 'lblWarn$s'></label></td>";
					echo "</tr>";
				}
				echo "</table> <br />";
				echo "<label id = 'lblWarnSizeInfo'></label>";
			}

            echo "</div>";
			
            echo "<div style = 'color: white;' id = 'divColor'><h3>T Shirt Colors</h3>";
			echo "<input type='button' value = 'Add T-Shirt Information' id = 'btnAddInfo'/>";
			echo "</div>";
			echo "<br><br>";
			echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
		}
	?>
</body>
</html>