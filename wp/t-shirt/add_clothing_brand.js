$(document).ready(function(){
	    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	$("#btnAddBrand").click(function(){
		$("#lblWarnBrand").html("");
		if($("#txtBrand").val() === ""){
			$("#lblWarnBrand").html("Empty Clothing Brand Name!");
            document.getElementById("txtBrand").style.border = '1px solid #dd4b39';

		}else{
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("t-shirt/add_clothing_brand.php",{cb:$("#txtBrand").val(), date:currentDate},function(data){
				//alert(data);
				if(data === "Success"){
					popupOk("Add Clothing Brand", "Clothing Brand Successfully Added!");
					$('.popup-ok').click(function(){
						window.location.replace("t-shirt/view_clothing_brand_form.php");
					});
				}else if(data === "Clothing Brand Exist"){
					popupOk("Add Clothing Brand", "Clothing Brand Already Exist!");
				}else{
					popupOk("Add Clothing Brand", "Something went wrong!");
				}
			});
		}
	});
});