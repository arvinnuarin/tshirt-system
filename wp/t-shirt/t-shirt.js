$(document).ready(function(){
    if($("#productDesignId").val() > 0){
    	$(".tabTshirtDesign").css("background-color", "#2c3b41");
	    $(".tabTshirtDesign").css("color","white");
	    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
    }else{
    	$(".tabTshirt").css("background-color", "#2c3b41");
	    $(".tabTshirt").css("color","white");
	    $(".tabTshirt").css("border-left","2px solid #e04545");
    }

	$("#btnSearch").click(function(){
		if($("#selSearch").val() <= 4 && $("#selSearch").val() >= 1){
			if($("#txtSearch").val() !== ""){
				window.location.href = "t-shirt/?start=0&search="+$("#txtSearch").val()+"&filter="+$("#selSearch").val()+"&product_design_id="+$("#productDesignId").val()+"&pm_id="+$("#pmId").val();
			}
		}
	});
	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	/*$('#txtDatePicker').click(
      function(e) {
        $('#txtDatePicker').AnyTime_noPicker().AnyTime_picker().focus();
        e.preventDefault();   
    } );*/

    var date = new Date();
    $("#fromdate").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#salesfrom").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});


	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	var date = new Date();
	$("#todate").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#fromdate").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$("#salesfrom").on("change", function(){
		var date = new Date();
    	$("#salesto").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#salesfrom").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$('#btnFilter').click(function(){
		if($("#fromdate").val() != "" || $("#todate").val() != ""){
			if($("#todate").val() >= $("#fromdate").val()){
				window.location.href = "t-shirt/?start=0&filter="+$("#selSearch").val()+"&product_design_id="+$("#productDesignId").val()+"&from_date="+$("#fromdate").val()+"&to_date="+$("#todate").val()+"&pm_id="+$("#pmId").val(); 
			}
		}
	});

	$('#btnFilterSales').click(function(){
		if($("#salesfrom").val() != "" || $("#salesto").val() != ""){
			if($("#salesto").val() >= $("#salesfrom").val()){
				window.location.href = "t-shirt/?start=0&filter="+$("#selSearch").val()+"&product_design_id="+$("#productDesignId").val()+"&start=0&from_date_sales="+$("#salesfrom").val()+"&to_date_sales="+$("#salesto").val()+"&pm_id="+$("#pmId").val(); 
			}
		}
	});

});