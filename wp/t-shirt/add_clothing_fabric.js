$(document).ready(function(){
	    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	$("#btnAddFabric").click(function(){
		$("#lblWarnFabric").html("");
		if($("#txtFabric").val() === ""){
			$("#lblWarnFabric").html("Empty Clothing Fabric Name!");
            document.getElementById("txtFabric").style.border = '1px solid #dd4b39';

		}else{
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("t-shirt/add_clothing_fabric.php",{cf:$("#txtFabric").val(), date:currentDate},function(data){
				//alert(data);
				if(data === "Success"){
					popupOk("Add Clothing Fabric", "Clothing Fabric Successfully Added!");
					$('.popup-ok').click(function(){
						window.location.replace("t-shirt/view_clothing_fabric_form.php");
					});
				}else if(data === "Clothing Fabric Exist"){
					popupOk("Add Clothing Fabric", "Clothing Fabric Already Exist!");
				}else{
					popupOk("Add Clothing Fabric", "Something went wrong!");
				}
			});
		}
	});
});