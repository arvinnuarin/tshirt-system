$(document).ready(function(){
	    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	for(var x = 0; x < arrSize.length; x++){
		$("#txtHeight"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		});
		$("#txtWidth"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		});
		$("#txtPrice"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		});
	}

	$("#btnAddInfo").click(function(){
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var arrSizesInfo = [];
		var filled = 0, ctr = 0;;
		$("#lblWarnBrand").html("");
		$("#lblWarnType").html("");
		$("#lblWarnFabric").html("");
		$("#lblWarnSizeInfo").html("");
		if($("#selBrand").val() == ""){
			$("#lblWarnBrand").html("Empty Brand Name!");
		}else{
			ctr += 1;
		}
		if($("#selType").val() == ""){
			$("#lblWarnType").html("Empty Type Name!");
		}else{
			ctr += 1;
		}
		if($("#selFabric").val() == ""){
			$("#lblWarnFabric").html("Empty Fabric Name!");
		}else{
			ctr += 1;
		}
		for(var x = 0; x < arrSize.length; x ++){
			$("#lblWarn"+arrSize[x]).html("");
				if($("#txtHeight"+arrSize[x]).val() != "" || $("#txtWidth"+arrSize[x]).val() != "" || $("#txtPrice"+arrSize[x]).val() != ""){
					var warn = "";
					if($("#txtHeight"+arrSize[x]).val() == ""){
						warn += "Height ";
					}
					if($("#txtWidth"+arrSize[x]).val() == ""){
						warn += "Width ";
					}
					if($("#txtPrice"+arrSize[x]).val() == ""){
						warn += "Price ";
					}
					if(warn != ""){
						warn = "Empty "+$.trim(warn).replace(/[ ,]+/g, ", ");
						$("#lblWarn"+arrSize[x]).html(warn);
					}else{
						filled += 1;
						var subSizesInfo = [$("#txtHeight"+arrSize[x]).val(), $("#txtWidth"+arrSize[x]).val(), $("#txtPrice"+arrSize[x]).val(), x];
						arrSizesInfo.push(subSizesInfo);
					}
				}	
			
		}
		
		if(arrSizesInfo.length == 0){
			$("#lblWarnSizeInfo").html("Please Fill Up at least one Size Information!");
		}else {
			var view = 0;
			for(x = 0; x < arrSizesInfo.length; x++){
				if(arrSizesInfo[x][3] <= 4 && arrSizesInfo[x][3] >= 0){
					view += 1;
				}
			}
			if(view < 5){
				$("#lblWarnSizeInfo").html("Please Fill Up All Required Sizes!");
			}else if(view == 5){
				ctr += 1;
			}
		}
		
		if(ctr == 4){		
			$.post("t-shirt/add_clothing.php",{brand:$("#selBrand").val(), type:$("#selType").val(), fabric:$("#selFabric").val(), sizeInfo:arrSizesInfo, date:currentDate}, function(data){
				if(data.indexOf("Success") != -1){
					var count = data.split("#");
					popupOk("Clothing Information", "Successfully Added!");
					$('.popup-ok').click(function(){
						window.location.replace("t-shirt/?start="+count[1]);
					});
				}else if(data == "Clothing exist"){
					popupOk("Warning", "Clothing Information already exist!");
				}else if(data == "Fill up all required sizes"){
					$("#lblWarnSizeInfo").html("Please Fill Up All Required Sizes!");
				}else{
					popupOk("Error", "Something went wrong!");
				}
			});

		}
	});
});