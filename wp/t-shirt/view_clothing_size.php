<html>
<head>
	<title></title>
</head>
<body>
<?php
	if(isset($_GET['clothingColorId']) && isset($_GET['clothingId'])){
		if(!empty($_GET['clothingColorId']) && !empty($_GET['clothingId'])){
			require_once('../../php/awp.php');
			$awp = new Awp();
			$clothing_id = $_GET['clothingId'];
			$clothing_color_id = $_GET['clothingColorId'];
			if($arr_shirt_color_stock = $awp -> view_shirt_color_stock($clothing_color_id)){
				session_start();
				require_once('../../php/config.php');
				require_once('../setup/setup_form.php');
				$arr_const_size = unserialize (SIZE);
				$setup = new SetUp($_SESSION['position']);

				$clothing = $awp -> get_clothing($clothing_id);
				$clothing_color = $awp -> get_color_name($clothing_color_id);

				echo "<br />";

				if(isset($_GET['clothingPage'])){
					if(!empty($_GET['clothingPage'])){
						echo "<a href = '../t-shirt?start=".$_GET['clothingPage']."'>Clothing Information</a>";
						echo ">";
					}	
				}
				
				
				echo "<a href = 'view_clothing_color.php?clothingId=".$clothing_id."'>".$clothing[0]['clothing']."</a>";
				echo ">";
				echo $clothing_color[0]['color'];

				echo "<h1>".$clothing[0]['clothing']."</h1>";
				echo "<b>Color: ".$clothing_color[0]['color']."</b> ";
				echo "<input readonly style = 'width:20px;background-color:".$clothing_color[0]['hex']."'/>";

				echo "<br />";

				//echo "<a href = 'modify_shirt_stock_form.php?shirtStockId=".$clothing_color_id."&shirtId=".$clothing_id."'>Modify Stocks</a>";
				echo "<table>";
				echo "<tr><td>Size</td><td>Current Stocks</td><td>Date Stocks Added</td></tr>";
				foreach ($arr_shirt_color_stock as $ascs) {
					echo "<tr>";
					echo "<td>".$arr_const_size[$ascs['size']]."</td>";
					echo "<td>".$ascs['stock']."</td>";
					$time = $awp->get_log_time("clothing_stock", $ascs['scs_id']);
					$datetime = new DateTime($time[0]['time']);
					echo "<td>".$datetime->format('F j, Y g:ia')."</td>";
					echo "<td><a href = '#'>Edit Stocks</a></td>";
					echo"</tr>";
				}
				echo "</table>";
			}
		}
	}
?>
</body>
</html>