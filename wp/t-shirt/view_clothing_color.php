<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			if(isset($_GET['clothing_id'])) {
				if(!empty($_GET['clothing_id'])){
					echo "<head>";
					?>
						
						<link rel = 'stylesheet' href = "../../css/popup.css" type = "text/css">
					    <link rel = 'stylesheet' href = "view_clothing.css">
						<link rel="stylesheet" type="text/css" href="../../css/jquery.datetimepicker.css">
						<link rel="stylesheet" type="text/css" href="../../css/loading.css">
						<script type = "text/javascript" src = '../../js/jquery-2.2.3.js'></script>
						<script type = "text/javascript" src = '../../js/loading.js'></script>
						<script type = "text/javascript" src = '../../js/popup_0.js'></script>
						<script type = "text/javascript" src = '../../js/jquery.datetimepicker.full.js'></script>
						<script type = "text/javascript" src = 'view_clothing.js'></script>
						<title>Clothing</title>
					
					<?php
					require_once('../setup/setup_form.php');
					echo "</head>";
					
					echo "<body style = 'font-family: 'Segoe UI';'>";
					require_once('../../php/awp.php');
					require_once('../../php/config.php');
					
					$awp = new Awp();
					$clothing_id = $_GET['clothing_id'];


					if(isset($_GET['start'])){
						
						if(!empty($_GET['start'])){
							if(is_numeric($_GET['start'])){
								$start = intval($_GET['start']);
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}

					if(isset($_GET['add_clothing_color'])){
						$stat = "add_clothing_color";
					}else{
						$stat = "";
					}
					
					if(isset($_GET['search'])){
						if(!empty($_GET['search'])){
							$search = $_GET['search'];
						}else{
							$search = "";
						}
					}else{
						$search = "";
					}

					if(isset($_GET['product_design_id'])){
						if(!empty($_GET['product_design_id'])){
							$product_design_id = $_GET['product_design_id'];
							$def_land = "product.product_id";
							$def_land_stat = "asc";
						}else{
							$product_design_id = 0;
							$def_land = "sci";
							$def_land_stat = "desc";
						}
					}else{
						$def_land = "sci";
						$def_land_stat = "desc";
						$print_method_id = 0;
						$product_design_id = 0;
					}

					if(isset($_GET['pm_id'])){
						if(!empty($_GET['pm_id'])){
							$print_method_id = $_GET['pm_id'];
							$def_land = "sci";
							$def_land_stat = "desc";
						}else{
							$print_method_id = 0;
							$def_land = "sci";
							$def_land_stat = "desc";
						}
					}else{
						$def_land = "sci";
						$def_land_stat = "desc";
						$print_method_id = 0;
					}

					$sort_trans = $arr_sort = array(array("id_asc", "sci", "asc"), array("id_desc", "sci", "desc"), array("color_asc", "clothing_color.color", "asc"), array("color_desc", "clothing_color.color", "desc"), array("stock_asc", "total_color_stock", "asc"), array("stock_desc", "total_color_stock", "desc"));
					foreach ($sort_trans as $st) {
						if(isset($_GET['sort'])){
							if(!empty($_GET['sort'])){
								$ctr = 0;
								if($st[0] == $_GET['sort']){
									$sort = $_GET['sort'];
									$col_order = $st[1];
									$order = $st[2];
									break;
								}else{
									$ctr += 1;
								}
								
								if(count($sort_trans) == $ctr){
									$sort = "";
									$col_order = $def_land_stat;
									$order = $def_land;
								}
							}else{
								$sort = "";
								$col_order = $def_land;
								$order = $def_land_stat;
							}
						}else{
							$sort = "";
							$col_order = $def_land;
							$order = $def_land_stat;
						}
					}

					if(isset($_GET['from_date']) && isset($_GET['to_date']) && !isset($_GET['from_date_sales']) && !isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date']) && !empty($_GET['to_date'])){
							$from_date = $_GET['from_date'];
							$to_date = $_GET['to_date'];
						}else{
							$from_date = "";
							$to_date = "";
						}
					}else{
						$from_date = "";
						$to_date = "";				
					}

					if(!isset($_GET['from_date']) && !isset($_GET['to_date']) && isset($_GET['from_date_sales']) && isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date_sales']) && !empty($_GET['to_date_sales'])){
							$from_date_sales = $_GET['from_date_sales'];
							$to_date_sales = $_GET['to_date_sales'];
						}else{
							$from_date_sales = "";
							$to_date_sales = "";
						}
					}else{
						$from_date_sales = "";
						$to_date_sales = "";				
					}

					$clothing = $awp -> get_clothing($clothing_id, $product_design_id, $print_method_id);
					if(!$clothing){
						echo "<script>window.location.replace('t-shirt');</script>";
					}

					$per_page = 5;
					$arr_const_size = unserialize (SIZE);
					$setup = new SetUp($_SESSION['position']);
					echo "<input type = 'hidden' value = '".$product_design_id."' id = 'productDesignId'>";
					echo "<input type = 'hidden' value = '".$print_method_id."' id = 'pmId'>";
					
					if($product_design_id == 0){
						echo "<ul class = 'topMenu'>";
						echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
						echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
						echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
						echo "<li><a class = 'active' href = 't-shirt/'>Clothing Information</a></li>";
						$count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
		                echo "<li><a href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
						echo "</ul>";
					}
					echo "<h1></h1>";
					echo "<br>";
					echo "<div id ='divForm'>";
					echo "<h3>Clothing: ".$clothing[0]['brand_name']." (".$clothing[0]['type']." ".$clothing[0]['name'].")</h3>";
					if($product_design_id > 0){
						//$product_design = $awp -> get_product_design_name_by_id($product_design_id);
						echo "<h3>";
						echo "Product Design: <b style = 'color:#d73925;'>".$clothing[0]['pd_name']."</b>";
						if($print_method_id > 0){
							echo " (".$clothing[0]['pm_name'].") ";
						}
						echo "</h3>";
						echo "<input type = 'hidden' id = 'pdNameSuffix' value = '".$clothing[0]['name'];
						if($print_method_id > 0)
							echo "(".$clothing[0]['pm_name'].")";
						echo "'>";
					}
					echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					echo "Search by Color: <input type = 'text' id = 'txtSearch' value = '$search'><input type = 'image' id = 'btnSearch' src ='t-shirt/icon/imgSearch.png' value = 'Search'>";


					$arr_sort = array(array("Clothing Color ID (Lowest to Highest)", "id_asc"), array("Clothing Color ID (Highest to Lowest)", "id_desc"), array("Color (A-Z)", "color_asc"), array("Color (Z-A)", "color_desc"));
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: <select id = 'selSort'>";	
					foreach($arr_sort as $s){
						$filter_url = "//www.mixlarts.com/MixlArts/wp/t-shirt/view_clothing_color.php?clothing_id=".$clothing_id."&";
						if($search != ""){
							$filter_url .= "search=$search&";
						}
						if($start !== ""){
							$filter_url .= "start=$start&";
						}
						if($stat != ""){
							$filter_url .= "add_clothing_color&";
						}
						if($product_design_id > 0){
							$filter_url .= "product_design_id=$product_design_id&";
						}
						$filter_url .= "sort=".$s[1];
						echo "<option ";
						if($sort == $s[1]){
							echo "selected";
						}
						echo " value = '".$filter_url."'>".$s[0]."</option>";
					}
					echo "</select>";

					echo "<br/><br/>";

					if($product_design_id > 0 && $print_method_id <= 0){
						echo "<a href = 'shirt_design/add_clothing_assoc_form.php?product_design_id=".$product_design_id."'>Add Associated Color</a> ";	
					}

					

					$record_count = $awp -> view_shirt_color_count($clothing_id, $search, $product_design_id, $print_method_id);
					
					if($search !== ""){
						echo "<label id = 'lblResult'>";
						echo $record_count[0]['count']." result";
						if($record_count[0]['count'] > 1)
							echo "s";
						echo " for Color \"". $search."\"";
						echo "</label>";
					}
					
					
					if($product_design_id == 0){
						echo " <a id = 'btnAddColor' href = 't-shirt/add_clothing_color_form.php?clothing_id=".$clothing_id."'>Add Color</a>";
					}

					echo "<br />";

					echo "<input type = 'hidden' value = '".$clothing[0]['ci']."' id = 'clothingId'>";

					echo "<input type = 'hidden' value = '".$clothing[0]['brand_name']." (".$clothing[0]['type']." ".$clothing[0]['name'].")"."' id = 'clothing'/>";

					echo "<br />";
					
					$arr_shirt_size_desc = $awp -> view_shirt_size_info($clothing_id);

					echo "<br />";

						
					if($arr_shirt_color = $awp -> view_shirt_color($clothing_id, $search, $start, $per_page, $col_order, $order, $product_design_id, $print_method_id)) {

						echo "<div class = 'cont'>";
						echo "<table class = 'color'>";
						echo "<tr><th>ID</th><th>Color</th><th>Clothing Template</th><th>Options</th></tr>";
						for($x = 0; $x < count($arr_shirt_color); $x++){
							
							
							echo "<tr>"; 
							echo "<td>CLOTHCOL-".$arr_shirt_color[$x]['sci']."</td>";
							echo "<td >".$arr_shirt_color[$x]['color']." <input readonly style = 'height:10px;width:10px;background-color:".$arr_shirt_color[$x]['hex']."' /></td>";
							echo "<td>";
							
							$src_front = "";
							$src_back = "";
							if($product_design_id > 0 || $print_method_id > 0){
								$src_front = "../php/clothing_product_design.php?shirt=../assets/shirt_template/".$clothing[0]['brand_name']."/".$clothing[0]['type']."/".$clothing[0]['name']."/".$arr_shirt_color[$x]['hex']."/front.png&design=../assets/shirt_design_template/".$clothing[0]['pd_name']."/front.png";
							}else{		
								$src_front = "../assets/shirt_template/".$clothing[0]['brand_name']."/".$clothing[0]['type']."/".$clothing[0]['name']."/".$arr_shirt_color[$x]['hex']."/front.png";
							}
							if($product_design_id > 0 || $print_method_id > 0){
								$src_back = "../php/clothing_product_design.php?shirt=../assets/shirt_template/".$clothing[0]['brand_name']."/".$clothing[0]['type']."/".$clothing[0]['name']."/".$arr_shirt_color[$x]['hex']."/front.png&design=../assets/shirt_design_template/".$clothing[0]['pd_name']."/back.png";

								if(!file_exists($src_back)){
									$src_back =  "../assets/shirt_template/".$clothing[0]['brand_name']."/".$clothing[0]['type']."/".$clothing[0]['name']."/".$arr_shirt_color[$x]['hex']."/back.png";
								}	
							}else{
								$src_back =  "../assets/shirt_template/".$clothing[0]['brand_name']."/".$clothing[0]['type']."/".$clothing[0]['name']."/".$arr_shirt_color[$x]['hex']."/back.png";
							}
							
							
							echo "<img style = 'height:50px;' src = '$src_front'/>";
							echo "<img style = 'height:50px;' src = '$src_back'/>";

							echo "</td>";
							
							echo"</td>";

							echo "<td>";
							echo "<a class = 'ci' href = '#' color-name = '".$arr_shirt_color[$x]['color']."' color-id = '".$arr_shirt_color[$x]['sci']."' pd-id = '".$product_design_id."' pm-id = '".$print_method_id."'>Clothing Color Information</a>";
							echo "</td>";
						}
						echo "</table>";
						echo "</div>";
					}else{
						echo "Empty Record!";
					}

					if($record_count[0]['count'] > 5){
						$page_url = "";

						if($search != ""){
							$page_url .= "&search=$search";
						}
						if($product_design_id != ""){
							$page_url .= "&product_design_id=$product_design_id";
						}
						if($stat == "add_clothing_color"){
							$page_url .= "&add_clothing_color";
						}

						if($sort != ""){
							$page_url .= "&sort=$sort";
						}

						if($page_url != ""){
							$page_url .= "&";
						}

						$page_url = "view_clothing_form.php?clothing_id=".$clothing[0]['ci'].$page_url."&";

						$prev = $start - $per_page;
						if($start > 0){
							echo "<a href = '".$page_url."start=$prev'>Prev</a>";
						}

						$i = 1;
						for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
							if($start != $x){
								echo " <a href = '".$page_url."start=$x'>$i</a> ";
							}else{
								echo " <a href = '".$page_url."start=$x'><b>$i</b></a> ";
							}
							$i++;
						}

						$next = $start + $per_page;
						if($start < $record_count[0]['count'] - $per_page ){
							echo " <a href = '".$page_url."start=$next'>Next</a>";
						}
					}
					echo "</br></br>";
					echo "</div>";	
					echo "</br>";
		    		echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
		    		echo "<div class='popup-ci'><div style = 'background-color:white;position:absolute;top:5%;width:90%;left:5%;'><h3>Clothing: ".$clothing[0]['brand_name']." (".$clothing[0]['type']." ".$clothing[0]['name'].")</h3><h3> Color: <label id = 'colorInfoTitle'></label></h3>";
		    		if($product_design_id > 0){
		    			echo "<h3> Product Design: <label id = 'pdInfoTitle'></label></h3>";
		    		}
		    		

		    		echo "<a href = '#' class = 'close'>x</a><table id = 'colorInfo' style = ''></table></div></div>";
	
					echo "</body>";
				}else{
					echo "<script>window.location.replace('../t-shirt');</script>";
				}			
			}else{
				echo "<script>window.location.replace('../t-shirt');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}

?>
</html>