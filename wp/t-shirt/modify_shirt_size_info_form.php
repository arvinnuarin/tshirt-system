<html>
	<?php
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
				if(isset($_GET['clothing_id']) && isset($_GET['product_design_id'])){
					if($_GET['clothing_id'] !== "" && $_GET['product_design_id'] !== ""){
						require_once('../../php/awp.php');
						require_once('../../php/config.php');
						$awp = new Awp();
						$clothing_id = $_GET['clothing_id'];
						$product_design_id = $_GET['product_design_id'];
						
						$arr_size = $awp -> get_size_info($clothing_id);
						$clothing = $awp -> get_clothing_by_id($clothing_id);
						if($arr_size && $clothing){

						?>
						<head>
							<title>Clothing</title>
							<link rel = 'stylesheet' href = "modify_shirt_size_info.css">
							<link rel = 'stylesheet' href = "../../css/popup.css">
							<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
							<script type= "text/javascript" src = "modify_shirt_size_info.js"></script>
							<script type = "text/javascript" src = "../../js/sizes.js"></script>
							<script type = "text/javascript" src = "../../js/popup_0.js"></script>
						</head>
						<?php
							echo "<body>";
							require_once('../setup/setup_form.php');
							$setup = new SetUp($_SESSION['position']);
							echo "<ul class = 'topMenu'>";
							echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
							echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
							echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
			                echo "<li><a class='active' href = 't-shirt'>Clothing Information</a></li>";
			                $count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
			                echo "<li><a href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
			                echo "</ul>";
			                
							echo "<h1>Add Size Information for ".$clothing[0]['clothing']."</h1>";
							echo "</br>";

							echo "<input type = 'hidden' value = '".$product_design_id."' id = 'product_design_id'>";
							echo "<input type = 'hidden' value = '".$clothing_id."' id = 'clothing-id'>";
							echo "<input type = 'hidden' value = '".$clothing[0]['clothing']."' id = 'clothing'>";

			            	echo "<div class = 'divSizeInfo'>";

							echo "<h3>Size Information</h3>";
							echo "</br>";
							echo "<table id = 'tblSizeInfo'>";
							echo "<th>Size</th><th>Height</th><th>Width</th><th>Price</th><th>Date Added</th>";
							$size = $arr_const_size = unserialize (SIZE);
							foreach($size as $s){
								echo "<tr>";
								echo "<td>$s</td>";
								$ctr = 0;
								foreach ($arr_size as $as) {
									if($size[$as['size']] == $s){
										echo "<td>".$as['height']."</td>";
										echo "<td>".$as['width']."</td>";
										echo "<td>".$as['price']."</td>";
										$datetime = new DateTime($as['time']);
										echo "<td>".$datetime->format('F j, Y g:ia')."</td>";
								echo "</tr>";

									}else{
										$ctr ++;
									}
								}
								if($ctr == count($arr_size)){
									echo "<td><input style = 'padding:2px; outline:none; width:100px;' type = 'text' id = 'txtHeight$s' kp = '$s'></td>";
									echo "<td><input style = 'padding:2px; outline:none; width:100px;' type = 'text' id = 'txtWidth$s' kp = '$s'></td>";
									echo "<td><input style = 'padding:2px; outline:none; width:100px;' type = 'text' id = 'txtPrice$s' kp = '$s'></td>";
								}
								echo "<td><label id = 'lblWarn$s' ></label></td>";
								echo "</tr>";
							}	
							echo "</table>";
							echo "<label id = 'lblWarnSizeInfo'></label> <br />";

							if(count($arr_size) < 10){
								echo "<input type='button' id = 'btnAddInfo' value = 'Add Size Information'>";
							}
							echo "<br><br>";
							echo "</div>";
							echo "<br>";

							echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";

							echo "</body>";
						}
					}else{
						echo "<script>window.location.replace('view_clothing_form.php');</script>";
					}
				}else{
					echo "<script>window.location.replace('view_clothing_form.php');</script>";
				}
			}else{
				echo "<script>window.location.replace('../../customer/');</script>";
			}
		}else{
			echo "<script>window.location.replace('../../customer/');</script>";
		}			
	?>
</html>

