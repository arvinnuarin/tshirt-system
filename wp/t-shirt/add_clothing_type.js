$(document).ready(function(){
	    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	$("#btnAddCT").click(function(){
		$("#lblWarnCTN").html("");
		if($("#txtCTN").val() === ""){
			$("#lblWarnCTN").html("Empty Clothing Type Name!");
            document.getElementById("txtCTN").style.border = '1px solid #dd4b39';
		}else{
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("t-shirt/add_clothing_type.php",{ct:$("#txtCTN").val(), date:currentDate},function(data){
				//alert(data);
				if(data === "Success"){
					popupOk("Add Clothing Type", "Clothing Type Successfully Added!");
					$('.popup-ok').click(function(){
						window.location.replace("t-shirt/view_clothing_type_form.php");
					});
				}else if(data === "Clothing Type Exist"){
					popupOk("Add Clothing Type", "Clothing Type Already Exist!");
				}else{
					popupOk("Add Clothing Type", "Something went wrong!");
				}
			});
		}
	});
});