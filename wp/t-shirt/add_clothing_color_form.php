<html>
	<?php
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
				if(isset($_GET['clothing_id'])){
					if(!empty($_GET['clothing_id'])){

						require_once("../../php/awp.php");
						require_once("../setup/setup_form.php");
						require_once("../../php/config.php");

						$awp = new Awp();
						$setup = new SetUp($_SESSION['position']);
						$clothing_id = $_GET['clothing_id'];

						if(($arr_size = $awp -> view_available_shirt_size($clothing_id)) && ($clothing = $awp -> get_clothing_by_id($clothing_id))){
							?>
							<head>
								<title>Clothing</title>
								<link rel = 'stylesheet' href = "../css/popup.css" type = "text/css">
								<link rel = "stylesheet" href = '../css/upload_photo.css' type = "text/css">
								<link rel = 'stylesheet' href = "t-shirt/modify_shirt.css" type = "text/css">
								<link id = "colorPickerMod" rel="stylesheet" type="text/css" href="../js/tinyColorPicker-master/demo/mod.css">
								<link rel = 'stylesheet'href = '../css/loading.css' type = "text/css">
								<script type = "text/javascript" src = "../js/jquery-2.2.3.js"></script>		
								<script type="text/javascript" src="../js/tinyColorPicker-master/colors.js"></script>
								<script type="text/javascript" src="../js/tinyColorPicker-master/jqColorPicker.js"></script>
								<script type="text/javascript" src="../js/tinyColorPicker-master/demo/index.js"></script>
								<script type = "text/javascript" src = '../js/cropit-master/dist/jquery.cropit.js'></script>
								<script type = "text/javascript" src = "t-shirt/add_clothing_color.js"></script>
								<script type = "text/javascript" src = "../js/popup_0.js"></script>
								<script type = "text/javascript" src = "../js/loading.js"></script>
							</head>
							<?php
							echo "<body>";
				            echo "<ul class = 'topMenu'>";
							echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
							echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
							echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
			                echo "<li><a class='active' href = 't-shirt'>Clothing Information</a></li>";
			                $count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
			                echo "<li><a href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
			                echo "</ul>";

				            echo "<div class = 'colorInfoMenu'>";
							echo "<h1>Add Color for ".$clothing[0]['clothing']."</h1>";
							echo "<input type = 'hidden' id = 'txtId' value = '$clothing_id' /><br />";
							echo "<input type = 'hidden' id = 'clothing' value = '".$clothing[0]['clothing']."'>";
				            echo "</div>";

				            echo "</div>";
							
				            echo "<div id = 'divColor'><h3>T Shirt Colors</h3>";


				            echo "</br>";
							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Color Name: <input type = 'text' id = 'txtColorName'/>";
							echo "<label id = 'lblWarnColor'></label><br />";
							echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Color Representation: <input readonly class='color' value='#000000' id = 'color'/><br />";
							echo "<label id = 'lblWarnColorHex'></label><br />";

							$faces = array("Front", "Back");
							foreach($faces as $f){
								echo "<div id = 'pic".$f."' class = 'image-editor'>";
															echo "</br>";
							echo "</br>";

								echo "<br><b>".$f."</b>";
								echo "<button class = 'btnClear' value = '".$f."'>Clear</button>";
								echo "<div class = 'cropit-preview' id = 'cp".$f."' style = 'height:550px; width:550px'></div>";
								echo "<div class='fileUpload btn btn-primary'>";
    							echo "<span id ='iconUpload'>&nbsp;&nbsp;Choose a file...</span>";
								echo "<input type = 'file' class='cropit-image-input'/>";
								echo "</div>";
								echo "<br><label id = 'lblWarnTemplateColor".$f."' style = 'margin-left:-15px; font-size:13px; font-weight:400; color:#d73925; margin-left:150px;'></label>";
								echo "</div>";
							}

							echo "<br/><br/><br/><h3>Stocks</h3> <br /><table><th>Size</th><th>Stock</th>";

							$arr_const_size = unserialize (SIZE);

							foreach($arr_size as $as){
								echo "<tr>";
								echo "<td>".$arr_const_size[$as['size']]."<input type = 'hidden' id = 'size".$as['size']."' class = 'size' value = '".$as['size']."'>"."</td><td><input size-id = '".$as['ssd_id']."' id = 'stocks".$as['size']."' style = 'padding:2px; outline:none; width:100px;' class = 'stocks' type = 'text'/></td>";
								echo"</tr>";
							}

							echo "</table><label id = 'lblWarnColorStock'></label>";
							echo "<br><br>";

							echo "<input type='button' value = 'Add Clothing Information' id = 'btnAddInfo'/>";
							echo "<br><br><br>";
							echo "</div>";
							echo "<br>";

							echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
							echo "</body>";
						}else{
							echo "<script>window.location.replace('view_clothing_form.php?clothing_id=".$clothing_id."');</script>";
						}
					}else{
						echo "<script>window.location.replace('view_clothing_form.php');</script>";
					}
				}else{
					echo "<script>window.location.replace('view_clothing_form.php');</script>";
				}
			}else{
				echo "<script>window.location.replace('../../wp/');</script>";
			}
		}else{
			echo "<script>window.location.replace('../../wp/');</script>";
		}	
	?>

</html>