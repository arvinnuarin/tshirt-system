<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			echo "<head>";
			?>
			<title>Critical Stocks</title>

			<link rel="stylesheet" type="text/css" href="critical_stocks.css">
			<link rel="stylesheet" type="text/css" href="../../css/loading.css">
			<link rel="stylesheet" type="text/css" href="../../css/popup.css">
			<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type = "text/javascript" src = "../../js/popup_0.js"></script>
			<script type = "text/javascript" src = "../../js/loading.js"></script>
			<script type = "text/javascript" src = "critical_stocks.js"></script>
			<?php
			require_once('../setup/setup_form.php');
			echo "</head>";
			echo "<body>";
			//$setup = new SetUp($_SESSION['position']);
			require_once("../../php/awp.php");
			require_once("../../php/config.php");
			$arr_const_size = unserialize(SIZE);
			$awp = new Awp();
			$header = array("Brand Name", "Type", "Fabric", "Color", "Size", "Stocks", "Date Stocks Added", "Options");
			if(isset($_GET['start'])){
				if(!empty($_GET['start'])){
					if(is_numeric($_GET['start'])){
						$start = intval($_GET['start']);
					}else{
						$start = 0;
					}
				}else{
					$start = 0;
				}
			}else{
				$start = 0;
			}
			$per_page = 10;
			if(isset($_GET['search']) && isset($_GET['filter'])){
				if($_GET['search'] !== "" && $_GET['filter'] !== ""){
					$search = $_GET['search'];
					$filter_id = $_GET['filter'];
					if($filter_id >= 0 && $filter_id <= 4){
						if($filter_id == 0){
							$filter = "clothing_brand.brand_name";
						}else if($filter_id == 1){
							$filter = "clothing_type.type";
						}else if($filter_id == 2){
							$filter = "clothing_fabric.name";
						}else if($filter_id == 3){
							$filter = "clothing_color.color";
						}else if($filter_id == 4){
							$filter = "clothing_color.size";
						}
					}else{
						$search = "";
						$filter = "clothing_brand.brand_name";
						$filter_id = -1;
					}
				}else{
					$search = "";
					$filter = "clothing_brand.brand_name";
					$filter_id = -1;
				}
			}else{
				$search = "";
				$filter = "clothing_brand.brand_name";
				$filter_id = -1;
			}

			$sort_trans = array(array("brand_asc", "brand_name", "asc"), array("brand_desc", "brand_name", "desc"), array("type_asc", "type", "asc"), array("type_desc", "type", "desc"), array("fabric_asc", "name", "asc"), array("fabric_desc", "name", "desc"), array("color_asc", "color", "asc"), array("color_desc", "color", "desc"), array("size_asc", "size", "asc"), array("size_desc", "size", "desc"), array("stocks_asc", "stock", "asc"), array("stocks_desc", "stock", "desc"), array("date_asc", "time", "asc"), array("date_desc", "time", "desc"));


			foreach ($sort_trans as $st) {
				if(isset($_GET['sort'])){
					if(!empty($_GET['sort'])){
						$ctr = 0;
						if($st[0] == $_GET['sort']){
							$sort = $_GET['sort'];
							$col_order = $st[1];
							$order = $st[2];
							break;
						}else{
							$ctr += 1;
						}
						
						if(count($sort_trans) == $ctr){
							$sort = "";
							$col_order = "clothing_brand.brand_name";
							$order = "asc";
						}
					}else{
						$sort = "";
						$col_order = "clothing_brand.brand_name";
							$order = "asc";
					}
				}else{
					$sort = "";
					$col_order = "clothing_brand.brand_name";
					$order = "asc";
				}
			}

			$arr_sort = array(array("Brand Name (A-Z)", "brand_asc"), array("Brand Name (Z-A)", "brand_desc"), array("Type (A-Z)", "type_asc"), array("Type (Z-A)", "type_desc"), array("Fabric (A-Z)", "fabric_asc"), array("Fabric (Z-A)", "fabric_desc"), array("Color (A-Z)", "color_asc"), array("Color (Z-A)", "color_desc"), array("Size (Ascending)", "size_asc"), array("Size (Descending)", "size_desc"), array("Stocks (Lowest to Highest)", "stocks_asc"), array("Stocks (Highest to Lowest)", "stocks_desc"), array("Date (Lowest to Highest)", "date_asc"), array("Date (Highest to Lowest)", "date_desc"));
			

			echo "<ul class = 'topMenu'>";
			echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
			echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
			echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
            echo "<li><a href = 't-shirt'>Clothing Information</a></li>";
            $count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
		    echo "<li><a class='active' href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
            echo "</ul>";

			echo "<h1>Clothing Critical Stocks</h1>";
			echo "<div id ='divCriticalStocks'>";
			echo "<div id = 'divSearch'>";
			echo "<label id = 'lblSearch'>Search via </label>" ;
			$select = array("Brand", "Type", "Fabric", "Color", "Size");
			echo "<select id = 'selSearch'>";
			for($x = 0; $x < count($select); $x++) {
				echo "<option value = '$x'"; 
				if($x == $filter_id){
					echo " selected ";
				}
				echo ">".$select[$x]."</option>";
			}
			echo "</select> ";
			echo "<input type = 'text' id = 'txtSearch' value = '$search'/> <input type = 'image' value = 'Search' src ='t-shirt/icon/imgSearch.png'/id = 'btnSearch'>";
			echo "</div>";
			echo "<div id = 'divSort'>";
			echo " Sort: <select id = 'selSort'>";	
			foreach($arr_sort as $s){
				$filter_url = "t-shirt/critical_stocks_form.php?";
				if($search != ""){
					$filter_url .= "search=$search&";
				}
				if($start !== ""){
					$filter_url .= "start=$start&";
				}
				$filter_url .= "sort=".$s[1];
				echo "<option ";
				if($sort == $s[1]){
					echo "selected";
				}
				echo " value = '".$filter_url."'>".$s[0]."</option>";
			}
			echo "</select>";

			$critical_stocks = $awp -> critical_stocks($start, $per_page, $col_order, $order,  $filter, $search);
			$record_count = $awp -> critical_stocks_count($filter, $search);

			if($search !== ""){
				echo "<label id = 'lblResult'>".$record_count[0]['count']."&nbsp;result/s for \"".$search."\"</label>";
			}
			echo "</div>";
			echo "<table>";
			foreach ($header as $h) {
				echo "<th>$h</th>";
			}
			foreach ($critical_stocks as $cs) {
				echo "<tr clothingId = '".$cs['clothing_id']."'' stock-id = '".$cs['scs_id']."'>";
				echo "<td class = 'brand'>".$cs['brand_name']."</td>";
				echo "<td class = 'type'>".$cs['type']."</td>";
				echo "<td class = 'fabric'>".$cs['fabric']."</td>";
				echo "<td>".$cs['color']."</td>";
				echo "<td><input type='hidden' class = 'size' value = '".$cs['size']."'>".$arr_const_size[$cs['size']]."</td>";
				echo "<td class = 'slot-stock' size-clone = '".$cs['stock']."'>".$cs['stock']."</td>";
				$datetime = new DateTime($cs['time']);
				echo "<td class = 'slot-date'>".$datetime->format('F j, Y g:ia')."</td>";
				echo "<td><a href = '#' class = 'updateStocks' btn-name = 'update' color-id = '".$cs['sc_id']."'>Update Stocks</a> <a href = '#' class = 'cancel-stock' style ='visibility:hidden;'>Cancel</a></td>";
				echo "</tr>";
			}
			echo "</table>";

			if($record_count[0]['count'] > 10){
				$page_url = "";

				if($search != ""){
					$page_url .= "&search=$search";
				}

				if($sort != ""){
					$page_url .= "&sort=$sort";
				}

				if($page_url != ""){
					$page_url .= "&";
				}

				$page_url = "t-shirt/critical_stocks_form.php?".$page_url."&";
		        
				echo "<div id = 'btnPagination'>";
				$prev = $start - $per_page;
				if($start > 0){
					echo "<a class = 'lblPagination' href = '".$page_url."start=$prev'>Prev</a>";
				}
				$i = 1;
				for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
					if($start != $x){
						echo " <a class = 'lblPagination' href = '".$page_url."start=$x'>$i</a> ";
					}else{
						echo " <a style='background-color: #dd4b39; color:white;'class = 'lblPagination' href = '".$page_url."start=$x'><b>$i</b></a> ";					}
					$i++;
				}

				$next = $start + $per_page;
				if($start < $record_count[0]['count'] - $per_page ){
					echo " <a href = '".$page_url."start=$next'>Next</a>";
				}
			}
			echo "</div>";
			echo "</div>";
			echo "</br></br></br>";
			echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
			echo "</body>";
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}
?>