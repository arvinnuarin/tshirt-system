<html>
	<?php
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])) {
				require_once('../../php/awp.php');
				require_once('../../php/config.php');
				$awp = new Awp();
				echo "<head>";
				?>      
					<title>Clothing Information</title>
					<link rel = 'stylesheet' href = "../../css/popup.css">
					<link rel = "stylesheet" href = '../../css/upload_photo.css' type = "text/css">
					<link rel = 'stylesheet' href = "modify_shirt.css">
					<link id="colorPickerMod" rel="stylesheet" type="text/css" href="../../js/tinyColorPicker-master/demo/mod.css">
					<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>		
					<script type="text/javascript" src="../../js/tinyColorPicker-master/colors.js"></script>
					<script type="text/javascript" src="../../js/tinyColorPicker-master/jqColorPicker.js"></script>
					<script type="text/javascript" src="../../js/tinyColorPicker-master/demo/index.js"></script>
					<script type = "text/javascript" src = '../../js/cropit-master/dist/jquery.cropit.js'></script>    
					<script type = "text/javascript" src = "add_clothing.js"></script>
					<script type = "text/javascript" src = "../../js/popup_0.js"></script>
					<script type = "text/javascript" src = "../../js/sizes.js"></script>
				
	            <?php
	            require_once('../setup/setup_form.php');
	            echo "</head>";
	            echo "<body>";
	            $setup = new SetUp($_SESSION['position']);
	            echo "<ul class = 'topMenu'>";
	            echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
	            echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
	            echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
	            echo "<li><a class = 'active'href = 't-shirt/index.php'>Clothing Information</a></li>";
	            echo "</ul>";

				$clothing_brand = $awp -> get_all_clothing_brand();
				$clothing_type = $awp -> get_all_clothing_type();
				$clothing_fabric = $awp -> get_all_clothing_fabric();
				echo "<h1>Add Clothing Information</h1>";
				echo "<br>";
	            echo "<div id = 'divColorMenu'>";
	            echo "<h3>Clothing Information</h3><br />";

				echo " &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand Name: ";
				echo "<select id = 'selBrand'><option value = ''>Select Brand</option>";
				foreach($clothing_brand as $cb) {
					echo "<option value = '".$cb['cb_id']."'>".$cb['brand_name']."</option>";
				}
				echo "</select>";
				echo "<label id = 'lblWarnBrand'></label>";

				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;&nbsp;Type: ";
				echo "<select id = 'selType'><option value = ''>Select Type</option>";
				foreach ($clothing_type as $ct) {
					echo "<option value = '".$ct['ct_id']."'>".$ct['type']."</option>";
				}
				echo "</select><br />";
				echo "<label id = 'lblWarnType'></label><br />";

				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fabric: ";
				echo "<select id = 'selFabric'><option value = ''>Select Fabric</option>";
				foreach ($clothing_fabric as $cf) {
					echo "<option value = '".$cf['cf_id']."'>".$cf['name']."</option>";
				}
				echo "</select>";
				echo "<label id = 'lblWarnFabric'></label>";
				echo "<br><br>";
				echo "<h3>Size Information</h3>";
				echo "<br>";
				echo "<label id = 'lblSizes'>\"*\" Required Sizes </label>";
				echo "<table id = 'tblSizeInfo'>";
				echo "<th>Size</th>";
	            echo "<th>Height</th>";
	            echo "<th>Width</th";
	            echo "><th>Price</th>";
	            
				$size = unserialize (SIZE);

				$ctr = 0;
				foreach($size as $s){
					echo "<tr>";
					echo "<td>";
					if($ctr <= 4)
						echo " * ";
					echo "$s</td>";
					echo "<td><input style = 'padding:2px; outline:none; width:100px;' type = 'text' id = 'txtHeight".$s."'></td>";
					echo "<td><input style = 'padding:2px; outline:none; width:100px;' type = 'text' id = 'txtWidth".$s."'></td>";
					echo "<td><input style = 'padding:2px; outline:none; width:100px;' type = 'text' id = 'txtPrice".$s."'></td>";
					echo "<td><label id = 'lblWarn$s'></label></td>";
					echo "</tr>";
					$ctr += 1;
				}
				echo "</table>";
				echo "<label id = 'lblWarnSizeInfo'></label>";

				echo "<br />";
				echo "<br />";

				echo "<input type='button' value = 'Add T-Shirt Information' id = 'btnAddInfo'/>";


	            echo "</div>";

				echo "<br>";
				echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
				echo "</body>";	
			} else{
				session_destroy();
				echo "<script>window.location.replace('../../wp/');</script>";
			}
		} else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}	
	?>
</html>