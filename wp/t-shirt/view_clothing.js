$(document).ready(function(){
    if($("#productDesignId").val() > 0){
    	$(".tabTshirtDesign").css("background-color", "#2c3b41");
	    $(".tabTshirtDesign").css("color","white");
	    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
    }else{
    	$(".tabTshirt").css("background-color", "#2c3b41");
	    $(".tabTshirt").css("color","white");
	    $(".tabTshirt").css("border-left","2px solid #e04545");
    }
	

	$("#btnSearch").click(function(){
		if($("#txtSearch").val() !== ""){
			window.location.href = "t-shirt/view_clothing_color.php?clothing_id="+$("#clothingId").val()+"&search="+$("#txtSearch").val()+"&product_design_id="+$("#productDesignId").val();
		}
	});	

	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	
    var date = new Date();
    $("#fromdate").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#salesfrom").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});


	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	var date = new Date();
	$("#todate").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#fromdate").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$("#salesfrom").on("change", function(){
		var date = new Date();
    	$("#salesto").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#salesfrom").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$('#btnFilterSales').click(function(){
		if($("#salesfrom").val() != "" || $("#salesto").val() != ""){
			if($("#salesto").val() >= $("#salesfrom").val()){
				window.location.href = "view_clothing_form.php?clothing_id="+$("#clothingId").val()+"&search="+$("#txtSearch").val()+"&product_design_id="+$("#productDesignId").val()+"&start=0&from_date_sales="+$("#salesfrom").val()+"&to_date_sales="+$("#salesto").val()+"&pm_id="+$("#pmId").val(); 

			}
		}
	});

	$('#btnFilter').click(function(){
		if($("#fromdate").val() != "" || $("#todate").val() != ""){
			if($("#todate").val() >= $("#fromdate").val()){
				window.location.href = "view_clothing_form.php?clothing_id="+$("#clothingId").val()+"&search="+$("#txtSearch").val()+"&product_design_id="+$("#productDesignId").val()+"&from_date="+$("#fromdate").val()+"&to_date="+$("#todate").val()+"&pm_id="+$("#pmId").val(); 
			}
		}
	});


	function updatePrice(e){
		var tr = $(this).parent().parent();
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var stocksVal = tr.find('.slot-price').html();
		var prevStocks = tr.find(".price").attr('def-val');
		var clone = tr.find('.slot-price').attr('price-clone');

		if($(this).attr('btn-name') == "update"){

			$(this).next().css('visibility', 'visible');
			$(this).next().click(function(e){
				tr.css("background-color", "white");
				tr.find(".price").css("visibility", "hidden");
				tr.find('.slot-price').html(stocksVal);
				$(this).prev().attr('btn-name', 'update');
				$(this).prev().html("Update Price");
				$(this).css("visibility", "hidden");
				e.preventDefault();
			});
			$(this).attr('btn-name', 'proceed');
			$(this).html('Proceed');
			tr.css("background-color", "#fbd89b");
			tr.find('.slot-price').html("<input class = 'price' type = 'number' def-val = '"+clone+"' value = '"+clone+"'/>");
			$(".price").on("input propertychange", function(){
				$(this).val($(this).val().replace(/[^0-9\.]/g,''));
			});
		}else if($(this).attr('btn-name') == "proceed"){
			var price = tr.find(".price").val();
			var priceId = tr.attr('price-id');
			
			if(clone != price){
				if(!price || !priceId){
					popupOk("Clothing Stocks", "Something went wrong!");
				}else if(price != "" && priceId != ""){
					loading();
					$.post("//www.mixlarts.com/MixlArts/wp/t-shirt/view_clothing.php", {do:"update_price", price:price, priceId:priceId, clothing:$("#clothing").val(), size:tr.find(".size").html(), date:currentDate}, function(data){
						if(data){
							$(".back").remove();
							if(data == "Success"){
								tr.find('.slot-price').html("Php "+eval(price).toFixed(2));
								tr.find('.btn-manipulator').find(".updateStocks").html("Update Stocks");
								popupOk("Clothing Size", "Price Successfully Updated!");
							}else if(data == "Fail to update price"){
								tr.find('.slot-price').html("Php "+prevStocks);
								popupOk("Clothing Size", "Price Failed to Update!");
							}else if(data == "Fail to add update price log"){
								tr.find('.slot-price').html("Php "+prevStocks);
								popupOk("Clothing Size", "Fail to Update Price Logs!");
							}						
						}
						alert(data);
					});
				}else{
					$(this).next().css("visibility", "hidden");
					tr.find('.slot-price').html("Php "+eval(prevStocks).toFixed(2));
				}
			}else{
				$(this).next().css("visibility", "hidden");
				tr.find('.slot-price').html("Php "+eval(prevStocks).toFixed(2));
			}

			tr.css("background-color", "white");
			tr.find(".price").css("visibility", "hidden");
			tr.find(".price").val("");
			$(this).attr('btn-name', 'update');
			$(this).html("Update Price");
			$(this).next().css("visibility", "hidden");
		}
		e.preventDefault();
	}


	function updateStocks(e){
		var tr = $(this).parent().parent();
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var stocksVal = tr.find('.slot-stock').html();
		var prevStocks = tr.find(".stocks").attr('def-val');
		if($(this).attr('btn-name') == "update"){
			$(this).next().css('visibility', 'visible');
			$(this).next().click(function(e){
				tr.css("background-color", "white");
				tr.find(".stocks").css("visibility", "hidden");
				tr.find('.slot-stock').html(stocksVal);
				$(this).prev().attr('btn-name', 'update');
				$(this).prev().html("Update Stocks");
				$(this).css("visibility", "hidden");
				e.preventDefault();
			});
			$(this).attr('btn-name', 'proceed');
			$(this).html('Proceed');
			tr.css("background-color", "#fbd89b");
			tr.find('.slot-stock').html("<input class = 'stocks' type = 'number' def-val = '"+stocksVal+"' value = '"+stocksVal+"'/>");
			$(".stocks").on("input propertychange", function(){
				$(this).val($(this).val().replace(/[^0-9]/g,''));
			});
		}else if($(this).attr('btn-name') == "proceed"){
			var clothing = $("#clothing").val();
			var size = tr.find(".slot-stock").attr('size-clone');
			var stocks = tr.find(".stocks").val();
			var stockId = tr.attr('stock-id');
			var colorId = $(this).attr('color-id');
			
			if(prevStocks != stocks){
				if(!size || !stockId || !clothing){
					popupOk("Clothing Stock", "Something went wrong!");
				}else if(size != "" && stocks != "" && stockId != "" && clothing != ""){
					loading();
					$.post("//www.mixlarts.com/MixlArts/wp/t-shirt/view_clothing.php", {do:"update", stocks:stocks, stockId:stockId, size:size, clothing:clothing, date:currentDate, clothingId:$("#clothingId").val() ,colorId:colorId, prodId: $("#productDesignId").val(), pmId:$("#pmId").val()}, function(data){
						if(data){
							$(".back").remove();
							if(data.indexOf("Success") != -1){
								tr.find('.slot-stock').html(stocks);
								tr.find('.btn-manipulator').find(".updateStocks").html("Update Stocks");
								var x = data.split("#");
								tr.find('.slot-date').html(x[1]);
								popupOk("Clothing Stock", "Stocks Successfully Updated!");
							}else{
								tr.find('.slot-stock').html(prevStocks);
								popupOk("Clothing Stock", "Stocks Failed to Update!");
							}					
						}	
					});
				}else{
					$(this).next().css("visibility", "hidden");
					tr.find('.slot-stock').html(prevStocks);
				}
			}else{
				$(this).next().css("visibility", "hidden");
				tr.find('.slot-stock').html(prevStocks);
			}

			tr.css("background-color", "white");
			tr.find(".stocks").css("visibility", "hidden");
			tr.find(".stocks").val("");
			$(this).attr('btn-name', 'update');
			$(this).html("Update Stocks");
			$(this).next().css("visibility", "hidden");
		}
		e.preventDefault();
	}



	function addStocks(e){
		var cont = $(this).parent().parent().parent().parent().parent().find(".color").children().children().find(".sub_stock");
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var tr = $(this).parent().parent();
		var prevStat = $(this).parent().parent().html();
		if($(this).attr('btn-name') == "add"){
			tr.find(".lblNA").css("display", "none");
			tr.find(".stocks").css("display", "inline");
			$(this).html("Proceed");
			$(this).attr('btn-name', 'proceed');
			$(this).next().css('visibility', 'visible');
			$(this).next().click(function(e){
				tr.css("background-color", "white");
				tr.find(".stocks").css("display", "none");
				tr.find(".stocks").val("");
				tr.find(".lblNA").html("N/A");
				$(this).prev().attr('btn-name', 'add');
				$(this).prev().html("Add Stocks");
				$(this).css("visibility", "hidden");
				tr.find(".lblNA").css("display", "inline");
				tr.find(".stocks").css("display", "none");
				
				e.preventDefault();
			});
			tr.css("background-color", "#fbd89b");
			tr.find(".stocks").css("visibility", "visible");
			tr.find(".stocks").css("width", "");
			tr.find(".lblNA").css("display", "none");
			
		}else if($(this).attr('btn-name') == "proceed"){
			var clothing = $("#clothing").val();
			var size = tr.find(".slot-stock").attr('size-clone');
			var stocks = tr.find(".stocks").val();
			var sizeId = tr.find(".stocks").attr('size-id');
			var color = tr.find(".stocks").attr('color-id');

			if(!sizeId || !color || !size || !clothing){
				popupOk("Clothing Stocks", "Something went wrong!");
				$(".popup-ok").click(function(){
					window.location.replace("");
				});
			}else if(size != "" && color != "" && stocks != "" && clothing != ""){
				loading();
				$.post("//www.mixlarts.com/MixlArts/wp/t-shirt/view_clothing.php", {do:"add", stocks:stocks, size:size, clothing:clothing, sizeId:sizeId, colorId:color, clothingId:$("#clothingId").val(), date:currentDate, prodId: $("#productDesignId").val(), pmId:$("#pmId").val()}, function(data){
					if(data){
						$(".back").remove();
						var x = data.split("#"); 
						if(x[0] == "Success"){
							tr.find('.slot-stock').html(stocks);
							tr.find('.slot-date').html(x[1]);
							tr.attr('stock-id', x[2]);
							tr.find('.btn-manipulator').html("<a href = '#' color-id = '"+color+"' class = 'updateStocks' btn-name = 'update' >Update Stocks</a>  <a href = '#' class = 'cancel-stock' style ='visibility:hidden;'>Cancel</a>");
							tr.find('.btn-manipulator').children().click(updateStocks);
							popupOk("Clothing Stocks", "Stocks Successfully Added!");
						}else{
							popupOk("Clothing Stocks", "Stocks failed to Add!");
							$(".popup-ok").click(function(){
								window.location.replace("");
							});
						}						
					}
				});
			}else{
				$(this).next().css("visibility", "hidden");
			}
			if(tr.find(".lblNA").html() === "N/A"){
				tr.find(".lblNA").css("display", "inline");
				tr.find(".stocks").css("display", "none");
			}
			tr.css("background-color", "white");
			tr.find(".stocks").css("visibility", "hidden");
			$(this).attr('btn-name', 'add');
			$(this).html("Add Stocks");
		}
		e.preventDefault();
	}

	$(".updatePrice").click(updatePrice);

	$(".ci").click(function(e){
		loading();
		var colorName = $(this).attr('color-name');
		$.post("//www.mixlarts.com/MixlArts/wp/t-shirt/view_clothing.php",{do:"get_color_info",colorId:$(this).attr('color-id'), pdId:$(this).attr('pd-id'), pmId:$(this).attr('pm-id')}, function(data){
			if(data){
				$(".back").remove();
				$("#colorInfo").html("<th>Size</th><th>Current Stocks</th><th>Date Stocks Added</th><th>Options</th>").append($.parseHTML(data));
				$(".updateStocks").click(updateStocks);
				$(".addStocks").click(addStocks);
				$("#colorInfoTitle").html(colorName);
				$("#pdInfoTitle").html($("#pdNameSuffix").val());
				$(".popup-ci").fadeIn(350);
			}
		});
		e.preventDefault();
	});

	$(".close").click(function(e){
		$(".popup-ci").fadeOut(350);
		e.preventDefault();
	});

});