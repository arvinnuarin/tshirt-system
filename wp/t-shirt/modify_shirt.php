<?php
	session_start();
	require_once('../../php/awp.php');
	$awp = new Awp();
	$employee_id = $_SESSION['id'];
	$brand = $_POST['brand'];
	$type = $_POST['type'];
	$fabric = $_POST['fabric'];
	$size_filled = $_POST['sizeFilled'];
	//$size_info = $_POST['sizeInfo'];
	$color_name = $_POST['colorName'];
	$color_hex = $_POST['colorHex'];
	$stocks = $_POST['stocks'];
	$date = $_POST['date'];
	$image_data = $_POST['imageData'];
	$image_name = $_POST['imageName'];
	$side = array("Front", "Back", "Left", "Right");


	//print_r($stocks);
	//print_r($size_filled);
	//print_r($total_color_stocks);

	if(!isset($_POST['prodId']) && isset($_POST['sizeInfo'])){
		if($awp -> is_brand_exist($brand, $type, $fabric)){
			echo "Brand Information Already Exist";
		}else{
			if(!empty($_POST['sizeInfo'])){
				$size_info = $_POST['sizeInfo'];
				//print_r($size_info);
				//print_r($awp -> add_shirt($brand, $type, $fabric, $thickness, $size_filled, $size_info, $color_name, $color_hex, $image_name, $stocks, $date, $employee_id, $side));
				if($awp -> add_shirt($brand, $type, $fabric, $size_filled, $size_info, $color_name, $color_hex, $image_name, $stocks, $date, $employee_id, $side)){
					
					for($x = 0; $x < count($image_data); $x++){
						for($y = 0; $y < 4; $y++){
							list($type, $data) = explode(';', $image_data[$x][$y]);
							list(, $data)      = explode(',', $data);
							$data = base64_decode($data);
							file_put_contents("../../".$image_name[$x][$y], $data);
						}
					}

					echo "Success";
				}else{
					echo "Add Shirt Query Failed!";
				}
			}
		}
	}else if(isset($_POST['prodId'])){
		//echo $_POST['prodId'];
		if(!empty($_POST['prodId'])){
			$prod_id = $_POST['prodId'];
			//echo $awp -> is_color_exist($color_name, $prod_id);
			if($awp -> is_color_name_exist($color_name, $prod_id)){
				echo "Color Name Exist!";
			}if($awp -> is_color_hex_exist($color_hex, $prod_id)){
				echo "Color Hex Exist!";
			}
			else if($awp -> add_shirt_color_info($prod_id, $color_name, $color_hex, $image_name, $stocks, $date, $size_filled, $employee_id, $side, $brand)){
				
				for($x = 0; $x < count($image_data); $x++){
					for($y = 0; $y < 4; $y++){
						list($type, $data) = explode(';', $image_data[$x][$y]);
						list(, $data)      = explode(',', $data);
						$data = base64_decode($data);
						file_put_contents("../../".$image_name[$x][$y], $data);
					}
				}

				echo "Success";
			}
		}
	}

	
	//print_r(count($image_data));



?>