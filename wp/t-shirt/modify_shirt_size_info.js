$(document).ready(function(){
    $(".tabTshirt").css("background-color", "#2c3b41");
    $(".tabTshirt").css("color","white");
    $(".tabTshirt").css("border-left","2px solid #e04545");
	for(var x = 0; x < arrSize.length; x++){
		$("#txtHeight"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		});
		$("#txtWidth"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		});
		$("#txtPrice"+arrSize[x]).on("input propertychange", function(){
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		});
	}

	$("#btnAddInfo").click(function(){
		$("#lblWarnSizeInfo").html("");
		var arrSizesInfo = [];
		for(var x = 0; x < arrSize.length; x ++){
			$("#lblWarn"+arrSize[x]).html("");
			if($("#txtHeight"+arrSize[x]).length > 0 && $("#txtWidth"+arrSize[x]).length  > 0 && $("#txtPrice"+arrSize[x]).length > 0){
				if($("#txtHeight"+arrSize[x]).val() != "" || $("#txtWidth"+arrSize[x]).val() != "" || $("#txtPrice"+arrSize[x]).val() != ""){
					var warn = "";
					if($("#txtHeight"+arrSize[x]).val() == ""){
						warn += "Height ";
					}
					if($("#txtWidth"+arrSize[x]).val() == ""){
						warn += "Width ";
					}
					if($("#txtPrice"+arrSize[x]).val() == ""){
						warn += "Price ";
					}
					if(warn != ""){
						warn = "Empty "+$.trim(warn).replace(/[ ,]+/g, ", ");
						$("#lblWarn"+arrSize[x]).html(warn);
					}else{
						var subSizesInfo = [x, $("#txtHeight"+arrSize[x]).val(), $("#txtWidth"+arrSize[x]).val(), $("#txtPrice"+arrSize[x]).val()];
						arrSizesInfo.push(subSizesInfo);
					}
				}	
			}
		}
		
		if(arrSizesInfo.length == 0){
			$("#lblWarnSizeInfo").html("Please Add at least one Size Information!");
		}else{
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("t-shirt/modify_shirt_size_info.php", {id:$("#clothing-id").val(), brandName: $("#clothing").val(), info:arrSizesInfo, date:currentDate}, function(data){
				if(data == "Success"){
					popupOk("Size Information", "Successfully Added!");
					$('.popup-ok').click(function(){
						window.location.replace("t-shirt/view_clothing_size_desc.php?clothing_id="+$("#clothing-id").val());
					});
				}else{
					popupOk("Error", "Something Went Wrong!");
				}
			});
		}
	});

});