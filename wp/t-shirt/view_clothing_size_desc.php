<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			if(isset($_GET['clothing_id'])) {
				if(!empty($_GET['clothing_id'])){

					echo "<head>";
					?>
						
						<link rel = 'stylesheet' href = "../../css/popup.css" type = "text/css">
					    <link rel = 'stylesheet' href = "view_clothing.css">
						<link rel="stylesheet" type="text/css" href="../../css/jquery.datetimepicker.css">
						<link rel="stylesheet" type="text/css" href="../../css/loading.css">
						<script type = "text/javascript" src = '../../js/jquery-2.2.3.js'></script>
						<script type = "text/javascript" src = '../../js/loading.js'></script>
						<script type = "text/javascript" src = '../../js/popup_0.js'></script>
						<script type = "text/javascript" src = '../../js/jquery.datetimepicker.full.js'></script>
						<script type = "text/javascript" src = 'view_clothing.js'></script>
						<title>Clothing</title>
					
					<?php
					require_once('../setup/setup_form.php');
					echo "</head>";
					
					echo "<body style = 'font-family: 'Segoe UI';'>";
					require_once('../../php/awp.php');
					require_once('../../php/config.php');
					
					$awp = new Awp();
					$clothing_id = $_GET['clothing_id'];


					if(isset($_GET['start'])){
						
						if(!empty($_GET['start'])){
							if(is_numeric($_GET['start'])){
								$start = intval($_GET['start']);
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}

					if(isset($_GET['add_clothing_color'])){
						$stat = "add_clothing_color";
					}else{
						$stat = "";
					}
					

					if(isset($_GET['product_design_id'])){
						if(!empty($_GET['product_design_id'])){
							$product_design_id = $_GET['product_design_id'];
							$def_land = "product.product_id";
							$def_land_stat = "asc";
						}else{
							$product_design_id = 0;
							$def_land = "sci";
							$def_land_stat = "desc";
						}
					}else{
						$def_land = "sci";
						$def_land_stat = "desc";
						$print_method_id = 0;
						$product_design_id = 0;
					}

					if(isset($_GET['pm_id'])){
						if(!empty($_GET['pm_id'])){
							$print_method_id = $_GET['pm_id'];
							$def_land = "product.product_id";
							$def_land_stat = "asc";
						}else{
							$print_method_id = 0;
							$def_land = "sci";
							$def_land_stat = "desc";
						}
					}else{
						$def_land = "sci";
						$def_land_stat = "desc";
						$print_method_id = 0;
					}

					$arr_const_size = unserialize (SIZE);
					$clothing = $awp -> get_clothing($clothing_id, $product_design_id, $print_method_id);
					if(!$clothing){
						echo "<script>window.location.replace('t-shirt');</script>";
					}

					//$setup = new SetUp($_SESSION['position']);
					if($product_design_id == 0){
						echo "<ul class = 'topMenu'>";
						echo "<li><a href = 't-shirt/view_clothing_type_form.php'>Clothing Type</a></li>";
						echo "<li><a href = 't-shirt/view_clothing_fabric_form.php'>Clothing Fabric</a></li>";
						echo "<li><a href = 't-shirt/view_clothing_brand_form.php'>Clothing Brand</a></li>";
						echo "<li><a class = 'active' href = 't-shirt/'>Clothing Information</a></li>";
						$count = $awp -> critical_stocks_count("clothing_brand.brand_name", "");
		                echo "<li><a href = 't-shirt/critical_stocks_form.php'>Critical Stocks <label style = 'color:#d73925;position:relative;top:5px;left:5px;'><strong>".$count[0]['count']."</strong></label></a></li>";
						echo "</ul>";
					}

					echo "<input type = 'hidden' value = '".$clothing[0]['brand_name']." (".$clothing[0]['type']." ".$clothing[0]['name'].")"."' id = 'clothing'/>";
					if($product_design_id == 0){
						echo " <a href = 't-shirt/modify_shirt_size_info_form.php?clothing_id=".$clothing_id."&product_design_id=$product_design_id'>Add Size</a>";
						
					}
					echo "<br>";
					echo "<div id ='divForm'>";
					
					echo "<h3>Clothing: ".$clothing[0]['brand_name']." (".$clothing[0]['type']." ".$clothing[0]['name'].")</h3>";
					if($product_design_id > 0){
						if($clothing[0]['pd_name']){
							echo "<h3>Product Design: <b style = 'color:#d73925;'> ".$clothing[0]['pd_name']."</b>";
							if(isset($clothing[0]['pm_name'])){
								echo " (".$clothing[0]['pm_name'].")";
							}
							echo "</h3>";							
						}
					}
		

					$arr_shirt_size_desc = $awp -> view_shirt_size_info($clothing_id);
				
					if(!count($arr_shirt_size_desc)){
						echo "No Sizes to show";
					}
					$design_price = $awp -> get_design_price($product_design_id, $print_method_id);
						echo "<table>"; 
						
						echo "<th>Size</td><th>Height</th><th>Width</th>";

						echo "<th>Clothing Price</th>";
						if($product_design_id > 0 && $print_method_id > 0 && $design_price)
							echo "<th>Clothing Price w/ Design</th>";
						
						else if($product_design_id <= 0 && $clothing_id > 0 && $print_method_id <= 0)
							echo "<th>Options</th>";

						foreach ($arr_shirt_size_desc as $assd) {
							if($assd['size'] == 4){
								$default_price = $assd['price'];
							}
						}

						//$design_price = $awp -> get_design_price($product_design_id, $print_method_id);

						foreach ($arr_shirt_size_desc as $assd) {
							echo "<tr price-id = '".$assd['ssd_id']."''>";
							echo "<td class = 'size'>".$arr_const_size[$assd['size']]."</td>";
							echo "<td>".$assd['height']."</td>";
							echo "<td>".$assd['width']."</td>";
							echo "<td class = 'slot-price' price-clone = '".$assd['price']."'>Php ".number_format($assd['price'], 2)."</td>";
							if($product_design_id > 0 && $print_method_id > 0){
								if($design_price){
									if($assd['size'] >= 0 && $assd['size'] <= 4)
										$final_price = ($default_price+$design_price[0]['default_price']); 
									else
										$final_price = ($assd['price']+$design_price[0]['default_price']+(($assd['size']+1)*$design_price[0]['price_increment']));	
									echo "<td>Php ".number_format($final_price, 2)."</td>";
								}else{
									echo "<td><a href = '#' class = 'updatePrice' btn-name = 'update' price-id ='".$assd['ssd_id']."'>Update Price</a> <a href = '#' class = 'cancel-stock' style ='visibility:hidden;'>Cancel</a></td>";
								}
								
							}else if($product_design_id == 0){
								echo "<td><a href = '#' class = 'updatePrice' btn-name = 'update' price-id ='".$assd['ssd_id']."'>Update Price</a> <a href = '#' class = 'cancel-stock' style ='visibility:hidden;'>Cancel</a></td>";
							}
							echo "</tr>";
						}	
						echo "</table>";

					echo "</div>";	
					echo "</br>";
		    		echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
	
					echo "</body>";
				}else{
					echo "<script>window.location.replace('t-shirt');</script>";
				}			
			}else{
				echo "<script>window.location.replace('t-shirt');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}

?>
</html>