<html>
<head>
    <link rel = 'stylesheet' href = "edit_product.css">
	<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "edit_product.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
	<title></title>
</head>
<body>
<?php
	if(isset($_GET['prodId'])){
		if(!empty($_GET['prodId'])){
			
			require_once('../../php/awp.php');
			$awp = new Awp();
			
			if($awp -> is_product_exist("id", $_GET['prodId'])){
				session_start();
				require_once('../setup/setup_form.php');
				$setup = new SetUp($_SESSION['position']);
				$prod_id = $_GET['prodId'];
				$details = $awp -> view_one_product_design($prod_id);
				echo "<h1>Edit Product</h1>";
                
                echo "</br><div id = 'divEditProduct'>";
                echo "<label id = 'lblEditProd'>Edit Product</label></br></br></br>";
                echo "<div id = 'divline'></div>";
                echo "<label id = 'pdId1'>ID:</label>";
				echo "<label id = 'pdId'>PROD - ".$details[0]['pd_id']."</label></br>";
                echo "</br><label id = 'pdName1'>Name:</label>";
				echo "<label id = 'pdName'>".$details[0]['name']."</label><br />";
				echo "</br>";

                echo "<label id = 'lblPriceSmall' >Price (XXS to Large):</label> <input id = 'txtPriceNormal' type = 'text' default = '".$details[0]['price_normal']."' value = '".$details[0]['price_normal']."'/><br /><br />";
				echo "<label id = 'lblWarnNormal'></label>";
				
                echo "<label id = 'lblPriceXL' >Price (XL): <input id = 'txtPriceXL' type = 'text' default = '".$details[0]['price_xl']."' value = '".$details[0]['price_xl']."'/><br /> <br />";
				echo "<label id = 'lblWarnXL'></label>";
				
                echo "<label id = 'lblPriceXXL' >Price (XXL): <input id = 'txtPriceXXL' type = 'text' default = '".$details[0]['price_xxl']."' value = '".$details[0]['price_xxl']."'/><br /> <br />";
				echo "<label id = 'lblWarnXXL'></label>";
                
				echo "<input id = 'btnEditProduct' type = 'button' value = 'Edit Product'>";
                echo "</div>";
			}
		}
	}
?>
    <div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>
</body>
</html>