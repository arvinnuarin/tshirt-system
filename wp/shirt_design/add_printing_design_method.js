$(document).ready(function(){
	    $(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	$("#btnPm").click(function(){
		var ctr = 0;
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		if($('#selPm').val() == 0){
			$("#lblWarnPM").html("Please Select Printing Method!");
		}else if($('option:selected', '#selPm').attr('pm-name') != "" && $("#selPm").val() != ""){
			ctr += 1;
		}else{
			popupOk("Something went wrong!");
			$('.popup-ok').click(function(){
				window.location.replace("../shirt_design/");
			});
		}

		if($("#defPrice").val() != ""){
			ctr += 1;
		}else{
			$("#lblWarnDefPrice").html("Empty Default Price!");
		}

		if($("#priceInc").val() != ""){
			ctr += 1;
		}else{
			$("#lblWarnPriceInc").html("Empty Price Increment!");
		}

		if($("#pdId").val() == ""){
			popupOk("Something went wrong! We need to reload the page immediately!");
			$('.popup-ok').click(function(){
				window.location.replace("../shirt_design/");
			});
		}else{
			ctr += 1;
		}
		
		if(ctr == 4){
			loading();
			$.post("shirt_design/add_printing_design_method.php", {pdId:$("#pdId").val(), defPrice:$("#defPrice").val(), priceInc:$("#priceInc").val(),pmName:$('option:selected', '#selPm').attr('pm-name'), pmId:$("#selPm").val(), date:currentDate}, function(data){
				if(data){
					$(".back").remove();
					if(data == "Success"){
						popupOk("Add Printing Method", "Printing Method Successfully Added for Product "+$("#prodName").val()+"!");
						$('.popup-ok').click(function(){
							window.location.replace("shirt_design/product_printing_method_form.php?product_design_id="+$("#pdId").val());
						});						
					}else{
						popupOk("Something went wrong!");
					}
				}	
			});
		}
	});

	$("#defPrice").on("input propertychange", function(){
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
	});

	$("#priceInc").on("input propertychange", function(){
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
	});
});