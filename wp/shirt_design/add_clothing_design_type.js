$(document).ready(function(){
	    $(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	$("#btnPrintType").click(function(){
		$("#lblWarnPrintType").html("");
		if($("#txtPrintType").val() == ""){
			$("#lblWarnPrintType").html("Empty Print Type!");
		}else{
			var datetime = new Date();
        	var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			loading();
			$.post("shirt_design/add_clothing_design_type.php", {type:$("#txtPrintType").val(), date:currentDate}, function(data){
				if(data){
					$(".back").remove();
					if(data.indexOf("Success") != -1){
						var count = data.split("#");
						popupOk("New Clothing Print Method Successfully Added!");
						$(".popup-ok").click(function(){
							window.location.replace("shirt_design/clothing_design_type_form.php?start="+count[1]);
						});
					}else if(data == "Print method exist"){
						popupOk("Print Method Already Exist!");
					}else{
						popupOk("Something went wrong! We need to reload the page immediately!");
						$(".popup-ok").click(function(){
							window.location.replace("");
						});
					}
				}
			});
		}
	});
});