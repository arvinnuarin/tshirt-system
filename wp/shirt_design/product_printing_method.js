$(document).ready(function(){
    $(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	$('#btnSearch').click(function(){
		var search = $.trim($('#txtSearch').val());
		window.location.href = "shirt_design/product_printing_method_form.php?product_design_id="+$("#prodId").val()+"&search="+search+"&start=0";
	});
	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	var date = new Date();
	$("#fromdate, #salesfrom").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$("#salesfrom").on("change", function(){
		var date = new Date();
    	$("#salesto").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#salesfrom").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});


	var date = new Date();
	$("#todate").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#fromdate").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#salesto").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#salesfrom").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});


	$('#btnFilter').click(function(){
		if($("#fromdate").val() != "" || $("#todate").val() != ""){
			if($("#todate").val() >= $("#fromdate").val()){
				window.location.href = "shirt_design/product_printing_method_form.php?product_design_id="+$("#prodId").val()+"search="+$('#txtSearch').val()+"&start=0&from_date="+$("#fromdate").val()+"&to_date="+$("#todate").val(); 
			}
		}
	});

	$('#btnFilterSales').click(function(){
		if($("#salesfrom").val() != "" || $("#salesto").val() != ""){
			if($("#salesto").val() >= $("#salesfrom").val()){
				window.location.href = "shirt_design/product_printing_method_form.php?product_design_id="+$("#prodId").val()+"search="+$('#txtSearch').val()+"&start=0&from_date_sales="+$("#salesfrom").val()+"&to_date_sales="+$("#salesto").val(); 
			}
		}
	});

	$(".update").click(function(e){
		var datetime = new Date();
		var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		var tr = $(this).parent().parent();
		if($(this).attr('btn-name') == "update"){
			$(this).attr('btn-name', 'proceed');
			$(this).next().css("display", "inline");
			$(this).html("Proceed");
			tr.find(".def-price").find(".lblDefPrice").css("display", "none");
			tr.find(".def-price").find(".txtDefPrice").css("display", "inline");
			tr.find(".price-inc").find(".lblPriceInc").css("display", "none");
			tr.find(".price-inc").find(".txtPriceInc").css("display", "inline");
			tr.css("background-color", "#fbd89b");
		}else if($(this).attr('btn-name') == "proceed"){
			var defPrice = tr.find(".def-price").find(".txtDefPrice");
			var lblDefPrice = tr.find(".def-price").find(".lblDefPrice");
			var priceInc = tr.find(".price-inc").find(".txtPriceInc");
			var lblPriceInc = tr.find(".price-inc").find(".lblPriceInc");
			var id = tr.attr('pdp_id');
			if((priceInc.val() !== "" && priceInc.val() !== priceInc.attr('base')) && (defPrice.val() !== "" && defPrice.val() !== defPrice.attr('base'))){
				loading();
				$.post("shirt_design/product_printing_method.php",{id:id, priceInc:priceInc.val(), defPrice:defPrice.val(), date:currentDate, productDesign:$("#productDesign").val(), printMethod:tr.find(".print_method").html()}, function(data){
					if(data){
						$(".back").remove();
						if(data == "Success"){
							priceInc.attr('base',priceInc.val());
							lblPriceInc.html("Php "+eval(priceInc.val()).toFixed(2)); 
							defPrice.attr('base',defPrice.val());
							lblDefPrice.html("Php "+eval(defPrice.val()).toFixed(2));
							popupOk("Printing Method", "Price Increment and Default Price Successfully Updated!");
						}else{
							popupOk("Error", "Failed to Update!");
						}
					}
				});
			}else if(priceInc.val() !== "" && priceInc.val() !== priceInc.attr('base')){
				loading();
				$.post("shirt_design/product_printing_method.php",{id:id, priceInc:priceInc.val(),date:currentDate, productDesign:$("#productDesign").val(), printMethod:tr.find(".print_method").html()}, function(data){
					if(data){
						$(".back").remove();
						if(data == "Success"){
							priceInc.attr('base',priceInc.val());
							lblPriceInc.html("Php "+eval(priceInc.val()).toFixed(2)); 
							popupOk("Printing Method", "Price Increment Successfully Updated!");
						}else{
							popupOk("Error", "Failed to Update!");
						}
					}
				});
			}else if(defPrice.val() !== "" && defPrice.val() !== defPrice.attr('base')){
				loading();
				$.post("shirt_design/product_printing_method.php",{id:id, defPrice:defPrice.val(),  date:currentDate, productDesign:$("#productDesign").val(), printMethod:tr.find(".print_method").html()}, function(data){
					if(data){
						$(".back").remove();
						if(data == "Success"){
							defPrice.attr('base',defPrice.val());
							lblDefPrice.html("Php "+eval(defPrice.val()).toFixed(2));
							popupOk("Printing Method", "Default Price Successfully Updated!");
						}else{
							popupOk("Error", "Failed to Update!");
						}
					}
				});
			}
			tr.css("background-color", "white");
			$(this).html("Update");
			$(this).next().css("display", "none");
			$(this).attr('btn-name', 'update');
			lblDefPrice.css("display", "inline");
			defPrice.css("display", "none");
			lblPriceInc.css("display", "inline");
			priceInc.css("display", "none");
		}
		e.preventDefault();
	});

	$(".cancel").click(function(e){
		var tr = $(this).parent().parent();
		$(this).css("display", "none");
		$(this).prev().css("display", "inline");
		$(this).prev().attr('btn-name', 'update');
		$(this).prev().html("Update");
		tr.find(".def-price").find(".lblDefPrice").css("display", "inline");
		tr.find(".def-price").find(".txtDefPrice").css("display", "none");
		tr.find(".price-inc").find(".lblPriceInc").css("display", "inline");
		tr.find(".price-inc").find(".txtPriceInc").css("display", "none");
		tr.css("background-color", "white");
		e.preventDefault();
	});

	$(".txtDefPrice, .txtPriceInc").on("input propertychange", function(){
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
	});
});