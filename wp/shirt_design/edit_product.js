$(document).ready(function(){
	$("#lnkSA").click(function(e){
		e.preventDefault();
		$(".chkColor").prop("checked", true);
	});

	$("#lnkUA").click(function(e){
		e.preventDefault();
		$(".chkColor").prop("checked", false);
	});

    $("#txtPriceNormal").on("input propertychange", function(){
    	$(this).val($(this).val().replace(/[^0-9\.]/g,''));
    });
        
    $("#txtPriceXL").on("input propertychange", function(){
    	$(this).val($(this).val().replace(/[^0-9\.]/g,''));
    });

    $("#txtPriceXXL").on("input propertychange", function(){
    	$(this).val($(this).val().replace(/[^0-9\.]/g,''));
    });

	$("#btnEditProduct").click(function(){
		var colors = $(".chkColor:checked").map(function() {return $(this).val();}).get();
		var ctr = 0, sub_color, priceNormal, priceXL, priceXXL;
		if($("#txtPriceNormal").val() == $("#txtPriceNormal").attr('default') && $("#txtPriceXL").val() == $("#txtPriceXL").attr('default') && $("#txtPriceXXL").val() == $("#txtPriceXXL").attr('default')
			&& colors.length == 0){
			popupOk("Product Information Editing", "No Changes Made!");
		}else{		
			if($("#txtPriceNormal").val() == ""){
				$("#lblWarnNormal").html("Empty Price!");
                document.getElementById("txtPriceNormal").style.border = '1px solid #dd4b39';

			}else{
				if($("#txtPriceNormal").val() == $("#txtPriceNormal").attr('default')){
					priceNormal = "null";
				}else{
					priceNormal = $("#txtPriceNormal").val();
				}
				ctr += 1;
			}
			if($("#txtPriceXL").val() == ""){
				$("#lblWarnXL").html("Empty Price!");
                document.getElementById("txtPriceXL").style.border = '1px solid #dd4b39';

			}else{
				if($("#txtPriceXL").val() == $("#txtPriceXL").attr('default')){
					priceXL = "null";
				}
				else{
					priceXL = $("#txtPriceXL").val();
				}
				ctr += 1;
			}
			if($("#txtPriceXXL").val() == ""){
                
				$("#lblWarnXXL").html("Empty Price!");
                document.getElementById("txtPriceXXL").style.border = '1px solid #dd4b39';

			}else{
				if($("#txtPriceXXL").val() == $("#txtPriceXXL").attr('default')){
					priceXXL = "null";
				}
				else{
					priceXXL = $("#txtPriceXXL").val();
				}
				ctr += 1;
			}
			if(colors.length > 0){
				sub_color = colors;
			}else{
				sub_color = 0;
			}
		}
		if(ctr == 3){
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("edit_product.php", {priceNormal: priceNormal, priceXL:priceXL, priceXXL:priceXXL,
				color:sub_color, prodId:$("#pdId").html(), prodName:$("#pdName").html(), date:currentDate}, function(data){
				if(data == "success"){
					popupOk("Product Information Editing", "Product Successfully Updated!");
					$('.popup-ok').click(function(){
						window.location.replace("../shirt_design/");
					});
				}else if(data == "failed"){
					popupOk("Product Information Editing", "Product Updating Failed!");
				}else{
					popupOk("Product Information Editing", "Something Went Wrong!");
				}
				//alert(data);
				//popupOk("Product Updating", "Product Successfully Updated!");
			});
		}
	});
});