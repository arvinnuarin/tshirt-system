<html>
	<?php
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
					require_once('../../php/awp.php');	
					$awp = new Awp();

					if(isset($_GET['start'])){
						if(!empty($_GET['start'])){
							if(is_numeric($_GET['start'])){
								$start = intval($_GET['start']);
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}

					if(isset($_GET['search']) && isset($_GET['filter'])){
						if(!empty($_GET['search']) && !empty($_GET['filter'])){
							$search = $_GET['search'];
							$filter_id = $_GET['filter'];
							if($filter_id > 0 && $filter_id <= 2){
								if($filter_id == 1){
									$filter = "product_design.name";
								}else if($filter_id == 2){
									$filter = "category.sub_cat_name";
								}
							}else{
								$search = "";
								$filter = "category.sub_cat_name";
								$filter_id = -1;
							}
						}else{
							$search = "";
							$filter = "category.sub_cat_name";
							$filter_id = -1;
						}
					}else{
						$search = "";
						$filter = "category.sub_cat_name";
						$filter_id = -1;
					}

					if(isset($_GET['sort'])){
						if(!empty($_GET['sort'])){
							$sort_trans = array(array("id_desc", "product_design.pd_id","desc"), array("id_asc", "product_design.pd_id", "asc"), array("cat_name_desc", "category.cat_name","desc"), array("prod_name_desc", "product_design.name","desc"), array("prod_name_asc", "product_design.name", "asc"), array("cat_name_desc", "category.cat_name","desc"), array("cat_name_asc", "category.cat_name", "asc"));
							$ctr = 0;
							foreach ($sort_trans as $st) {
								if($st[0] == $_GET['sort']){
									$sort = $_GET['sort'];
									$col_order = $st[1];
									$order = $st[2];
								}else{
									$ctr += 1;
								}
							}
							if(count($sort_trans) == $ctr){
								$sort = "";
								$col_order = "product_design.pd_id";
								$order = "";
							}
						}else{
							$sort = "";
							$col_order = "product_design.pd_id";
							$order = "";
						}
					}else{
						$sort = "";
						$col_order = "product_design.pd_id";
						$order = "";
					}
					if(isset($_GET['from_date']) && isset($_GET['to_date']) && !isset($_GET['from_date_sales']) && !isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date']) && !empty($_GET['to_date'])){
							$from_date = $_GET['from_date'];
							$to_date = $_GET['to_date'];
						}else{
							$from_date = "";
							$to_date = "";
						}
					}else{
						$from_date = "";
						$to_date = "";				
					}

					if(!isset($_GET['from_date']) && !isset($_GET['to_date']) && isset($_GET['from_date_sales']) && isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date_sales']) && !empty($_GET['to_date_sales'])){
							$from_date_sales = $_GET['from_date_sales'];
							$to_date_sales = $_GET['to_date_sales'];
						}else{
							$from_date_sales = "";
							$to_date_sales = "";
						}
					}else{
						$from_date_sales = "";
						$to_date_sales = "";				
					}

					$per_page = 10;

					echo "<head>";
					?>
						<title>Clothing Design Management</title>
						<link rel = 'stylesheet' href = "../../css/popup.css">
						<link rel="stylesheet" type="text/css" href="product.css">
						<link rel="stylesheet" href="../../js/photobox-master/photobox/photobox.css">
						<link rel="stylesheet" type="text/css" href="../../css/jquery.datetimepicker.css">
						<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
						<script type = "text/javascript" src = 'product.js'></script>
						<script type = "text/javascript" src = '../../js/jquery.datetimepicker.full.js'></script>
					<?php
					require_once('../setup/setup_form.php');
					echo "</head>";
					echo "<body>";

					$setup = new SetUp($_SESSION['position']);
					
					$record_count = $awp ->get_all_product_design_count($filter, $search);

					$products = $awp -> view_product_design($start, $per_page, $filter, $search, $col_order, $order, $from_date, $to_date, $from_date_sales, $to_date_sales);
					echo "<ul class = 'topMenu'>";
					echo "<li><a  href = 'shirt_design/clothing_design_type_form.php'>Printing Method</a></li>";
					echo "<li><a class = 'active' href = 'shirt_design'>Clothing Design</a></li>";
		            echo "</ul>";
				
					echo "<h1>Clothing Design Management</h1>";
			        echo "<a id = 'btnAddProduct' href = 'shirt_design/modify_product_form.php'>Add Clothing Design</a> ";
			        echo "<div id = 'divShirtDesign'> ";
					echo "<label id = 'lblSearch'><br>&nbsp; &nbsp; &nbsp;&nbsp;Search via:&nbsp; </label>";
					echo "<select id = 'selSearch'>";
					echo "<option value = '1'";
					if($filter_id == 1){
						echo "selected";
					}	
					echo ">Clothing Name</option>";
					echo "<option value = '2'";
					if($filter_id == 2){
						echo "selected";
					}	
					echo ">Sub Category</option>";
					echo "</select> ";
			        echo "<input type = 'text' id = 'txtSearch' value = '$search' placeholder = 'Search..'/>";
			        echo "<input type='image' value = 'Search' id = 'btnSearch'  src ='shirt_design/icon/imgSearch.png'/>";

					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Sort by: </label> &nbsp;";
					$arr_sort = array(array("Clothing ID (Lowest to Highest) ", "id_asc"), array("Clothing ID (Highest to Lowest) ", "id_desc"), array("Clothing Name (A - Z) ", "prod_name_asc"), array("Clothing Name (Z - A) ", "prod_name_desc"), array("Sub Category Name (A - Z) ", "cat_name_asc"), array("Sub Category Name (Z - A) ", "cat_name_desc"));
					echo "<select id = selSort>";
					foreach($arr_sort as $s){
						$filter_url = "shirt_design/index.php?";
						if($search != ""){
							$filter_url .= "search=$search&";
						}
						if($filter_id <=2 && $filter_id > 0){
							$filter_url .= "filter=$filter_id&";
						}
						if($start !== ""){
							$filter_url .= "start=$start&";
						}
						$filter_url .= "sort=".$s[1];
						echo "<option ";
						if($sort == $s[1]){
							echo "selected";
						}
						echo " value = '".$filter_url."'>".$s[0]."</option>";
					}
					echo "</select>";
			        
					echo "<br />";

					
					if($search !== ""){
						echo "<label id = 'lblResult'>";
						echo $record_count[0]['count']." result";
						if($record_count[0]['count'] > 1)
							echo "s";
						echo " for \"". $search."\"";
						echo "</label>";
					}
					
					
					if(count($products) > 0){
						
						
						$header = array("ID", "Name", "Description", "Product Design","Sub Category", "Options");
						echo "<table>";
						foreach ($header as $h) {
							echo "<th ";
							if($h == "Options")
								echo " colspan = '4' ";
							echo " >$h</th>";
						}
						foreach ($products as $p) {
							echo "<tr>";

							echo "<td>SD - ".$p['pi']."</td>";
							echo "<td>".$p['name']."</td>";
							echo "<td>".$p['description']."</td>";
							echo "<td>";
							echo "<img style = 'height:50px; width:50px;' src = '../assets/shirt_design_template/".$p['name']."/front.png'>";
							if(file_exists("../../assets/shirt_design_template/".$p['name']."/back.png")){
								echo "<img style = 'height:50px; width:50px;'src = '../assets/shirt_design_template/".$p['name']."/Back.png'>";
							}
							
							echo"</td>";
							
							echo "<td>".$p['sub_cat_name']."</td>";
							echo "<td><a href = 'shirt_design/add_clothing_assoc_form.php?product_design_id=".$p['pi']."'>Add Color</a></td>";
							echo "<td><a href = 't-shirt/?product_design_id=".$p['pi']."'>View Clothing</a></td>";
							echo "<td><a href = 'shirt_design/add_printing_design_method_form.php?product_design_id=".$p['pi']."'> Add Printing Method</a></td>";
							echo "<td><a href = 'shirt_design/product_printing_method_form.php?product_design_id=".$p['pi']."'>View Printing Method</a></td>";
							echo "</tr>";
							
						}
						echo "</table>";
						
						if($record_count[0]['count'] > 10){
							$page_url = "shirt_design/index.php?";

							if($search != ""){
								$page_url .= "search=$search&";
							}
							if($filter_id > 0 && $filter_id <= 2){
								$page_url .= "filter=$filter_id&";
							}
							if($sort != ""){
								$page_url .= "sort=$sort&";
							}
				            echo "<div id = 'btnPagination'>";
							$prev = $start - $per_page;
							if($start > 0){
								echo "<a class = 'lblPagination' href = '".$page_url."start=$prev'>Prev</a>";
							}

							$i = 1;
							for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
								if($start != $x){
									echo " <a class = 'lblPagination' href = '".$page_url."start=$x'>$i</a> ";
				                    
								}else{
									echo " <a style='background-color: #dd4b39; color:white;' class = 'lblPagination' href = '".$page_url."start=$x'><b>$i</b></a> ";
				                }
				                
								$i++;
							}

							$next = $start + $per_page;
							if($start < $record_count[0]['count'] - $per_page ){
								echo " <a class = 'lblPagination' href = '".$page_url."start=$next'>Next</a>";
							}
						}
						
			            echo "</div>";
					}
			    	echo "</div>";
			    	echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
					echo "</body>";
			}else{
				session_destroy();
				echo "<script>window.location.replace('../../wp/');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
		
	?>
</html>

