$(document).ready(function(){
	    $(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	$(".availability").on("change", function(){
		var datetime = new Date();
        var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
		loading();
		$.post("clothing_design_type.php",{date:currentDate, pmId:$(this).val(), av:$('option:selected', this).attr('av')}, function(data){
			if(data){
				$(".back").remove();
				if(data == "Success"){
					popupOk("Print Method Availability Successfully Updated!");
				}else{
					popupOk("Something went wrong!");
				}
			}
		});
	});

	$("#btnSearch").click(function(){
		if($("#txtSearch").val() != ""){
			window.location.href = "shirt_design/clothing_design_type_form.php?search="+$("#txtSearch").val();
		}
	});

	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});
});