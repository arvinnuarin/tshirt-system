<?php
	session_start();
	require_once("../../php/awp.php");
	$awp = new Awp();
	if(isset($_POST['stat'])){
		if($_POST['stat'] !== ""){
			$stat = $_POST['stat'];
			if($stat == "getColor"){
				if(isset($_POST['clothingId']) && isset($_POST['pdId'])){
					if($_POST['clothingId'] !== "" && $_POST['pdId'] !== ""){
						$clothing_id = $_POST['clothingId'];
						$pd_id = $_POST['pdId'];
						$colors = $awp -> get_unlist_colors($pd_id, $clothing_id);
						foreach ($colors as $c) {
							echo "<option value = '".$c['sc_id']."'>".$c['color']."</option>";
						}
					}else
						echo "Some data are empty";
				}else
					echo "Some data are not set";
			}else if($stat == "addProd"){
				if(isset($_POST['colorId']) && isset($_POST['pdId']) && isset($_POST['date'])){
					if($_POST['colorId'] !== "" && $_POST['pdId'] !== "" && $_POST['date'] !== ""){
						$employee_id = $_SESSION['id'];
						$color_id = $_POST['colorId'];
						$pd_id = $_POST['pdId'];
						$date = $_POST['date'];
						$result = $awp -> add_prod($pd_id, $color_id, $employee_id, $date);
						$count = $awp -> get_all_shirt_count("clothing.clothing_id", "", $pd_id, "");
						$lp = $count[0]['count'];
						while($lp%10 != 0){
							$lp += 1;
						}
						$lp = $lp - 10;
						if($result == "Success")
							echo $result."#".$lp;
						else
							echo $result;
					}else
						echo "Some data are empty";
				}else
					echo "Some data are not set";				
			}
		}else
			echo "Some data are empty";
	}else
		echo "Some data are not set";	


?>