<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			require_once('../../php/awp.php');
			$awp = new Awp();
			echo "<head>";
			?>
			<link rel = "stylesheet" href = '../../css/upload_photo.css' type = "text/css">
			<link rel = "stylesheet" href = '../../css/modify_product.css' type = "text/css">
			<link rel = 'stylesheet' href = "../../css/popup.css">
			<link rel="stylesheet" type="text/css" href="modify_product.css">
			<link rel = "stylesheet" type="text/css" href="../../css/loading.css">
			<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type = "text/javascript" src = '../../js/cropit-master/dist/jquery.cropit.js'></script>
			<script type = "text/javascript" src = "../../js/popup_0.js"></script>
			<script type = "text/javascript" src = "../../js/loading.js"></script>
			<script type = "text/javascript" src = 'modify_product.js'></script>
			<?php
			require_once('../setup/setup_form.php');
			echo "</head>";

			echo "<body>";
			$setup = new SetUp($_SESSION['position']);
			echo "<h1>Add Clothing Design</h1> <br />";
	        echo "<div id = 'divAddShirt'>";
			echo "<h3>Product Information</h3>";	
			
			echo "</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sub Category: <select id = 'selProdCat'><option>Select Category</option>";
				$arr_cat = $awp->get_all_category();
				foreach($arr_cat as $ac){
					echo "<option value = '".$ac['category_id']."'>".$ac['sub_cat_name']."</option>";
				}
			echo "</select>";
			echo "<label id = 'lblWarnCat'></label>";
			echo "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Name:";
			echo "<input type = 'text' id = 'txtProdName'/>";
			echo "<label id = 'lblWarnProdName'></label>";
			echo "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Description: ";
			echo "<br><textarea id = 'txtProdDesc'></textarea>";
			echo "<label id = 'lblWarnProdDesc'></label>";
			
			echo "<h3>Product Design</h3>";

			$faces = array("Front", "Back");

			foreach($faces as $f){
				echo "<div id = 'pic".$f."' class = 'image-editor'>";
				echo "<br>";
				echo "<b>".$f."</b>";
				echo "<button class = 'btnClear' value = '".$f."'>Clear</button>";
				echo "<div class = 'cropit-preview' id = 'cp".$f."' style = 'height:550px; width:550px'></div>";
				echo "<input type = 'file' class='cropit-image-input'/>";
				echo "<label id = 'lblWarn".$f."'></label>";
				echo "</div>";
				
			}
				echo "<br><br><br>";
				echo "<button id = 'btnModifyProduct'>Add Product</button>";
				echo "<br><br><br>";
			echo "</div>";			
			echo "</body>";
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}
?>
	<div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>
</html>