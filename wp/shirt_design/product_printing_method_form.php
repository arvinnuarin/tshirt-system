<html>
	<?php
		session_start();
		if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
			if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
				if(isset($_GET['product_design_id'])){
					if($_GET['product_design_id'] !== ""){
					require_once('../../php/awp.php');	
					$awp = new Awp();

					if(isset($_GET['start'])){
						if(!empty($_GET['start'])){
							if(is_numeric($_GET['start'])){
								$start = intval($_GET['start']);
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}

					if(isset($_GET['search'])){
						if(!empty($_GET['search'])){
							$search = $_GET['search'];
						}else{
							$search = "";
						}
					}else{
						$search = "";
					}

					$sort_trans = array(array("pm_asc", "print_method.name", "asc"), array("pm_desc", "print_method.name", "desc"), array("stock_asc", "print_method.total_color_stock", "asc"), array("stock_desc", "print_method.total_color_stock", "desc"), array("av_asc", "print_method.availability", "asc"), array("av_desc", "print_method.availability", "desc"));
			        foreach ($sort_trans as $st) {
			          if(isset($_GET['sort'])){
			            if(!empty($_GET['sort'])){
			              $ctr = 0;
			              if($st[0] == $_GET['sort']){
			                $sort = $_GET['sort'];
			                $col_order = $st[1];
			                $order = $st[2];
			                break;
			              }else{
			                $ctr += 1;
			              }
			              
			              if(count($sort_trans) == $ctr){
			                $sort = "";
			                $col_order = "product_design_print.pm_id";
			                $order = "";
			              }
			            }else{
			              $sort = "";
			              $col_order = "product_design_print.pm_id";
			              $order = "";
			            }
			          }else{
			            $sort = "";
			            $col_order = "product_design_print.pm_id";
			            $order = "";
			          }
			        }

					if(isset($_GET['from_date']) && isset($_GET['to_date']) && !isset($_GET['from_date_sales']) && !isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date']) && !empty($_GET['to_date'])){
							$from_date = $_GET['from_date'];
							$to_date = $_GET['to_date'];
						}else{
							$from_date = "";
							$to_date = "";
						}
					}else{
						$from_date = "";
						$to_date = "";				
					}

					if(!isset($_GET['from_date']) && !isset($_GET['to_date']) && isset($_GET['from_date_sales']) && isset($_GET['to_date_sales'])){
						if(!empty($_GET['from_date_sales']) && !empty($_GET['to_date_sales'])){
							$from_date_sales = $_GET['from_date_sales'];
							$to_date_sales = $_GET['to_date_sales'];
						}else{
							$from_date_sales = "";
							$to_date_sales = "";
						}
					}else{
						$from_date_sales = "";
						$to_date_sales = "";				
					}

					$product_design_id = $_GET['product_design_id'];

					echo "<head>";
					?>
						<title>Printing Methods</title>
						<link rel = 'stylesheet' href = "../../css/popup.css">
						<link rel="stylesheet" type="text/css" href="../../css/loading.css">
						<link rel="stylesheet" type="text/css" href="product_printing_method.css">
						<link rel="stylesheet" type="text/css" href="../../css/jquery.datetimepicker.css">
						<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
						<script type="text/javascript" src = "../../js/popup_0.js"></script>
						<script type = "text/javascript" src = 'product_printing_method.js'></script>
						<script type = "text/javascript" src = '../../js/jquery.datetimepicker.full.js'></script>
						<script type = "text/javascript" src = '../../js/loading.js'></script>
					<?php
					require_once('../setup/setup_form.php');
					//$setup = new SetUp($_SESSION['position']);
					$per_page = 5;
					$print_method = $awp -> get_all_print_method_for_prod($search, $col_order, $order, $product_design_id, $from_date, $to_date, $from_date_sales, $to_date_sales);
					$record_count = $awp ->get_all_print_method_for_prod_count($search, $product_design_id);

					echo "</head>";
					echo "<body>";
					echo "<ul class = 'topMenu'>";
					echo "<li><a class = 'active' href = 'shirt_design/clothing_design_type_form.php'>Printing Method</a></li>";
					echo "<li><a href = 'shirt_design/'>Clothing Design</a></li>";
					echo "</ul>";
					if($product_design = $awp -> get_product_design_name_by_id($product_design_id)){
						echo "<h1>Printing Methods for Product Design <b style = 'color: #d73925';>".$product_design[0]['name']."</b></h1>";
						echo "<a href = 'shirt_design/add_clothing_assoc_form.php?product_design_id=".$product_design_id."'>Add Printing Method</a>";
						echo "<input type = 'hidden' value = '".$product_design[0]['name']."' id = 'productDesign' />";						
					}else{
						echo "<script type = 'text/javascript'>window.location.replace('//www.mixlarts.com/MixlArts/wp/shirt_design/');</script>";
					}
					
			        echo "<div id = 'divShirtDesign'> </br>";
					echo "<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search by Printing Method:&nbsp; </label>";
					
			        echo "<input type = 'text' id = 'txtSearch' value = '$search' placeholder = 'Search..'/>";
			        echo "<input type='image' value = 'Search' id = 'btnSearch'  src ='shirt_design/icon/imgSearch.png'/>";

			        if($search !== ""){
						echo "<label id = 'lblResult'>";
						echo $record_count[0]['count']." result";
						if($record_count[0]['count'] > 1)
							echo "s";
						echo " for Color \"". $search."\"";
						echo "</label>";
					}

					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Sort by:</label> &nbsp;";

					$arr_sort = array(array("Print Method (A-Z)", "pm_asc"), array("Print Method (Z-A)", "pm_desc"), array("Availability (A-Z)", "av_asc"), array("Availability (Z-A)", "av_desc"));
					
					echo "<select id = selSort>";
					foreach($arr_sort as $s){
						$filter_url = "shirt_design/product_printing_method_form.php?product_design_id=$product_design_id&";
						if($search != ""){
							$filter_url .= "search=$search&";
						}
						if($start !== ""){
							$filter_url .= "start=$start&";
						}
						$filter_url .= "sort=".$s[1];
						echo "<option ";
						if($sort == $s[1]){
							echo "selected";
						}
						echo " value = '".$filter_url."'>".$s[0]."</option>";
					}
					echo "</select>";

					       
					if(isset($_GET['start'])){
						if(!empty($_GET['start'])){
							if(is_numeric($_GET['start'])){
								$start = intval($_GET['start']);
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}		
							
							
							
							$header = array("Printing Method", "Availability", "Default Price", "Price Increment","View Clothing","Options");
							
							
							if(count($print_method) > 0){
								echo "<table>";
								foreach ($header as $h) {
									echo "<th>$h</th>";
								}
								foreach ($print_method as $mpm) {
									echo "<tr pdp_id = '".$mpm['pd_id']."'>";
									echo "<td class = 'print_method'>".$mpm['name']."</td>";
									echo "<td>".$mpm['availability']."</td>";
									echo "<td class = 'def-price'><label class = 'lblDefPrice'>Php ".number_format($mpm['default_price'],2)."</label><input class = 'txtDefPrice' style = 'display:none;' type = 'text' value = '".$mpm['default_price']."' base = '".$mpm['default_price']."'></td>";
									echo "<td class = 'price-inc'><label class = 'lblPriceInc'>Php ".number_format($mpm['price_increment'],2)."</label><input class = 'txtPriceInc' type = 'text' style = 'display:none;' value = '".$mpm['price_increment']."' base = '".$mpm['price_increment']."'></td>";
									
									
									echo "<td><a id = 'btnView' href = '//www.mixlarts.com/MixlArts/wp/t-shirt/?product_design_id=".$product_design_id."&pm_id=".$mpm['pmi']."'>View Clothing</a></td>";
									echo "<td><a href = '#' class = 'update' btn-name = 'update'>Update</a> <a href = '#' style = 'display:none;' class = 'cancel'>Cancel</a></td>";
									echo "</tr>";

								}
								echo "</table>";
							}
							
				            echo "</br>";
							$page_url = "shirt_design/product_printing_method_form.php?product_design_id=$product_design_id&";

							if($search != ""){
								$page_url .= "search=$search&";
							}
							if($sort != ""){
								$page_url .= "sort=$sort&";
							}
				            echo "<div id = 'btnPagination'>";
							$prev = $start - $per_page;
							if($start > 0){
								echo "<a class = 'lblPagination' href = '".$page_url."start=$prev'>Prev</a>";
							}

							$i = 1;
							for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
								if($start != $x){
									echo " <a class = 'lblPagination' href = '".$page_url."start=$x'>$i</a> ";
				                    
								}else{
									echo " <a style='background-color: #dd4b39; color:white;' class = 'lblPagination' href = '".$page_url."start=$x'><b>$i</b></a> ";
				                }
				                
								$i++;
							}

							$next = $start + $per_page;
							if($start < $record_count[0]['count'] - $per_page ){
								echo " <a class = 'lblPagination' href = '".$page_url."start=$next'>Next</a>";
							}

				            echo "</div>";
						}else{
							echo "<label id='lblResult'>Empty Record!</label>";
						}
				    	echo "</div>";
				    	echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
				    	echo "<input type = 'hidden' value = '$product_design_id' id = 'prodId'>";
						echo "</body>";
					}
				
					
			}else{
				session_destroy();
				echo "<script>window.location.replace('../../wp/');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
		
	?>


	
</html>

