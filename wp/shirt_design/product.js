$(document).ready(function(){
    $(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	$('#btnSearch').click(function(){
		if($.trim($('#txtSearch').val()) !== ""){
			if($("#selSearch").val() > 0 && $("#selSearch").val() <= 2){
				var search = $.trim($('#txtSearch').val());
				window.location.href = "shirt_design/index.php?filter="+$("#selSearch").val()+"&search="+search+"&start=0";
			}
		}
	});
	$("#selSort").change(function(){
		window.location.href = $(this).val();
	});

	var date = new Date();
	$("#fromdate, #salesfrom").datetimepicker({
    	format:'Y-m-d',
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#fromdate").on("change", function(){
		var date = new Date();
    	$("#todate").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#fromdate").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});

	$("#salesfrom").on("change", function(){
		var date = new Date();
    	$("#salesto").datetimepicker({
	    	format:'Y-m-d',
	    	minDate:$("#salesfrom").val(),
	    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
	    	timepicker:false,
		});
	});


	var date = new Date();
	$("#todate").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#fromdate").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});

	$("#salesto").datetimepicker({
    	format:'Y-m-d',
    	minDate:$("#salesfrom").val(),
    	maxDate:date.getYear()+"-"+date.getMonth()+"-"+date.getDay(),
    	timepicker:false,
	});


	$('#btnFilter').click(function(){
		if($("#fromdate").val() != "" || $("#todate").val() != ""){
			if($("#todate").val() >= $("#fromdate").val()){
				window.location.href = "shirt_design/index.php?filter="+$("#selSearch").val()+"&search="+$('#txtSearch').val()+"&start=0&from_date="+$("#fromdate").val()+"&to_date="+$("#todate").val(); 
			}
		}
	});

	$('#btnFilterSales').click(function(){
		if($("#salesfrom").val() != "" || $("#salesto").val() != ""){
			if($("#salesto").val() >= $("#salesfrom").val()){
				window.location.href = "shirt_design/index.php?filter="+$("#selSearch").val()+"&search="+$('#txtSearch').val()+"&start=0&from_date_sales="+$("#salesfrom").val()+"&to_date_sales="+$("#salesto").val(); 
			}
		}
	});
});