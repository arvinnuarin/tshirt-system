<html>
<?php
  session_start();
  if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
    if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
      require_once('../../php/awp.php');
      $awp = new Awp();
	  $per_page = 10;

      if(isset($_GET['start'])){
        
        if(!empty($_GET['start'])){
          if(is_numeric($_GET['start'])){
            $start = intval($_GET['start']);
          }else{
            $start = 0;
          }
        }else{
          $start = 0;
        }
      }else{
        $start = 0;
      }

      if(isset($_GET['search'])){
        if(!empty($_GET['search'])){
          $search = $_GET['search'];
        }else{
          $search = "";
        }
      }else{
        $search = "";
      }

      $sort_trans = array(array("id_asc", "print_method.pm_id", "asc"), array("id_desc", "print_method.pm_id", "desc"),array("pm_asc", "print_method.name", "asc"), array("pm_desc", "print_method.name", "desc"), array("stock_asc", "print_method.total_color_stock", "asc"), array("stock_desc", "print_method.total_color_stock", "desc"), array("av_asc", "print_method.availability", "asc"), array("av_desc", "print_method.availability", "desc"));

        foreach ($sort_trans as $st) {
          if(isset($_GET['sort'])){
            if(!empty($_GET['sort'])){
              $ctr = 0;
              if($st[0] == $_GET['sort']){
                $sort = $_GET['sort'];
                $col_order = $st[1];
                $order = $st[2];
                break;
              }else{
                $ctr += 1;
              }
              
              if(count($sort_trans) == $ctr){
                $sort = "";
                $col_order = "print_method.pm_id";
                $order = "";
              }
            }else{
              $sort = "";
              $col_order = "print_method.pm_id";
              $order = "";
            }
          }else{
            $sort = "";
            $col_order = "print_method.pm_id";
            $order = "";
          }
        }
        echo "<head>";
      ?>
	    <link rel = 'stylesheet' href ="clothing_design_type.css" type = "text/css">
        <link rel = 'stylesheet' href = "../../css/popup.css" type = "text/css">
        <link rel = "stylesheet" type="text/css" href="../../css/loading.css">
        <script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script> 
        <script type = "text/javascript" src = "../../js/popup_0.js"></script>
        <script type = "text/javascript" src = "../../js/loading.js"></script>
        <script type = "text/javascript" src = "clothing_design_type.js"></script>
        <title>Printing Method</title>
      <?php
      require_once('../setup/setup_form.php');
      echo "</head>";
      echo "<body>";
      $setup = new SetUp($_SESSION['position']);



		echo "<ul class = 'topMenu'>";
		echo "<li><a class = 'active' href = 'shirt_design/clothing_design_type_form.php'>Printing Method</a></li>";
		echo "<li><a href = 'shirt_design/'>Clothing Design</a></li>";
		echo "</ul>";
      	echo "<h1>Printing Method Management</h1>";
      	echo "<a id = 'btnAddType' href = 'shirt_design/add_clothing_design_type_form.php'>Add Printing Method</a>";

	echo "<div id = 'divAddType'>";

      echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search Print Method: <input type = 'text' id = 'txtSearch' value = '$search' />";
      echo "<input type = 'image' value = 'Search' id = 'btnSearch' src ='shirt_design/icon/imgSearch.png'>";

      $arr_sort = array(array("Print Method ID (Lowest to Highest)", "id_asc"), array("Print Method ID (Highest to Lowest)", "id_desc"),array("Print Method (A-Z)", "pm_asc"), array("Print Method (Z-A)", "pm_desc"), array("Availability (A-Z)", "av_asc"), array("Availability (Z-A)", "av_desc"));

      echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: <select id = 'selSort'>"; 
      foreach($arr_sort as $s){
        $filter_url = "shirt_design/clothing_design_type_form.php?";
        if($search != ""){
          $filter_url .= "search=$search&";
        }
        if($start !== ""){
          $filter_url .= "start=$start&";
        }
        $filter_url .= "sort=".$s[1];
        echo "<option ";
        if($sort == $s[1]){
          echo "selected";
        }
        echo " value = '".$filter_url."'>".$s[0]."</option>";
      }
      echo "</select>";

      echo "<br />";

      $record_count =  $awp -> get_all_print_method_count($search, $col_order, $order);

      if($search !== ""){
		echo "<label id = 'lblResults'>";
        echo "".$record_count[0]['count']." result/s for Print Method <b>\"". $search."\"</b>";
        echo "</label>";
      }

      if(($print_method = $awp -> get_all_print_method($start, $per_page, $search, $col_order, $order))) {
        echo "<table>";
        echo "<th>ID</th>";
        
        echo "<th>Print Method</th>";

        echo "<th>Availability</th>";
        foreach ($print_method as $pm) {
          echo "<tr>";
          echo "<td>PRNTMET-".$pm['pm_id']."</td>";
         
          echo "<td>".$pm['name']."</td>";
          
            
            echo "<td><select class = 'availability'>";
            echo "<option av = 'Available' value = '".$pm['pm_id']."' ";
            if($pm['availability'] == "Available"){
              echo " selected ";
            }
            echo " />Available</option>";
            echo "<option av = 'Unavailable' value = '".$pm['pm_id']."' ";
            if($pm['availability'] == "Unavailable"){
              echo " selected ";
            }
            echo ">Unvailable</option>";
            echo "</select></td>";

          echo "</tr>";
        }
        echo "</table>";
		echo "</br></br>";

        if($record_count[0]['count'] > 10){
          $page_url = "";

          if($search != ""){
            $page_url .= "search=$search&";
          }

          if($sort != ""){
            $page_url .= "sort=$sort&";
          }

          $page_url = "clothing_design_type_form.php?".$page_url;
			echo "<div id = 'btnPagination'>";
          $prev = $start - $per_page;
          if($start > 0){
			echo "<a class = 'lblPagination' href = '".$page_url."start=$prev'>Prev</a>";
          }
          $i = 1;
          for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
            if($start != $x){
			echo " <a class = 'lblPagination' href = '".$page_url."start=$x'>$i</a> ";
            }else{
            echo " <a style='background-color: #dd4b39; color:white;' class = 'lblPagination' href = '".$page_url."start=$x'><b>$i</b></a> ";
            }
            $i++;
          }

          $next = $start + $per_page;
          if($start < $record_count[0]['count'] - $per_page ){
			echo " <a class = 'lblPagination' href = '".$page_url."start=$next'>Next</a>";
          }
        }
		  		echo "</div>";
      }
	  echo "</br></br></br>";
	  echo "</div>";
	  echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
      echo "</body>";
    }
  }
?>
</html>