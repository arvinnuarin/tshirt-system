<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) && isset($_SESSION['name'])) {
			if(isset($_GET['product_design_id'])){
				if($_GET['product_design_id'] !== ""){
					require_once("../../php/awp.php");
					$awp = new Awp();
					$product_design_id = $_GET['product_design_id'];
					echo "<head>";
					?>
						<link rel = "stylesheet" type="text/css" href="add_clothing_assoc.css">
						<link rel = "stylesheet" type="text/css" href="../../css/loading.css">
						<link rel = 'stylesheet' href = "../../css/popup.css" type = "text/css">
						<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
						 <script type = "text/javascript" src = "../../js/loading.js"></script>
						<script type = "text/javascript" src = "add_clothing_assoc.js"></script>
						<script type = "text/javascript" src = "../../js/popup_0.js"></script>
						<title></title>
					
					<?php
					require_once('../setup/setup_form.php');
					echo "</head>";
					echo "<body>";

					$setup = new SetUp($_SESSION['position']);
					echo "<ul class = 'topMenu'>";
					echo "<li><a class = 'active' href = 'shirt_design/clothing_design_type_form.php'>Printing Method</a></li>";
					echo "<li><a href = 'shirt_design/'>Clothing Design</a></li>";
					echo "</ul>";

					$product_design = $awp -> get_product_design_name_by_id($product_design_id);
		        	echo "<h1>Add Clothing Color for Product Design <b style = 'color: #d73925';>".$product_design[0]['name']."</b></h1>";
		        	echo "<input id = 'pdName' type = 'hidden' value = '".$product_design[0]['name']."'>";
					echo "<br>";
					echo "<div id = 'divAddType'>";
					echo "<label id = 'lblAdd'>Add Clothing Color</label>";
					echo "<div id = 'divline'></div>";
    				echo "<br><br>"; 
					$clothing = $awp -> get_unlisted_clothing($product_design_id);
					echo "<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Select: ";
					echo "<select id = 'selClothing'>";
					echo "<option>Select Clothing</option>";
					foreach ($clothing as $c) {
						echo "<option value = '".$c['clothing_id']."'>".$c['name']."</option>";
					}
					echo "</select>";

					echo "<label id = 'lblWarnClothing'></label>";
					
				 	echo "<br><br>"; 
					echo "<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Select: ";
					echo "<select id = 'selColor'>";
					echo "<option>Select Color</option>";
					echo "</select>";

					echo "<br />";

					echo "<label id = 'lblWarnColor'></label>";

					echo "<br />";

					echo "<input id = 'btnAdd' type = 'button' value = 'Add Color for Product Design' />";

					echo "<input type = 'hidden' value = '".$product_design_id."' id = 'pdId'>";

					echo "</div>";
					echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
					echo "</body>";
				}else{
					echo "<script>window.location.replace('../shirt_design');</script>";
				}
			}else{
				echo "<script>window.location.replace('../shirt_design');</script>";
			}
			
		}
	}
	
?>

</html>