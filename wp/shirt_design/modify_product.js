$(document).ready(function(){
    $(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	var side = ['Front', 'Back'];

	$('#btnModifyProduct').click(function() {
		var ctr = 0;
		$('#lblWarnST').html("");
		$('#lblWarnProdName').html("");
		$('#lblWarnProdDesc').html("");
		$("#lblWarnPic").html("");
		$("#lblWarnCat").html("");

		if($('#txtProdName').val()==""){
			$('#lblWarnProdName').html("Empty Product Name");
		}else{
			ctr += 1;
		}
		if($('#txtProdDesc').val()==""){
			$('#lblWarnProdDesc').html("Empty Product Description");
		}else{
			ctr += 1;
		}

		var imageData = [];

		for(var x = 0; x < side.length; x++){
			var idpic = "#pic"+side[x];
			if($(idpic).length){
				if($(idpic+" .cropit-preview-image").attr('src')){
					$(".lblSideFront").html("");
					if($(idpic).cropit('export')){
						imageData.push($(idpic).cropit('export'));
						ctr += 1;
					}
				}
				else{
					$("#lblWarn"+side[x]).html("Empty "+side[x]+"!");
				}
			}
		}

		
		if($("#selProdCat :selected").attr('value')){
			ctr += 1;
		}else{
			$("#lblWarnCat").html("Please Choose a Category!");
		}

		if(ctr >=  4 && ctr <= 5 ){

			loading();
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("shirt_design/modify_product.php", {catId:$("#selProdCat :selected").val(), prodName: $.trim($("#txtProdName").val()), prodDesc:$.trim($("#txtProdDesc").val()), date:currentDate,
					imageData:imageData, date:currentDate}, function(data){
				if(data){
					$(".back").remove();
					if(data == "Product Exist"){
						popupOk("Warning", "Clothing Design already Exist!");
					}else if(data.indexOf("Success") != -1){
						var x = data.split("#"); 
						popupOk("Clothing Design", "Successfully Added!");
						$('.popup-ok').click(function(){
							window.location.replace("shirt_design/?start="+x[1]);
						});
					}else{
						popupOk("Error", "Something went wrong!");
					}
				}
			});
		}
    });

	$(".btnClear").click(function(){
		var id  = "#"+$(this).parent().prop('id');
		if($(id+" .cropit-preview-image")){
			$(id+" .cropit-preview-image").removeAttr('src');
			$(id+" .cropit-preview").removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
			$(id+" .cropit-image-input")[0].value = null;
		}
	});

    $(".image-editor").cropit({
		quality: .9,
		originalSize: true,
		exportZoom: 1.00,
		allowDragNDrop: true,
		onImageLoaded:function(){
			for(var x = 0; x < side.length; x++){
				var img = $("#pic"+side[x]).find('.cropit-preview-image');

				if((img.outerHeight() > 550 || img.outerWidth() < 550) && img.attr('src')){
					
					$(img).removeAttr('src');
					$("#pic"+side[x]).find('.cropit-preview').removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
					$("#pic"+side[x]).find(" .cropit-image-input")[0].value = null;
					popupOk("Warning", "The height must be equal to 550px and the width must be equal to 550px.");
				}
			}
		},
		onImageError:function(){
			popupOk("Warning", "The height must be equal to 550px and the width must be equal to 550px.");
		}
	});

});