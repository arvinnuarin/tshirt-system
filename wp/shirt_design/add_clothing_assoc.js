$(document).ready(function(){
	$(".tabTshirtDesign").css("background-color", "#2c3b41");
    $(".tabTshirtDesign").css("color","white");
    $(".tabTshirtDesign").css("border-left","2px solid #e04545");
	$("#selClothing").on("change", function(){
		if($(this, ":selected").html() != "Select Clothing"){
			$.post("shirt_design/add_clothing_assoc.php", {clothingId:$(this).val(), pdId:$("#pdId").val(), stat:"getColor"}, function(data){
				$("#selColor").html($.parseHTML("<option>Select Color</option>"+data));
			});
		}else{
			$("#selColor").html($.parseHTML("<option>Select Color</option>"));
		}
	});

	$("#btnAdd").click(function(){
		if($("#selClothing :selected").html() == "Select Clothing")
			$("#lblWarnClothing").html("Please Choose a Clothing!");
		if($("#selColor :selected").html() == "Select Color")
			$("#lblWarnColor").html("Please Choose a Color!");
		else {
			loading();
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("shirt_design/add_clothing_assoc.php", {colorId:$("#selColor :selected").val(), pdId:$("#pdId").val(), stat:"addProd", date:currentDate}, 
				function(data){
				if(data){
					$(".back").remove();
					if(data.indexOf("Success") != -1){
						popupOk("Add Dedicated Color", "New Color Successfully Added for Product Design "+$("#pdName").val()+"!");
						$(".popup-ok").click(function(){
							var x = data.split("#");
							window.location.replace("../wp/t-shirt/view_clothing_color.php?clothing_id="+$("#selClothing").val()+"&product_design_id="+$("#pdId").val()+"&start="+x[1]);
						});

					}else{
						popupOk("Error", "Something went wrong!");
					}
				}
			});
		}		
	});
});