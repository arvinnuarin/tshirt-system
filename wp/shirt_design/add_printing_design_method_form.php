<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['position'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) && isset($_SESSION['name'])) {
			if(isset($_GET['product_design_id'])){
				if($_GET['product_design_id'] !== ""){
					require_once("../../php/awp.php");
					$awp = new Awp();
					$product_design_id = $_GET['product_design_id'];
					$product_design = $awp -> get_product_design_name_by_id($product_design_id);
					
					echo "<head>";
					?>
					<head>
						<link rel = 'stylesheet' href = "add_printing_design_method.css" type = "text/css">
						<link rel = 'stylesheet' href = "../../css/popup.css" type = "text/css">
				        <link rel = "stylesheet" type="text/css" href="../../css/loading.css">
				        <script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script> 
				        <script type = "text/javascript" src = "../../js/popup_0.js"></script>
				        <script type = "text/javascript" src = "../../js/loading.js"></script>
				        <script type = "text/javascript" src = "add_printing_design_method.js"></script>
						<title></title>
					</head>
					<?php
					require_once('../setup/setup_form.php');
					echo "</head>";
					echo "<body>";
					$setup = new SetUp($_SESSION['position']);
					
					echo "<ul class = 'topMenu'>";
					echo "<li><a href = 'shirt_design/clothing_design_type_form.php'>Printing Method</a></li>";
					echo "<li><a class = 'active' href = 'shirt_design/'>Clothing Design</a></li>";
					echo "</ul>";

					echo "<h1>Add Printing Method  for Product Design <b style = 'color: #d73925';>".$product_design[0]['name']."</b></h1>";
					echo "<br>";
					echo "<div id = 'divAddDesign'>";
					echo "<div id = 'divline'></div>";
    				echo "<br><br>"; 
					echo "<input type = 'hidden' value = '".$product_design[0]['name']."' id = 'prodName'/>";
					$print_method = $awp -> get_all_unlist_print_method($product_design_id);
					echo "<input type = 'hidden' value = '".$product_design_id."' id = 'pdId' />";
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Printing Method: ";
					echo "<select id = 'selPm'><option value = '0'><i>Select Printing Method</i></option>";
					print_r($print_method);
					foreach ($print_method as $pm) {
						echo "<option pm-name = '".$pm['name']."' value = '".$pm['pm_id']."'>".$pm['name']."</option>";
					}
					echo "</select> ";

					echo "<br />";

					echo "<label id = 'lblWarnPM'></label>";

					echo "<br />";

					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Default Price: <input type = 'text' id = 'defPrice' />";

					echo "<br />";

					echo "<label id = 'lblWarnDefPrice'></label>";

					echo "<br />";

					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price Increment: <input type = 'text' id = 'priceInc'>";

					echo "<br />";

					echo "<label id = 'lblWarnPriceInc'></label>";

					echo "<input id = 'btnPm' type = 'button' value = 'Add Printing Method' />";

					echo "<br />";

					
					echo "</div>";
					echo "</body>";
				}else{
					echo "<script>window.location.replace('../shirt_design');</script>";
				}
			}else{
				echo "<script>window.location.replace('../shirt_design');</script>";
			}
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}

?>
			<div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>

</html>