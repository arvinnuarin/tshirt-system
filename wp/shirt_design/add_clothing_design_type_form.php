<html>
<?php  
session_start();
if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
    if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
    	echo "<head>";
    	?>
		<head>
			<link rel = 'stylesheet' href = "add_clothing_design.css" type = "text/css">
			<link rel = 'stylesheet' href = "../../css/popup.css" type = "text/css">
			<link rel = "stylesheet" type="text/css" href="../../css/loading.css">
			<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type = "text/javascript" src = "add_clothing_design_type.js"></script>
			<script type = "text/javascript" src = "../../js/popup_0.js"></script>
			<script type = "text/javascript" src = "../../js/loading.js"></script>
			<title>Printing Method</title>
		</head>
    	<?php
    	require_once('../setup/setup_form.php');
    	echo "</head>";
		
		echo "<body>";

		$setup = new SetUp($_SESSION['position']);
		echo "<ul class = 'topMenu'>";
		echo "<li><a class = 'active' href = 'shirt_design//clothing_design_type_form.php'>Printing Method</a></li>";
		echo "<li><a href = 'shirt_design/'>Clothing Design</a></li>";
		echo "</ul>";
		echo "<h1>Add Clothing Design Print Method</h1>";
		echo "<br>";
		echo "<div id = 'divAddType'>";
		echo "<label id = 'lblAdd'>Add Print Method</label>";
		echo "<div id = 'divline'></div>";
    	echo "<br><br>"; 
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Printing Type: <input type = 'text' id = 'txtPrintType'/>";
		echo "<br><label id = 'lblWarnPrintType'></label>";
		echo "<input type = 'button' value = 'Add Printing Method' id = 'btnPrintType'>";
		echo "</div>";
		echo "</body>";
    }
}

?>
		<div class="footer">Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>

</html>