<?php
	session_start();
	require_once('../../php/mixlarts.php');
	require_once('../../php/awp.php');
	$user = "employee";
	$mixlarts = new MixlArts();
	$awp = new Awp();
	$id = $_SESSION['id'];
	$code = $_SESSION['code'];
	$gen_code = $mixlarts -> get_cp_code($id, $user);
	$txtVerifyCode = $_POST['txtVerifyCode'];
	
	
	if(isset($_POST['txtChangeCP'])){
		if(!empty($_POST['txtChangeCP'])){
			$txtChangeCP = "+63".$_POST['txtChangeCP'];
			$mixlarts -> update_cn($id, $txtChangeCP, $user);
		}
	}else{
		$txtChangeCP = "";
	}

	if($txtVerifyCode=== $gen_code[0]['contact_no_ver']){
		if($mixlarts -> update_user_status($_SESSION['id'], "Fully Verified", $user) == 1){
			$result = $awp -> sub_log_in($_SESSION['id'], $_SESSION['code']);
			$_SESSION['name'] = $result[0]['uname'];
			$_SESSION['email'] = $result[0]['email'];
			$_SESSION['status'] = $result[0]['status'];
			$_SESSION['position'] = $result[0]['position'];
			$_SESSION['id'] = $result[0]['employee_id'];
			echo "Account Fully Verified!";
		}else{
			echo "Failed to Verified!";
		}
		
	}else{
		echo "Code doesn't match!";
	}
?>