$(document).ready(function(){
	if (typeof emailVerified !== 'undefined') {
	    window.location.replace("../verify_cn");
	}
	var x = "";
	$("#spanChangeCP").hide();
	$("#changeCP").click(function(){
		$(this).hide();
		$("#spanChangeCP").show();
	});

	$("#cancelCPChange").click(function(){
		$("#spanChangeCP").hide();
		$("#changeCP").show();
		$("#txtChangeCP").val("");
		$("#lblWarnChangeCP").html("");
	});

	$("#txtChangeCP").on("input propertychange",function(){

		$(this).val($(this).val().replace(/\D/g,''));
		if($(this).val().length > 10){
			$(this).val($(this).val().slice(0,10));
		}
		
	});

	$("#btnVerifyCP").click(function(){

		if($("#spanChangeCP").css('display') == 'block'){
			if($("#txtChangeCP").val() == ""){
				$("#lblWarnChangeCP").html("Empty CP!");
			}else{
				if($("#txtVerifyCode").val() == ""){
					$("#lblWarnVerifyCode").html("Empty CP Address!");
				}else if($('#txtVerifyCode').val().length > 10 && $('#txtChangeCP').val().length <10){
					$("#lblWarnVerifyCode").html("Invalid CP Address!");
				}
				else{
					$.post("../acc_cn_auth/acc_auth.php",{txtChangeCP: $("#txtChangeCP").val(), txtVerifyCode:$.trim($('#txtVerifyCode').val())},
			  		function(data){
			  			popupOk(data);
			  			$(".popup-ok").click(function(){
					        if(data == "Account Fully Verified!"){
					        	window.location.replace("../verify_cn");
					        }
				    	});
			  		});
				}				
			}


		}else if($("#spanChangeCP").css('display') == 'none'){
			if($("#txtVerifyCode").val() == ""){
				$("#lblWarnVerifyCode").html("Empty Verification Code!");
			}else if($('#txtVerifyCode').val().length != 6){
				$("#lblWarnVerifyCode").html("Invalid Verification Code!");
			}else{
				$.post("../acc_cn_auth/acc_auth.php",{txtVerifyCode:$.trim($("#txtVerifyCode").val())},
		  		function(data){
			  			popupOk(data);
			  			$(".popup-ok").click(function(){
					        if(data == "Account Fully Verified!"){
					        	window.location.replace("../verify_cn");
					        }
				    	});
		  		});
			}
		}

	});
	

});

