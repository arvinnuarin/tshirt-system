<html>
	<head>
		<link rel = 'stylesheet' href = "/MixlArts/css/popup.css">
		<link rel = 'stylesheet' href = "acc_auth.css">
		<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
		<script type = "text/javascript" src = "/MixlArts/js/mixlarts.js"></script>
		<script type = "text/javascript" src = "/MixlArts/js/popup_0.js"></script>
		<script type = "text/javascript" src = "acc_auth.js"></script>
	</head>
	<body style = "background-color:#d8cfc0;">
		<div id = "bg1"></div>
		<img id = "IconPhone" src = "Icons/IconPhone.png">
		<img id = "IconPhones" src = "Icons/IconPhones.png">
		<label id = "lblCPV">Cellphone Number Verification</label>
		<label id = "lblCCN">Your cellphone number is:</label> 
		<div id = "Cpnumber">
		<?php 
			require_once('../../php/mixlarts.php');
			session_start();
			$mixlarts = new MixlArts(); 
			if(isset($_SESSION['email'])){
				$user = "employee";
				$cp = $mixlarts -> get_cp($_SESSION['id'], $user);
				print_r($cp);
				echo $cp[0]['contact_no'];
				$sc = $mixlarts -> send_code($_SESSION['id'], $user);
				if($mixlarts -> get_user_stat($_SESSION['id'], "Fully Verified", $user)){
					echo "<script type = 'text/javascript'>var emailVerified = true;</script>";
				}
			}
		?></div>
		<button id = 'changeCP'>Change your cellphone number</button><br />
		<span id = 'spanChangeCP'>
			<label id = "lblChange">+63 </label>
			<input type = 'text' id = 'txtChangeCP' placeholder = "Enter new cellphone number">
			<label id = 'lblWarnChangeCP'></label>
			<button id = 'cancelCPChange'>Cancel</button>
		</span>
		<label id = "lblPlease">Please enter the code that is sent to your phone for cellphone verification.</label>
		<label id = "lblVerification">Enter verification code </label>

		<input type = 'text' id = 'txtVerifyCode' placeholder = "******"> 
		<label id = 'lblWarnVerifyCode'></label>
		<button id = 'btnVerifyCP'>Verify</button>
		
		<label id = "lblResend">Didn't get your code? Sometimes it can take up to 10 minutes. If it's been longer than that,</label>
		<a id = "btnResend" href='../acc_cn_auth'>try again.</a>
		
	</body>
</html>