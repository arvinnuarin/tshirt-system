window.onload = move;
        
window.onpageshow = function(evt) { if (evt.persisted) window.history.forward();}

function move() {
    var elem = document.getElementById("successBar"); 
    var width = 1;
    var id = setInterval(frame, 50);
    function frame() {
        if (width >= 100) {
            clearInterval(id);
            window.location.replace("../2fa");
        } else {
            width++; 
            elem.style.width = width + '%'; 
        }
    }
}