<html>
<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){	
			echo "<head>";
			?>
			<title>Home Banner</title>
			<link rel = 'stylesheet' href = "edit_main_category.css">
			<link rel = 'stylesheet' href = "/MixlArts/css/upload_photo.css">
			<link rel="stylesheet" type="text/css" href="../../css/loading.css">
			<link rel="stylesheet" type="text/css" href="../../css/popup.css">
			<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type="text/javascript" src = "../../js/loading.js"></script>
			<script type="text/javascript" src = "../../js/popup_0.js"></script>
			<script type = "text/javascript" src = '../../js/cropit-master/dist/jquery.cropit.js'></script>
			<script type = "text/javascript" src = "edit_main_category.js"></script>
			<?php
			require_once('../../php/mixlarts.php');
			require_once('../../php/awp.php');
			require_once('../setup/setup_form.php');
			if(isset($_GET['image'])){
				if(!empty($_GET['image'])){
					if(is_numeric($_GET['image'])){
						$image = $_GET['image'];
						echo "<script type = 'text/javascript'>var hb = '../cms/main_category_banner/$image.png'; </script>";
					}
				}
			}	
			echo "</head>";

			echo "<body>";
			$setup = new SetUp($_SESSION['position']);
			$arrangement = array("Boy", "Girl", "Men", "Women");
			echo "<h1>Edit \"".$arrangement[$image-1]."\" Main Category Banner</h1>";

			echo "<br />";
			echo "<div id = 'divAddEdit'>";
			echo "<div class = 'image-editor'>";
			echo "<br />";
			echo "<label id = 'lblHB'>Edit Main Category Banner:</label>";
			echo "<button class = 'btnClear'><span id ='iconClear'></span>Clear</button>";
			echo "<div class = 'cropit-preview'  style = 'height:500px; width:500px'></div>";
			echo "<div id = 'divTool'>";
			echo "<input type = 'range' class = 'cropit-image-zoom-input'>";
			echo "<input type ='image' class = 'rotate-ccw' src ='banner/Icons/ccs.png'>";
			echo "<input type ='image' class = 'rotate-cw' src ='banner/Icons/cs.png'><br />";
			echo "</div>";
			echo "<div class='fileUpload btn btn-primary'>";
    		echo "<span id ='iconUpload'>&nbsp;&nbsp;Choose a file...</span>";
			echo "<input type = 'file' class='cropit-image-input'/>";
			echo "</div>";
			echo "</div>";
			echo "<br />";
			echo "<input type = 'button' value='Edit Main Category Banner' id = 'btnModifyBanner'/>";
			echo "<br></br>";
			echo "<label id = 'lblBanner'></label>";
			echo "</div>";
			echo "<input type = 'hidden' value = '$image' id = 'bannerName'>";
			echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
			echo "</body>";		
		}else{
			echo "<script>window.location.replace('../../wp/');</script>";
			session_destroy();
		}
	}else{
		echo "<script>window.location.replace('../../wp/');</script>";
		session_destroy();
	}

?>
</html>