$(document).ready(function(){
	var prev = $('.image-editor').cropit('export');
    $(".tabHomeBanner").css("background-color", "#2c3b41");
    $(".tabHomeBanner").css("color","white");
    $(".tabHomeBanner").css("border-left","2px solid #e04545");
    
if(typeof hb === "undefined"){
	hb = "";
}
$(".btnClear").click(function(){
      if($(".cropit-preview-image")){
        $(".cropit-preview-image").removeAttr('src');
        $(".cropit-image-zoom-input").val("0.5");
        $(".cropit-preview").removeClass('cropit-preview cropit-image-loaded').addClass('cropit-preview');
        $(".cropit-image-input")[0].value = null;
      }
    });

    $('.image-editor').cropit({
	quality: .9,
    	originalSize: true,
    	exportZoom: 1.00,
    	allowDragNDrop: true,
	    imageState: {
	        src: hb,
	},
	onImageError:function(){
	        popupOk("Warning", "Height must be 250 px or higher and width must be 1000px or higher!");
	}
});

$('.rotate-cw').click(function() {
	$('.image-editor').cropit('rotateCW');
});

$('.rotate-ccw').click(function() {
    $('.image-editor').cropit('rotateCCW');
});

$("#btnModifyBanner").click(function(){
loading();
if($(".cropit-preview-image").attr('src')){
      if($(".cropit-preview-image")){
        $('#lblBanner').html("");
        	if(prev !== $('.image-editor').cropit('export')){
	        	$.post('banner/home_banner.php',{bannerName:$("#bannerName").val(), banner:$('.image-editor').cropit('export')}, function(data){
		        	if(data){
		        		$(".back").remove();
					if(data == "Success"){
						popupOk("Home Banner", "Successfully Updated!");
						$(".popup-ok").click(function(){
							window.location.replace("banner");
						});
			        	}else if(data == "Failed"){
							popupOk("Error", "Home Banner Failed to Update!");
			        	}else if(data == "User not set" || data == "User is empty"){
							popupOk("Notice", "You Need to Log In First!");
							$(".popup-ok").click(function(){
								window.location.replace("../../wp");
							});
			        	}else{
			        		popupOk("Error", "Something went wrong!");
			        	}
		        	}
		        });
        	}  
      }
    }else{
      $('#lblBanner').html("Empty Home Banner!");
      $(".back").remove();
    }
});
});
