<html>
<?php
session_start();
if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
	if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
	echo "<head>";
	?>
	    <title> Home Banner </title>
		<link rel = "stylesheet" href = 'home_banner.css'>
		<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
		<script type = "text/javascript" src = 'index.js'></script>
	<?php
		require_once('../setup/setup_form.php');
		echo "</head>";
		echo "<body>";
		require_once('../../php/awp.php');
		$setup = new SetUp($_SESSION['position']);
		echo "<ul class = 'topMenu'>";
		echo "<li class = 'active'><a href = 'banner/'>Home Banner</a></li>";
		echo "<li><a href = 'banner/main_category_banner_form.php'>Main Category Banner</a></li>";
		echo "</ul>";
		
		echo "<h1>Home Banners</h1>";
		
		echo "<table>";
		echo "<th>Image</th><th>Edit Home Banner </th>";
		for($x = 1; $x <= 5; $x++){
			echo "<tr>";
			echo "<td><img src = '../cms/home_banner/$x.png' style = 'height:50px;'/></td>";
			echo "<td><a id = 'btnEditHomeBanner' href = 'banner/home_banner_form.php?image=$x'><span id ='iconEdit'></span>Edit Home Banner</a></td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<div class='footer'>Copyright © 2016 <strong>MixlArts</strong>. All rights reserved.</div>";
		echo "</body>";
	}else{
		echo "<script>window.location.replace('../../wp/');</script>";
		session_destroy();
	}
}else{
	echo "<script>window.location.replace('../../wp/');</script>";
	session_destroy();
}
?>

</html>
