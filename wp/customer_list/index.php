<?php
	session_start();
	if(isset($_SESSION['position']) && isset($_SESSION['email']) && isset($_SESSION['id']) && isset($_SESSION['name'])){
		if(!empty($_SESSION['position']) && !empty($_SESSION['email']) && !empty($_SESSION['id']) &&    isset($_SESSION['name'])){
			echo "<head>";
			?>
			<title>Customer List</title>
			<link rel="stylesheet" type="text/css" href="../../css/loading.css">
			<link rel="stylesheet" type="text/css" href="../../css/popup.css">
			<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
			<script type = "text/javascript" src = "../../js/popup_0.js"></script>
			<script type = "text/javascript" src = "../../js/loading.js"></script>
			<script type = "text/javascript" src = "customer_list.js"></script>
			<?php
			require_once('../setup/setup_form.php');
			echo "</head>";
			echo "<body>";
			//$setup = new SetUp($_SESSION['position']);
			require_once("../../php/awp.php");
			$awp = new Awp();

			if(isset($_GET['start'])){
				if(!empty($_GET['start'])){
					if(is_numeric($_GET['start'])){
						$start = intval($_GET['start']);
					}else{
						$start = 0;
					}
				}else{
					$start = 0;
				}
			}else{
				$start = 0;
			}
			$per_page = 10;

			$filters = array("customer.customer_id", "customer.fname", "customer.lname", "customer.email", "customer.contact_number", "customer.home_add", "customer.brgy", "customer.town", "customer.province");

			if(isset($_GET['search']) && isset($_GET['filter'])){
				if($_GET['search'] !== "" && $_GET['filter'] !== ""){
					$search = $_GET['search'];
					$filter_id = $_GET['filter'];
					if($filter_id >= 0 && $filter_id <= 4){
						for ($x = 0; $x < count($filters); $x++) {
							if($filter_id == $x){
								$filter = $filters[$x];
							}
						}
					}else{
						$search = "";
						$filter = "customer.customer_id";
						$filter_id = -1;
					}
				}else{
					$search = "";
					$filter = "customer.customer_id";
					$filter_id = -1;
				}
			}else{
				$search = "";
				$filter = "customer.customer_id";
				$filter_id = -1;
			}

			$sort_trans = array(array("id_asc", "customer_id", "asc"), array("id_desc", "customer_id", "desc"), array("fn_asc", "fname", "asc"), array("fn_desc", "fname", "desc"), array("ln_asc", "lname", "asc"), array("ln_desc", "lname", "desc"), array("em_asc", "email", "asc"), array("em_desc", "email", "desc"), array("home_asc", "home_add", "asc"), array("home_desc", "home", "desc"), array("brgy_asc", "brgy", "asc"), array("brgy_desc", "brgy", "desc"), array("town_asc", "town", "asc"), array("town_desc", "town", "desc"), array("prov_asc", "prov", "asc"), array("prov_desc", "prov", "desc"));


			foreach ($sort_trans as $st) {
				if(isset($_GET['sort'])){
					if(!empty($_GET['sort'])){
						$ctr = 0;
						if($st[0] == $_GET['sort']){
							$sort = $_GET['sort'];
							$col_order = $st[1];
							$order = $st[2];
							break;
						}else{
							$ctr += 1;
						}
						
						if(count($sort_trans) == $ctr){
							$sort = "";
							$col_order = "customer_id";
							$order = "asc";
						}
					}else{
						$sort = "";
						$col_order = "customer_id";
						$order = "asc";
					}
				}else{
					$sort = "";
					$col_order = "customer_id";
					$order = "asc";
				}
			}

			$per_page = 10;
			$customer_list = $awp -> get_customer($start, $per_page, $filter, $search);
			$record_count = $awp -> get_customer_count($filter, $search);

			
			echo "<h1>Customer List</h1>";

			$arr_sort = array(array("ID (Lowest to Highest)", "id_asc"), array("ID (Highest to Lowest)", "id_desc"), array("First Name (A-Z)", "fn_asc"), array("First Name (Z-A)", "fn_desc"), array("Last Name (A-Z)", "ln_asc"), array("Last Name (Z-A)", "ln_desc"), array("Email (A-Z)", "em_asc"), array("Email (Z-A)", "em_desc"), array("Home Address (A-Z)", "home_asc"), array("Home Address (Z-A)", "home_desc"), array("Brgy (A-Z)", "brgy_asc"), array("Brgy (Z-A)", "brgy_desc"), array("Town (A-Z)", "town_asc"), array("Town (Z-A)", "town_desc"), array("Province (A-Z)", "prov_asc"), array("Province (Z-A)", "prov_desc"));
			
			echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search via: ";
			$select = array("ID", "First Name", "Last Name", "Email", "Contact Number", "Home Address", "Brgy", "Town", "Province");
			echo "<select id = 'selSearch'>";
			for($x = 0; $x < count($select); $x++) {
				echo "<option value = '$x'"; 
				if($x == $filter_id){
					echo " selected ";
				}
				echo ">".$select[$x]."</option>";
			}
			echo "</select> ";
			echo "<input type = 'text' id = 'txtSearch' value = '$search'/> <input type = 'button' value = 'Search' id = 'btnSearch' />";
			
			echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort: <select id = 'selSort'>";	
			foreach($arr_sort as $s){
				$filter_url = "//www.mixlarts.com/MixlArts/wp/customer_list/?";
				if($search != ""){
					$filter_url .= "search=$search&";
				}
				if($start !== ""){
					$filter_url .= "start=$start&";
				}
				if($status !== "")
					$filter_url .= "$status&";
				$filter_url .= "sort=".$s[1];
				echo "<option ";
				if($sort == $s[1]){
					echo "selected";
				}
				echo " value = '".$filter_url."'>".$s[0]."</option>";
			}
			echo "</select>";

			if(count($customer_list) > 0){
				$header = array("ID", "First Name", "Last Name", "Email", "Contact Number", "Home Address", "Brgy", "Town", "Province", "Registration Type");
				echo "<table>";
				foreach ($header as $h) {
					echo "<th>$h</th>";
				}
				foreach ($customer_list as $cl){
					echo "<tr>";
					echo "<td>".$cl['customer_id']."</td>";
					echo "<td>".$cl['fname']."</td>";
					echo "<td>".$cl['lname']."</td>";
					echo "<td>".$cl['email']."</td>";
					echo "<td>+63".$cl['contact_number']."</td>";
					echo "<td>".$cl['home_add']."</td>";
					echo "<td>".$cl['brgy']."</td>";
					echo "<td>".$cl['town']."</td>";
					echo "<td>".$cl['province']."</td>";
					echo "<td>".strtoupper($cl['type'])."</td>";
					echo "</tr>";
				}
				echo "</table>";
			}else{
				echo "Empty Raw Materials!";
			}
			
			if($record_count[0]['count'] > 10){
				$page_url = "";

				if($search != ""){
					$page_url .= "&search=$search";
				}

				if($sort != ""){
					$page_url .= "&sort=$sort";
				}

				$page_url = "wp/customer_list/?".$page_url."&";

				$prev = $start - $per_page;
				if($start > 0){
					echo "<a href = '".$page_url."start=$prev'>Prev</a>";
				}

				$i = 1;
				for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
					if($start != $x){
						echo " <a href = '".$page_url."start=$x'>$i</a> ";
					}else{
						echo " <a href = '".$page_url."start=$x'><b>$i</b></a> ";
					}
					$i++;
				}

				$next = $start + $per_page;
				if($start < $record_count[0]['count'] - $per_page ){
					echo " <a href = '".$page_url."start=$next'>Next</a>";
				}
			}
			echo "</body>";
		}else{
			session_destroy();
			echo "<script>window.location.replace('../../wp/');</script>";
		}
	}else{
		session_destroy();
		echo "<script>window.location.replace('../../wp/');</script>";
	}

?>