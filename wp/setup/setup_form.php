<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="//www.mixlarts.com/MixlArts/wp/">
	<link rel = 'stylesheet' href = "setup/setup.css" type = "text/css">
</head>
<body style = "background-color:#ecf0f5;">

	<?php
		class SetUp{
			public function __construct($position){
				$this -> for_admin();
			}

			private function for_admin(){
				echo "<ul class='sidenav'>";
				echo "<li class = 'main'>MIXLARTS ADMIN</li>";
				echo "<li class = 'tabLinks'><a class = 'tabHome' href = 'home'><span id ='iconHome'></span>Home</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabCat' href = 'category'><span id ='iconCat'></span>Category</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabHomeBanner' href = 'banner'><span id ='iconHomeBanner'>
				</span>Banners</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabDesign'href = '../create/wp/manager'><span id ='iconCustom'></span>Customize T-shirt</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabTshirtDesign'href = 'shirt_design'><span id ='iconShirtDesign'></span>Clothing Design</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabTshirt' href = 't-shirt'><span id ='iconShirt'></span>Clothing</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabRaw' href = 'raw_material'><span id ='iconRawMaterials'></span>Raw Materials</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabCusList' href = 'customer_List'><span id ='iconSupplier'></span>Customer List</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabShip' href = 'shipping'><span id ='iconSupplier'></span>Shipping Courier</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabCusTransaction'href = 'orders/?status=PAID'><span id ='iconCustomerTransaction'></span>Customer Transaction</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabLogs'href = 'logs'><span id ='iconActivityLog'></span>Activity Log</a></li>";
				echo "<li class = 'tabLinks'><a class = 'tabReports'href = 'reports'><span id ='iconReports'></span>Reports</a></li>";
				echo "</ul>";
			}
			private function for_web_personnel(){
			}
		}
	?>
</body>
