<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Oops! 403 Forbidden</title>
    <base href="//www.mixlarts.com/MixlArts/errordocs/403/">
	<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <link rel="stylesheet" href="index.css" type="text/css">
</head>
<body>

	<?php 
		require_once("../../php/customer.php"); 
		require_once('../../header/header.php');
		
	?>
	<div id='container'></div>
    <div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
            
            <div id = 'forbcontainer'>
                <div id = 'imgcontainer'><img id = 'imgforbidden' src = '//www.mixlarts.com/MixlArts/errordocs/403/forbidden.png'/></div>
                <div id = 'lblcontainer' ><label><span>403</span><br>ACCESS<br>FORBIDDEN</label></div>
            </div>
        </div>
	</div>
	<?php include '../../header/footer.php';?>
</body>
</html>