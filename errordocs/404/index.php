<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>Oops! 404 Not Found</title>
	<base href="//www.mixlarts.com/MixlArts/errordocs/404/">
	<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
	<link rel="stylesheet" href="index.css" type="text/css">
    
</head>
<body>
   <?php include '../../header/header.php';?>
    <div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
            
            <div id = 'forbcontainer'>
                <div id = 'imgcontainer'><img id = 'img404' src = '404.png'/></div>
                <div id = 'lblcontainer' ><label><span>404</span><br>Page<br>Not Found</label></div>
            </div>
        </div>
	</div>
   <?php include '../../header/footer.php';?>
</body>
</html>