-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2016 at 08:30 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbmixlarts`
--
CREATE DATABASE IF NOT EXISTS `dbmixlarts` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbmixlarts`;

-- --------------------------------------------------------

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
CREATE TABLE IF NOT EXISTS `address_book` (
  `ab_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `home_add` varchar(75) NOT NULL,
  `brgy` varchar(75) NOT NULL,
  `town` varchar(75) NOT NULL,
  `province` varchar(75) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`ab_id`),
  KEY `ab_customer` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address_book`
--

INSERT INTO `address_book` (`ab_id`, `fname`, `lname`, `contact_number`, `home_add`, `brgy`, `town`, `province`, `customer_id`, `status`) VALUES
(1, 'Mio', 'Villena', '09069136165', '#40 Network Ave. Meralco Village ', 'LIAS', 'MARILAO', 'BULACAN', 1, 'PRIMARY'),
(3, 'Bill', 'Maclang', '9359445057', '24 De Agosto Street', 'Caingin', 'CITY OF MALOLOS (Capital)', 'BULACAN', 3, 'PRIMARY'),
(4, 'John Arvin', 'Nuarin', '9054528253', '#12 Daisy St. Lhinette Homes', 'Saog', 'MARILAO', 'BULACAN', 4, 'PRIMARY');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `customer_cart` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `date`, `customer_id`, `status`) VALUES
(1, '2016-11-21 02:51:09', 3, 'PAID'),
(2, '2016-11-22 09:23:52', 4, 'PAID'),
(3, '2016-11-22 10:03:50', 4, 'PENDING'),
(4, '2016-11-22 11:12:40', 3, 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_cat_name` varchar(50) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `sub_cat_name`, `cat_name`) VALUES
(4, 'Custom', ''),
(6, 'Anime', 'Boys'),
(13, 'Simple', 'Men'),
(14, 'Building', 'Men');

-- --------------------------------------------------------

--
-- Table structure for table `clothing`
--

DROP TABLE IF EXISTS `clothing`;
CREATE TABLE IF NOT EXISTS `clothing` (
  `clothing_id` int(11) NOT NULL AUTO_INCREMENT,
  `cb_id` int(11) NOT NULL,
  `ct_id` int(11) NOT NULL,
  `cf_id` int(11) NOT NULL,
  PRIMARY KEY (`clothing_id`),
  KEY `clothing_cb_fk` (`cb_id`),
  KEY `clothing_cf_fk` (`cf_id`),
  KEY `ct_clothing_fk` (`ct_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing`
--

INSERT INTO `clothing` (`clothing_id`, `cb_id`, `ct_id`, `cf_id`) VALUES
(5, 1, 1, 1),
(6, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `clothing_brand`
--

DROP TABLE IF EXISTS `clothing_brand`;
CREATE TABLE IF NOT EXISTS `clothing_brand` (
  `cb_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(100) NOT NULL,
  PRIMARY KEY (`cb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing_brand`
--

INSERT INTO `clothing_brand` (`cb_id`, `brand_name`) VALUES
(1, 'Blue Corner'),
(2, 'Kentucky'),
(3, 'Bench');

-- --------------------------------------------------------

--
-- Table structure for table `clothing_color`
--

DROP TABLE IF EXISTS `clothing_color`;
CREATE TABLE IF NOT EXISTS `clothing_color` (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(50) NOT NULL,
  `hex` varchar(10) NOT NULL,
  `clothing_id` int(11) NOT NULL,
  PRIMARY KEY (`sc_id`),
  KEY `sc_fk` (`clothing_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing_color`
--

INSERT INTO `clothing_color` (`sc_id`, `color`, `hex`, `clothing_id`) VALUES
(1, 'Black', '000000', 5),
(2, 'White', 'FFFFFF', 5);

-- --------------------------------------------------------

--
-- Table structure for table `clothing_fabric`
--

DROP TABLE IF EXISTS `clothing_fabric`;
CREATE TABLE IF NOT EXISTS `clothing_fabric` (
  `cf_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`cf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing_fabric`
--

INSERT INTO `clothing_fabric` (`cf_id`, `name`) VALUES
(1, 'Cotton'),
(2, 'Wool');

-- --------------------------------------------------------

--
-- Table structure for table `clothing_size_desc`
--

DROP TABLE IF EXISTS `clothing_size_desc`;
CREATE TABLE IF NOT EXISTS `clothing_size_desc` (
  `ssd_id` int(11) NOT NULL AUTO_INCREMENT,
  `size` int(11) NOT NULL,
  `height` double NOT NULL,
  `width` double NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `clothing_id` int(11) NOT NULL,
  PRIMARY KEY (`ssd_id`),
  KEY `ssd_fk` (`clothing_id`),
  KEY `ss_fk` (`size`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing_size_desc`
--

INSERT INTO `clothing_size_desc` (`ssd_id`, `size`, `height`, `width`, `price`, `clothing_id`) VALUES
(21, 0, 1, 2, '3', 5),
(22, 1, 4, 5, '6', 5),
(23, 2, 7, 8, '9', 5),
(24, 3, 10, 11, '12', 5),
(25, 4, 13, 14, '15', 5),
(26, 5, 16, 17, '18', 5),
(27, 6, 19, 20, '21', 5),
(28, 7, 22, 23, '24', 5),
(29, 8, 25, 26, '27', 5),
(30, 9, 28, 29, '30', 5),
(31, 0, 4, 4, '10', 6),
(32, 1, 5, 5, '10', 6),
(33, 2, 6, 6, '10', 6),
(34, 3, 7, 7, '10', 6),
(35, 4, 8, 8, '10', 6);

-- --------------------------------------------------------

--
-- Table structure for table `clothing_stock`
--

DROP TABLE IF EXISTS `clothing_stock`;
CREATE TABLE IF NOT EXISTS `clothing_stock` (
  `scs_id` int(11) NOT NULL AUTO_INCREMENT,
  `size` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `sc_id` int(11) NOT NULL,
  PRIMARY KEY (`scs_id`),
  KEY `scs_fk` (`sc_id`),
  KEY `sci_fk` (`size`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing_stock`
--

INSERT INTO `clothing_stock` (`scs_id`, `size`, `stock`, `sc_id`) VALUES
(1, 21, 100, 1),
(2, 22, 2, 1),
(3, 23, 3, 1),
(4, 24, 4, 1),
(5, 25, 5, 1),
(6, 21, 5, 2),
(7, 22, 23, 2),
(8, 23, 5, 2),
(9, 24, 4, 2),
(10, 25, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `clothing_type`
--

DROP TABLE IF EXISTS `clothing_type`;
CREATE TABLE IF NOT EXISTS `clothing_type` (
  `ct_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`ct_id`),
  UNIQUE KEY `ct_id` (`ct_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothing_type`
--

INSERT INTO `clothing_type` (`ct_id`, `type`) VALUES
(1, 'V Neck'),
(2, 'Round Neck');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL,
  `status` varchar(15) NOT NULL,
  `email_ver` varchar(100) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `email`, `password`, `lname`, `fname`, `type`, `status`, `email_ver`) VALUES
(1, 'bjmaclang@yahoo.com', '2b0351fe998c6e5b4d674eab37167bb7', 'Maclang', 'Bill Jefferson', '', 'Fully Verified', ''),
(3, 'bjmaclang@gmail.com', '2b0351fe998c6e5b4d674eab37167bb7', 'Maclang', 'Bill', '', 'Fully Verified', 'c183326933eea5c9048056756ca7abdc'),
(4, 'johnarvinnuarin@gmail.com', '', 'Nuarin', 'John Arvin', 'facebook', 'Fully Verified', ''),
(5, 'johnmarktan17@gmail.com', '', 'Tan', 'John Mark', 'google', 'Fully Verified', '');

-- --------------------------------------------------------

--
-- Table structure for table `draw_build`
--

DROP TABLE IF EXISTS `draw_build`;
CREATE TABLE IF NOT EXISTS `draw_build` (
  `build_no` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `pr_name` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL,
  `location` varchar(10000) NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ssd_id` int(11) NOT NULL,
  `sc_id` int(11) NOT NULL,
  `pm_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sht_qty` int(11) NOT NULL,
  `budget` double NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`build_no`),
  KEY `fk_build` (`customer_id`),
  KEY `blssd_fk` (`ssd_id`),
  KEY `blpm_fk` (`pm_id`),
  KEY `blcol_fk` (`sc_id`),
  KEY `prodfk_col` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `draw_build`
--

INSERT INTO `draw_build` (`build_no`, `customer_id`, `pr_name`, `type`, `location`, `status`, `date_created`, `date_modified`, `ssd_id`, `sc_id`, `pm_id`, `product_id`, `sht_qty`, `budget`, `price`) VALUES
(1, 3, 'test', 'folders', 'build/3/test/', 'PROCESSED', '2016-11-21 02:39:32', '2016-11-21 02:39:44', 21, 2, 1, 4, 1, 100, 200),
(3, 3, 'Subject', 'folders', 'build/3/Subject/', 'PROCESSED', '2016-11-21 21:43:14', '2016-11-21 21:43:28', 21, 2, 1, 6, 1, 100, 500),
(4, 3, 'Subject-suggest', 'folders', 'build/3/Subject-suggest/', 'PROCESSED', '2016-11-22 09:07:18', '2016-11-22 09:07:32', 21, 2, 1, 7, 1, 100, 100),
(5, 4, 'Cart', 'folders', 'build/4/Cart/', 'PAID', '2016-11-22 09:24:23', '2016-11-22 09:24:34', 21, 2, 1, 8, 5, 100, 200),
(6, 4, 'Cart-suggest', 'folders', 'build/4/Cart-suggest/', 'PROCESSED', '2016-11-22 09:25:01', '2016-11-22 09:26:24', 21, 2, 1, 9, 5, 100, 100),
(7, 4, 'Untitled_1', 'folders', 'build/4/Untitled_1/', 'RAW', '2016-11-22 10:09:01', '2016-11-22 10:09:37', 21, 2, 1, 10, 1, 100, 0),
(8, 1, 'Untitled_1', 'folders', 'build/1/Untitled_1/', 'PROCESSED', '2016-11-23 12:11:58', '2016-11-23 12:14:34', 21, 2, 1, 14, 1, 100, 200),
(9, 1, 'Untitled_2', 'folders', 'build/1/Untitled_2/', 'PROCESSED', '2016-11-23 12:14:49', '2016-11-23 12:15:19', 21, 2, 1, 15, 1, 100, 200),
(10, 1, 'Untitled_1-suggest', 'folders', 'build/1/Untitled_1-suggest/', 'PROCESSED', '2016-11-23 12:16:46', '2016-11-23 12:16:52', 21, 2, 1, 16, 1, 100, 100),
(11, 1, 'Untitled_2-suggest', 'folders', 'build/1/Untitled_2-suggest/', 'PROCESSED', '2016-11-23 12:17:38', '2016-11-23 12:17:50', 21, 2, 1, 17, 1, 100, 100),
(12, 1, 'ghfdfgd', 'folders', 'build/1/ghfdfgd/', 'PROCESSED', '2016-11-23 12:27:33', '2016-11-23 12:28:13', 21, 2, 1, 18, 1, 100, 500),
(13, 1, 'ghfdfgd-suggest', 'folders', 'build/1/ghfdfgd-suggest/', 'PROCESSED', '2016-11-23 12:28:51', '2016-11-23 12:28:58', 21, 2, 1, 19, 1, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `status` varchar(15) NOT NULL,
  `home_add` varchar(75) NOT NULL,
  `brgy` varchar(75) NOT NULL,
  `town` varchar(75) NOT NULL,
  `province` varchar(75) NOT NULL,
  `contact_no_ver` varchar(100) NOT NULL,
  `email_ver` varchar(100) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `fname`, `lname`, `position`, `email`, `password`, `contact_no`, `status`, `home_add`, `brgy`, `town`, `province`, `contact_no_ver`, `email_ver`) VALUES
(1, 'Bill Jefferson', 'Maclang', 'Admin', 'bjmaclang@yahoo.com', '2b0351fe998c6e5b4d674eab37167bb7', '639359445057', 'Fully Verified', '24 De Agosto Street ', 'Caingin', 'City of Malolos', 'Bulacan', 'c00ca9', 'c857766485bfaeff3720c4733ca6a53f');

-- --------------------------------------------------------

--
-- Table structure for table `employee_logs`
--

DROP TABLE IF EXISTS `employee_logs`;
CREATE TABLE IF NOT EXISTS `employee_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `employee_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `tbl_name` varchar(50) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `employee_logs` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_logs`
--

INSERT INTO `employee_logs` (`log_id`, `time`, `employee_id`, `description`, `entity_id`, `tbl_name`) VALUES
(1, '2016-11-05 02:12:55', 1, 'Add clothing brand "Blue Corner"', 1, 'clothing_brand'),
(2, '2016-11-05 02:13:09', 1, 'Add clothing fabric "V Neck"', 1, 'clothing_fabric'),
(3, '2016-11-05 02:14:30', 1, 'Add clothing type "V Neck"', 1, 'clothing_type'),
(4, '2016-11-05 02:44:34', 1, 'Add clothing information for brand Blue Corner(V Neck Cotton)', 1, 'clothing'),
(5, '2016-11-05 02:44:34', 1, 'Add size XXS Information for clothing Blue Corner(V Neck Cotton)', 1, 'clothing_size_desc'),
(6, '2016-11-05 02:44:34', 1, 'Add size XS Information for clothing Blue Corner(V Neck Cotton)', 2, 'clothing_size_desc'),
(7, '2016-11-05 02:44:34', 1, 'Add size S Information for clothing Blue Corner(V Neck Cotton)', 3, 'clothing_size_desc'),
(8, '2016-11-05 02:44:34', 1, 'Add size M Information for clothing Blue Corner(V Neck Cotton)', 4, 'clothing_size_desc'),
(9, '2016-11-05 02:44:34', 1, 'Add size L Information for clothing Blue Corner(V Neck Cotton)', 5, 'clothing_size_desc'),
(10, '2016-11-05 02:50:03', 1, 'Add clothing information for brand Blue Corner(V Neck Cotton)', 2, 'clothing'),
(11, '2016-11-05 02:50:03', 1, 'Add size XXS Information for clothing Blue Corner(V Neck Cotton)', 6, 'clothing_size_desc'),
(12, '2016-11-05 02:50:03', 1, 'Add size XS Information for clothing Blue Corner(V Neck Cotton)', 7, 'clothing_size_desc'),
(13, '2016-11-05 02:50:03', 1, 'Add size S Information for clothing Blue Corner(V Neck Cotton)', 8, 'clothing_size_desc'),
(14, '2016-11-05 02:50:03', 1, 'Add size M Information for clothing Blue Corner(V Neck Cotton)', 9, 'clothing_size_desc'),
(15, '2016-11-05 02:50:03', 1, 'Add size L Information for clothing Blue Corner(V Neck Cotton)', 10, 'clothing_size_desc'),
(16, '2016-11-05 02:56:57', 1, 'Add clothing information for brand Blue Corner(V Neck Cotton)', 3, 'clothing'),
(17, '2016-11-05 02:56:57', 1, 'Add size XXS Information for clothing Blue Corner(V Neck Cotton)', 11, 'clothing_size_desc'),
(18, '2016-11-05 02:56:57', 1, 'Add size XS Information for clothing Blue Corner(V Neck Cotton)', 12, 'clothing_size_desc'),
(19, '2016-11-05 02:56:57', 1, 'Add size S Information for clothing Blue Corner(V Neck Cotton)', 13, 'clothing_size_desc'),
(20, '2016-11-05 02:56:57', 1, 'Add size M Information for clothing Blue Corner(V Neck Cotton)', 14, 'clothing_size_desc'),
(21, '2016-11-05 02:56:57', 1, 'Add size L Information for clothing Blue Corner(V Neck Cotton)', 15, 'clothing_size_desc'),
(22, '2016-11-05 03:00:15', 1, 'Add clothing information for brand Blue Corner(V Neck Cotton)', 4, 'clothing'),
(23, '2016-11-05 03:00:15', 1, 'Add size XXS Information for clothing Blue Corner(V Neck Cotton)', 16, 'clothing_size_desc'),
(24, '2016-11-05 03:00:15', 1, 'Add size XS Information for clothing Blue Corner(V Neck Cotton)', 17, 'clothing_size_desc'),
(25, '2016-11-05 03:00:15', 1, 'Add size S Information for clothing Blue Corner(V Neck Cotton)', 18, 'clothing_size_desc'),
(26, '2016-11-05 03:00:15', 1, 'Add size M Information for clothing Blue Corner(V Neck Cotton)', 19, 'clothing_size_desc'),
(27, '2016-11-05 03:00:15', 1, 'Add size L Information for clothing Blue Corner(V Neck Cotton)', 20, 'clothing_size_desc'),
(28, '2016-11-05 03:07:57', 1, 'Add clothing information for brand Blue Corner(V Neck Cotton)', 5, 'clothing'),
(29, '2016-11-05 03:07:57', 1, 'Add size XXS Information for clothing Blue Corner(V Neck Cotton)', 21, 'clothing_size_desc'),
(30, '2016-11-05 03:07:57', 1, 'Add size XS Information for clothing Blue Corner(V Neck Cotton)', 22, 'clothing_size_desc'),
(31, '2016-11-05 03:07:57', 1, 'Add size S Information for clothing Blue Corner(V Neck Cotton)', 23, 'clothing_size_desc'),
(32, '2016-11-05 03:07:57', 1, 'Add size M Information for clothing Blue Corner(V Neck Cotton)', 24, 'clothing_size_desc'),
(33, '2016-11-05 03:07:57', 1, 'Add size L Information for clothing Blue Corner(V Neck Cotton)', 25, 'clothing_size_desc'),
(34, '2016-11-05 03:46:24', 1, 'Add color Black for brand Blue Corner(Cotton V Neck)', 1, 'clothing_color'),
(35, '2016-11-05 03:46:24', 1, 'Add 1 Stocks for size XXS of brand Blue Corner(Cotton V Neck)', 1, 'clothing_stock'),
(36, '2016-11-05 03:46:24', 1, 'Add 2 Stocks for size XS of brand Blue Corner(Cotton V Neck)', 2, 'clothing_stock'),
(37, '2016-11-05 03:46:24', 1, 'Add 3 Stocks for size S of brand Blue Corner(Cotton V Neck)', 3, 'clothing_stock'),
(38, '2016-11-05 03:46:24', 1, 'Add 4 Stocks for size M of brand Blue Corner(Cotton V Neck)', 4, 'clothing_stock'),
(39, '2016-11-05 03:46:24', 1, 'Add 5 Stocks for size L of brand Blue Corner(Cotton V Neck)', 5, 'clothing_stock'),
(40, '2016-11-05 03:47:42', 1, 'Add color White for brand Blue Corner(Cotton V Neck)', 2, 'clothing_color'),
(41, '2016-11-05 03:47:42', 1, 'Add 1 Stocks for size XXS of brand Blue Corner(Cotton V Neck)', 6, 'clothing_stock'),
(42, '2016-11-05 03:47:42', 1, 'Add 2 Stocks for size XS of brand Blue Corner(Cotton V Neck)', 7, 'clothing_stock'),
(43, '2016-11-05 03:47:42', 1, 'Add 3 Stocks for size S of brand Blue Corner(Cotton V Neck)', 8, 'clothing_stock'),
(44, '2016-11-05 03:47:42', 1, 'Add 4 Stocks for size M of brand Blue Corner(Cotton V Neck)', 9, 'clothing_stock'),
(45, '2016-11-05 03:47:42', 1, 'Add 5 Stocks for size L of brand Blue Corner(Cotton V Neck)', 10, 'clothing_stock'),
(46, '2016-11-05 04:15:54', 1, 'Add Clothing Design "Gon"', 1, 'product_design'),
(47, '2016-11-05 08:06:04', 1, 'Add size XL Information for clothing Blue Corner(Cotton V Neck)', 26, 'clothing_size_desc'),
(48, '2016-11-05 08:06:32', 1, 'Add size 2XL Information for clothing Blue Corner(Cotton V Neck)', 27, 'clothing_size_desc'),
(49, '2016-11-05 09:17:11', 1, 'Add Product with Product Design "Gon" with Clothing "Blue Corner(V Neck Cotton)" color "Black"', 1, 'product'),
(50, '2016-11-05 09:22:27', 1, 'Add Printing Method "Silk Screen" ', 1, 'print_method'),
(51, '2016-11-05 09:27:59', 1, 'Add printing method "Silk Screen" for product design "Gon"', 1, 'product_design_print'),
(52, '2016-11-06 06:26:11', 1, 'Add Product with Product Design "Gon" with Clothing "Blue Corner(V Neck Cotton)" color "Black"', 1, 'product'),
(53, '2016-11-06 06:32:29', 1, 'Add clothing brand "Kentucky"', 2, 'clothing_brand'),
(54, '2016-11-06 06:35:13', 1, 'Add clothing brand "Bench"', 3, 'clothing_brand'),
(55, '2016-11-06 06:37:46', 1, 'Add clothing fabric "Wool"', 2, 'clothing_fabric'),
(56, '2016-11-06 06:39:04', 1, 'Add clothing type "Round Neck"', 2, 'clothing_type'),
(57, '2016-11-06 06:41:46', 1, 'Add size 3XL Information for clothing Blue Corner(Cotton V Neck)', 28, 'clothing_size_desc'),
(58, '2016-11-06 06:41:46', 1, 'Add size 4XL Information for clothing Blue Corner(Cotton V Neck)', 29, 'clothing_size_desc'),
(59, '2016-11-06 06:41:46', 1, 'Add size 5XL Information for clothing Blue Corner(Cotton V Neck)', 30, 'clothing_size_desc'),
(60, '2016-11-06 07:22:11', 1, 'Add clothing information for brand Blue Corner(V Neck Wool)', 6, 'clothing'),
(61, '2016-11-06 07:22:11', 1, 'Add size XXS Information for clothing Blue Corner(V Neck Wool)', 31, 'clothing_size_desc'),
(62, '2016-11-06 07:22:11', 1, 'Add size XS Information for clothing Blue Corner(V Neck Wool)', 32, 'clothing_size_desc'),
(63, '2016-11-06 07:22:11', 1, 'Add size S Information for clothing Blue Corner(V Neck Wool)', 33, 'clothing_size_desc'),
(64, '2016-11-06 07:22:11', 1, 'Add size M Information for clothing Blue Corner(V Neck Wool)', 34, 'clothing_size_desc'),
(65, '2016-11-06 07:22:11', 1, 'Add size L Information for clothing Blue Corner(V Neck Wool)', 35, 'clothing_size_desc'),
(66, '2016-11-06 22:48:12', 1, 'Update 23 Stocks for size S of brand "Blue Corner(V NeckCotton)"', 7, 'clothing_stock'),
(67, '2016-11-06 22:48:40', 1, 'Add Raw Material "fsfsafs" with stocks 12', 1, 'raw_material'),
(68, '2016-11-06 22:50:38', 1, 'Update Raw Materials''  Stocks to 1  Price to 2 ', 1, 'raw_material'),
(69, '2016-11-06 22:53:46', 1, 'Update Raw Materials''  Stocks to 4  Price to 6 ', 1, 'raw_material'),
(70, '2016-11-06 22:55:24', 1, 'Update Raw Materials''  Stocks to 41  Price to 44 ', 1, 'raw_material'),
(71, '2016-11-06 22:56:23', 1, 'Update Raw Materials''  Stocks to 4  Price to 5 ', 1, 'raw_material'),
(72, '2016-11-06 22:56:58', 1, 'Update Raw Materials''  Stocks to 45  Price to 55 ', 1, 'raw_material'),
(73, '2016-11-06 22:59:36', 1, 'Update 5 Stocks for size XS of brand "Blue Corner(V NeckCotton)"', 6, 'clothing_stock'),
(74, '2016-11-06 23:01:01', 1, 'Update 5 Stocks for size M of brand "Blue Corner(V NeckCotton)"', 8, 'clothing_stock'),
(75, '2016-11-07 15:47:13', 1, 'Add Clothing Design "Renzadsaljkdjklas"', 28, 'product_design'),
(76, '2016-11-22 19:26:06', 1, 'Add Clothing Design "Apink"', 10, 'product_design'),
(77, '2016-11-22 19:33:25', 1, 'Add Clothing Design "Twice"', 11, 'product_design'),
(78, '2016-11-22 19:34:45', 1, 'Add Product with Product Design "Twice" with Clothing "Blue Corner(V Neck Cotton)" color "White"', 11, 'product'),
(79, '2016-11-22 19:35:22', 1, 'Add printing method "Silk Screen" for product design "Twice"', 2, 'product_design_print'),
(80, '2016-11-22 20:39:52', 1, 'Add Clothing Design "Warning"', 12, 'product_design'),
(81, '2016-11-22 20:40:04', 1, 'Add Product with Product Design "Warning" with Clothing "Blue Corner(V Neck Cotton)" color "Black"', 12, 'product'),
(82, '2016-11-22 20:40:35', 1, 'Add printing method "Silk Screen" for product design "Warning"', 3, 'product_design_print'),
(83, '2016-11-22 20:41:14', 1, 'Add Clothing Design "Kpop"', 13, 'product_design'),
(84, '2016-11-22 20:41:26', 1, 'Add Product with Product Design "Kpop" with Clothing "Blue Corner(V Neck Cotton)" color "White"', 13, 'product'),
(85, '2016-11-22 20:41:42', 1, 'Add printing method "Silk Screen" for product design "Kpop"', 4, 'product_design_print');

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

DROP TABLE IF EXISTS `home_banner`;
CREATE TABLE IF NOT EXISTS `home_banner` (
  `hb_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_banner` varchar(500) NOT NULL,
  PRIMARY KEY (`hb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

DROP TABLE IF EXISTS `order_history`;
CREATE TABLE IF NOT EXISTS `order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `ab_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `total_sales` double NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`order_history_id`),
  KEY `customer_trans` (`customer_id`),
  KEY `ordcrt_fk` (`cart_id`),
  KEY `ordab_fk` (`ab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_history`
--

INSERT INTO `order_history` (`order_history_id`, `customer_id`, `cart_id`, `ab_id`, `date`, `update_date`, `total_sales`, `status`) VALUES
(6, 3, 1, 3, '2016-11-22 08:31:10', '2016-11-22 09:05:14', 760.4, 'DELIVERED'),
(7, 4, 2, 4, '2016-11-22 09:45:58', '2016-11-23 12:20:24', 1398.8, 'SHIPPED');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method` varchar(15) NOT NULL,
  `session` varchar(50) NOT NULL,
  `order_history_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `payord_fk` (`order_history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_method`, `session`, `order_history_id`) VALUES
(2, 'PAYPAL', 'EC-6AL49783VN5050420', 6),
(3, 'PAYPAL', 'EC-14389467FU649513U', 7);

-- --------------------------------------------------------

--
-- Table structure for table `print_method`
--

DROP TABLE IF EXISTS `print_method`;
CREATE TABLE IF NOT EXISTS `print_method` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `availability` varchar(11) NOT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `print_method`
--

INSERT INTO `print_method` (`pm_id`, `name`, `availability`) VALUES
(1, 'Silk Screen', 'Available');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `pd_id` int(11) NOT NULL,
  `clothing_color_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `prod_pc_fk` (`pd_id`),
  KEY `sc_pc_fk` (`clothing_color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `pd_id`, `clothing_color_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 3, 2),
(5, 4, 1),
(6, 5, 2),
(7, 6, 2),
(8, 7, 2),
(9, 8, 2),
(10, 9, 2),
(11, 11, 2),
(12, 12, 1),
(13, 13, 2),
(14, 14, 2),
(15, 15, 2),
(16, 16, 2),
(17, 17, 2),
(18, 18, 2),
(19, 19, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_comment`
--

DROP TABLE IF EXISTS `product_comment`;
CREATE TABLE IF NOT EXISTS `product_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `comment_date` datetime NOT NULL,
  `rating` int(1) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comfk` (`product_id`),
  KEY `comcustfk` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_comment`
--

INSERT INTO `product_comment` (`id`, `customer_id`, `product_id`, `comment_date`, `rating`, `comment`) VALUES
(8, 1, 1, '2016-11-06 22:15:10', 5, 'Ok na comment');

-- --------------------------------------------------------

--
-- Table structure for table `product_design`
--

DROP TABLE IF EXISTS `product_design`;
CREATE TABLE IF NOT EXISTS `product_design` (
  `pd_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`pd_id`),
  KEY `pd_cat_fk` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_design`
--

INSERT INTO `product_design` (`pd_id`, `name`, `description`, `category_id`) VALUES
(1, 'Gon', 'Hunter x Hunter', 6),
(2, 'Untitled_1', 'Custom Design Project', 4),
(3, 'test', 'Custom Design Project', 4),
(4, 'Gon_2016-11-21 20:33:06', 'Custom Design Project', 4),
(5, 'Subject', 'Custom Design Project', 4),
(6, 'Subject-suggest', 'Custom Design Project', 4),
(7, 'Cart', 'Custom Design Project', 4),
(8, 'Cart-suggest', 'Custom Design Project', 4),
(9, 'Untitled_1', 'Custom Design Project', 4),
(10, 'Apink', 'Apink Tshirt', 2),
(11, 'Apink', 'Apink Tshirt', 6),
(12, 'Warning', 'dadada', 6),
(13, 'Kpop', 'dajdhjadha', 6),
(14, 'Untitled_1', 'Custom Design Project', 4),
(15, 'Untitled_2', 'Custom Design Project', 4),
(16, 'Untitled_1-suggest', 'Custom Design Project', 4),
(17, 'Untitled_2-suggest', 'Custom Design Project', 4),
(18, 'ghfdfgd', 'Custom Design Project', 4),
(19, 'ghfdfgd-suggest', 'Custom Design Project', 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_design_print`
--

DROP TABLE IF EXISTS `product_design_print`;
CREATE TABLE IF NOT EXISTS `product_design_print` (
  `pdp_id` int(11) NOT NULL AUTO_INCREMENT,
  `pm_id` int(11) NOT NULL,
  `default_price` decimal(10,0) NOT NULL,
  `price_increment` decimal(10,0) NOT NULL,
  `pd_id` int(11) NOT NULL,
  PRIMARY KEY (`pdp_id`),
  KEY `pdp_pm_fk` (`pm_id`),
  KEY `pdp_pd_fk` (`pd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_design_print`
--

INSERT INTO `product_design_print` (`pdp_id`, `pm_id`, `default_price`, `price_increment`, `pd_id`) VALUES
(1, 1, '100', '10', 1),
(2, 1, '100', '20', 11),
(3, 1, '200', '10', 12),
(4, 1, '200', '10', 13);

-- --------------------------------------------------------

--
-- Table structure for table `raw_material`
--

DROP TABLE IF EXISTS `raw_material`;
CREATE TABLE IF NOT EXISTS `raw_material` (
  `rm_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`rm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `raw_material`
--

INSERT INTO `raw_material` (`rm_id`, `name`, `description`, `price`, `stock`) VALUES
(1, 'fsfsafs', 'safasf', '55', 45);

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

DROP TABLE IF EXISTS `shipping`;
CREATE TABLE IF NOT EXISTS `shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_history_id` int(11) NOT NULL,
  `courier` varchar(20) NOT NULL,
  `ship_cost` double NOT NULL,
  `tracking_no` varchar(30) NOT NULL,
  `date_send` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_history_id_2` (`order_history_id`),
  KEY `order_history_id` (`order_history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `order_history_id`, `courier`, `ship_cost`, `tracking_no`, `date_send`) VALUES
(1, 6, 'LBC', 0, '1234567', '2016-11-22 08:59:10'),
(2, 7, 'JRS', 0, '212321', '2016-11-23 12:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `ship_courier`
--

DROP TABLE IF EXISTS `ship_courier`;
CREATE TABLE IF NOT EXISTS `ship_courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courier_name` varchar(20) NOT NULL,
  `ship_fee` double NOT NULL,
  `track_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courier_name` (`courier_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship_courier`
--

INSERT INTO `ship_courier` (`id`, `courier_name`, `ship_fee`, `track_url`) VALUES
(1, 'LBC', 300, 'http://www.lbcexpress.com/track/?tracking_no='),
(2, 'XEND', 150, 'http://tracker.xend.com.ph/?waybill='),
(4, 'JRS', 400, 'jrsexpress.com');

-- --------------------------------------------------------

--
-- Table structure for table `sub_cart`
--

DROP TABLE IF EXISTS `sub_cart`;
CREATE TABLE IF NOT EXISTS `sub_cart` (
  `sub_cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `size_id` int(11) NOT NULL,
  `pm_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `draw_location` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`sub_cart_id`),
  KEY `cart_sub_cart` (`cart_id`),
  KEY `product_sub_cart_fk` (`product_id`),
  KEY `scart_size_id` (`size_id`),
  KEY `scart_pm_fk` (`pm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_cart`
--

INSERT INTO `sub_cart` (`sub_cart_id`, `quantity`, `price`, `size_id`, `pm_id`, `product_id`, `cart_id`, `draw_location`) VALUES
(9, 3, 115, 21, 1, 1, 1, NULL),
(10, 1, 200, 21, 1, 4, 1, 'build/3/test/'),
(11, 2, 115, 21, 1, 1, 2, NULL),
(12, 5, 200, 21, 1, 8, 2, 'build/4/Cart/'),
(14, 5, 100, 21, 1, 9, 3, 'build/4/Cart-suggest/'),
(15, 1, 115, 21, 1, 1, 3, NULL),
(16, 1, 115, 21, 1, 1, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE IF NOT EXISTS `wishlist` (
  `wishlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pm_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  PRIMARY KEY (`wishlist_id`),
  KEY `ci_wishlist_fk` (`customer_id`),
  KEY `product_wishlist_fk` (`product_id`),
  KEY `pm_wishlist_fk` (`pm_id`),
  KEY `size_wishlist_fk` (`size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address_book`
--
ALTER TABLE `address_book`
  ADD CONSTRAINT `ab_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `customer_cart_fk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Constraints for table `clothing`
--
ALTER TABLE `clothing`
  ADD CONSTRAINT `clothing_cb_fk` FOREIGN KEY (`cb_id`) REFERENCES `clothing_brand` (`cb_id`),
  ADD CONSTRAINT `clothing_cf_fk` FOREIGN KEY (`cf_id`) REFERENCES `clothing_fabric` (`cf_id`),
  ADD CONSTRAINT `ct_clothing_fk` FOREIGN KEY (`ct_id`) REFERENCES `clothing_type` (`ct_id`);

--
-- Constraints for table `clothing_color`
--
ALTER TABLE `clothing_color`
  ADD CONSTRAINT `sc_fk` FOREIGN KEY (`clothing_id`) REFERENCES `clothing` (`clothing_id`);

--
-- Constraints for table `clothing_size_desc`
--
ALTER TABLE `clothing_size_desc`
  ADD CONSTRAINT `ssd_fk` FOREIGN KEY (`clothing_id`) REFERENCES `clothing` (`clothing_id`);

--
-- Constraints for table `clothing_stock`
--
ALTER TABLE `clothing_stock`
  ADD CONSTRAINT `csd_clothing_fk` FOREIGN KEY (`size`) REFERENCES `clothing_size_desc` (`ssd_id`),
  ADD CONSTRAINT `scs_fk` FOREIGN KEY (`sc_id`) REFERENCES `clothing_color` (`sc_id`);

--
-- Constraints for table `employee_logs`
--
ALTER TABLE `employee_logs`
  ADD CONSTRAINT `employee_logs` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `order_history`
--
ALTER TABLE `order_history`
  ADD CONSTRAINT `customer_trans` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  ADD CONSTRAINT `ordab_fk` FOREIGN KEY (`ab_id`) REFERENCES `address_book` (`ab_id`),
  ADD CONSTRAINT `ordcrt_fk` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payord_fk` FOREIGN KEY (`order_history_id`) REFERENCES `order_history` (`order_history_id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `prod_pc_fk` FOREIGN KEY (`pd_id`) REFERENCES `product_design` (`pd_id`),
  ADD CONSTRAINT `sc_pc_fk` FOREIGN KEY (`clothing_color_id`) REFERENCES `clothing_color` (`sc_id`);

--
-- Constraints for table `product_comment`
--
ALTER TABLE `product_comment`
  ADD CONSTRAINT `comcustfk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  ADD CONSTRAINT `comfk` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`);

--
-- Constraints for table `product_design_print`
--
ALTER TABLE `product_design_print`
  ADD CONSTRAINT `pdp_pd_fk` FOREIGN KEY (`pd_id`) REFERENCES `product_design` (`pd_id`),
  ADD CONSTRAINT `pdp_pm_fk` FOREIGN KEY (`pm_id`) REFERENCES `print_method` (`pm_id`);

--
-- Constraints for table `shipping`
--
ALTER TABLE `shipping`
  ADD CONSTRAINT `shipping_ibfk_1` FOREIGN KEY (`order_history_id`) REFERENCES `order_history` (`order_history_id`);

--
-- Constraints for table `sub_cart`
--
ALTER TABLE `sub_cart`
  ADD CONSTRAINT `cart_sub_cart` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`),
  ADD CONSTRAINT `product_sub_cart_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `scart_pm_fk` FOREIGN KEY (`pm_id`) REFERENCES `print_method` (`pm_id`),
  ADD CONSTRAINT `scart_size_id` FOREIGN KEY (`size_id`) REFERENCES `clothing_size_desc` (`ssd_id`);

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `ci_wishlist_fk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  ADD CONSTRAINT `pm_wishlist_fk` FOREIGN KEY (`pm_id`) REFERENCES `print_method` (`pm_id`),
  ADD CONSTRAINT `product_wishlist_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `size_wishlist_fk` FOREIGN KEY (`size_id`) REFERENCES `clothing_size_desc` (`ssd_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
