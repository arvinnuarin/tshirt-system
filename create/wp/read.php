<html>
<head>

	<title>MixlArts Admin: Drawing</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		include("../conf/config.php");
		echo '<base href='.$wpbhref.'/>'
	?>
	<link rel="stylesheet" type="text/css" href="css/read.css">

	<script src="lib/fabric.js" type="text/javascript"></script>
	<script src="lib/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="lib/jquerytransform2d.js" type="text/javascript"></script>
	<script src="lib/sortable.js" type="text/javascript"></script>
	<script src="js/custom.js" type="text/javascript"></script>
</head>
<body class="unselectable">

<?php
	$exist = false; $prname="";
	if (isset($_GET['prname'])) {

        $stmt = $dbc->prepare("SELECT bui.build_no, bui.pr_name, bui.location, CONCAT(cu.lname,', ',cu.fname), bui.price FROM draw_build bui INNER JOIN customer cu ON bui.customer_id=cu.customer_id WHERE bui.pr_name='".$_GET['prname']."' AND bui.type='folders' AND bui.status ='PENDING' LIMIT 1");

		$stmt->execute();
		$stmt->store_result();
		$data = $stmt->num_rows();

		if ($data == 1){

			$stmt->bind_result($b, $o, $loct, $n, $p);

			while($stmt->fetch()) {

				$exist = true;
				$bld = $b;
				$prname = $o;
				$loc = $loct;
				$cname = $n;
				$price = $p;
			}
		}
	}

	if ($exist == true) {
		showDrawing($bld, $prname, $loc, $cname, $price);
	} else {
		header('Location: '.$wpbhref.'manager');
	}

	function showDrawing($bld, $p, $l, $c, $pri) {
	echo "
		<div id = 'container'>
			<div id = 'wrapper'>

				<div id = 'divtool'>
                         
					<div id = 'toolwrapper'>
                        <div id = 'tool1'>
							<label id = 'bno'>Build No: </label>
							<label id='bldNo'>$bld</label>
							<label id = 'custname'>Customer Name: $c</label>
							<label id = 'shinfo'>Shirt Details: </label>
							<label id='shtInfo'></label>
						</div>
                        
						<div id = 'tool3'>
                           
							<div id = 'tool3wrapper'>
				                <ul class='tab'>
                                    <li><a id='btnClone' title='Suggest Project'><img id='icon' src='./res/clone.png'></a></li>
									<li><a id='btnLayer'><img id='icon' src='./res/layer.png'></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div id = 'content'>
					<div id = 'contentwrapper'>
						<div class ='frnt_sht'>
							<img id='shirt'>
							<div id= 'frnt_cnv'>
								<canvas id='c' width='278' height='462'></canvas>
							</div>
                            <br><button id='btnDownload' onclick='downloadProject()'>DOWNLOAD</button>
                            
						</div>
                         
                        <div id = 'rightnav'>
                             <div id = 'tbldiv'>
                                <table id ='tblDesc' >
                                    <col width='250px' />
                                    <col width='150px'/>
                                    <thead>
                                        <tr>
                                            <th>Attributes</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                                
                        	<div class='rsidenav'>
                               
                                <div id = 'divlayer'>
                                    <div id = 'lbllayer'>LAYERS</div>
                                </div>

                                <div id='draw-layers'>
                                    <div id='listWithHandle' class='list-group'>
                                    </div>
                                </div>
                                
                                <div id = 'divitemb'>
                                	<label>Shirt Budget: &#8369</label>
                                     <input type='number' min='1' max='100000' id='txtAddBgt' readonly></input>
                                    <textarea spellcheck='false' type='text' id='txtAddNote'></textarea>
                                    <label>Actual Shirt Price: &#8369</label>
                                     <input type='number' min='1' max='100000' id='txtActPrice' value='$pri'></input>
                                     <button id='btnPub'>PUBLISH</button>
                                </div>
                            </div>
					
					   </div>
                       
					</div>
				</div>
			</div>
		</div>

		";
		echo "
		<script src='js/draw1.js?random=<?php echo uniqid(); ?>'></script>
		<script>
		loadProject('$p','$l','front');
		</script>
		";

	}
?>
</body>
</html>