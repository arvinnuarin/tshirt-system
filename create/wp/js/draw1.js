
var canvas = this.__canvas = new fabric.Canvas('c', {
	isDrawingMode: false,
    selection:false
});

canvas.onselectstart = function () { return false; }

var prJSON;

//layering
fabric.Canvas.prototype.getItemByMyID = function(myID) {
	var object = null,
	objects = this.getObjects();

	for (var i = 0, len = this.size(); i < len; i++) {

		if (objects[i].id&& objects[i].id=== myID) {
			object = objects[i];
			break;
		}
	}
	return object;
};

var layerNo;
$("#listWithHandle").on('click', '.list-group-item', function () {
	layerNo = $(this).attr('id');
	canvas.renderAll();
	canvas.setActiveObject(canvas.getItemByMyID(layerNo));

    $('.list-group-item').removeClass('active');
    $(this).addClass('active');

});

Sortable.create(listWithHandle, {
	group: "sorting",
	sort: true,

	onSort: function (evt) {

		var obj = canvas.getItemByMyID(layerNo);
		if (evt.newIndex > evt.oldIndex) {

			for (x=evt.newIndex; x > evt.oldIndex; x--){

				canvas.sendBackwards(obj);
				console.log("backward " +x);
			}
		} else{

			for (x=evt.newIndex; x < evt.oldIndex; x++){
				canvas.bringForward(obj);
				console.log("forward " + x);
			}

		}
		canvas.setActiveObject(obj);
		canvas.renderAll();
	}

});

var numLayers=0;
canvas.on('object:added', function(e) {

	e.target.lockMovementX = true;
	e.target.lockMovementY = true;
	e.target.lockRotation = true;
	e.target.lockScalingX = true;
	e.target.lockScalingY = true;
	e.target.lockUniScaling = true;

	e.target.id = 'obj'+numLayers;
	$('#listWithHandle').prepend("<div class='list-group-item' id='obj"+numLayers+"'>Layer "+ numLayers + " : " +e.target.type+"</div");
	numLayers++;
});

canvas.on('object:selected',function(e){
    var actObj = canvas.getActiveObject(),actGrp = canvas.getActiveGroup();
    shapeCtr = 0;
    $('.list-group-item').removeClass('active');

    if (actObj) {
      $('#'+actObj.id).addClass('active');
  	}
  	else if (actGrp) {
  			var objectsInGroup = actGrp.getObjects();
  			objectsInGroup.forEach(function(o) {
  				$('#'+o.id).addClass('active');
  			});
  	}
  	viewDesc((actObj.id).substr(3));
});

canvas.on('selection:cleared', function() {
    $('.list-group-item').removeClass('active');
});

var prnm, prloc;
var sQty, sBrand, sFtype, sPtype, sType, sSize, cBgt, sColor;

function loadProject(p, l, a){
	canvas.clear();
	prnm = p;
	prloc = '../../custom/' +l;
	l= prloc +'/'+a+'.json';

	$('#listWithHandle').empty();
	numLayers=0;

	$.getJSON(l, function(data) {

		var flTxt=$.grep(data.objects, function( n, i ) {
  			return n.type =="textbox";
		});

		$.each(flTxt, function (idx, obj) {
		   $('head').append('<link rel="stylesheet" href="https://fonts.googleapis.com/css?family='+obj.fontFamily+ '" type="text/css" />');
		});
		canvas.loadFromJSON(data);
		canvas.renderAll();
		$("#shirt").attr("src", "../../../assets/shirt_template/" + data.tempLoc+ "/front.png");
		$("#shirt").attr("name", data.shirtColor);
		$("#shirt").attr("shirtType", data.shirtType);
		$('#txtAddBgt').val(data.shirtBudget);
		$("#txtAddNote").val(data.shirtNote);

		sColor = data.shirtColor;
		cBgt = data.shirtBudget;
		sQty = data.shirtQty;
		sFtype = data.shirtFtype;
		sSize = data.shirtSize;
		sBrand = data.shirtBrand;
		sType = data.shirtType;
		sPtype = data.shirtPtype;

		var info = sQty + ' X ' + sFtype + ' ' + sSize + ' ' + sBrand + ' ' + sType + ' via ' + sPtype;
		$('#shtInfo').text(info);
		prJSON = data;
	});
}

function downloadProject(){
	$.post('php/downloadFile.php', {prname:prnm, prlocation: prloc, prtype:'folders'}, function(data){

		data = data.trim();
		var x = data.split('*');
		if (x[0] == 'success') {
			window.location.href='../php/download.php?file='+x[1];
		} else {
			alert(data);
		}
	});
}

function viewDesc(objNo){

	$('#tblDesc').find('tbody').empty();
    $('#tblDesc').find('tbody:last').append('<tr><td>SHIRT COLOR</td><td>#'+prJSON.shirtColor+
		'</td></tr>');
    var tr='<tr>';
	$.each(prJSON.objects[objNo], function(keys, val) {

    	tr += '<td>' + keys.toUpperCase() + '</td>';
        tr += '<td>' + val + '</td>';
        tr +='</tr>';
	});
	$('#tblDesc').find('tbody:last').append(tr);
}

$('#btnClone').click(function(){

	$.post('php/cloneProj.php', {prlocation: prloc}, function(data){
				
		var z = data.split('*');
		if(z[0] == 'success'){
		 	window.location.href='../../mixl/project/' + z[1] + '/'+prnm +'-suggest';
		} else {
			alert(data);
		}
	});
});

$('#btnPub').click(function(){

	var pr = $('#txtActPrice').val();
	var bno = $('#bldNo').text();

	if (pr > 0){

		$.post('php/publish.php', {price: pr, buildNo: bno}, function(data){
			
			if (parseInt(pr) > parseInt(cBgt)){
				$('#btnClone').show();
				alert("The price has been sent to the customer. However, the actual product price exceed customer budget. Try to suggest one.");
			} else {
				alert('The price has been sent to the customer.');
			}
		});
	}
});