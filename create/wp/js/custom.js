$(document).ready(function() {

	$(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });
    
    $('#btnDownload').hover( function() {
		$('#btnDownload').fadeIn(300);
		$('#btnDownload').css("background-color","white");
		$('#btnDownload').css("color","black");

	}, function() {
		$('#btnDownload').fadeIn(300);
		$('#btnDownload').css("background-color"," #222d32");
		$('#btnDownload').css("color","white");
		$('#btnDownload').css("font-weight","bold");
	});
    
    $("#btnLayer").click(function () {
        rightSideNav();
    });
    
    function rightSideNav(){
        var width = $('#rightnav').width();

        if (width == 0 ){
            $('#rightnav').width('727px');
        }

        else{
            $('#rightnav').width('0px');
        }
    }
    

});