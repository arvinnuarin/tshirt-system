$(document).ready(function(){

	$(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });

	var filemanager = $('.filemanager'), fileList = filemanager.find('.data');

	function getFiles(st) {
		$.post('../create/wp/manager/scan.php',{status: st}, function(data) {

		var response = [data], currentPath = '';
		var folders = [], files = [];

		$(window).on('hashchange', function(){

			goto(window.location.hash);
		}).trigger('hashchange');
            
        filemanager.find('.search').click(function(){

			$(this).find('span').hide();
			$('#txtSearch').show().focus();

		});

		$('#txtSearch').on('input', function(e){

			folders = [];
			files = [];

			var value = this.value.trim();

			if(value.length) {

				filemanager.addClass('searching');
				window.location.hash = 'search=' + value.trim();
			} else {

				filemanager.removeClass('searching');
				window.location.hash = encodeURIComponent(currentPath);
			}

		}).on('keyup', function(e){

			var search = $(this);

			if(e.keyCode == 27) {

				search.trigger('focusout');
			}

		}).focusout(function(e){

			var search = $(this);

			if(!search.val().trim().length) {

				window.location.hash = encodeURIComponent(currentPath);
				search.hide();
				search.parent().find('span').show();
			}
		});

		fileList.on('click', 'li.folders', function(e){
			e.preventDefault();
			var x= $(this).find('a').attr('href').split('/');
			var id = x[1];

			window.location.href = './../mixl/open/' + id + '/' + $(this).find('span.name').text();
		});

		fileList.on('click', 'li.files', function(e){
			e.preventDefault();

			var nextDir = $(this).find('span.name2').text();

			alert(nextDir);

		});

		function goto(hash) {

			hash = decodeURIComponent(hash).slice(1).split('=');

			if (hash.length) {
				var rendered = '';

				if (hash[0] === 'search') {

					filemanager.addClass('searching');
					rendered = searchData(response, hash[1].toLowerCase());

					if (rendered.length) {
						currentPath = hash[0];
						render(rendered);
					}
					else {
						render(rendered);
					}

				}

				else if (hash[0].trim().length) {

					rendered = searchByPath(hash[0]);

					if (rendered.length) {

						currentPath = hash[0];
						render(rendered);
					}
					else {
						currentPath = hash[0];
						render(rendered);
					}
				}
				else {
					currentPath = data.path;
					render(searchByPath(data.path));
				}
			}
		}

		function searchByPath(dir) {
			var path = dir.split('/'),
			demo = response,
			flag = 0;

			for(var i=0;i<path.length;i++){
				for(var j=0;j<demo.length;j++){
					if(demo[j].name === path[i]){
						flag = 1;
						demo = demo[j].items;
						break;
					}
				}
			}

			demo = flag ? demo : [];
			return demo;
		}

		function searchData(data, searchTerms) {

			data.forEach(function(d){
				if(d.type == 'folder') {

					searchData(d.items,searchTerms);

					if(d.name.toLowerCase().match(searchTerms)) {
						folders.push(d);
					}
				}
				else if(d.type == 'file') {
					if(d.name.toLowerCase().match(searchTerms)) {
						files.push(d);
					}
				}
			});
			return {folders: folders, files: files};
		}

		function render(data) {

			var scannedFolders = [],
			scannedFiles = [];

			if(Array.isArray(data)) {

				data.forEach(function (d) {

					if (d.type === 'folder') {
						scannedFolders.push(d);
					}
					else if (d.type === 'file') {
						scannedFiles.push(d);
					}

				});

			}
			else if(typeof data === 'object') {

				scannedFolders = data.folders;
				scannedFiles = data.files;

			}

			fileList.empty().hide();

			if(!scannedFolders.length && !scannedFiles.length) {
				filemanager.find('.nothingfound').show();
				fileList.hide();
			}
			else {
				filemanager.find('.nothingfound').hide();
				fileList.show();
			}

			if(scannedFolders.length) {

				scannedFolders.forEach(function(f) {

					name = escapeHTML(f.name),
					icon = '<img class="logo" src = "../create/custom/'+f.path+ '/output/front_shirt.png">';

					var folder = $('<li build="'+f.build+'" class="folders" style="background-color: gray;"><a href="'+ f.path +'" class="folders">'+icon+'<span class="name">' + name + '</span></li>');
					folder.appendTo(fileList);
				});

			}

			if(scannedFiles.length) {

				scannedFiles.forEach(function(f) {

					var fileSize = bytesToSize(f.size),
					name = escapeHTML(f.name),
					fileType = name.split('.'),
                    bg,
					icon = '<span class="icon file"></span>';

					fileType = fileType[fileType.length-1];
                    if ((fileType== "doc") || (fileType== "docx") ||(fileType== "psd")||(fileType== "txt")){
                        bg = "lightblue";
                    } 
                    else if ((fileType== "rar")|| (fileType== "zip")|| (fileType== "7z") || (fileType== "deb")|| (fileType== "dmg")|| (fileType== "gz") ){
                        bg = "violet";
                    }
                    
                    else if ((fileType== "gif")|| (fileType== "jpg")|| (fileType== "jpeg") || (fileType== "pdf")|| (fileType== "png")|| (fileType== "gz") ){
                        bg = "#ffa734";
                    }

					icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';

					var file = $('<li build="'+f.build+'"class="files" style="background-color:'+bg+';"><a href="'+ f.path+'" title="'+ f.path +'" class="files">'+icon+'<span class="name2">'+ name +'</span> </a></li>');
                    
					file.appendTo(fileList);
				});
			}
			fileList.css({'display':'inline-block'});
			var cntProjects  = scannedFolders.length + scannedFiles.length +1;
			$('#txtProjName').val('Untitled_'+cntProjects);
		}

		function escapeHTML(text) {
			return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
		}

		function bytesToSize(bytes) {
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
			if (bytes == 0) return '0 Bytes';
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
			return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

	});
	}

	getFiles('PENDING');

	function downloadProject(p,l,t){
		l = '../../custom/' + l;
		$.post('../create/wp/php/downloadFile.php', {prname:p, prlocation: l, prtype: t}, function(data){
			
			data = data.trim();
			var x = data.split('*');
			if (x[0] == 'success') {
				window.location.href='../create/wp/php/download.php?file='+x[1];
			} else {
				alert(data);
			}
		});
	}

	var reloc, rebld;
    $('#btnPen').click(function(){
		getFiles('PENDING');
		curStat = 'pen';
        
        $('.fileicon').removeClass('active');
        $('#btnPen').addClass('active');
	});

	$('#btnProc').click(function(){

		getFiles('PROCESSED');
        $('.fileicon').removeClass('active');
        $("#btnProc").addClass('active');
	});

	$('#btnPcart').click(function(){
		getFiles('PAID');
        $('.fileicon').removeClass('active');
        $("#btnPcart").addClass('active');
	});

	var loc;
	$.contextMenu({selector: 'li', callback: function(key, options) {

		if (key == "open") {
			loc = $(this).find('a').attr('href');
			var x= loc.split('/');
			var id = x[1];

			window.location.href = '../mixl/open/' + id + '/' + $(this).find('span.name').text();
		}
		else if (key == "download"){
			var loc = $(this).find('a').attr('href'), type = $(this).find('a').attr('class');

			if (type == 'folders'){
				downloadProject($(this).find('span.name').text(),loc, type);
			} else {
				downloadProject($(this).find('span.name2').text(),loc, type);
			}
		}
       	else if (key == "properties"){

        		loc = $(this).find('a').attr('href');
        		console.log(loc);

        		$.post('../create/custom/manager/getProperties.php',{prloc: loc}, function(data){
			$('#divpropwrapper').empty();
			$('#divpropwrapval').empty();

			$.each(data, function(key, val){
				$('#divpropwrapper').append('<div><label>'+key+'</label></div>');
				$('#divpropwrapval').append('<div><label>'+val+'</label></div>');
		});
        		},"json");
        	
        		$('#imgeshirt').attr('src', '../create/custom/' +loc + "output/front_shirt.png");
        		checkImg();
		$('#divproperties').show();
            		$('#page-cover2').show();

            		$('#btnfrnt').click(function(){

		 	shirtLoc = "../create/custom/"+loc + "output/front_shirt.png";
	    		$('#imgeshirt').attr('src', shirtLoc );
	    		checkImg();
   	 	});

   	  	$('#btnback').click(function(){

		 	shirtLoc = "../create/custom/"+loc + "output/back_shirt.png";
	    		$('#imgeshirt').attr('src', shirtLoc );
	    		checkImg();
   	 	});
	}
        else if (key == "pricing"){
        	loc = $(this).find('a').attr('href');
        	$.post('../create/wp/manager/getProperties.php',{prloc: loc}, function(data){
				$('#lblbudget2').text(data['Actual Price:']);
        	},"json");

			$('#divpricing').show();
            $('#page-cover2').show();
		}
	},
	items: {
		"open": {name: "Open", icon: "fa-mail-forward", visible: function(key, opt){
			if($(this).find('a').attr('class') =='files'){ return false; } else {return true;}
		}},
		"download": {name: "Download", icon: "fa-cloud-download"},
		"properties": {name: "Properties", icon: "fa-database"},
		"pricing": {name: "Pricing", icon: "fa-money", visible: function(key, opt){
			if($(this).find('a').attr('class') =='files'){ return true; } else {return false;}
		}},
		}
	});

	$('#divbtn #btnSubmit').click(function(){
		var price=$('#inputprice').val();	

		$.post('../create/wp/manager/setprice.php',{location: loc, price: price}, function(data){
			alert(data);
			getFiles('PENDING');
			$('#divpricing').hide();
        	$('#page-cover2').hide();
		});
	});

	function checkImg(){

		$("#imgeshirt").each(function() {
            
		            $(this).on('error', function() {
		              
		               $(this).attr('src', "../create/custom/res/noimage.png");
		            }); 
   		 });
	    }
          
	 

   $(document).on("mouseover", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});
    
    
   $(document).on("mouseover", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

     $(document).on("mouseover", "#btnNew", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnNew", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

    $('#btnNext').hover( function() {
		$('#btnNext').fadeIn(300);
		$('#btnNext').css("background-color","white");
		$('#btnNext').css("color","black");

	}, function() {
		$('#btnNext').fadeIn(300);
		$('#btnNext').css("background-color","#222d32");
		$('#btnNext').css("color","white");
		$('#btnNext').css("font-weight","bold");
	});
    
    $('#btnCancel').hover( function() {
		$('#btnCancel').fadeIn(300);
		$('#btnCancel').css("background-color","white");
		$('#btnCancel').css("color","black");

	}, function() {
		$('#btnCancel').fadeIn(300);
		$('#btnCancel').css("background-color","#222d32");
		$('#btnCancel').css("color","white");
		$('#btnCancel').css("font-weight","bold");
	});
    
    $('#btnUplink').hover( function() {
		$('#btnUplink').fadeIn(300);
		$('#btnUplink').css("background-color","white");
		$('#btnUplink').css("color","black");

	}, function() {
		$('#btnUplink').fadeIn(300);
		$('#btnUplink').css("background-color","#222d32");
		$('#btnUplink').css("color","white");
		$('#btnUplink').css("font-weight","bold");
	});
    
    $('#btnUpFile').hover( function() {
		$('#btnUpFile').fadeIn(300);
		$('#btnUpFile').css("background-color","white");
		$('#btnUpFile').css("color","black");

	}, function() {
		$('#btnUpFile').fadeIn(300);
		$('#btnUpFile').css("background-color","#222d32");
		$('#btnUpFile').css("color","white");
		$('#btnUpFile').css("font-weight","bold");
	});
    
    $('#btnSubmit').hover( function() {
		$('#btnSubmit').fadeIn(300);
		$('#btnSubmit').css("background-color","white");
		$('#btnSubmit').css("color","black");

	}, function() {
		$('#btnSubmit').fadeIn(300);
		$('#btnSubmit').css("background-color","#222d32");
		$('#btnSubmit').css("color","white");
		$('#btnSubmit').css("font-weight","bold");
	});
    
    $('#btnBack2').hover( function() {
		$('#btnBack2').fadeIn(300);
		$('#btnBack2').css("background-color","white");
		$('#btnBack2').css("color","black");

	}, function() {
		$('#btnBack2').fadeIn(300);
		$('#btnBack2').css("background-color","#222d32");
		$('#btnBack2').css("color","white");
		$('#btnBack2').css("font-weight","bold");
	});
    
    $('#btnNext2').hover( function() {
		$('#btnNext2').fadeIn(300);
		$('#btnNext2').css("background-color","white");
		$('#btnNext2').css("color","black");

	}, function() {
		$('#btnNext2').fadeIn(300);
		$('#btnNext2').css("background-color","#222d32");
		$('#btnNext2').css("color","white");
		$('#btnNext2').css("font-weight","bold");
	});
    
    //btnexit
    $('#btnexit').click( function() {
        $('#divproperties').hide();
        $('#page-cover2').hide();
    });
 



      $.post('../create/wp/php/getShirt.php', {com: 'brand'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbBrand').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    $.post('../create/wp/php/getShirt.php', {com: 'type'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbType').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    $.post('../create/wp/php/getShirt.php', {com: 'print'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbPrint').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');


    $('#cmbBrand').change(function(){
        var brd = $(this).children(":selected").attr("id");
         var type = $('#cmbType').children(":selected").attr("id");

         getFabric(brd, type);
        
     });

    $('#cmbType').change(function(){
        var type = $(this).children(":selected").attr("id");
        var brd = $('#cmbBrand').children(":selected").attr("id");
            
            getFabric(brd, type);
     });

    $('#cmbFabric').change(function(){

        getShirtColor($(this).children(":selected").attr("id"));
        getSize($(this).children(":selected").attr("id"));
    });
    
    function getFabric(b,t) {

         $.post('../create/wp/php/getShirt.php', {com: 'fabric', brand: b, type: t }, function (data) {

            $('#cmbFabric').empty();
            for(var i=1; i<data.length; i++){
                $('#cmbFabric').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
            }

        },'json');
    }

    function getSize(s) {
            $.post('../create/wp/php/getShirt.php', {com: 'size', clothing: s}, function (data) {

            	$('#cmbSize').empty();
            	for(var i=1; i<data.length; i++){
                $('#cmbSize').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
            }
        },'json');
    }

    var temploc="";
    function getShirtColor(c) {

        $.post('../create/wp/php/getShirt.php', {com: 'color', clothing: c}, function (data) {

             $('#avlclr').empty();
            for (var i=1; i<data.length;i++){

                $div = $('<div data-role="fieldcontain"/>');
                $("<button id = '"+data[i].id+"' class='btnclr' style ='background-color:#"+data[i].Hex+";'  name='"+data[i].Hex +"' loc='"+data[i].Location+"'/>").appendTo($div.clone()).appendTo('#avlclr');
            }

        },'json');
    }


    var color='';
    $("#avlclr").on('click', '.btnclr', function () {
	
	color = $(this).attr("id");
              temploc = $(this).attr("loc");
            
            $('.btnclr').animate({
                transform: 'rotate(0deg)'
            });
            $(this).animate({
              transform: 'rotate(135deg)'
            });
    });
    
    //opacity

    $('#divcreate').click( function() {
        $('#divuplink').removeClass('active');
        $('#divupfile').removeClass('active');
        $('#divuplink').find('#opacitywrapper').show();
        $('#divupfile').find('#opacitywrapper').show();
        $(this).find('#opacitywrapper').hide();
        $(this).addClass('active');
    });
    
     $('#divuplink').click( function() {
        $('#divcreate').removeClass('active');
        $('#divupfile').removeClass('active');
        $('#divcreate').find('#opacitywrapper').show();
        $('#divupfile').find('#opacitywrapper').show();
        $(this).find('#opacitywrapper').hide();
        $(this).addClass('active');
    });
    
    $('#divupfile').click( function() {
        $('#divcreate').removeClass('active');
        $('#divuplink').removeClass('active');
        $('#divcreate').find('#opacitywrapper').show();
        $('#divuplink').find('#opacitywrapper').show();
        $(this).find('#opacitywrapper').hide();
        $(this).addClass('active');
    });
    
    //upldfile
     $('#btnUpFile').click(function() {
		$('#upldfile').trigger('click');
	});
    
    //btnnew
    
    $('#btnNew').click( function() {

    	getFabric($('#cmbBrand').children(":selected").attr("id"), $('#cmbType').children(":selected").attr("id"));
   	setTimeout(function() {
  		
  		getShirtColor($('#cmbFabric').children(":selected").attr("id"));
   		setTimeout(function() {

   			getSize($('#cmbFabric').children(":selected").attr("id"));
   		},500);

	}, 500);

        $('#divnewcustomer').show();
        $('#page-cover2').show();
    });
    
    //btnNext
    $('#btnNext').click( function() {

    	var proname = $('#txtProjName').val(), type = $('#cmbType').val(), brand = $('#cmbBrand').val(),
    	size = $('#cmbSize').val(), qty = $('#txtAddQty').val(), fabric= $('#cmbFabric').val(), print = $('#cmbPrint').val();

    	if (proname !='' && qty!='' && color!=''){

        	$('#pop-up-form').hide();
        	$('#pop-up-form2').css('display','inline-block');
    	} else {
    		alert('Fill out the form first');
    	}
    	
    });
    
    //btncancel
    $('#btnCancel').click( function() {
        $('#pop-up-form').hide();
        $('#page-cover2').hide();
    });
    
     //btnback2
    $('#btnBack2').click( function() {
        $('#pop-up-form2').css('display','none');
        $('#pop-up-form').show();
    });

    $('#btnSubmit').click(function() {
    	var proname = $('#txtProjName').val(), ssdID = $('#cmbSize').children(":selected").attr("id"), colorID = color, 
    	printID = $('#cmbPrint').children(":selected").attr('id'), qty = $('#txtAddQty').val(),
    	budget = $('#txtAddBgt').val(), note = $('#txtAddNote').val();
    	var pType = $('#popup2wrapper div.active').attr('id');
    	var upldNote = $('#txtLink').val();
    	var uId = $('#txtsearch2').val();

    	if (proname !=""  && qty!="" ){

    		if (pType == 'divupfile'){
    			var fData = new FormData();
			fData.append('file', $('#upldfile').prop('files')[0]);
			fData.append('prname', proname);
			fData.append('shirtQty', qty);
			fData.append('shirtColor', colorID);
			fData.append('shirtSize', ssdID);
			fData.append('shirtBgt', budget);
			fData.append('shirtNote', note);
			fData.append('shirtPrint', printID);
			fData.append('customer_id', uId);
			fData.append('temploc', temploc);

			var ext = $('#upldfile').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['psd','zip','rar', 'txt']) == -1) {
		    	alert('invalid extension!');
			} else {
				$.ajax({
				url: '../create/wp/manager/upload.php',dataType: 'text',cache: false,contentType: false,
				processData: false, data: fData, type: 'post',success: function(data){
					
	    			if(data == 'success'){
	    				$('#pop-up-form2').hide();
	    				$('#page-cover2').hide();
	    				getFiles('RAW');
	    			} else {
	    				alert(data);
	    			}
				}
				});
			}
    		}
    		else {

    			$.post('../create/wp/manager/create.php', {prname: proname, shirtQty: qty, shirtSize: ssdID, prType: pType, shirtColor: colorID, shirtPrint: printID, upldNote: upldNote, shirtBgt: budget, shirtNote: note, customer_id: uId, temploc: temploc}, function(data){

    			if(data == 'success'){
    				$('#pop-up-form2').hide();
    				$('#page-cover2').hide();
    				getFiles('RAW');
    			} else if (data == 'created'){
    				$('#pop-up-form2').hide();
    				$('#page-cover2').hide();
    			
				window.location.href = '../create/mixl/project/' + uId + '/' +proname;
    			} 
    			else {
    				alert(data);
    			}
    		});
    		}
    		
    	} else {
    		alert('Fill out the form first');
    	} 
    });
    
    $('#btnexit').click( function() {
        $('#divproperties').hide();
        $('#page-cover2').hide();
    });
    
    $('#btnexit2').click( function() {
        $('#divpricing').hide();
        $('#page-cover2').hide();
    });
    
    $('#btnexit3').click( function() {
        $('#divnewcustomer').hide();
        $('#page-cover2').hide();
    });
    
    $('#btnSubmit').hover( function() {
		$('#btnSubmit').fadeIn(300);
		$('#btnSubmit').css("background-color","white");
		$('#btnSubmit').css("color","black");

	}, function() {
		$('#btnSubmit').fadeIn(300);
		$('#btnSubmit').css("background-color","#222d32");
		$('#btnSubmit').css("color","white");
		$('#btnSubmit').css("font-weight","bold");
	});
    
    //btnsearch
    $(document).on("mouseover", "#btnSearch2", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
        $("#imgsearch2").attr("src","../create/wp/res/search2.png");
	});
	
	$(document).on("mouseout", "#btnSearch2", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
        $("#imgsearch2").attr("src","../create/wp/res/search.png");
	});
    
    $('#btnSearch2').click( function() {

    	var id = $('#txtsearch2').val();
    	$.post('../create/wp/manager/checkUser.php',{custID: id}, function(data){
    		if(data == 'success'){
    			$('#pop-up-form').show();
       			$('#divnewcustomer').hide();
    		} else {
    			alert('Customer name not found.');
    		}
    	});
    });

});
