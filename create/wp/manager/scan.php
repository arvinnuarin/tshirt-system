<?php
include("../../conf/config.php");

$stat = $_POST['status'];
$stmt = $dbc->prepare("SELECT build_no, pr_name, type, location FROM draw_build WHERE status='$stat'");
$stmt->execute();
$stmt->store_result();
$data = $stmt->num_rows();

$files[] = array();

if ($data>0) {
	
	$stmt->bind_result($bno, $prname, $type, $path);
	while($stmt->fetch()) {
	
		if ($type == 'folders'){

			array_push($files, array('name'=> $prname, 'type'=> 'folder', 'build'=>$bno, 'path'=>$path, 'items'=> scandir('../../custom/'.$path)));
		}
		else if ($type=='files') {
			$ext = pathinfo($path, PATHINFO_EXTENSION);

			array_push($files, array('name'=> $prname.'.'.$ext, 'type'=> 'file', 'build'=>$bno, 'path'=>$path, 'size'=> filesize('../../custom/'.$path)));
		}
	}
}

header('Content-type: application/json');

echo json_encode(array(
	"name" => "projects",
	"type" => "folder",
	"path" => "build/projects",
	"items" => $files
)); 


?>