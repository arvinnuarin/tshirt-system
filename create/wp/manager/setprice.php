<?php
include('../../conf/config.php');

$loc = $_POST['location'];
$price =$_POST['price'];
$stat = 'PROCESSED';

$stmt = $dbc->prepare("UPDATE draw_build SET price=?, status=? WHERE location=?");
$stmt->bind_param('dss', $price, $stat, $loc);
$stmt->execute();
$stmt->store_result();
$data = $stmt->num_rows();

echo 'The project price has been updated.';
?>