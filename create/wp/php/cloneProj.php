<?php
include("../../conf/config.php");

$prloc = $_POST['prlocation'];
$stat = 'PROCESSED';
$type = 'folders';

$x = explode('/',$prloc);
$uID = $x[4];
$prnm = $x[5].'-suggest';

$dbprloc  =  'build/'.$uID.'/'.$x[5].'/';

$cr = date("Y-m-d H:i:s");
$loc = 'build/'.$uID.'/'.$prnm.'/';
$clone = '../../custom/'.$loc;
$custom_id = 4;

function copyFolder($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                copyFolder($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir);
    return true; 
} 

if(copyFolder($prloc, $clone) == true){

    $stmt = $dbc->prepare("SELECT ssd_id, sc_id, pm_id, sht_qty, budget FROM draw_build WHERE location=?");
    $stmt->bind_param("s", $dbprloc);
    $stmt->execute();
    $stmt->store_result();
    $data = $stmt->num_rows();

        if ($data == 1){

            $stmt->bind_result($s, $c, $p, $q,$b);

            while($stmt->fetch()){

                    $ssd = $s; $clrId =$c; $pmId = $p; $qty = $q; $bgt = $b;
            }

            $stmt = $dbc->prepare("INSERT INTO product_design(name, description, category_id) VALUES ('$prnm', 'Custom Design Project', $custom_id);");
            $stmt->execute();

              if ($stmt->affected_rows == 1) {
                    $stmt = $dbc->prepare("INSERT INTO product(pd_id, clothing_color_id) VALUES (LAST_INSERT_ID(), $clrId)");
                    $stmt->execute();
                    $prodId = $dbc->insert_id;
             }

             $stmt = $dbc->prepare("INSERT INTO draw_build (customer_id, pr_name, type, location, status, date_created, date_modified, ssd_id, sc_id, pm_id, product_id, sht_qty, budget, price)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

             $stmt->bind_param("issssssiiiiidd", $uID, $prnm, $type, $loc, $stat, $cr, $cr, $ssd, $clrId, $pmId, $prodId, $qty, $bgt, $bgt);
             $stmt->execute();

                if ($stmt->affected_rows == 1) {

                    echo 'success*'.$uID;    
                }
                else{
        
                    echo 'something go wrong';
                }
        } else {

            echo 'Project not found';
        }
}	

?>