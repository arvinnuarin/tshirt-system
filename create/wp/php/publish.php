<?php
include('../../conf/config.php');

$price = $_POST['price'];
$bno = $_POST['buildNo'];
$stat = 'PROCESSED';

$stmt = $dbc->prepare("UPDATE draw_build SET status=?, price=? WHERE build_no=?");
$stmt->bind_param("sdi",$stat,$price,$bno);
$stmt->execute();

?>