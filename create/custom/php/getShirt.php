<?php


$com = $_POST['com'];

if ($com == "brand") {
	fetchData("SELECT cb_id, brand_name FROM clothing_brand");
} else if($com == "type") {
	fetchData("SELECT ct_id, type FROM clothing_type");
} else if($com == "print") {
	fetchData("SELECT pm_id, name FROM print_method WHERE availability='Available'");
} else if ($com == 'fabric'){

	$brd = $_POST['brand']; $ty = $_POST['type'];
	fetchData("SELECT DISTINCT cl.clothing_id, fb.name FROM clothing cl INNER JOIN clothing_fabric fb ON cl.cf_id=fb.cf_id WHERE cl.cb_id =$brd and cl.ct_id =$ty"); 
} else if($com == "color") {

	$cID = $_POST['clothing'];
	fetchColor($cID); 
} 

else if ($com == 'size'){

	$cID = $_POST['clothing'];
	fetchSize("SELECT ssd_id, size FROM clothing_size_desc WHERE clothing_id =$cID ORDER BY size ASC");
}

function fetchData($query){

include("../../conf/config.php");

	$stmt = $dbc->prepare($query);
	$stmt->execute();
	$stmt->store_result();
	$data = $stmt->num_rows();

	$output[] = array();

		if ($data>0) {

			$stmt->bind_result($bID, $bName);
			while($stmt->fetch()) {

				array_push($output, array('id'=>$bID, 'name'=> $bName));
			}
		}

		header('Content-type: application/json');
		echo json_encode($output);

$stmt->close();	
}

function fetchColor($c){

include("../../conf/config.php");

	$stmt = $dbc->prepare("SELECT clothing_color.sc_id, CONCAT(clothing_brand.brand_name, '/',clothing_type.type, '/',clothing_fabric.name, '/', clothing_color.hex) AS 'location', clothing_color.hex , clothing_color.color FROM clothing_color INNER JOIN clothing on clothing.clothing_id = clothing_color.clothing_id INNER JOIN clothing_brand ON clothing_brand.cb_id = clothing.cb_id INNER JOIN clothing_type ON clothing_type.ct_id = clothing.ct_id INNER JOIN clothing_fabric ON clothing_fabric.cf_id = clothing.cf_id WHERE clothing.clothing_id = ?");
	$stmt->bind_param("i", $c);
	$stmt->execute();
	$stmt->store_result();
	$data = $stmt->num_rows();

	$output[] = array();

		if ($data>0) {

			$stmt->bind_result($id, $loc, $hex, $color);
			while($stmt->fetch()) {

				array_push($output, array('id'=>$id, 'Name'=> $color, 'Hex'=> $hex, 'Location'=>$loc));
			}
		}

		header('Content-type: application/json');
		echo json_encode($output);

$stmt->close();	
}

function fetchSize($query){

include("../../conf/config.php");

	$size = unserialize(SIZE);
	$stmt = $dbc->prepare($query);
	$stmt->execute();
	$stmt->store_result();
	$data = $stmt->num_rows();

	$output[] = array();

		if ($data>0) {

			$stmt->bind_result($bID, $bName);
			while($stmt->fetch()) {

				array_push($output, array('id'=>$bID, 'name'=> $size[$bName]));
			}
		}

		header('Content-type: application/json');
		echo json_encode($output);

$stmt->close();	
}



?>