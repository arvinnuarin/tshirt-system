<?php
include('../../conf/config.php');
session_start();

$prData = $_POST['prData'];
$area = $_POST['shtArea'];
$color = $_POST['shtColor'];
$bgt = $_POST['shtBudget'];
$qty = $_POST['shtQty'];
$note = $_POST['shtNote'];
$custName = $_SESSION['customer_id'];
$bNo = $_POST['buildNo'];
$cs = date("D F j, Y, g:i a");

$clrId = $_POST['colorID'];
$pmId = $_POST['pmID'];
$ssdId = $_POST['ssdID'];

$brand = $_POST['shtBrand'];
$type = $_POST['shtType'];
$size = $_POST['shtSize'];
$fType = $_POST['fType'];
$pType = $_POST['pType'];

$tloc = $_POST['tempLoc'];

$loc = '../'. $_POST['prloc'] . '/';
$imgloc = $loc.'output/'.$area.'_design.png';

$noteTxt = 'Customer ID: '.$custName .PHP_EOL. 'Date Saved: '.$cs.PHP_EOL.'Type: '.$type.PHP_EOL.'Brand: '.$brand.PHP_EOL.'Color: '.$color.PHP_EOL.'Size: '.$size.PHP_EOL.'Quantity: '.$qty.PHP_EOL.'Budget: '.$bgt.PHP_EOL.'Fabric Type: '.$fType.PHP_EOL.'Printing Type: '.$pType.PHP_EOL.'Note: '.$note;

function createShirtImage($shtColor, $shtDesign, $ar, $shtLoc, $tLoc) {

        $stamp = imagecreatefrompng($shtDesign);
        $im = imagecreatefrompng("../../../assets/shirt_template/". $tLoc. "/".$ar.".png");
        $marge_right = 140;
        $marge_bottom = 30;
        imagesavealpha( $im, true );
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);
        imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

        imagepng($im, $shtLoc, 0);
        imagedestroy($im); 
} 

if (json_decode($prData) != null) {

        $file = fopen($loc.$area.'.json','w+');
        fwrite($file, $prData);
        fclose($file);

        if ($area == 'front'){

              $ftxt = fopen($loc.'notes.txt','w+');
              fwrite($ftxt, $noteTxt);
              fclose($ftxt);
        }

        $eData = explode(',', $_POST['desImg']);
        $img = imagecreatefromstring(base64_decode($eData[1]));
        imageAlphaBlending($img, true);
        imageSaveAlpha($img, true);

        if ($img) {

              imagepng($img, $imgloc, 0);
              imagedestroy($img); 

              createShirtImage($color, $imgloc, $area, $loc.'/output/'.$area.'_shirt.png', $tloc);
              echo 'Project has been saved successfully.';
        }  else {
              
              echo 'failed';
        } 
} else  {
        echo 'error';
}	

$cr = date("Y-m-d H:i:s");
$stmt = $dbc->prepare("UPDATE draw_build SET date_modified=?,ssd_id=?, sc_id=?, pm_id=?, sht_qty=?, budget=? WHERE build_no=?");
$stmt->bind_param("siiiidi",$cr, $ssdId, $clrId, $pmId, $qty, $bgt, $bNo);
$stmt->execute();

?>