<?php

	$prloc = $_POST['prloc'];
	$target = '../'.$prloc.'/res/'.uniqid().'_'.basename( $_FILES['file']['name']);

	$types = array('image/jpeg', 'image/gif', 'image/png');
	
	if (in_array($_FILES['file']['type'], $types)) {
		
		if(move_uploaded_file($_FILES['file']['tmp_name'], $target)){
				echo 'uploaded*custom/'.$target;
		} else {
			echo "There was a problem uploading your file.";
		}

	} else {
		echo "Image files are only accepted.";
	} 
?>
