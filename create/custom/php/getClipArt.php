<?php

$type = $_POST['type'];

$dir = '../res/clipart/'.$type;
$path ='./res/clipart/'.$type;

$response = scan($dir, $path);

function scan($dir, $fpath){
	$files = array();

	if(file_exists($dir)){

		foreach(new DirectoryIterator($dir) as $f) {
		
			if($f->isDot()) {
				continue;
			}

			if ($f->isFile()){

				$files[] = array(
					"name" => $f->getBasename('.png'),
					"path" => $fpath.'/' . $f->getFilename()
				);
			}
		}
	}
	else {
		echo ' does not exist';
	}

	return $files;
}

header('Content-type: application/json');

echo json_encode(array(
	"name" => "clipart",
	"path" => $dir,
	"items" => $response
)); 