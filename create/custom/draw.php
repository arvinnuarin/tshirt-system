<!DOCTYPE html>
<html>
<head>

	<title>MixlArts: Drawing</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
		include("../conf/config.php");
		echo '<base href='.$cusbhref.'/>'
	?>
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.contextMenu.css"/>
	<link rel="stylesheet" type="text/css" href="css/fontselect.css"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-confirm.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/introjs.css"/>
    <link rel='stylesheet' href='css/spectrum.css' />
	
    <script src="lib/fabric.js" type="text/javascript"></script>
	<script src="lib/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="lib/circle-progress.min.js" type="text/javascript"></script>
    <script src="lib/jquerytransform2d.js" type="text/javascript"></script>
	<script src="lib/jquery-hotkeys.js" type="text/javascript"></script>
	<script src="lib/fonts.js" type="text/javascript"></script>
	<script src="lib/sortable.js" type="text/javascript"></script>
	<script src="lib/jquery.fontselect.js" type="text/javascript"></script>
 	<script src="lib/jquery.contextMenu.js" type="text/javascript"></script>
    <script src="lib/jquery.ui.position.js" type="text/javascript"></script>
    <script src="lib/jquery-confirm.min.js" type="text/javascript"></script>
    <script src="lib/jquery.panzoom.js" type="text/javascript"></script>
	<script src="js/custom.js?random=<?php echo uniqid(); ?>" type="text/javascript"></script>
    <script src='lib/spectrum.js'></script>
    <script src='lib/intro.js'></script>
</head>
<body class="unselectable">

<?php
session_start();
$usrID = $_SESSION['customer_id'];
	$exist = false; $prname="";
	if (isset($_GET['prname'])) {

        $prname = $_GET['prname'];

		$stmt = $dbc->prepare("SELECT CONCAT(cs.lname , ', ', cs.fname) , bui.build_no, bui.pr_name, bui.location, bui.price FROM draw_build bui INNER JOIN customer cs ON bui.customer_id=cs.customer_id WHERE bui.pr_name=? AND bui.customer_id=? LIMIT 1");
        $stmt->bind_param('si', $prname, $usrID);
		$stmt->execute();
		$stmt->store_result();
		$data = $stmt->num_rows();

		if ($data == 1){

			$stmt->bind_result($n, $b, $pr, $loct, $p);

			while($stmt->fetch()) {

				$exist = true;
                $cname = $n;
                $bl = $b;
				$prname = $pr;
				$loc = $loct;
                $actP = $p;
			}
		}
	}

	if ($exist == true) {
		showDrawing($cname, $bl, $prname, $loc, $actP);
	} else {
		header('Location: '.$cusbhref.'manager');
	}

	function showDrawing($c, $bld, $p, $l, $ap) {
	echo "
		<div id = 'container'>
			<div id = 'wrapper'>
				<div id = 'divtool'>
					<div id = 'toolwrapper'>
						<div id = 'tool1'>
							<ul class='tab'>
								<li title = 'Hide/Show Tool Tab'><a id='btnDrawer'><img id='icon' src='./res/drawer.png'></a></li>
                                <li title='Save and Exit'><a id='btnSvExit'><img id='icon' src='./res/saveexit.png'></a></li>
								<li title = 'Save Project'><a id='btnSave'><img id='icon' src='./res/save.png'></a></li>
							</ul>
						</div>

                        <div id='tool0'>
                            <label id = 'bno'>Build No: </label>
                            <label id='bldNo'>$bld</label>
                        </div>

						<div id = 'tool2'>
							<div id = 'tool2wrapper'>
								<ul class='tab2'>
                                    <li id='tutsht' title = 'Shirt' ><a id = 'item' class='tablinks active' name='Item'><img id='icon' src='./res/shirt.png'></a></li>
                                    
							<li id='tutTool' title = 'Tool' ><a id='btnCancel' name='Select' class='tablinks'><img id='icon' src='./res/pointer.png'></a></li>
							
							<li id='tutDraw' title = 'Draw'><a id='btnPen' class='tablinks' name='Pen'><img id='icon' src='./res/pen.png'></a></li>
							<li id= 'tutText' title = 'AddText' ><a id='btnText' class='tablinks' name='Text'><img id='icon' src='./res/text.png'></a></li>
							<li id='tutShape' title = 'Add Shape'><a id='btnShape' class='tablinks' name='Shape'><img id='icon' src='./res/shape.png'></a></li>
							<li id='tutImg' title = 'Add Image/Clipart'><a id='btnImg' class='tablinks' name='Image'><img id='icon' src='./res/image.png'></a></li>
								</ul>
							</div>
						</div>

						<div id = 'tool3'>
							<div id = 'tool3wrapper'>
								<ul class='tab'>
                                    <li title = 'Help'> <a id='btnHelp'><img id='icon' src='./res/help.png'></a></li>
									<li title = 'Hide/Show Layer Tab'><a id='btnLayer'><img id='icon' src='./res/layer.png'></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div id='leftSidenav' class='lsidenav'>
							<div id = 'lbltool'>Item</div>
                            <div id='tabcontent1'></div>
                        <div id = 'lwrapper'>
                        
                            <div id='Item' class='tabcontent'>
                                <div id  = 'divitema'>
                                    <label id = 'lblcolor'>Actual Shirt Price</label>
                                    <input type='number' min='1' max='100000' id='txtShtPr' readonly value = ''></input>
                                </div>
                            
                                <div id ='tutShtPref'>
                                <div id = 'divitemd'>
                                    <div id = 'divType'>
                                        <label id = 'lblarea'>Shirt Type</label><br>  
                                        <div>
                                        <select id='cmbType' class = 'styled-select blue semi-square'>
                                        </select>
                                        </div>
                                    </div>
                                    
                                    <div id = 'divBrand' >
                                        <label id = 'lblarea'>Shirt Brand</label><br>  
                                        <div>
                                            <select id='cmbBrand' class = 'styled-select blue semi-square'>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div id = 'divitemd'>
                                    <div id = 'divSize'>
                                        <label id = 'lblarea'>Size</label><br>  
                                        <div>
                                            <select id='cmbSize' class = 'styled-select blue semi-square'>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div id = 'divQuan'>
                                        <label id = 'lblarea'>Quantity</label><br>  
                                        <div>
                                           <input type = 'number' min='1' max = '1000' id = 'txtAddQty'></input>
                                        </div>
                                    </div>
                                </div>
                                
                                <div id = 'divitemd'>
                                    <div id = 'divFabric'>
                                        <label id = 'lblarea'>Fabric Type</label><br>  
                                        <div>
                                            <select id='cmbFabric' class = 'styled-select blue semi-square'>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div id = 'divPrint'>
                                        <label id = 'lblarea'>Printing Type</label><br>  
                                        <div>
                                           <select id='cmbPrint' class = 'styled-select blue semi-square'>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div id='shtSw'>
                                <div id = 'divitema'>
                                        <label id = 'lblarea'>Shirt Area</label><br>  
                                        <div id = 'divitemabtn'>
                                            <button id='btnFront' class= 'active'>FRONT</button>
                                            <button id='btnBack' class= ''>BACK</button>
                                        </div>
                                </div>
                                </div>
                                
                                <div id ='shtChColor'>
                                <div id  = 'divitema'>
                                    <label id = 'lblcolor'>Shirt Color</label>
                                    <div id='avlclr'></div>
                                </div>
                                </div>
                                
                                <div id='shtAddBgt'>
                                <div id = 'divitema'>
                                    <div id = 'divitemb' >
                                        &#8369;<input type='number' min='1' max='100000' id='txtAddBgt' placeholder='Your Shirt Budget' per Piece></input>
                                        <textarea spellcheck='false' type='text' id='txtAddNote' placeholder ='Type Notes here'></textarea>
                                    </div>
                                </div>
                                </div>
							</div>
                        
                            <div id='Select' class='tabcontent'>
								<div id='sel-major'>
                                    <label>Major Tools</label>
                                    <div id='sel-options'>
                                        <img title = 'Move/Zoom Tool' id='btnPan'  class = 'selecttool1' src = './res/move.png'>
                                    </div>
                                </div>
                                <div id='sel-other'> 
                                    <label>Other Tools</label>
                                    <div id='sel-options'>
                                        <img title = 'Undo' id='btnCnvUndo' class = 'selecttool' src = './res/undo.png'>
                                        <img title = 'Redo' id='btnCnvRedo' class = 'selecttool' src = './res/redo.png'>
                                        <img title = 'Clear Layer' id='btnClr' class = 'selecttool' src = './res/clear.png'>
                                    </div>
                                </div>
                            </div>

							<div id='Pen' class='tabcontent'>
								 <div id='line-options'>
                                    <div id = 'penline'>
                                        <label>Line Mode</label>
                                        <div>
                                            <img title = 'Pencil' id='btnPencil'  class = 'pentool' src='./res/pen.png' onclick = 'PenPencil()' >
                                            <img title = 'Circle' id='btnCircle' class = 'pentool' src='./res/circle.png' onclick = 'PenCircle()' >
                                            <img title = 'Spray' id='btnSpray' class = 'pentool' src='./res/spray.png'onclick = 'PenSpray()'>
                                            <img title = 'Square' id='btnSquare' class = 'pentool' src='./res/square.png'onclick = 'PenSquare()'>
                                        </div>
                                    </div>

                                    <div id = 'penpattern'>
                                        <label>Line Pattern</label>
                                        <div>
                                            <img title = 'Vertical Lines' id='btnHLine' class = 'pentool' src='./res/vline.png' onclick = 'PatternHLine()' >
                                            <img title = 'Diamond' id='btnDiamond' class = 'pentool' src='./res/diamond.png' onclick = 'PatternDiamond()' >
                                            <img title = 'Dots' id='btnDots' class = 'pentool'src='./res/dots.png' onclick = 'PatternDots()' >
                                            <img title = 'Square' id='btnSquarePattern' class = 'pentool' src='./res/psquare.png' onclick = 'PatternSquare()'>
                                        </div>
                                    </div>
								</div>
							</div>

							<div id='Text' class='tabcontent'>
								<div id = 'text-options'>
                                    <div id = 'fonttool'>
                                        <label>Font</label>
                                        <div id='addfont'>
                                        <div id = 'fonttool1'>
                                            <div id = 'divfont'>
                                                <input id='selectfont' type='text''></select>
                                                <select id='fnt_size' class='styled-select blue semi-square'></select>
                                            </div>
                                            
                                            <div id = 'divalign'>
                                               <div id = 'divtleft' class='talign'><img title = 'Left Align' id='btnTLeft' class='icon3' src='./res/tleft.png'></div>
                                               <div id= 'divtright' class='talign'><img title = 'Right Align' id='btnTRight' class='icon3' src='./res/tright.png'></div>
                                               <div id= 'divtcenter' class = 'talign active'><img title = 'Center' id='btnTCen' class='icon3' src='./res/tcenter.png'></div>
                                               <div id= 'divtjust' class='talign'><img title = 'Justify' id='btnTJus' class='icon3' src='./res/tjustify.png'></div>
                                            </div>
                                            
                                            <div id = 'divweight'>
                                                <div id = 'divtbold' class = 'tweight'><img title = 'Bold' id='btnTBold' class='icon4' src='./res/tbold.png'></div>
                                                <div id= 'divtitalic' class = 'tweight'><img title = 'Italic' id='btnTItalic' class='icon4' src='./res/titalic.png'></div>
                                            </div>
                                        </div>
            		</div></div>

            	    <div id = 'fonttool'>
                                        <label>Text</label>
                                        <div id='addtxt'>
                                        <div id = 'fonttool1'>
                                            <textarea spellcheck='false' type='text' id='txtAddText' placeholder ='Enter Text'></textarea>
                                            <button id='btnTxt'>Add Text</button>
                                        </div>
                                    </div>
                                    </div>
					</div>
							</div>

							<div id='Shape' class='tabcontent'>
								<div id='shape-options'>
									<img title = 'Rectangle' id='btnRect' class='shape' src = './res/rectangle.png'>
									<img title = 'Circle' id='btnCirc' class='shape' src = './res/circle.png'>
									<img title = 'Triangle' id='btnTri' class='shape' src = './res/triangle.png'>
									<img title = 'Star' id='btnStar' class='shape' src = './res/star.png'>
									<img title = 'Heart' id='btnHrt' class='shape' src = './res/heart.png'>
									<img title = 'Diamond' id='btnDia' class='shape' src = './res/diamond2.png'>
									<img title = 'Line' id='btnSLine' class='shape' src = './res/line.png'>
									<img title = 'Pentagon' id='btnPenGon' class='shape' src = './res/pentagon.png'>
									<img title = 'Hexagon' id='btnHexGon' class='shape' src = './res/hexagon.png'>
									<img title = 'Heptagon' id='btnHepGon' class='shape' src = './res/heptagon.png'>
									<img title = 'Octagon' id='btnOctGon' class='shape' src = './res/octagon.png'>
									<img title = 'Heptagram' id='btnHepGram' class='shape' src = './res/heptagram.png'>
									<img title = 'Dodecagram' id='btnDodGram' class='shape' src = './res/dodecagram.png'>
									<img title = 'Shuriken' id='btnShKen' class='shape' src = './res/shuriken.png'>
									<img title = 'Bubble' id='btnBubble' class='shape' src = './res/bubble.png'>
								</div>
							</div>

							<div id='Image' class='tabcontent'>

                                                                                                            <div id='upload-img'>
								<div id='img-options'>
                                    <button class='inputimg'>Upload Image</button>
									<input accept='image/*' type='file' id='upldimg' style= 'display:none;'/>
								</div></div>
                                
                                <div id = 'lblClip'>CLipart</div>
                                
                                <div id='select-clip'>
                                <div id='img-options'>
                                    <select id='cmbClipArt' class = 'styled-select blue semi-square'>
                                        <option name='animals'>Animals</option>
                                        <option name='emoticon'>Emoticon</option>
                                        <option name='place'>Places</option>
                                        <option name='transpo'>Transportation</option>
                                        <option name='pokemon'>Pokemon</option>
                                    </select>
                                   
							     </div></div>
                                 
                                 <div id='img-options'>
                                     <div id = 'clipwrapper'>
                                        <div id='clipprogress'>
                                                <strong>100<i>%</i></strong>
                                         </div>
                                         <div id = 'clipblack'></div>
                                        <div id='clpart-options'></div>
							         </div>
							     </div>
							</div>

							

                            <div id= 'color-options'>
                                <div id = 'colortool'>
	                                <div id = 'penwidth'>
	                                    <label>Stroke Width</label>
	                                    <div>
	                                     <input type='range' value='2' min='2' max='20' id='ln_width'><br>
										</div>
									</div>
                                    <label>Stroke color</label>
                                    <div id='strokecol'>
                                    <div id = 'colortool1'>
                                    
                                        <div id = 'ctwrapper'>
                                            <input type='text' id='ln_color'/>
                                            <input id = 'lbllncolor' value = '#000000'  disabled/>
                                        </div>
                                         
                                        <div id = 'ctwrapper2'>
                                            <label class='switch'>
                                            <input type='checkbox' id='enStrokeCol' checked>
                                            <div class='slider round'></div>
                                            </label>
                                       </div>
                                       </div>
                                    </div>
                                </div>
                                
                                <div id = 'colortool'>
                                    <label>Fill color</label>
                                    <div id='fillcol'>
                                    <div id = 'colortool1'>
                                    
                                        <div id = 'ctwrapper'>
                                            <input type='text' id='ln_fill'/>
                                            <input id = 'lblfillcolor' value = '#f00000' disabled/>
                                        </div>
                                        
                                        <div id = 'ctwrapper2'>
                                            <label class='switch'>
                                            <input type='checkbox' id='enFillCol' checked>
                                            <div class='slider round'></div>
                                            </label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
				    </div>
				</div>

	           <div id='rightSidenav' class='rsidenav'>
                    <div id = 'divlayer' >
                        <div id = 'lbllayer'>LAYERS</div>
                        <div id = 'divlayertool'>
                            <img title = 'Delete Layer' onclick = 'deleteObjects()' id = 'layertool-del' src = './res/delete.png'>
                            <img title = 'Duplicate Layer' onclick = 'duplicateObj()' id = 'layertool-duplicate' src = './res/duplicate.png'>
                        </div>
                    </div>

					<div id='draw-layers'>
						<div id='listWithHandle' class='list-group'>
                        </div>
					</div>
				</div>

				<div id = 'content'>
					<div id = 'contentwrapper' >
						<div class ='frnt_sht' >
                            <div id = 'canvaswrapper' >
                                <img id='shirt' src='./res/shirt_col/00000/front.png' name = '00000'>
                                <div id= 'frnt_cnv'>
                                    <canvas id='c' width='250' height='400'></canvas>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

         <div id = 'page-cover3'>
            
        </div> 
        <div id = 'help-popup2'>
            <img id = 'btnexit' src = '../res/exit.png'/>
            <table id = 'helpcontrols'>
            <th>Shortcut Key</th><th>Description</th>
            </table>
        </div> 
		";
        
		echo "
		<script src='js/draw1.js?random=<?php echo uniqid(); ?>'  type='text/javascript'></script>
		<script src='js/draw.js?random=<?php echo uniqid(); ?>'></script>
		<script>
		loadProject('$p','$l','front', '$c', $ap);
		</script>
		";

	}
?>
</body>
</html>