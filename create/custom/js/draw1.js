var canvas = this.__canvas = new fabric.Canvas('c', {
	isDrawingMode: false,

});

canvas.onselectstart = function () { return false; }
saveState();
var newFont = 'Aclonica';
var newFontSize = 40;

//layering
fabric.Canvas.prototype.getItemByMyID = function(myID) {
	var object = null,
	objects = this.getObjects();

	for (var i = 0, len = this.size(); i < len; i++) {

		if (objects[i].id&& objects[i].id=== myID) {
			object = objects[i];
			break;
		}
	}
	return object;
};

function modal(title,txt){
    
    $.alert({
            icon: 'fa fa-info',type: 'green',useBootstrap: false,animation: 'rotateXR',closeAnimation: 'scale',
                title: title,
                content: txt,
        }); 
}     

var layerNo;
$("#listWithHandle").on('click', '.list-group-item', function () {
	layerNo = $(this).attr('id');
	canvas.renderAll();
	canvas.setActiveObject(canvas.getItemByMyID(layerNo));

    $('.list-group-item').removeClass('active');
    $(this).addClass('active');

});

var chkClk = false;
$('#listWithHandle').on('click', '.visb', function(){

	var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();
	var view = false;

	if (chkClk == false){
		view = false;
  		chkClk = true;
  		$(this).attr("src","./res/hide.png");
	} else {
		view = true;
  		chkClk = false;
  		$(this).attr("src","./res/show.png");
	}

  	if (actObj) {
  		actObj.visible = view;
  	}
  	else if (actGrp) {

  		var objGrp = actGrp.getObjects();
  		canvas.discardActiveGroup();
  		objGrp.forEach(function(o) {
  			o.visible = view;
  		});
  	}
  	canvas.renderAll();
});

Sortable.create(listWithHandle, {
	group: "sorting",
	sort: true,

	onSort: function (evt) {

		var obj = canvas.getItemByMyID(layerNo);
		if (evt.newIndex > evt.oldIndex) {

			for (x=evt.newIndex; x > evt.oldIndex; x--){

				canvas.sendBackwards(obj);
			}
		} else{

			for (x=evt.newIndex; x < evt.oldIndex; x++){
				canvas.bringForward(obj);
			}
		}
		canvas.setActiveObject(obj);
		canvas.renderAll();
	}
});

//context menu
$.contextMenu({
	selector: '.list-group-item',
	callback: function(key, options) {

		if (key == "delete") {
			deleteObjects();
		} else if (key == "duplicate") {
			duplicateObj();
		} else if (key == "flipX") {
			flipObject('x');
		} else if (key == "flipY") {
			flipObject('y');
		} else if (key == "rotLeft") {
				rotateObj(-45);
		} else if (key == "rotRight") {
				rotateObj(45);
		}
	},
	items: {
		"duplicate": {name: "Duplicate", icon: "fa-files-o"},
		"delete": {name: "Delete", icon: "fa-trash"},
		"flipX": {name: "Flip Horizontal", icon: "fa-exchange"},
		"flipY": {name: "Flip Vertical", icon: "fa-long-arrow-up"},
		"rotLeft": {name: "Rotate Left", icon: "fa-undo"},
		"rotRight": {name: "Rotate Right", icon: "fa-repeat"},
	}
});

canvas.on('object:selected',function(e){

    var actObj = canvas.getActiveObject(),actGrp = canvas.getActiveGroup();
    shapeCtr = 0;
    $('.list-group-item').removeClass('active');
    allowShape = false;
    allowClip=false;
    if (actObj) {
      $('#'+actObj.id).addClass('active');
  	}
  	else if (actGrp) {
  			var objectsInGroup = actGrp.getObjects();
  			objectsInGroup.forEach(function(o) {
  				$('#'+o.id).addClass('active');
  			});
  	}

$.contextMenu({
		selector: '.upper-canvas',
		callback: function(key, options) {

			
			if (key == "delete") {
				deleteObjects();
			} else if (key == "duplicate") {
				duplicateObj();
			} else if (key == "flipX") {
				flipObject('x');
			} else if (key == "flipY") {
				flipObject('y');
			} else if (key == "rotLeft") {
				rotateObj(-45);
			} else if (key == "rotRight") {
				rotateObj(45);
			}
		},
		items: {
			"duplicate": {name: "Duplicate", icon: "fa-files-o"},
			"delete": {name: "Delete", icon: "fa-trash"},
			"flipX": {name: "Flip Horizontal", icon: "fa-exchange"},
			"flipY": {name: "Flip Vertical", icon: "fa-long-arrow-up"},
			"rotLeft": {name: "Rotate Left", icon: "fa-undo"},
			"rotRight": {name: "Rotate Right", icon: "fa-repeat"},
		}
	});
});

//tool functions
function drawPath(){
	canvas.isDrawingMode = true;
}

function createRegularPoly(sideCount,radius){
    var sweep=Math.PI*2/sideCount;
    var cx=radius;
    var cy=radius;
    var points=[];
    for(var i=0;i<sideCount;i++){
        var x=cx+radius*Math.cos(i*sweep);
        var y=cy+radius*Math.sin(i*sweep);
        points.push({x:x,y:y});
    }
    return(points);
}

function createStarPoly(spikeCount, outerRadius, innerRadius) {
  var rot = Math.PI / 2 * 3;
  var cx = outerRadius;
  var cy = outerRadius;
  var sweep = Math.PI / spikeCount;
  var points = [];
  var angle = 0;
  var shape;

  for (var i = 0; i < spikeCount; i++) {
    var x = cx + Math.cos(angle) * outerRadius;
    var y = cy + Math.sin(angle) * outerRadius;
    points.push({x: x, y: y});
    angle += sweep;

    x = cx + Math.cos(angle) * innerRadius;
    y = cy + Math.sin(angle) * innerRadius;
    points.push({x: x, y: y});
    angle += sweep
  }
  return (points);
}

var isDown = false, allowShape = true, shapeCtr = 0;
var shapeEnable = [false, false, false, false, false, false,false, false, false, false, false, false,false,false,false];
function drawShape(source){
	for (var x=0; x<shapeEnable.length; x++) {
		shapeEnable[x] = false;
	}

	if (source=='Circle'){
		shapeEnable[0] = true;
	} else if (source == 'Rectangle') {
		shapeEnable[1] = true;
	} else if (source == 'Triangle') {
		shapeEnable[2] = true;
	} else if (source == 'Diamond') {
		shapeEnable[3] = true;
	} else if (source == 'Line') {
		shapeEnable[4] = true;
	} else if (source == 'Heart') {
		shapeEnable[5] = true;
	} else if (source == 'Star') {
		shapeEnable[6] = true;
	} else if (source == 'Pentagon') {
		shapeEnable[7] = true;
	} else if (source == 'Hexagon') {
		shapeEnable[8] = true;
	} else if (source == 'Heptagon') {
		shapeEnable[9] = true;
	} else if (source == 'Octagon') {
		shapeEnable[10] = true;
	} else if (source == 'Heptagram') {
		shapeEnable[11] = true;
	} else if (source == 'Dodecagram') {
		shapeEnable[12] = true;
	} else if (source == 'Shuriken') {
		shapeEnable[13] = true;
	} else if (source == 'Speech') {
		shapeEnable[14] = true;
	}
	source ='';

	var coorX, coorY, shape, x=0;
	canvas.isDrawingMode = false;
	canvas.on('mouse:down', function(o){
	if (allowShape == true) {
		shapeCtr++;
		if (shapeEnable[0] == true) { //circle
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;

			shape = new fabric.Circle({
			left:coorX, top: coorY, originX: 'left', originY: 'top', radius: 60,
			angle: 0, fill: fll, stroke:str, strokeWidth:strWid,});

			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[1] == true){ //rectangle

			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;

			shape = new fabric.Rect({
			left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
			height: 50, angle: 0, fill: fll, stroke:str, strokeWidth:strWid,});

			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}

		else if (shapeEnable[2] == true){ //triangle

			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;

			shape = new fabric.Triangle({
			left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80, height: 80,
			angle: 0, fill: fll, stroke:str, strokeWidth:strWid,});

			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[3] == true){ //diamond

			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createRegularPoly(4,50),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 50,
					height: 50, angle: 0, fill: fll, stroke:str,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[4] == true){ //line
			isDown=true;
			var pointer = canvas.getPointer(o.e);
			var points = [ pointer.x, pointer.y, pointer.x, pointer.y ];
			shape = new fabric.Line(points, {
			    strokeWidth: 5, fill: fll, stroke: str, strokeWidth: strWid, originX: 'center', originY: 'center'
			});
  			if (shapeCtr == 1) {
				canvas.add(shape);
			}
		}
		else if (shapeEnable[5] == true){ //heart
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;

			 shape = new fabric.Path('M 272.70141,238.71731 \
			    C 206.46141,238.71731 152.70146,292.4773 152.70146,358.71731  \
			    C 152.70146,493.47282 288.63461,528.80461 381.26391,662.02535 \
			    C 468.83815,529.62199 609.82641,489.17075 609.82641,358.71731 \
			    C 609.82641,292.47731 556.06651,238.7173 489.82641,238.71731  \
			    C 441.77851,238.71731 400.42481,267.08774 381.26391,307.90481 \
			    C 362.10311,267.08773 320.74941,238.7173 272.70141,238.71731  \
			    z ');

			var scale = 0.3;
			shape.set({ left: coorX, top: coorY, scaleX: scale, scaleY: scale, fill: fll, stroke: str, strokeWidth:strWid});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[6] == true){ //pentagram
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createStarPoly(5,50,30),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, fill: fll, stroke:str,
					strokeWidth:strWid,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[7] == true){ //pentagon
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createRegularPoly(5,50),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, fill: fll, stroke:str,
					strokeWidth:strWid,
				});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[8] == true){ //hexagon
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createRegularPoly(6,50),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, fill: fll, stroke:str,
					strokeWidth:strWid,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[9] == true){ //heptagon
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createRegularPoly(7,50),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, perPixelTargetFind: true, fill: fll, stroke:str,
					strokeWidth:strWid,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[10] == true){ //octagon
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createRegularPoly(8,50),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, perPixelTargetFind: true, fill: fll, stroke:str,
					strokeWidth:strWid,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[11] == true){ //heptagram
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createStarPoly(7,50,30),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, fill: fll, stroke:str,
					strokeWidth:strWid,
				});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[12] == true){ //dodecagram
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createStarPoly(12,50,30),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, fill: fll, stroke:str,
					strokeWidth:strWid,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[13] == true){ //shuriken
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Polygon(
				createStarPoly(4,80,30),{
					left:coorX, top: coorY, originX: 'left', originY: 'top', width: 80,
					height: 80, angle: 0, fill: fll, stroke:str,
					strokeWidth:strWid,});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
		else if (shapeEnable[14] == true){ //shuriken
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			shape = new fabric.Path('M 45.673,0 C 67.781,0 85.703,12.475 85.703,27.862 C 85.703,43.249 67.781,55.724 45.673,55.724 C 38.742,55.724 32.224,54.497 26.539,52.34 C 15.319,56.564 0,64.542 0,64.542 C 0,64.542 9.989,58.887 14.107,52.021 C 15.159,50.266 15.775,48.426 16.128,46.659 C 9.618,41.704 5.643,35.106 5.643,27.862 C 5.643,12.475 23.565,0 45.673,0 M 45.673,2.22 C 24.824,2.22 7.862,13.723 7.862,27.863 C 7.862,34.129 11.275,40.177 17.472,44.893 L 18.576,45.734 L 18.305,47.094 C 17.86,49.324 17.088,51.366 16.011,53.163 C 15.67,53.73 15.294,54.29 14.891,54.837 C 18.516,53.191 22.312,51.561 25.757,50.264 L 26.542,49.968 L 27.327,50.266 C 32.911,52.385 39.255,53.505 45.673,53.505 C 66.522,53.505 83.484,42.002 83.484,27.862 C 83.484,13.722 66.522,2.22 45.673,2.22 L 45.673,2.22 z ');

			var scale = 2;
			shape.set({ left: coorX, top: coorY, scaleX: scale, scaleY: scale, fill: fll, stroke: str, strokeWidth:strWid});
			if (shapeCtr == 1) {
				canvas.add(shape);
				canvas.setActiveObject(shape);
			}
		}
	}
	});

	canvas.on('mouse:move', function(o){
		if(isDown) {
			var pointer = canvas.getPointer(o.e);
			if (shapeEnable[0] == true) {
				var radius = Math.max(Math.abs(coorY - pointer.y),Math.abs(coorX - pointer.x))/2;

				if (radius > shape.strokeWidth) {
					radius -= shape.strokeWidth/2;
				}
				shape.set({ radius: radius});

				if(coorX>pointer.x){
					shape.set({originX: 'right' });
				} else {
					shape.set({originX: 'left' });
				}

				if(coorY>pointer.y){
					shape.set({originY: 'bottom'  });
				} else {
					shape.set({originY: 'top'  });
				}
			}
			else if (shapeEnable[4] == true){
				shape.set({ x2: pointer.x, y2: pointer.y });
			}
			else {
				if(coorX>pointer.x){
				shape.set({ left: Math.abs(pointer.x) });
				}

				if(coorY>pointer.y){
					shape.set({ top: Math.abs(pointer.y) });
				}
			shape.set({ width: Math.abs(coorX - pointer.x) });
			shape.set({ height: Math.abs(coorY - pointer.y) });
			}
			canvas.renderAll();
		}
	});

	canvas.on('mouse:up', function(o){
		isDown = false;
		canvas.isDrawingMode = false;
	});
}

var clipPath='', sourceClip='clip', clipCtr=0, allowClip=true;
function addClipArt(){

	var isDown, coorX, coorY, image;
	canvas.isDrawingMode= false;
	canvas.on('mouse:down', function(o){

		if (sourceClip == 'clip'){
			if(allowClip == true){
				clipCtr++;
				isDown = true;
				var pointer = canvas.getPointer(o.e);
				coorX = pointer.x;
				coorY = pointer.y;
				var imgObj = new Image();
				imgObj.src = clipPath;
				image = new fabric.Image(imgObj);

				if(clipCtr == 1){
					imgObj.onload = function () {
						image.set({left: coorX, top: coorY, angle: 0, width: 100, height:100});
						canvas.add(image);
						canvas.setActiveObject(image);
					}
				}	
			}
		}
	});

	canvas.on('mouse:move', function(o){
		if(isDown) {
			var pointer = canvas.getPointer(o.e);

				if(coorX>pointer.x){
				image.set({ left: Math.abs(pointer.x) });
				}

				if(coorY>pointer.y){
					image.set({ top: Math.abs(pointer.y) });
				}
			image.set({ width: Math.abs(coorX - pointer.x) });
			image.set({ height: Math.abs(coorY - pointer.y) });
			}
			canvas.renderAll();
	});

	canvas.on('mouse:up', function(o){
		isDown = false;
		clipCtr=0;
	});
} 

$('#clpart-options').on('click', '.clipTools', function(){
	clipPath = $(this).attr('src');
	sourceClip = 'clip';
	addClipArt();
});

$('#upldimg').change(function(e) {
	var formData = new FormData();
	formData.append('file', this.files[0]);
	formData.append('prloc', prlocation);

	$.ajax({

            xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.addEventListener("progress", function(evt){
               if (evt.lengthComputable) {
                   var percentComplete = evt.loaded / evt.total;

                      $('#clipprogress').circleProgress({
                            value: percentComplete,
                            size: 80,
                            fill: {gradient: [['#0681c4', .5], ['#4ac5f8', .5]], gradientAngle: Math.PI / 3}
                       }).on('circle-animation-progress', function(event, progress) {
                                $(this).find('strong').html(parseInt(100 * progress) + '<i>%</i>');
                      });
                }    
           }, false);

           return xhr;
        },

        url: './php/uploadImg.php',dataType: 'text',cache: false,contentType: false,
		processData: false, data: formData, type: 'post',success: function(data){

			var z = data.split('*');
			if(z[0] == "uploaded") {

				 $('#clipprogress').remove();
              	$('#clipblack').remove();  
				var imgObj = new Image();
				imgObj.src = z[1];
				var image = new fabric.Image(imgObj);
				imgObj.onload = function () {
					image.set({
						left: 10,top: 10,angle: 0,width: 200,height:200,
					});
					canvas.add(image);
				}
				$('#upldimg').val('');
			} else {
				modal(z[0]);
			}
		}
    });
});


function pan(com) {
var panX, panY;

	if (com == 'left'){
		panX =50;
		panY=0;
	} else if(com == 'right'){
		panX =-50;
		pany=0;
		$("#canvaswrapper").panzoom("pan", -50, 0, {
              relative: true,
              animate: true
     });
	} else if(com == 'top'){
		panY =50;
		panX=0;
	} else if(com == 'down'){
		panY =-50;
		panX=0;
	}

	$("#canvaswrapper").panzoom("pan", panX, panY, {
              relative: true,
              animate: true
     });
}

var panSw = false;
function panning() {

	if (panSw == false){
		$("#canvaswrapper").panzoom("destroy");
		$("#canvaswrapper").panzoom({
			disablePan: false,
			disableZoom: false,
	        minScale: 1,
	        increment: 0.2,
	        duration:300
    	});
    	panSw = true;
    	canvas.selection=false;

	} else{
		$("#canvaswrapper").panzoom("destroy");
		$("#canvaswrapper").panzoom({
			disablePan: true,
			disableZoom: true,
	        minScale: 1,
	        increment: 0.2,
	        duration:300
    	});
		panSw = false;
		canvas.selection=true;
	}
}

function CnvZoom(cm) {

	if(panSw == true){

		if (cm == 'zoomIn'){
		$('#canvaswrapper').panzoom("zoom");
		}
		else {
			$('#canvaswrapper').panzoom("zoom", true);
		}
	}
}

function duplicateObj() {
	var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();

  	if (actObj) {

  		var dupOb = fabric.util.object.clone(actObj);
		dupOb.set("top", dupOb.top+30);
		dupOb.set("left", dupOb.left+30);
		canvas.add(dupOb);

  	}
  	else if (actGrp) {

  		var objGrp = actGrp.getObjects();
  		canvas.discardActiveGroup();
  		objGrp.forEach(function(o) {

  			var dupOb = fabric.util.object.clone(o);
			dupOb.set("top", dupOb.top+30);
			dupOb.set("left", dupOb.left+30);
			canvas.add(dupOb);
  		});
  	}
}

var chkFlpX = false, chkFlpY=false;
function flipObject(axis){
	var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();
	var flcm = false;

	if (axis == 'x'){
		if (chkFlpX == false){
			flcm = true;
			chkFlpX = true;
		} else{
			flcm = false;
			chkFlpX = false;
		}
	} else {

		if (chkFlpY == false){
			flcm = true;
			chkFlpY = true;
		} else{
			flcm = false;
			chkFlpY = false;
		}
	}

  	if (actObj){
  		if(axis == 'x'){
  			actObj.flipX = flcm;
  		} else {
  			actObj.flipY = flcm;
  		}
  	}
  	else if (actGrp){
  		if(axis == 'x'){
  			actGrp.flipX = flcm;
  		} else {
  			actGrp.flipY = flcm;
  		}
  	}
  	canvas.renderAll();
}

function deleteObjects() {
	var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();

    if(actGrp){
	$.confirm({
		icon: 'fa fa-warning',type: 'blue',useBootstrap: false,
    		title: "Delete Confirmation",
    		content: "Do you want to delete these selected objects?",
    		buttons: {
        			yes:  {
        				btnClass: 'btn-red',
        				action: function(){
        					actGrp.forEachObject(function(o){
						canvas.remove(o);
					            	$('#'+o.id).remove();
				              });
        					canvas.discardActiveGroup().renderAll(); 
        				}
        			},
        			no: {
        				btnClass: 'btn-green',
        				action: function(){}
        			}
    		}
	}); 
    }
    else {

    	$.confirm({
		icon: 'fa fa-warning',type: 'blue',useBootstrap: false,
    		title: "Delete Confirmation",
    		content: "Do you want to delete this object?",
    		buttons: {
        			yes:  {
        				btnClass: 'btn-red',
        				action: function(){
        					 canvas.remove(actObj);
              				$('#'+actObj.id).remove();
              				canvas.renderAll();
        				}
        			},
        			no: {
        				btnClass: 'btn-green',
        				action: function(){}
        			}
    		}
	}); 
    }

  	if (canvas.getObjects().length == 0) {
  		numLayers = 0;
  	}
}

function selectAllObj(){

    canvas.deactivateAll();
	var objs = canvas.getObjects().map(function(o) {
      return o.set('active', true);
    });

    var group = new fabric.Group(objs, {
      originX: 'center',
      originY: 'center'
    });

    canvas.setActiveGroup(group.setCoords()).renderAll();
}

function moveObj(drc){

	if (panSw == false){
		var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();
		if (actObj) {

			if (drc == 'left') {
				actObj.left -=5;
			} else if (drc == 'right'){
				actObj.left +=5;
			} else if (drc == 'up'){
				actObj.top -=5;
			} else if (drc == 'down'){
				actObj.top +=5;
			}
	  		actObj.setCoords();
	  	}
	  	else if (actGrp) {

	  		if (drc == 'left') {
				actGrp.left -=5;
			} else if (drc == 'right'){
				actGrp.left +=5;
			} else if (drc == 'up'){
				actGrp.top -=5;
			} else if (drc == 'down'){
				actGrp.top +=5;
			}
	  		actGrp.setCoords();
	  	}
	     canvas.renderAll();
 	}
}

function rotateObj(deg){
	var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();

  	if (actObj){
  		actObj.setAngle(actObj.getAngle() + deg);
  	}
  	else if (actGrp){
  		actGrp.setAngle(actGrp.getAngle() + deg);
  	}
  	canvas.renderAll();
}

//load canvas
var prnm, prlocation, cname;

function loadProject(p, l, a, c, ap){
	canvas.clear();
	prnm = p;
	cname =c;
	prlocation = l;
	l= l +'/'+a+'.json';
	redo=[];
	undo=[];
	$('#listWithHandle').empty();
	numLayers=0;
	$.getJSON(l, function(data) {

		var flTxt=$.grep(data.objects, function( n, i ) {
  			return n.type =="textbox";
		});

		$.each(flTxt, function (idx, obj) {
		   $('head').append('<link rel="stylesheet" href="https://fonts.googleapis.com/css?family='+obj.fontFamily+ '" type="text/css" />');
		});

		canvas.loadFromJSON(data);
		canvas.renderAll();
		$("#shirt").attr("src", "../../../assets/shirt_template/" + data.tempLoc+ "/front.png");
		$("#shirt").attr("name", data.shirtColor);
		$("#shirt").attr("colid", data.colid);
		$('#shirt').attr("loc", data.tempLoc) ;
		
		if(ap > 0){
			$('#txtShtPr').show();
			$('#lblcolor').show();
			$('#txtShtPr').val(ap);
		} else{
			$('#txtShtPr').hide();
			$('#lblcolor').hide();
		}

		$("#cmbType").val(data.shirtType).change();
		$("#cmbBrand").val(data.shirtBrand).change();
		$("#cmbSize").val(data.shirtSize).change();
		$('#cmbFabric').val(data.shirtFtype).change();
		$('#cmbPrint').val(data.shirtPtype).change();
		$("#txtAddQty").val(data.shirtQty);
		$("#txtAddBgt").val(data.shirtBudget);
		$("#txtAddNote").val(data.shirtNote);

		state=JSON.stringify(data);
	});
}
//save canvas
function saveProject(area){

	var sType = $('#cmbType').val();
	var sBrand = $('#cmbBrand').val(); 
	var sSize = $('#cmbSize').val();
	var sBgt = $('#txtAddBgt').val();
	var sNote= $('#txtAddNote').val();
	var sQty = $('#txtAddQty').val();
	var sFt = $('#cmbFabric').val();
	var sPt = $('#cmbPrint').val();
	var bNo = $('#bldNo').text();
	var clrId = $("#shirt").attr("colid"), ssdId = $('#cmbSize').children(":selected").attr("id"), pmId = $('#cmbPrint').children(":selected").attr("id");
	var tempLoc = $("#shirt").attr("loc");

	canvas.deactivateAll();
	canvas.shirtColor = $('#shirt').attr('name');
	canvas.shirtType = sType;
	canvas.shirtBrand = sBrand;
	canvas.shirtSize = sSize;
	canvas.shirtBudget = sBgt;
	canvas.shirtQty  = sQty;
	canvas.shirtNote = sNote;
	canvas.shirtFtype = sFt;
	canvas.shirtPtype = sPt;
	canvas.tempLoc = tempLoc;
	canvas.colid = clrId;

	var JSONdata = JSON.stringify(canvas.toJSON(['shirtColor', 'shirtType', 'shirtBrand', 'shirtSize',
		'shirtBudget','shirtQty','shirtNote', 'shirtFtype', 'shirtPtype', 'tempLoc', 'colid']));

	var design = canvas.toDataURL('png');
	$.post('./php/save.php', {prData:JSONdata, prloc: prlocation, desImg: design, shtArea: area, shtColor: canvas.shirtColor,
		shtType: sType, shtBrand: sBrand, shtSize: sSize, shtBudget: sBgt, shtNote: sNote, fType: sFt, 
		pType: sPt, buildNo: bNo, shtQty: sQty, colorID: clrId, pmID : pmId,  ssdID: ssdId, tempLoc: tempLoc}, function(resp){
		modal('Saved Successfully',resp);
	});
}

var state,undo = [],redo = [];
function replayState(playStack, saveStack, buttonsOn, buttonsOff) {
    saveStack.push(state);
    state = playStack.pop();
    var on = $(buttonsOn);
    var off = $(buttonsOff);

    on.prop('disabled', true);
    off.prop('disabled', true);
    canvas.clear();
    numLayers=0;
    $('#listWithHandle').empty();

    canvas.loadFromJSON(state, function() {
        canvas.renderAll();
        on.prop('disabled', false);
        if (playStack.length) {
            off.prop('disabled', false);
        }
    });
}

function saveState() {
    redo = [];
    $('#btnCnvRedo').prop('disabled', true);
    if (state) {
        undo.push(state);
        $('#btnCnvUndo').prop('disabled', false);
    }
    state = JSON.stringify(canvas);
}

//font selector
$('#selectfont').fontselect().change(function(){

	var font = $(this).val().replace(/\+/g, ' ');
	font = font.split(':');
	newFont = font[0];

	var obj = canvas.getActiveObject();
	if(obj){
		obj.set('fontFamily',newFont);
	}

});

$("#fnt_size").change(function(e){
	newFontSize = $(this).val();

	var obj = canvas.getActiveObject();
	if(obj){
		obj.set('fontSize',newFontSize);
	}
});

var txtAlign = 'center', txtWeight = '';
function addText(source){

	var isDown, coorX, coorY, itxt;
	canvas.isDrawingMode = false;
	canvas.on('mouse:down', function(o){

		if (source == 'newText'){
			isDown = true;
			var pointer = canvas.getPointer(o.e);
			coorX = pointer.x;
			coorY = pointer.y;
			var txtbox = new fabric.Textbox($('#txtAddText').val(), {
				fontFamily: newFont,
				left: coorX,
				top: coorY ,
				fontSize: newFontSize,
				fontWeight: txtWeight,
				textAlign: txtAlign,
				stroke: str,
				fill: fll,});

			canvas.add(txtbox);
			canvas.setActiveObject(txtbox);
			txtbox.enterEditing();
			txtbox.hiddenTextarea.focus();
		}
	});

	canvas.on('mouse:up', function(o){
		isDown = false;
		source='';
	});
}

function setTxtAlign(loc){

	txtAlign = loc;
	var obj = canvas.getActiveObject();

	if (obj.type == 'textbox'){
		obj.set('textAlign', txtAlign);
		canvas.renderAll();
	}
}

function setTxtWeight(style){

	txtWeight = style;
	var obj = canvas.getActiveObject();

	if (obj.type == 'textbox'){
		obj.set('fontWeight', txtWeight);
		canvas.renderAll();
	}
}


//strokes and colors
$('#obj_opcty').change(function(){
	var actObj = canvas.getActiveObject(), actGrp = canvas.getActiveGroup();
	var opVal = parseInt($(this).val(),10)/10;

 	if (actObj) {
  		actObj.opacity = opVal;
  	}
  	else if (actGrp) {

  		var objGrp = actGrp.getObjects();
  		canvas.discardActiveGroup();
  		objGrp.forEach(function(o) {
  			o.opacity = opVal;
  		});
  	}
  	canvas.renderAll();
});

var strWid = 5;
$('#ln_width').change(function(){
	canvas.freeDrawingBrush.width = parseInt($(this).val(),10);
	strWid = parseInt($(this).val(),10);

	var obj = canvas.getActiveObject();

	if (obj){
		obj.set('strokeWidth', parseInt($(this).val(),10));
		canvas.renderAll();
	}
});

var str='f00000';
$("#ln_color").spectrum({
color: "#000000",

change: function(color) {
    $("#lbllncolor").val(color.toHexString());
    canvas.freeDrawingBrush.color = color.toHexString();

    str = color.toHexString();
    var obj = canvas.getActiveObject();

    if (obj.type!= 'text'){
        obj.set('stroke',str);
        canvas.renderAll();
    }
}
});

var fll = "#f00000";

$("#ln_fill").spectrum({
color: "#f00000",

change: function(color) {
    $("#lblfillcolor").val(color.toHexString());

    fll = color.toHexString()
    var obj = canvas.getActiveObject();

    if (obj){
        obj.set('fill', fll);
        canvas.renderAll();
    }
}
});

$('#enStrokeCol').change(function() {

    if(this.checked) {
		$("#ln_color").spectrum("enable");
		str = $("#lbllncolor").val();
    }
    else {
    	$("#ln_color").spectrum("disable");
    	$("#ln_fill").spectrum("enable");
    	$('#enFillCol').prop('checked', true);
    	str='';
    }

    var obj= canvas.getActiveObject();
    if (obj){
        obj.set('stroke', str);
        obj.set('fill', fll);
        canvas.renderAll();
    }
});


$('#enFillCol').change(function() {

    if(this.checked) {
		$("#ln_fill").spectrum("enable");
		fll=$("#lblfillcolor").val();
    }
    else {
    	$("#ln_fill").spectrum("disable");
    	$("#ln_color").spectrum("enable");
    	$('#enStrokeCol').prop('checked', true);
    	fll='';
    }

    var obj= canvas.getActiveObject();
    if (obj){
        obj.set('fill', fll);
        obj.set('stroke', str);
        canvas.renderAll();
    }
});


//linemode
function PenPencil(){
    $(".pentool").removeClass("active");
    $("#btnPencil").addClass("active");
    canvas.freeDrawingBrush = new fabric['PencilBrush'](canvas);
    canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
    canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

function PenCircle(){
    $(".pentool").removeClass("active");
    $("#btnCircle").addClass("active");
    canvas.freeDrawingBrush = new fabric['CircleBrush'](canvas);
    canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
    canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

function PenSpray(){
    $(".pentool").removeClass("active");
    $("#btnSpray").addClass("active");
    canvas.freeDrawingBrush = new fabric['SprayBrush'](canvas);
    canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
    canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

function PenSquare(){
    $(".pentool").removeClass("active");
    $("#btnSquare").addClass("active");
    canvas.freeDrawingBrush = new fabric.SquareBrush(canvas);
    canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
    canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}
//linepattern
function PatternHLine(){
        $(".pentool").removeClass("active");
        $("#btnHLine").addClass("active");
       var hLinePatternBrush = new fabric.PatternBrush(canvas);
            hLinePatternBrush.getPatternSrc = function() {

                var patternCanvas = fabric.document.createElement('canvas');
                patternCanvas.width = patternCanvas.height = 10;
                var ctx = patternCanvas.getContext('2d');

                ctx.strokeStyle = this.color;
                ctx.lineWidth = 5;
                ctx.beginPath();
                ctx.moveTo(5, 0);
                ctx.lineTo(5, 10);
                ctx.closePath();
                ctx.stroke();

                return patternCanvas;
            };
            canvas.freeDrawingBrush = hLinePatternBrush;
            canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
            canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

function PatternDiamond(){
    $(".pentool").removeClass("active");
    $("#btnDiamond").addClass("active");
    var diamondPatternBrush = new fabric.PatternBrush(canvas);
		diamondPatternBrush.getPatternSrc = function() {

			var squareWidth = 10, squareDistance = 5;
			var patternCanvas = fabric.document.createElement('canvas');
			var rect = new fabric.Rect({
				width: squareWidth,
				height: squareWidth,
				angle: 45,
				fill: this.color
			});

			var canvasWidth = rect.getBoundingRectWidth();
			patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
			rect.set({ left: canvasWidth / 2, top: canvasWidth / 2 });
			var ctx = patternCanvas.getContext('2d');
			rect.render(ctx);

			return patternCanvas;
		};
		canvas.freeDrawingBrush = diamondPatternBrush;
        canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
		canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

function PatternDots(){
    $(".pentool").removeClass("active");
    $("#btnDots").addClass("active");
    canvas.freeDrawingBrush = new fabric['PatternBrush'](canvas);
    canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
    canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

function PatternSquare(){
        $(".pentool").removeClass("active");
        $("#btnSquarePattern").addClass("active");
        var squarePatternBrush = new fabric.PatternBrush(canvas);
		squarePatternBrush.getPatternSrc = function() {

			var squareWidth = 10, squareDistance = 2;
			var patternCanvas = fabric.document.createElement('canvas');
			patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
			var ctx = patternCanvas.getContext('2d');

			ctx.fillStyle = this.color;
			ctx.fillRect(0, 0, squareWidth, squareWidth);

			return patternCanvas;
		};
		canvas.freeDrawingBrush = squarePatternBrush;
        canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
		canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}

if (canvas.freeDrawingBrush){
    canvas.freeDrawingBrush.width = parseInt($('#ln_width').val(), 10);
    canvas.freeDrawingBrush.color = $('#lbllncolor').val();
}


//canvas event
var numLayers=0;
canvas.on('object:added', function(e) {
	console.log(e.target.type);

	e.target.id = 'obj'+numLayers;
	$('#listWithHandle').prepend("<div class='list-group-item' id='obj"+numLayers+"'><img src= './res/show.png' class='visb'><label>Layer "+ numLayers + " : " +e.target.type+"</label></div>");
	numLayers++;
});

canvas.on('object:modified', function(e) {
    saveState();
});



canvas.on('selection:cleared', function() {
    $('.list-group-item').removeClass('active');
    allowShape = true;
    allowClip= true;
});

function showTutorial(c) {

 	var tour = introJs();
 	if (c == "all") {
 		tour = introJs();
 	} else {
 		tour = introJs(c);
 	}
	tour.setOption('tooltipPosition', 'auto');
	tour.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
	tour.start();

	console.log(c);
}