$(document).ready(function() {

	$("#avlclr").empty();
	$(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });

     $(this).bind('keydown', 'ctrl+-', function(e){
        e.preventDefault();
    });

    $(this).bind('keydown', 'ctrl+=', function(e){
        e.preventDefault();
    });

    $(this).bind('keydown', 'ctrl+s', function(e){
        e.preventDefault();
    });

    $(this).bind('keydown', 'ctrl+d', function(e){
        e.preventDefault();
    });

    for (x=10; x<100; x+=5) {

        $('#fnt_size').append("<option>"+x+"</option>");
    }

    $.post('./php/getShirt.php', {com: 'brand'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbBrand').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    $.post('./php/getShirt.php', {com: 'type'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbType').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    $.post('./php/getShirt.php', {com: 'print'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbPrint').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    setTimeout(function() {

        getFabric($('#cmbBrand').children(":selected").attr("id"), $('#cmbType').children(":selected").attr("id"));

         setTimeout(function() {
        
                getSize($('#cmbFabric').children(":selected").attr("id"));
                setTimeout(function() {
        
                    getShirtColor($('#cmbFabric').children(":selected").attr("id"));
            }, 500);
         }, 500);
    }, 500);
   

    $('#cmbBrand').change(function(){
        var brd = $(this).children(":selected").attr("id");
         var type = $('#cmbType').children(":selected").attr("id");

         getFabric(brd, type);
        
     });

    $('#cmbType').change(function(){
        var type = $(this).children(":selected").attr("id");
        var brd = $('#cmbBrand').children(":selected").attr("id");
            
            getFabric(brd, type);
     });

    $('#cmbFabric').change(function(){

        getShirtColor($(this).children(":selected").attr("id")); 
        getSize($(this).children(":selected").attr("id"));
    });
    
    function getFabric(b,t) {

         $.post('./php/getShirt.php', {com: 'fabric', brand: b, type: t }, function (data) {

            $('#cmbFabric').empty();
            for(var i=1; i<data.length; i++){
                $('#cmbFabric').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
            }

        },'json');
    }
       
     function getSize(s) {
            $.post('../php/getShirt.php', {com: 'size', clothing: s}, function (data) {

                $('#cmbSize').empty();
                for(var i=1; i<data.length; i++){
                $('#cmbSize').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
            }
        },'json');
    }  

    function getShirtColor(c) {

        $.post('../php/getShirt.php', {com: 'color', clothing: c}, function (data) {

             $('#avlclr').empty();
            for (var i=1; i<data.length;i++){

                $div = $('<div data-role="fieldcontain"/>');
                $("<button id = '"+data[i].id+"' class='btnclr' style ='background-color:#"+data[i].Hex+";'  name='"+data[i].Hex +"' loc='"+data[i].Location+"'/>").appendTo($div.clone()).appendTo('#avlclr');
            }

        },'json');
    }

    $("#avlclr").on('click', '.btnclr', function () {
	  var loc = $(this).attr("loc");
                var hex = $(this).attr("name");
                var id = $(this).attr("id");
            
            $('.btnclr').animate({
                transform: 'rotate(0deg)'
            });
                                          $("#shirt").attr("colid", id);
                                          $('#shirt').attr("name", hex);
                                           $("#shirt").attr("src", "../../../assets/shirt_template/" + loc+ "/front.png");
                                           $('#shirt').attr("loc", loc) ;

            $(this).animate({
              transform: 'rotate(135deg)'
            });
    });

    $('#layertool-del')
        .on('mouseover', function() {
            $('#layertool-del').attr('src', './res/deletehover.png');

            $('#layertool-del').animate({
                transform: 'scale(1.2,1.2)'
            });

        })
        .on('mouseout', function() {
            $('#layertool-del').attr('src', './res/delete.png');
            $('#layertool-del').animate({
                transform: 'scale(1,1)'
            });
        });

      $('#layertool-duplicate')
        .on('mouseover', function() {
            $('#layertool-duplicate').attr('src', './res/duplicatehover.png');
            $('#layertool-duplicate').animate({
                transform: 'scale(1.2,1.2)'
            });

        })
        .on('mouseout', function() {
            $('#layertool-duplicate').attr('src', './res/duplicate.png');
            $('#layertool-duplicate').animate({
                transform: 'scale(1,1)'
            });
        });

    function leftSideNav(){
        var width = $('#leftSidenav').width();

        if (width == 0 ){
            $('#leftSidenav').width('250px');
        }

        else{
            $('#leftSidenav').width('0px');
        }
    }

    var shLayer = false;

    function rightSideNav(){
        var width = $('#rightSidenav').width();

        if (width == 0 ){
            $('#rightSidenav').width('250px');
            
            if (shLayer == false) {
                    showTutorial('Layers');
                    shLayer = true;    
            }
            console.log("Layer: " +shLayer);
        }

        else{
            $('#rightSidenav').width('0px');
        }
    }

    function useCursor(){
        $('.tablinks').toggleClass('active');
        $('#tabcontent1').show();
        $('.tabcontent').hide();
        $('#btnCancel').toggleClass('active');
        $('#lbltool').text('SELECT');
    }

    var shTools =[false, false, false, false, false, false];
    function openTool(toolName, toolID) {

        if (toolName == "Item" && shTools[0] == false) {
            showTutorial(toolName);
            shTools[0] = true;
        } else if (toolName == "Select" && shTools[1] == false) {
            showTutorial(toolName);
            shTools[1] = true;
        } else if (toolName == "Pen" && shTools[2] == false) {
            showTutorial(toolName);
            shTools[2] = true;
        } else if (toolName == "Text" && shTools[3] == false) {
            showTutorial(toolName);
            shTools[3] = true;
        } else if (toolName == "Shape" && shTools[4] == false) {
            showTutorial(toolName);
            shTools[4] = true;
        } else if (toolName == "Image" && shTools[5] == false) {
            showTutorial(toolName);
            shTools[5] = true;
        } 

        $('#tabcontent1').hide();
        $('#color-options').hide();
        
        $('.tabcontent').each(function(e) {
            $(this).hide();
        });

        $('.tablinks').each(function(e) {
             if ($(this).attr('class')!==undefined) {
                $(this).attr('class',$(this).attr('class').replace('active'));
            }
        });
        
        $('#'+toolID).toggleClass('active');
        $('#lbltool').text(toolName);
        $('#' + toolName).show();
        $('#leftSidenav').width('250px');
        
        if (toolName == 'Pen'|| toolName =='Text' || toolName =='Shape'){
            $('#color-options').show();
        }
    }

    $("#tool2").on('click', '.tablinks', function () {
        openTool($(this).attr('name'), $(this).attr('id'));
    });

    $("#btnDrawer").click(function () {
        leftSideNav();
    });

    $("#btnLayer").click(function () {
        rightSideNav();

    });

    $("#btnCancel").click(function () {
        useCursor();
    });
    
    $('#Item').show();
    
    $('#color-options').hide();
    
    
    $('#btnFront').hover(function() {
		$('#btnFront').fadeIn(300);
		$('#btnFront').css("background-color","#2289b7");
		$('#btnFront').css("color","white");

	}, function() {
        if($('#btnFront').attr('class') == "active") {
            
         }
        else{
            $('#btnFront').fadeIn(300);
            $('#btnFront').css("background-color"," #354851");
            $('#btnFront').css("color","white");
            $('#btnFront').css("font-weight","bold");
        }
	});
    
    $('#btnBack').hover(function() {
		$('#btnBack').fadeIn(300);
		$('#btnBack').css("background-color","#2289b7");
		$('#btnBack').css("color","white");

	}, function() {
        if($('#btnBack').attr('class') == "active") {
            
        }
        else{
            $('#btnBack').fadeIn(300);
            $('#btnBack').css("background-color"," #354851");
            $('#btnBack').css("color","white");
            $('#btnBack').css("font-weight","bold");
        }
            
	});
    
     $('#btnTxt').hover(function() {
		$('#btnTxt').fadeIn(300);
		$('#btnTxt').css("background-color","#2ba9e2");
		$('#btnTxt').css("color","white");

	}, function() { 
            $('#btnTxt').fadeIn(300);
            $('#btnTxt').css("background-color"," #2289b7");
            $('#btnTxt').css("color","white");
            $('#btnTxt').css("font-weight","bold");
	});
    
    $('#btnFront').on('click' ,function(){
        $('#btnBack').removeClass("active");
        $('#btnBack').css("background-color"," #354851");
        $('#btnFront').addClass("active");
        $("#shirt").attr("src", "../../../assets/shirt_template/" + $('#shirt').attr('loc')+ "/back.png");
        $("#avlclr").show();
        $('#lblcolor').show();
    });
    
    $('#btnBack').on('click' ,function(){
        $('#btnFront').removeClass("active");
        $('#btnFront').css("background-color"," #354851");
        $('#btnBack').addClass("active");
        $("#shirt").attr("src", "../../../assets/shirt_template/" + $('#shirt').attr('loc')+ "/back.png");
        $("#avlclr").hide();
        $('#lblcolor').hide();
    });
    
    $('#btnPan').on('click' ,function(){
        if ($(this).hasClass("active")){
            $(this).removeClass("active");
            $(this).css("background-color","none");
        }
        else{
            $(this).addClass("active");
        }
    });
    
    $('#btnHelp').on('click' ,function(){

        $('#helpcontrols').find('tbody').empty();
        $('#helpcontrols').find('tbody:last').append('<th>Shortcut Key</th><th>Description</th>');
        var tr='<tr>';
        
        $.getJSON("../tut/shortcut.json", function(data){

            $.each(data.shortcut, function (idx, obj) {

                tr += '<td>' + obj.key + '</td>';
                tr += '<td>' + obj.desc + '</td>';
                tr +='</tr>';           
            });
            $('#helpcontrols').find('tbody:last').append(tr);
        });


        $('#page-cover3').show();
        $('#help-popup2').show();
    });
    
    //btnexit
    $('#btnexit').click( function() {
        $('#page-cover3').hide();
        $('#help-popup2').hide();
    });
    
    $('.inputimg').click(function() {
		$('#upldimg').trigger('click');
	});
    $('#clpart-options').empty();

            $.ajax({

                    xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.addEventListener("progress", function(evt){
                       if (evt.lengthComputable) {
                           var percentComplete = evt.loaded / evt.total;

                              $('#clipprogress').circleProgress({
                                    value: percentComplete,
                                    size: 80,
                                    fill: {gradient: [['#0681c4', .5], ['#4ac5f8', .5]], gradientAngle: Math.PI / 3}
                               }).on('circle-animation-progress', function(event, progress) {
                                        $(this).find('strong').html(parseInt(100 * progress) + '<i>%</i>');
                              });
                        }    
                   }, false);

                   return xhr;
                },

                url: './php/getClipArt.php',
                data: 'type=animals',
                method: "POST",
                cache: false,
                success: function(data){

                      $('#clipprogress').remove();
                      $('#clipblack').remove();  
                      $.each(data.items, function (idx, obj) {

                            $('#clpart-options').append("<img title = '"+obj.name+"' class='clipTools' src = '"+obj.path+"'>");                     
                        });
                }
            });

    $('#cmbClipArt').change(function(){
    $('#clpart-options').empty();
        $( "select option:selected" ).each(function() {
            if($(this).attr('name') != undefined){
                var clipType = $(this).attr('name');
                $.ajax({

                    xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.addEventListener("progress", function(evt){
                       if (evt.lengthComputable) {
                           var percentComplete = evt.loaded / evt.total;

                      $('#clipprogress').circleProgress({
                            value: percentComplete,
                            size: 80,
                            fill: {gradient: [['#0681c4', .5], ['#4ac5f8', .5]], gradientAngle: Math.PI / 3}
                       }).on('circle-animation-progress', function(event, progress) {
                                $(this).find('strong').html(parseInt(100 * progress) + '<i>%</i>');
                      });
           }
       }, false);

       return xhr;
    },
                url: './php/getClipArt.php',
                data: 'type='+clipType,
                method: "POST",
                cache: false,
                success: function(data){

                      $('#clipprogress').remove();
                      $('#clipblack').remove();
                      $.each(data.items, function (idx, obj) {
                            $('#clpart-options').append("<img title = '"+obj.name+"' class='clipTools' src = '"+obj.path+"'>");                     
                        });
                }
            });
            }
        });
    }); 
    
     $('#clpart-options').on('click','.clipTools' ,function(){
        $('.clipTools').removeClass("active");
        $(this).addClass("active");
    });


    function showTutorial(loc) {
        $.getJSON( "../tut/"+loc+".json", function(data) {

            var intro = introJs();
           intro.setOptions(data);
           intro.setOption('tooltipPosition', 'auto');
            intro.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
           intro.start();
        }); 
    } 

    showTutorial('home');
 
});

