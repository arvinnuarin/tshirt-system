(function() {

	var canvas = this.__canvas;
	canvas.onselectstart = function () { return false; }

	//msc
	$('#btnClr').click(function(){
		canvas.clear();
		numLayers=0;
		$('#listWithHandle').html('');
	});

	$("#tool2").on('click', '.tablinks', function () {
		shapeEnable = [false, false, false, false, false, false, false, false, false, false, false, false, false,false, false];
		sourceClip='';
    	
    });

	$('#btnCancel').click(function(){
		canvas.isDrawingMode = false;
		shapeEnable = [false, false, false, false, false, false, false, false, false, false, false, false, false,false, false];
		sourceClip='';
	});

	$('#btnSave').click(function(){
		saveProject(shirtArea);
	});
	
	$('#btnSvExit').click(function(){
		saveProject(shirtArea);
		window.location.href='../manager/';
	});

	$('#btnAdmSvExit').click(function(){
		saveProject(shirtArea);
		window.location.href='../../wp/manager/';
	});

	$('#btnCnvUndo').click(function(){
		 replayState(undo, redo, '#btnCnvRedo', this);
	});


	$('#btnCnvRedo').click(function(){
		 replayState(redo, undo, '#btnCnvUndo', this);
	});

	$('#btnPan').click(function(){
		panning();
	});	

	//tools
	$('#btnPen').click(function() {
    	drawPath();
	});

	$('.shape').click(function(){
		drawShape($(this).attr('title'));
		$(".shape").removeClass("active");
        $('#'+$(this).attr('id')).addClass("active");
	});

	$('#btnTxt').click(function() {
    	addText('newText');
	});

	$('#btnImg').click(function() {
    	canvas.isDrawingMode = false;
	});

	$('#btnTLeft').click(function() {
        $(".talign").removeClass("active");
        $("#divtleft").addClass("active");
    	setTxtAlign('left');
	});

	$('#btnTRight').click(function() {
        $(".talign").removeClass("active");
        $("#divtright").addClass("active");
    	setTxtAlign('right');
	});

	$('#btnTCen').click(function() {
        $(".talign").removeClass("active");
        $("#divtcenter").addClass("active");
    	setTxtAlign('center');
	});

	$('#btnTJus').click(function() {
        $(".talign").removeClass("active");
        $("#divtjust").addClass("active");
    	setTxtAlign('justify');
	});

	$('#btnTBold').click(function() {
        $(".tweight").removeClass("active");
        $("#divtbold").addClass("active");
    	setTxtWeight('bold');
	});

	$('#btnTItalic').click(function() {
        $(".tweight").removeClass("active");
        $("#divtitalic").addClass("active");
    	setTxtWeight('italic');
	});

	$('#btnObjShow').click(function() {
    	canvas.getActiveObject().visible = true;
    	canvas.renderAll();
	});

	var shirtArea ='front';
	$('#btnFront').click(function(){
        var color = $('#shirt').attr("name");
        shirtArea='front';
        loadProject(prnm, prlocation, shirtArea);
    });

    $('#btnBack').click(function(){
        var color = $('#shirt').attr("name");
        shirtArea='back';
        loadProject(prnm, prlocation, shirtArea);
     });

	$(document).bind('keydown', 'ctrl+d', function(){
	  	duplicateObj();
	});

	$(document).bind('keydown', 'ctrl+a', function(){
	  	selectAllObj();  
	});

	$(document).bind('keydown', 'ctrl+s', function(){
	  	saveProject(shirtArea);
	});

	$(document).bind('keydown', 'ctrl+q', function(){

	  	saveProject(shirtArea);
		window.location.href='../manager/';
	});

	$(document).bind('keydown', 'del', function(){
	  	deleteObjects();
	});
    
	$(document).bind('keydown', 'left', function(){
	  	moveObj('left');
	  pan('left');
	});

	$(document).bind('keydown', 'right', function(){
	  	moveObj('right');
		pan('right');
	});
	
	$(document).bind('keydown', 'up', function(){
	  	moveObj('up');
		pan('top');
	});

	$(document).bind('keydown', 'down', function(){
	  	moveObj('down');
		pan('down');
	});

	$(document).bind('keydown', 'ctrl+z', function(){
	  	replayState(undo, redo, '#btnCnvRedo', '#btnCnvUndo');
	});

	$(document).bind('keydown', 'ctrl+y', function(){
	  	replayState(redo, undo, '#btnCnvUndo', '#btnCnvRedo');
	});

	$(document).bind('keydown', 'ctrl+=', function(){
	  	CnvZoom('zoomIn');
	});

	$(document).bind('keydown', 'ctrl+-', function(){
	  	CnvZoom('zoomOut');
	});


	 $(document).bind('DOMMouseScroll mousewheel', function(e){
	    if(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
	        CnvZoom('zoomIn');
	    } else{
	        CnvZoom('zoomOut');
	    }
	});	 
})();
