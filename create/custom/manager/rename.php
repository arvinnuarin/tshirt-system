<?php
include('../../conf/config.php');
session_start();

$uID = $_SESSION['customer_id'];
$newName = $_POST['newprname'];
$loc = $_POST['location'];
$bNo = $_POST['buildNo'];

$newloc = 'build/'.$uID.'/'.$newName.'/';

$stmt = $dbc->prepare("UPDATE draw_build SET pr_name=? , location=? WHERE build_no=?");
$stmt->bind_param("ssi", $newName, $newloc, $bNo);
$stmt->execute();

if ($stmt->affected_rows == 1) {

	rename('../'.$loc, '../'.$newloc);
	echo 'success';
} else {
	echo 'failed';
}
?>