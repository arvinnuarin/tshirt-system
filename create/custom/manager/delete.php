<?php
include('../../conf/config.php');

$target = $_POST['location'];

function rrmdir($dir) { 
   
	if (is_dir($dir)) { 
		$objects = scandir($dir); 
		foreach ($objects as $object) { 
		
		if ($object != "." && $object != "..") { 
			if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
		} 
	} 
	reset($objects); 
	rmdir($dir); 
	} 
 }


$stmt = $dbc->prepare("DELETE FROM draw_build WHERE location=?");
$stmt->bind_param("s",$target);
$stmt->execute();

if ($stmt->affected_rows == 1) {

	rrmdir('../'.$target);
	echo 'The project has been deleted';
} else {
	echo "There was a problem deleting the project.";
}

?>