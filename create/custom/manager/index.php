<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>MixlArts Project Manager</title>
    
    <link href="css/manager.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="../css/fontawesome.css">
	<link rel="stylesheet" type="text/css" href="../css/jquery.contextMenu.css"/>
            <link rel="stylesheet" type="text/css" href="../css/animate.css"/>
            <link rel="stylesheet" type="text/css" href="../css/introjs.css"/>
            <link rel="stylesheet" type="text/css" href="../css/jquery-confirm.min.css"/>
	
    <script src="../lib/jquery-3.1.0.js"></script>
    <script src="../lib/jquery-confirm.min.js"></script>    
    <script src="../lib/jquerytransform2d.js" type="text/javascript"></script>
    <script src="../lib/jquery.contextMenu.js" type="text/javascript"></script>
    <script src="../lib/jquery.ui.position.js" type="text/javascript"></script>
    <script src="../lib/intro.js" type="text/javascript"></script>
    <script src="js/manager.js?random=<?php echo uniqid(); ?>" ></script>
   
</head>
<body>
    <?php include '../../../header/header.php';?>
    <div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
	       <div class="filemanager">
                
                <div id = 'divbuttons' >
                    <button id='btnNew' title = 'New' ><img src = '../res/new.png'/>  New</button>
                    
                </div>
               
               <div class="search">
                    <input type="search" id='txtSearch' placeholder="Find a file.." />
               </div>
               
                <div id = 'divbuttons2'>
                        <button id='btnRaw' title = 'Raw' class = 'fileicon active'><img  src = '../res/raw.png'/> Raw</button>
                         <button id='btnPen' title = 'Pending' class = 'fileicon'><img src = '../res/pending.png'/> Pending</button>
                         <button id='btnProc' title = 'Processed' class = 'fileicon' ><img  src = '../res/processed.png'/> Processed</button>
                         <button id='btnPaid' title = 'Paid' class = 'fileicon' ><img  src = '../res/processed.png'/> Paid Projects</button>
                </div>
            
                <div id='box'>
                <ul class="data"></ul>
                </div>

                <div class="nothingfound">
                    <div class="nofiles"></div>
                    <span>No files here.</span>
                </div>
	       </div>
        </div>
	</div>
    
    
    <div id = 'pop-up-container'>    
        <div id = 'pop-up-log-in'>
            <label>Rename</label><br>
            <input type="text" name="Rename" id="txtRename">
            <div id='divrename'>
                <button id='btnRename'>Rename</button>
                <button id='btnRCancel'>Cancel</button>
            </div>
        </div>
        
        
        <div id = 'pop-up-form' class="animated fadeIn">
            <div>
                <label>Project Name</label>
                <div>
                    <input type="text" name="Project_Name" id="txtProjName">
                </div>
            </div>
           
            <div id = 'divitemd'>
                <div id = 'divType'>
                    Shirt Type
                    <div>
                    <select id='cmbType' class = 'styled-select blue semi-square'>
                    </select>
                    </div>
                </div>

                <div id = 'divBrand'>
                    Shirt Brand
                    <div>
                        <select id='cmbBrand' class = 'styled-select blue semi-square'>
                        </select>
                    </div>
                </div>
            </div>

            <div id = 'divitemd'>
                <div id = 'divSize'>
                    Size
                    <div>
                        <select id='cmbSize' class = 'styled-select blue semi-square'>
                        </select>
                    </div>
                </div>

                <div id = 'divQuan'>
                    Quantity
                    <div>
                       <input type = 'number' min='1' max = '1000' value='1' id='txtAddQty'></input>
                    </div>
                </div>
            </div>

            <div id = 'divitemd'>
                <div id = 'divFabric'>
                    Fabric Type  
                    <div>
                        <select id='cmbFabric' class = 'styled-select blue semi-square'>
                        </select>
                    </div>
                </div>

                <div id = 'divPrint'>
                    Printing Method
                    <div>
                       <select id='cmbPrint' class = 'styled-select blue semi-square'>
                        </select>
                    </div>
                </div>
            </div>

            <div id = 'divitemd'>
                <div id = 'divFabric'>
                    Shirt Budget  
                    <div>
                      <input type='number' min='100' max='100000' value='100' id='txtAddBgt'></input>
                    </div>
                </div>

                <div id = 'divPrint'>
                    Note
                    <div>
                      <textarea spellcheck='false' type='text' id='txtAddNote' placeholder ='Type Notes here'></textarea>
                    </div>
                </div>
            </div>
        
            <div id  = 'divitema'>
                Shirt Color
                <div id='avlclr'></div>
            </div>
        
            <div id  = 'divbtnNext'>
                <button id='btnCancel'>Cancel</button>
                <button id='btnNext'>Next</button>
            </div>
        </div>
	</div>
    
    <div id = 'pop-up-form2'>
        <div id = 'popup2wrapper'>
            <div id = 'divcreate'>
             
                <label> MixlArts Drawing</label>
                
                <div id = 'btnCreate'>
                    <img id = 'imgcreate' src = '../res/create.png'/>
                </div>
                <div id = 'opacitywrapper'>
                </div>
            </div> 
            
            
        </div>
          
        <div id = 'popup2wrapper'>
            <div id = 'divuplink'>
                 <label>Upload Link</label>
                <input type = 'text' id = 'txtLink'>
                
                <div id = 'opacitywrapper'>
                </div>
            </div>
            
            <div id = 'divupfile'>
                <label id = 'label1'>Upload File</label>
                
                <button id='btnUpFile'>Upload</button>
                <input type='file' id='upldfile' style= 'display:none;'/>
                
                <label>*Rar, 7z, Zip, Psd</label><br>
                <label>*30mb Maximum</label>
                
                <div id = 'opacitywrapper'>
                </div>
            </div>
        </div>
        
        <div id = 'divbtns'>
            <button id='btnBack2'>Back</button>
            <button id='btnSubmit'>SUBMIT</button>
        </div>
	</div>

    <div id = 'divproperties'>
        <img id = 'btnexit' src = '../res/exit.png'/>

        <div id = 'divprop1'>
            <div><img id = 'imgeshirt' onerror="this.onerror=null"/></div>
            <button id='btnfrnt'>Front</button> <button id='btnback'>Back</button>
        </div>

        <div id = 'divprop1'>
            <div  id ='divproperties2'><label id = 'lblproperties'>Project Properties</label></div>
            <div id = 'divpropwrapper'></div>
            <div id = 'divpropwrapval'></div>
        </div>  
    </div>

    <div id = "page-cover2"></div>
    <div id = "page-cover3"></div>
    <?php include '../../../header/footer.php';?>

</body>
</html>