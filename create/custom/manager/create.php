<?php
include("../../conf/config.php");
session_start();

$uID = $_SESSION['customer_id'];
$cr = date("Y-m-d H:i:s");

$prnm = $_POST['prname'];
$prloc = 'build/'.$uID.'/'.$prnm.'/';
$sQty = $_POST['shirtQty'];
$sSize = $_POST['shirtSize'];
$sColor = $_POST['shirtColor'];
$sBudget = $_POST['shirtBgt'];
$sNote = $_POST['shirtNote'];
$prType = $_POST['prType'];
$sPtype = $_POST['shirtPrint'];
$upldNote = $_POST['upldNote'];
$temploc = $_POST['temploc'];
$stat = 'RAW';
$price = 0.0;
$custom_id =4;
$s = unserialize(SIZE);
$data = array();
$noteTxt="";
$prodId = 56;

function makeDir($path){

	if (!file_exists($path)) {
		
		mkdir($path);
	}
}

if($prType == 'divcreate') {
	$type = 'folders';
} else if ($prType == 'divuplink'){
	$type = 'files';
	$prloc = $prloc.$prnm.'.txt';
}	

$stmt = $dbc->prepare("SELECT pr_name FROM draw_build WHERE customer_id=? AND pr_name=?");
$stmt->bind_param("is", $uID,$prnm);
$stmt->execute();	
$stmt->store_result();
$data = $stmt->num_rows();

if ($data>0) {
	echo 'Project already exists';
} else {

	$stmt = $dbc->prepare("INSERT INTO product_design(name, description, category_id) VALUES ('$prnm', 'Custom Design Project', $custom_id);");
	$stmt->execute();

	if ($stmt->affected_rows == 1) {
		$stmt = $dbc->prepare("INSERT INTO product(pd_id, clothing_color_id) VALUES (LAST_INSERT_ID(), $sColor)");
		$stmt->execute();
		$prodId = $dbc->insert_id;
	}
	
	$stmt = $dbc->prepare("INSERT INTO draw_build (customer_id, pr_name, type, location, status, date_created, date_modified, ssd_id, sc_id, pm_id, product_id, sht_qty, budget, price)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

	$stmt->bind_param("issssssiiiiidd", $uID, $prnm, $type, $prloc, $stat, $cr, $cr, $sSize, $sColor, $sPtype, $prodId, $sQty, $sBudget, $price); 
	$stmt->execute();

	if ($stmt->affected_rows == 1) {

		$stmt = $dbc->prepare("SELECT ty.type, br.brand_name, col.hex, dc.size, fc.name, pm.name FROM clothing cl INNER JOIN clothing_brand br ON cl.cb_id=br.cb_id INNER JOIN clothing_type ty ON cl.ct_id=ty.ct_id INNER JOIN clothing_fabric fc ON cl.cf_id=fc.cf_id INNER JOIN clothing_size_desc dc ON cl.clothing_id=dc.clothing_id INNER JOIN draw_build bui ON bui.ssd_id=dc.ssd_id INNER JOIN clothing_color col ON col.sc_id = bui.sc_id INNER JOIN print_method pm ON pm.pm_id = bui.pm_id WHERE bui.location='$prloc'");
		$stmt->execute();
		$stmt->store_result();
		$data = $stmt->num_rows();

		if ($data == 1){
			$stmt->bind_result($sType, $sBrand, $sColorHex, $sSize, $sFtype, $sPtype);

			while($stmt->fetch()){
				$noteTxt = 'Customer ID: '.$uID .PHP_EOL. 'Date Saved: '.$cr.PHP_EOL.'Type: '.$sType.PHP_EOL.'Brand: '.$sBrand.PHP_EOL.'Color: '.$sColorHex.PHP_EOL.'Size: '.$s[$sSize].PHP_EOL.'Quantity: '.$sQty.PHP_EOL.'Budget: '.$sBudget.PHP_EOL.'Fabric Type: '.$sFtype.PHP_EOL.'Printing Type: '.$sPtype.PHP_EOL.'Note: '.$sNote;
			
				$data = array('objects'=>'', 'shirtColor'=> $sColorHex, 'shirtType'=> $sType, 'shirtBrand'=>$sBrand, 'shirtSize'=>$s[$sSize], 'shirtBudget'=>$sBudget,'shirtQty'=> $sQty, 'shirtNote'=>$sNote,'shirtFtype'=>$sFtype, 'shirtPtype'=>$sPtype, 'tempLoc'=>$temploc, 'colid'=>$sColor);
			}
		}

		makeDir('../build/'.$uID);
		makeDir('../build/'.$uID.'/'.$prnm);

		$notes = '../build/'.$uID.'/'.$prnm.'/notes.txt';
		$file = fopen($notes, 'w');
		fwrite($file, $noteTxt);
		fclose($file);

		if($prType == 'divcreate') {
			makeDir('../build/'.$uID.'/'.$prnm. '/res');
			makeDir('../build/'.$uID.'/'.$prnm. '/output');

			$frSht = '../build/'.$uID.'/'.$prnm.'/front.json';
			$bkSht = '../build/'.$uID.'/'.$prnm.'/back.json';

			$shtData = json_encode($data, JSON_FORCE_OBJECT);

			$file = fopen($frSht, 'w');
			fwrite($file, $shtData);
			fclose($file);

			$file = fopen($bkSht, 'w');
			fclose($file);
			echo 'created';	
		} 
		else if ($prType == 'divuplink'){
			$note = '../'.$prloc;
			$file = fopen($note, 'w');
			fwrite($file, $upldNote);
			fclose($file);
			echo 'success';	
		} 
	}
	else {
		echo 'Unable to insert.';
	}
}

$stmt->close();
?>