$(document).ready(function(){

	$(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });

	var filemanager = $('.filemanager'), fileList = filemanager.find('.data');

function modal(title,txt){
    
    $.alert({
            icon: 'fa fa-info',type: 'green',useBootstrap: false,animation: 'rotateXR',closeAnimation: 'scale',
                title: title,
                content: txt,
        }); 
}     

	function getFiles(st) {
		$.post('scan.php',{status: st}, function(data) {

		var response = [data], currentPath = '';
		var folders = [], files = [];

		$(window).on('hashchange', function(){

			goto(window.location.hash);
		}).trigger('hashchange');
            
        filemanager.find('.search').click(function(){

			$(this).find('span').hide();
			$('#txtSearch').show().focus();

		});

		$('#txtSearch').on('input', function(e){

			folders = [];
			files = [];

			var value = this.value.trim();

			if(value.length) {

				filemanager.addClass('searching');
				window.location.hash = 'search=' + value.trim();
			} else {

				filemanager.removeClass('searching');
				window.location.hash = encodeURIComponent(currentPath);
			}

		}).on('keyup', function(e){

			var search = $(this);

			if(e.keyCode == 27) {

				search.trigger('focusout');
			}

		}).focusout(function(e){

			var search = $(this);

			if(!search.val().trim().length) {

				window.location.hash = encodeURIComponent(currentPath);
				search.hide();
				search.parent().find('span').show();
			}
		});

		fileList.on('click', 'li.folders', function(e){
			e.preventDefault();
		});

		fileList.on('click', 'li.files', function(e){
			e.preventDefault();
		});

		function goto(hash) {

			hash = decodeURIComponent(hash).slice(1).split('=');

			if (hash.length) {
				var rendered = '';

				if (hash[0] === 'search') {

					filemanager.addClass('searching');
					rendered = searchData(response, hash[1].toLowerCase());

					if (rendered.length) {
						currentPath = hash[0];
						render(rendered);
					}
					else {
						render(rendered);
					}

				}

				else if (hash[0].trim().length) {

					rendered = searchByPath(hash[0]);

					if (rendered.length) {

						currentPath = hash[0];
						render(rendered);
					}
					else {
						currentPath = hash[0];
						render(rendered);
					}
				}
				else {
					currentPath = data.path;
					render(searchByPath(data.path));
				}
			}
		}

		function searchByPath(dir) {
			var path = dir.split('/'),
			demo = response,
			flag = 0;

			for(var i=0;i<path.length;i++){
				for(var j=0;j<demo.length;j++){
					if(demo[j].name === path[i]){
						flag = 1;
						demo = demo[j].items;
						break;
					}
				}
			}

			demo = flag ? demo : [];
			return demo;
		}

		function searchData(data, searchTerms) {

			data.forEach(function(d){
				if(d.type == 'folder') {

					searchData(d.items,searchTerms);

					if(d.name.toLowerCase().match(searchTerms)) {
						folders.push(d);
					}
				}
				else if(d.type == 'file') {
					if(d.name.toLowerCase().match(searchTerms)) {
						files.push(d);
					}
				}
			});
			return {folders: folders, files: files};
		}

		function render(data) {

			var scannedFolders = [],
			scannedFiles = [];

			if(Array.isArray(data)) {

				data.forEach(function (d) {

					if (d.type === 'folder') {
						scannedFolders.push(d);
					}
					else if (d.type === 'file') {
						scannedFiles.push(d);
					}

				});

			}
			else if(typeof data === 'object') {

				scannedFolders = data.folders;
				scannedFiles = data.files;

			}

			fileList.empty().hide();

			if(!scannedFolders.length && !scannedFiles.length) {
				filemanager.find('.nothingfound').show();
				fileList.hide();
			}
			else {
				filemanager.find('.nothingfound').hide();
				fileList.show();
			}

			if(scannedFolders.length) {

				scannedFolders.forEach(function(f) {

					name = escapeHTML(f.name),
					icon = '<img class="logo" src = "../'+f.path +'output/front_shirt.png">';

					var folder = $('<li build="'+f.build+'" class="folders" style="background-color: gray;"><a href="'+ f.path +'" class="folders">'+icon+'<span class="name">' + name + '</span></li>');
					folder.appendTo(fileList);
				});

			}

			if(scannedFiles.length) {

				scannedFiles.forEach(function(f) {

					var fileSize = bytesToSize(f.size),
					name = escapeHTML(f.name),
					fileType = name.split('.'),
                    bg,
					icon = '<span class="icon file"></span>';

					fileType = fileType[fileType.length-1];
                    if ((fileType== "doc") || (fileType== "docx") ||(fileType== "psd")||(fileType== "txt")){
                        bg = "lightblue";
                    } 
                    else if ((fileType== "rar")|| (fileType== "zip")|| (fileType== "7z") || (fileType== "deb")|| (fileType== "dmg")|| (fileType== "gz") ){
                        bg = "violet";
                    }
                    
                    else if ((fileType== "gif")|| (fileType== "jpg")|| (fileType== "jpeg") || (fileType== "pdf")|| (fileType== "png")|| (fileType== "gz") ){
                        bg = "#ffa734";
                    }

					icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';

					var file = $('<li build="'+f.build+'"class="files" style="background-color:'+bg+';"><a href="'+ f.path+'" title="'+ f.path +'" class="files">'+icon+'<span class="name2">'+ name +'</span> </a></li>');
                    
					file.appendTo(fileList);
				});
			}
			fileList.css({'display':'inline-block'});
			var cntProjects  = scannedFolders.length + scannedFiles.length +1;
			$('#txtProjName').val('Untitled_'+cntProjects);

			if(cntProjects <=10) {
				$('#btnNew').show();
			} else {
				$('#btnNew').hide();
				modal('Error','You have reached the maximum number of projects. Please delete some unused projects.');
			}
		}

		function escapeHTML(text) {
			return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
		}

		function bytesToSize(bytes) {
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
			if (bytes == 0) return '0 Bytes';
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
			return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

	});
	}

/*
	$('#upldFile').change(function(e) {
	var formData = new FormData();
	formData.append('file', this.files[0]);

	var ext = $('#upldFile').val().split('.').pop().toLowerCase();
	if($.inArray(ext, ['psd','zip','rar', 'txt']) == -1) {
    	modal('invalid extension!');
	} else {
		$.ajax({
		url: 'upload.php',dataType: 'text',cache: false,contentType: false,
		processData: false, data: formData, type: 'post',success: function(data){
			modal(data);
			getFiles();
		}
		});
	}
	}); */

	$('#box').on('dragenter', function (e){
		e.stopPropagation();
    	e.preventDefault();
	});

	$('#box').on('dragover', function (e){
    	e.stopPropagation();
    	e.preventDefault();
	});

	$('#box').on('drop', function (e){
    	e.preventDefault();

    	var files = e.originalEvent.dataTransfer.files;

      	for (var i = 0; i < files.length; i++) {
        	var formData = new FormData();
        	formData.append('file', files[i]);

	       	var ext = files[i].name.split('.').pop().toLowerCase();

			if($.inArray(ext, ['psd','zip','rar', 'txt']) == -1) {
	    		modal('Error','invalid extension!');
			} else {

				$.ajax({
				url: 'upload.php',dataType: 'text',cache: false,contentType: false,
				processData: false, data: formData, type: 'post',success: function(data){
					modal("Upload Complete", data);
					getFiles('RAW');
				}
				});
			}
   		}
	});
	getFiles('RAW');


	function deleteFile(f){
		
		$.confirm({
			icon: 'fa fa-warning',type: 'blue',useBootstrap: false,
	    		title: "Delete Confirmation",
	    		content: "Do you want to delete this project?",
	    		buttons: {
	        			yes:  {
	        				btnClass: 'btn-red',
	        				action: function(){
	        					$.post('delete.php', {location: f}, function(data) {
							modal("Successfully Deleted", data);
							getFiles('RAW');
						});
	        				}
	        			},
	        			no: {
	        				btnClass: 'btn-green',
	        				action: function(){}
	        			}
	    		}
		}); 

		
	}

	function sendShirt(p){

		$.confirm({
			icon: 'fa fa-warning',type: 'blue',useBootstrap: false,
	    		title: "Send Shirt Confirmation",
	    		content: "This will send the project to be accessed by the administrator. Do you want to continue?",
	    		buttons: {
	        			yes:  {
	        				btnClass: 'btn-blue',
	        				action: function(){
	        					$.post('sendShirt.php', {buildNo: p}, function(data) {
							if(data == 'success'){
								getFiles('RAW');
							} else {
								modal("An Error Occured", data);
							}
						});
	        				}
	        			},
	        			no: {
	        				btnClass: 'btn-green',
	        				action: function(){}
	        			}
	    		}
		}); 
	}

	function downloadProject(p,l,t){
		$.post('../php/downloadFile.php', {prname:p, prlocation: l, prtype: t}, function(data){
			if(data.trim() != 'failed'){
				window.location.href='../php/download.php?file='+p +data.trim();
			}else {
				modal("An Error Occured", data.trim());
			}
		});
	}

	function renameProject(name, location, b){
		$.post('rename.php', {newprname: name, location: location, buildNo: b}, function(data){
			if(data == 'success'){
            	$('#pop-up-log-in').hide();
            	$('#page-cover2').hide();
				getFiles('RAW');
			} else {
				modal("An Error Occured", data);
			}
		});
	}

	function addToCart(l)  {
		$.post('addToCart.php', {location: l}, function(data) {
			
			modal("Cart Message", data);
		});
	}

	var reloc, rebld, curStat='raw';
	$('#btnRaw').click(function(){
		getFiles('RAW');
		$('#box').removeClass();
		$('#box').addClass('animated bounceIn');
		curStat = 'raw';
        
        $('.fileicon').removeClass('active');
        $("#btnRaw").addClass('active');
      
	});
    
    $('#btnPen').click(function(){


		getFiles('PENDING');
		curStat = 'pen';

        $('#box').removeClass();
        $('#box').addClass('animated pulse');
        $('.fileicon').removeClass('active');
        $('#btnPen').addClass('active');
	});

	$('#btnProc').click(function(){
		getFiles('PROCESSED');
		curStat ='pro';
        
        $('#box').removeClass();
        $('#box').addClass('animated fadeIn');
        $('.fileicon').removeClass('active');
        $("#btnProc").addClass('active'); 
	});

	$('#btnPaid').click(function(){
		getFiles('PAID');
		curStat ='paid';
        
        $('#box').removeClass();
        $('#box').addClass('animated fadeIn');
        $('.fileicon').removeClass('active');
        $("#btnPaid").addClass('active'); 
	});

	$('#btnRename').click(function(){
		renameProject($('#txtRename').val(), reloc, rebld);
	});

	var loc='';
	$.contextMenu({selector: '#box li', callback: function(key, options) {

		if (key == "open") {

			window.location.href = '../../mixl/project/' + $(this).find('span.name').text();
		}
		else if (key =="rename"){
            $('#pop-up-log-in').show();
            $('#page-cover2').show();
			reloc = $(this).find('a').attr('href');
			rebld = $(this).attr('build');
			$('#txtRename').val($(this).find('span.name').text());
		}
		else if (key == "delete") {
			deleteFile($(this).find('a').attr('href'));
		} 
		else if (key == "adm"){
			sendShirt($(this).attr('build'));
		}
		else if (key == "download"){
			loc = $(this).find('a').attr('href'), type = $(this).find('a').attr('class');

			if (type == 'folders'){
				downloadProject($(this).find('span.name').text(),loc, type);
			} else {
				downloadProject($(this).find('span.name2').text(),loc, type);
			}
		}
	        	else if (key == "properties"){

	        		loc = $(this).find('a').attr('href');

	        		$.post('getProperties.php',{prloc: loc}, function(data){
			$('#divpropwrapper').empty();
			$('#divpropwrapval').empty();

			$.each(data, function(key, val){
				$('#divpropwrapper').append('<div><label>'+key+'</label></div>');
				$('#divpropwrapval').append('<div><label>'+val+'</label></div>');
			});
	        		},"json");
	        	
	        		$('#imgeshirt').attr('src', '../' +loc + "output/front_shirt.png");
	        		checkImg();
			$('#divproperties').show();
	            		$('#page-cover2').show();
		}
		else if (key=="cart") {
			addToCart($(this).find('a').attr('href'));
		}
	},
	items: {
		"open": {name: "Open", icon: "fa-mail-forward", visible: function(key, opt){
			if($(this).find('a').attr('class') =='files' || curStat== 'pen'  || curStat == 'pro' || curStat=='paid'){ return false; } else {return true;}
		}},
		"rename": {name: "Rename", icon:"fa-pencil-square-o",visible: function(key, opt){
			if(curStat =='raw' && $(this).find('a').attr('class') =='folders'){ return true; } else {return false;}
		 }},
		"delete": {name: "Delete", icon: "fa-trash", visible: function(key, opt){
			if(curStat =='pen' || curStat=='paid'){ return false; } else {return true;}
		 }},
		"adm": {name: "Submit to Admin", icon: "fa-money",visible: function(key, opt){
			if(curStat =='raw'){ return true; } else {return false;}
		 }},
		"cart":{name: "Add to Cart", icon: "fa-cart-arrow-down", visible: function(key, opt){
			if(curStat =='pro'){ return true; } else {return false;}
		 }},
		"download": {name: "Download", icon: "fa-cloud-download"},
		"properties": {name: "Properties", icon: "fa-database"},
		}
	});

	function checkImg(){

		$("#imgeshirt").each(function() {
            
		            $(this).on('error', function() {
		              
		               $(this).attr('src', "../res/noimage.png");
		            }); 
   		 });
	    }
          
	 $('#btnfrnt').click(function(){

	 	shirtLoc = "../"+loc + "output/front_shirt.png";
    		$('#imgeshirt').attr('src', shirtLoc );
    		checkImg();
   	 });

   	  $('#btnback').click(function(){

	 	shirtLoc = "../"+loc + "output/back_shirt.png";
    		$('#imgeshirt').attr('src', shirtLoc );
    		checkImg();
   	 });

   $(document).on("mouseover", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});
    
    
   $(document).on("mouseover", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnUpload", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

     $(document).on("mouseover", "#btnNew", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
        $(this).find('img').attr('src','../res/new2.png');
	});
	
	$(document).on("mouseout", "#btnNew", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
        $(this).find('img').attr('src','../res/new.png');
	});

  	$(document).on("mouseover", "#btnfrnt", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnfrnt", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

  	$(document).on("mouseover", "#btnback", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnback", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

    //properties

    //rename
    
    $('#btnRename').hover( function() {
		$('#btnRename').fadeIn(300);
		$('#btnRename').css("background-color","white");
		$('#btnRename').css("color","black");

	}, function() {
		$('#btnRename').fadeIn(300);
		$('#btnRename').css("background-color","#222d32");
		$('#btnRename').css("color","white");
		$('#btnRename').css("font-weight","bold");
	});
        
    $('#btnRCancel').hover( function() {
		$('#btnRCancel').fadeIn(300);
		$('#btnRCancel').css("background-color","white");
		$('#btnRCancel').css("color","black");

	}, function() {
		$('#btnRCancel').fadeIn(300);
		$('#btnRCancel').css("background-color","#222d32");
		$('#btnRCancel').css("color","white");
		$('#btnRCancel').css("font-weight","bold");
	});
    
    $('#btnNext').hover( function() {
		$('#btnNext').fadeIn(300);
		$('#btnNext').css("background-color","white");
		$('#btnNext').css("color","black");

	}, function() {
		$('#btnNext').fadeIn(300);
		$('#btnNext').css("background-color","#222d32");
		$('#btnNext').css("color","white");
		$('#btnNext').css("font-weight","bold");
	});
    
    $('#btnCancel').hover( function() {
		$('#btnCancel').fadeIn(300);
		$('#btnCancel').css("background-color","white");
		$('#btnCancel').css("color","black");

	}, function() {
		$('#btnCancel').fadeIn(300);
		$('#btnCancel').css("background-color","#222d32");
		$('#btnCancel').css("color","white");
		$('#btnCancel').css("font-weight","bold");
	});
    
    $('#btnUplink').hover( function() {
		$('#btnUplink').fadeIn(300);
		$('#btnUplink').css("background-color","white");
		$('#btnUplink').css("color","black");

	}, function() {
		$('#btnUplink').fadeIn(300);
		$('#btnUplink').css("background-color","#222d32");
		$('#btnUplink').css("color","white");
		$('#btnUplink').css("font-weight","bold");
	});
    
    $('#btnUpFile').hover( function() {
		$('#btnUpFile').fadeIn(300);
		$('#btnUpFile').css("background-color","white");
		$('#btnUpFile').css("color","black");

	}, function() {
		$('#btnUpFile').fadeIn(300);
		$('#btnUpFile').css("background-color","#222d32");
		$('#btnUpFile').css("color","white");
		$('#btnUpFile').css("font-weight","bold");
	});
    
    $('#btnSubmit').hover( function() {
		$('#btnSubmit').fadeIn(300);
		$('#btnSubmit').css("background-color","white");
		$('#btnSubmit').css("color","black");

	}, function() {
		$('#btnSubmit').fadeIn(300);
		$('#btnSubmit').css("background-color","#222d32");
		$('#btnSubmit').css("color","white");
		$('#btnSubmit').css("font-weight","bold");
	});
    
    $('#btnBack2').hover( function() {
		$('#btnBack2').fadeIn(300);
		$('#btnBack2').css("background-color","white");
		$('#btnBack2').css("color","black");

	}, function() {
		$('#btnBack2').fadeIn(300);
		$('#btnBack2').css("background-color","#222d32");
		$('#btnBack2').css("color","white");
		$('#btnBack2').css("font-weight","bold");
	});
    
    $('#btnNext2').hover( function() {
		$('#btnNext2').fadeIn(300);
		$('#btnNext2').css("background-color","white");
		$('#btnNext2').css("color","black");

	}, function() {
		$('#btnNext2').fadeIn(300);
		$('#btnNext2').css("background-color","#222d32");
		$('#btnNext2').css("color","white");
		$('#btnNext2').css("font-weight","bold");
	});
    
    $('#btnRename').click( function() {
        //function
    });
    
    $('#btnRCancel').click( function() {
        $('#pop-up-log-in').hide();
        $('#page-cover2').hide();
    });
    
    //btnexit
    $('#btnexit').click( function() {
        $('#divproperties').hide();
        $('#page-cover2').hide();
    });
    
  
 
/*color*/
    var color='';

     $.post('../php/getShirt.php', {com: 'brand'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbBrand').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    $.post('../php/getShirt.php', {com: 'type'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbType').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');

    $.post('../php/getShirt.php', {com: 'print'}, function (data) {

        for(var i=1; i<data.length; i++){
             $('#cmbPrint').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
        }

    },'json');


    $('#cmbBrand').change(function(){
        var brd = $(this).children(":selected").attr("id");
         var type = $('#cmbType').children(":selected").attr("id");

         getFabric(brd, type);
        
     });

    $('#cmbType').change(function(){
        var type = $(this).children(":selected").attr("id");
        var brd = $('#cmbBrand').children(":selected").attr("id");
            
            getFabric(brd, type);
     });

    $('#cmbFabric').change(function(){

        getShirtColor($(this).children(":selected").attr("id"));
        getSize($(this).children(":selected").attr("id"));
    });
    
    function getFabric(b,t) {

         $.post('../php/getShirt.php', {com: 'fabric', brand: b, type: t }, function (data) {

            $('#cmbFabric').empty();
            for(var i=1; i<data.length; i++){
                $('#cmbFabric').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
            }

        },'json');
    }

    function getSize(s) {
            $.post('../php/getShirt.php', {com: 'size', clothing: s}, function (data) {

            	$('#cmbSize').empty();
            	for(var i=1; i<data.length; i++){
                $('#cmbSize').append("<option id='"+data[i].id+"'>"+data[i].name+"</option>");
            }
        },'json');
    }

	var temploc="";
    function getShirtColor(c) {

        $.post('../php/getShirt.php', {com: 'color', clothing: c}, function (data) {

             $('#avlclr').empty();
            for (var i=1; i<data.length;i++){

                $div = $('<div data-role="fieldcontain"/>');
                $("<button id = '"+data[i].id+"' class='btnclr' style ='background-color:#"+data[i].Hex+";'  name='"+data[i].Hex +"' loc='"+data[i].Location+"'/>").appendTo($div.clone()).appendTo('#avlclr');
            }

        },'json');
    }

    $("#avlclr").on('click', '.btnclr', function () {
	  		color = $(this).attr("id");
			temploc = $(this).attr("loc");
            
            $('.btnclr').animate({
                transform: 'rotate(0deg)'
            });

            $(this).animate({
              	transform: 'rotate(135deg)'
            });
    });
    
    //opacity

    $('#divcreate').click( function() {
        $('#divuplink').removeClass('active');
        $('#divupfile').removeClass('active');
        $('#divuplink').find('#opacitywrapper').show();
        $('#divupfile').find('#opacitywrapper').show();
        $(this).find('#opacitywrapper').hide();
        $(this).addClass('active');
    });
    
     $('#divuplink').click( function() {
        $('#divcreate').removeClass('active');
        $('#divupfile').removeClass('active');
        $('#divcreate').find('#opacitywrapper').show();
        $('#divupfile').find('#opacitywrapper').show();
        $(this).find('#opacitywrapper').hide();
        $(this).addClass('active');
    });
    
    $('#divupfile').click( function() {
        $('#divcreate').removeClass('active');
        $('#divuplink').removeClass('active');
        $('#divcreate').find('#opacitywrapper').show();
        $('#divuplink').find('#opacitywrapper').show();
        $(this).find('#opacitywrapper').hide();
        $(this).addClass('active');
    });
    
    //upldfile
     $('#btnUpFile').click(function() {
	$('#upldfile').trigger('click');
     });
    
    //btnnew
    
    $('#btnNew').click( function() {

   	getFabric($('#cmbBrand').children(":selected").attr("id"), $('#cmbType').children(":selected").attr("id"));
   	setTimeout(function() {
  		
  		getShirtColor($('#cmbFabric').children(":selected").attr("id"));
   		setTimeout(function() {

   			getSize($('#cmbFabric').children(":selected").attr("id"));
   		},500);

	}, 500);

        $('#pop-up-form').show();
        $('#page-cover2').show();
    });
    
    //btnNext
    $('#btnNext').click( function() {

    	var proname = $('#txtProjName').val(), qty = $('#txtAddQty').val();

    	console.log(color);
    	if (proname !="" &&  qty!="" && color.length > 0){

        	$('#pop-up-form').hide();
        	$('#pop-up-form2').css('display','inline-block');
    	} else {
    		modal('Error','Fill out the form first');
    	}
    	
    });
    
    //btncancel
    $('#btnCancel').click( function() {
        $('#pop-up-form').hide();
        $('#page-cover2').hide();
    });
    
     //btnback2
    $('#btnBack2').click( function() {
        $('#pop-up-form2').css('display','none');
        $('#pop-up-form').show();
    });

    $('#btnSubmit').click(function() {
    	var proname = $('#txtProjName').val(), ssdID = $('#cmbSize').children(":selected").attr("id"), colorID = color, 
    	printID = $('#cmbPrint').children(":selected").attr('id'), qty = $('#txtAddQty').val(),
    	budget = $('#txtAddBgt').val(), note = $('#txtAddNote').val();
    	var pType = $('#popup2wrapper div.active').attr('id');
    	var upldNote = $('#txtLink').val();	

    	if (proname !="" &&  qty!="" && color.length > 0){

    		if (pType == 'divupfile'){
    			var fData = new FormData();
			fData.append('file', $('#upldfile').prop('files')[0]);
			fData.append('prname', proname);
			fData.append('shirtQty', qty);
			fData.append('shirtColor', colorID);
			fData.append('shirtSize', ssdID);
			fData.append('shirtBgt', budget);
			fData.append('shirtNote', note);
			fData.append('shirtPrint', printID);
			fData.append('temploc', temploc);

			var ext = $('#upldfile').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['psd','zip','rar', 'txt']) == -1) {
		    	modal('Error','invalid extension!');
			} else {
				$.ajax({
				url: 'upload.php',dataType: 'text',cache: false,contentType: false,
				processData: false, data: fData, type: 'post',success: function(data){
					
		    			if(data == 'success'){
		    				$('#pop-up-form2').hide();
		    				$('#page-cover2').hide();
		    				getFiles('RAW');
		    			} else {
		    				modal(data);
		    			}
				}
				});
			}
    		}
    		else {
    			$.post('create.php', {prname: proname, shirtQty: qty, shirtSize: ssdID, prType: pType, shirtColor: colorID, shirtPrint: printID, upldNote: upldNote, shirtBgt: budget, shirtNote: note, temploc: temploc}, function(data){

    			if(data == 'success'){
    				$('#pop-up-form2').hide();
    				$('#page-cover2').hide();
    				getFiles('RAW');
    			} else if (data == 'created'){
    				$('#pop-up-form2').hide();
    				$('#page-cover2').hide();
    			
				window.location.href = '../../mixl/project/' + proname;
    			} 
    			else {
    				modal(data);
    			}
    		});
    		}
    		
    	} else {
    		modal('Error','Fill out the form first');
    	} 
    });
    
 function showTutorial(c) {

var tour = introJs();
if (c == "all") {
	tour = introJs();
} else {
	tour = introJs(c);
}
tour.setOption('tooltipPosition', 'auto');
tour.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
tour.start();

console.log(c);
}

$.getJSON( "tutorials.json", function(data) {

      var intro = introJs();
       intro.setOptions(data);
       intro.setOption('tooltipPosition', 'auto');
       intro.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
       intro.start();
}); 

 
});
