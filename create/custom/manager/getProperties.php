<?php
include('../../conf/config.php');
$loc = $_POST['prloc'];
$s = unserialize(SIZE);

$stmt = $dbc->prepare("SELECT  bui.pr_name, bui.build_no, bui.price, bui.budget  , ty.type, br.brand_name, col.color , dc.size, bui.sht_qty, fc.name, pm.name, bui.date_created, bui.date_modified FROM clothing cl INNER JOIN clothing_brand br ON cl.cb_id=br.cb_id  INNER JOIN clothing_type ty ON cl.ct_id=ty.ct_id INNER JOIN clothing_fabric fc ON cl.cf_id=fc.cf_id INNER JOIN clothing_size_desc dc ON cl.clothing_id=dc.clothing_id  INNER JOIN draw_build bui ON bui.ssd_id=dc.ssd_id INNER JOIN clothing_color col ON col.sc_id = bui.sc_id  INNER JOIN print_method pm ON pm.pm_id = bui.pm_id WHERE bui.location='$loc'");
$stmt->execute();
$stmt->store_result();
$data = $stmt->num_rows();

if ($data == 1){

	$stmt->bind_result($name, $b, $price, $bgt, $type, $brand, $col, $size, $qty, $ftype, $ptype, $cr, $cm);

	while($stmt->fetch()) {
		
				$total = $price * $qty;

		$prop = array('Project Name:'=>$name, 'Build Number:'=>$b, 'Shirt Price: /pc'=>'₱ '.$price,'Total Price'=>'₱ '.$total, 'Budget:'=>'₱ '.$bgt, 'Type:'=>$type, 'Brand:'=>$brand, 'Color:'=>$col, 'Size'=>$s[$size], 'Quantity:'=>$qty, 'Fabric Type:'=>$ftype, 'Printing Type:'=>$ptype, 'Date Created:'=>$cr, 'Date Modified:'=>$cm);

		echo json_encode($prop);
	}
}
?>