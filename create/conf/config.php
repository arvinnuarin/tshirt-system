<?php
	//Database config
	DEFINE('DB_USER', 'root');
	DEFINE('DB_PASSWORD', '');
	DEFINE('DB_SERVER', 'localhost');
	DEFINE('DB_NAME', 'dbmixlarts');
	DEFINE ("SIZE", serialize (array ("XXS", "XS", "S", "M", "L", "XL", "2XL", "3XL", "4XL", "5XL")));


	$dbc = @mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
	//Set timezone
	date_default_timezone_set('Asia/Manila');

	$cusbhref = '//www.mixlarts.com/MixlArts/create/custom/';
	$wpbhref ='//www.mixlarts.com/MixlArts/create/wp/';
?>
