$(document).ready(function(){
	$('#divwomen, #divmen ,#divgirl, #divboy').hover( function() {
		$(this).find('.img-title').css("opacity","50");
		$(this).find('.img-title').fadeIn(300);
		$(this).find('#btnview').css("top","70%");
		$(this).find('#btnview').css("opacity",1);

	}, function() {
		$(this).find('.img-title').fadeOut(300);
		$(this).find('#btnview').css("top","90%");
		$(this).find('#btnview').css("opacity",0.3);
	});
	

	
	$(document).on("mouseover", "#prod", function() {
		$(this).css("border","1px solid lightgray");
	});
	
	$(document).on("mouseout", "#prod", function() {
		$(this).css("border","none");
	});
	
	$('#imgsearch').click(function(){
		if($("#txt-search").val() !== ""){
		  window.location.href = "search/?search="+$("#txt-search").val();
		}
	});
		$("#txt-search").bind('keypress',function(e){
		if ($("#txt-search").val()!= "")
		  if(e.which==13){
				window.location.href = "search/?search="+$("#txt-search").val();
			
		  }	
	});
	

	
	$(document).on("mouseover", "#btnSeeMore", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).find('#lblcart').css("color","black");
	});
	
	$(document).on("mouseout", "#btnSeeMore", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).find('#lblcart').css("color","white");
		$(this).find('#lblcart').css("font-weight","bold");
	});
});