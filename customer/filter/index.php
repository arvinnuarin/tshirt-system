<!DOCTYPE html>
<html>
<head>

	<base href="//www.mixlarts.com/MixlArts/customer/filter/">
	<link rel = 'stylesheet' href = "filter.css">
	<?php
		session_start();
		require_once('../../php/customer.php');
		$customer = new Customer();
	?>
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript">var logInDir = "../";</script>
    <script type = "text/javascript" src = "../login/login.js"></script>
	<script type = "text/javascript" src = "../../js/command.js"></script>
	<script type = "text/javascript" src = "../../js/mixlarts.js"></script>
	<script type = "text/javascript" src = "filter.js"></script>

	<title>Shop Mixl Custom Tshirt</title>
</head>
<body>
	
    <?php include '../../header/header.php';?>
    
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
			<div id = "divfilter">
			<?php
				if(isset($_GET['filter'])){
					if(!empty($_GET['filter'])){
						$filter = $_GET['filter'];
						if(isset($_GET['start'])){
							if(!empty($_GET['start'])){
								if(is_numeric($_GET['start'])){
									$start = intval($_GET['start']);
								}else{
									$start = 0;
								}
							}else{
								$start = 0;
							}
						}else{
							$start = 0;
						}
						$per_page = 6;
						
						$prev = $start - $per_page;
						
						
						if($filter === "brand_new"){
							if($arr_filter = $customer -> get_preview_product_by_newest($start, $per_page)){
								$record_count = $customer -> get_preview_product_by_newest_count();
								echo "<h1 id = 'cheader'>Brand New Products</h1	>";
								echo "<div id = 'prodcontainer'>";
								foreach($arr_filter as $n){
									echo "<a href = '../view/product/".$n['name']."'><div id = 'prod'>";
									echo "<img id = 'prodimg' src = '../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$n['clothing']."/front.png&design=../assets/shirt_design_template/".$n['name']."/front.png'><br />";
									echo "<p id = 'pname'>".$n['name']."</p>";
									echo "<p id = 'pdesc'>".$n['description']."</p>";
									echo "<p id = 'pprice'>Php ".number_format($n['price'])."<p/>";
									echo "</div></a>";
								}
								echo"</div>";
							}
						}else if($filter === "popular"){
							if($arr_filter = $customer -> get_preview_product_by_best_selling($start, $per_page)){
								$record_count = $customer -> get_preview_product_by_best_selling_count();
								echo "<h1 id = 'cheader'>Best Selling Products</h1>";
								echo "<div id = 'prodcontainer'>";
								foreach($arr_filter as $b){
									echo "<a href = '../view/product/".$b['name']."'><div id = 'prod'>";
									echo "<img id = 'prodimg' src = '../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$b['clothing']."/front.png&design=../assets/shirt_design_template/".$b['name']."/front.png'><br />";
									echo "<p id = 'pname'>".$b['name']."</p>";
									echo "<p id = 'pdesc'>".$b['description']."</p>";
									echo "<p id = 'pprice'>Php".number_format($b['price'])."</p>";
									echo "</div></a>";
								}
								echo"</div>";
							}
						}
						if(isset($arr_filter) && isset($record_count)){
							
							
							$prev = $start - $per_page;
							echo "<div id = 'numcontainer'>";
							if($start > 0){
								echo "<a id = 'num' href = '../filter/?start=$prev&filter=$filter'>Prev</a>";
							}

							$i = 1;
							for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
								if($start != $x){
									echo " <a id = 'num' href = '../filter/?start=$x&filter=$filter'>$i</a> ";
								}else{
									echo " <a id = 'num' href = '../filter/?start=$x&filter=$filter'><b>$i</b></a> ";
								}
								$i++;
							}

							$next = $start + $per_page;
							if($start < $record_count[0]['count'] - $per_page ){
								echo " <a id = 'num' href = '../filter/?start=$next&filter=$filter'>Next</a>";
							}
							echo "</div>";
						}
					}	
				}
			?>
			</div>
		</div>
	</div>
	
    <?php include '../footer2.php';?>
</body>
</html>