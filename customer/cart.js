$(document).ready(function(){
	$(".btn-delete-cart").click(function(){
		if($(this).attr("cart-id") == ""){
			alert("Something went wrong!");
		}
		$.post("cart.php", {cartId:$(this).attr("cart-id")}, function(data){
			if(data === "Something went wrong!"){
				alert(data);
			}else if(data === "Cart Not Successfully Deleted!"){
				alert(data);
			}else if(data === "Cart Successfully Deleted!"){
				alert(data);
				window.location.replace("../cart");
			}
			alert(data);
		});
	});
		
	 $('#imgsearch').click(function(){
		if($("#txt-search").val() !== ""){
		  window.location.href = "../search/?search="+$("#txt-search").val();
		}
	  });
		$("#txt-search").bind('keypress',function(e){
		if ($("#txt-search").val()!= ""){
			if(e.which==13){
				window.location.href = "../search/?search="+$("#txt-search").val();
		  }	
		}
	});
		
	$('.btnProceedCheck').hover( function() {
		$('.btnProceedCheck').fadeIn(300);
		$('.btnProceedCheck').css("background-color","white");
		$('.btnProceedCheck').css("color","black");
		$('#rarrow').css("left","35px");

	}, function() {
		$('.btnProceedCheck').fadeIn(300);
		$('.btnProceedCheck').css("background-color"," #222d32");
		$('.btnProceedCheck').css("color","white");
		$('.btnProceedCheck').css("font-weight","bold");
		$('#rarrow').css("left","13px");
	});
});