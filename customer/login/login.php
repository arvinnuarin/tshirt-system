<?php
	require_once('../../php/customer.php');
		
	session_start();

	$customer = new Customer();

	$txtEm = $_POST['txtEm'];
	$txtPw = $_POST['txtPw'];

	if (!filter_var($txtEm, FILTER_VALIDATE_EMAIL)){
		echo "Invalid Email";
	}else{
		$result = $customer -> log_in($txtEm, $txtPw);
		if($result[0] == "Incorrect Password" || $result[0] == "Email Doesn't Exist" || $result[0] == "Email doesn't match any account in our website!" || $result[0] == "Your Password is incorrect!"){
			echo $result[0];
		}else if ($result[0] == "Success"){
			$_SESSION['customer_name'] = $result[2];
			$_SESSION['customer_email'] = $result[3];
			$_SESSION['customer_id'] = $result[1];
			echo "Successfully Log In!";
		}else{
			$_SESSION['customer_id'] = $result[1];
			$_SESSION['customer_email'] = $result[2];
			if($result[0] == "Email is only Verified!"){
				$customer -> send_code($result[2]);
				$_SESSION['customer_password'] = $result[3];
			}
			echo $result[0];
		}
	}

?>