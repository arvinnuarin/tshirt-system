$(document).ready(function(){

  $("#txtPw").keypress(function(e) {
    if(e.which == 13) {
        login();
    }
  });

  $("#btnLog").click(function(){
      login();
  });

  function login(){

    var txtEm = $('#txtEm').val();
    var txtPw = $('#txtPw').val();
    var ctr = 0;

    if (txtEm == "") {
      $("#lblWarnEmail").text("Enter Email Address!");
    }else if(!isValidEmailAddress(txtEm)){
      $("#lblWarnEmail").text("Invalid Email Address!");
    }else {
      $("#lblWarnEmail").text("");
      ctr += 1;
    }
  
    if (txtPw == "") {
      $("#lblWarnPass").text("Enter Password!");
    } else {
      $("#lblWarnPass").text("");
      ctr += 1;
    }

    if(ctr == 2){
      $.post(logInDir+"login/login.php",{txtEm:txtEm, txtPw:txtPw},
      function(data){
        if(data == "Something Went Wrong"){
          alert("Something went wrong. We need to reload the page immediately");
          window.location.replace(logInDir+"../customer/");
        }else if(data == "Invalid Email"){
          $("#lblWarnEmail").text("Invalid Email Address!");
        }else if(data == "Incorrect Password"){
          $("#lblWarnPass").text("Incorrect Password!");
        }else if(data == "Email Doesn't Exist"){
          $("#lblWarnEmail").html("Email Doesn't Exist!");
        }else if(data == "Account not already Verified!" || data == "Verification Email Already Sent!"){
          window.location.replace(logInDir+"acc_em_auth");
        }else if(data == "Email is only Verified!"){
          window.location.replace(logInDir+"acc_cn_auth");
        }else if(data == "Successfully Log In!"){
          var curUrl = window.location.href;
          if(curUrl.indexOf("product/view_product.php?product_id=") == 0)
            window.location.replace("");
          else
            window.location.replace(logInDir+"../customer/");        
        }
      });
    }
  }
});
