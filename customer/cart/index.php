<!DOCTYPE html>
<html>
<head>		

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	<link rel="stylesheet" href="cart.css" type="text/css">

	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	
	<script type = "text/javascript" src = "../../js/command.js"></script>
	<script type = "text/javascript" src = "../login/login.js"></script>

	<script type = "text/javascript" src = "cart.js"></script>
	
</head>

<body>
	<?php	
	require_once('../../php/customer.php');	
	require_once("../../php/config.php");
	$customer = new Customer();
	$arr_const_size = unserialize(SIZE);
	session_start();
    ?>
    
    <?php include '../../header/header.php';?>
        
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
		<?php
		echo "<div id = 'divcart'>";
		echo "<a id = 'lblcart' class = 'act' >Cart</a> ";
		echo "<label id = 'vline'>|</label>";
		echo "<a id = 'lblcart' class = 'act1'> Checkout</a>";
		echo "</div>";	

		if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
			if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
				$customer_id = $_SESSION['customer_id'];
				require_once('../../php/customer.php');	
				$customer = new Customer();
				
				echo "<div id = 'div_cart'>";
				if($cart = $customer -> get_cart($customer_id)){
					$header = array("", "Name", "Description", "Color", "Size", "Quantity", "Unit Price", "Price","");
					echo "<table id = 'tblprodcart'>";
					foreach ($header as $h) {
						if($h == "Clothing"){
							echo "<th colspan = '2'>";
						}else{
							echo "<th>";
						}
						echo "$h</th>";
					}
					$sum_total_price = 0;
					$vat_price = 0;
					$desc="";
					foreach ($cart as $c) {

						$_SESSION['cart_id'] = $c['cart_id'];
						echo "<tr id = 'sc_".$c['sub_cart_id']."'>";
                        $src_front = ""; 

                        	if (!empty($c['draw_location'])) {

                        		 $src_front = "../../create/custom/".$c['draw_location'].'output/front_shirt.png';
                        		  $desc = $c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method']. " (CUSTOM)";
                        	        
                        	}  else {

                        	    if(file_exists("../../assets/shirt_design_template/".$c['product_name']."/front.png")){
			                        $src_front = "../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$c['brand']."/".$c['type']."/".$c['fabric']."/".$c['hex']."/front.png&design=../assets/shirt_design_template/".$c['product_name']."/front.png";
					            }else{
			                            $src_front = "../../assets/shirt_template/".$c['brand']."/".$c['type']."/".$c['fabric']."/".$c['hex']."/front.png";
				                     }
				                     $desc = $c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method'];
                        	}

	                    echo "<td><img style = 'height:100px;' src = '$src_front'></td>";
	                    echo "<td><b>".$c['product_name']."</b></td>";
	                    echo "<td>".$desc."</td>";
	                    echo "<td>".$c['color']."</td>";
	                    echo "<td>".$arr_const_size[$c['size']]."</td>";  
						
						echo "<td>".$c['quantity']." pc";
						if($c['quantity'] > 0)
							echo "s";
						echo ". </td>";
						$total_price = $c['price'] * $c['quantity'];
						echo "<td>Php ".number_format($c['price'],2)."</td>";
						echo "<td>Php ".number_format($total_price,2)."</td>";
						echo "<td><image src = '../../resources/delete.png' cart-id = '".$c['sub_cart_id']."' class = 'delete btn-delete-cart' id = 'btndeleteitem'/></td>";
						echo "<td style = 'display:none;'><label class = 'price' style = 'display:none;'>".$c['price']."</label>";
						echo "<label class = 'quantity' style = 'display:none;'>".$c['quantity']."</label></td>";
						echo "</tr>";
						$sum_total_price += $total_price;
					}
					echo "</table>";
					$vat = ($sum_total_price * 0.12);
					$vat_price = $sum_total_price + $vat;
                    
					echo "<div id = 'divtotal2'>";
						echo "<label class = 'lbls' id = 'total'>Total Price: PHP ".$sum_total_price."</label><br>";
						echo "<label class = 'lbls' id = 'vat'>Vat: PHP ".$vat."</label><br>";
						echo "<label class = 'lbls' id = 'vatPrice'>Vatable Price: PHP ".$vat_price."</label><br>";
						$_SESSION['total'] = $vat_price;
//                        echo"<div>";
//                        echo "<table>";
//                        echo "<tr>";
//                        echo "<td>Total Price:</td><td class = 'lbls' id = 'total'>PHP ".$sum_total_price."</td>";
//                        echo "</tr>";
//                        echo "<tr>";
//                        echo "<td>Vat:</td><td class = 'lbls' id = 'total'>PHP ".$vat."</td>";
//                        echo "</tr>";
//                        echo "<tr>";
//                        echo "<td>Vatable Price:</td><td class = 'lbls' id = 'total'>PHP ".$vat_price."</td>";
//                        echo "</tr>";
//                        echo "</table>";
//					    echo"</div>";
					echo"<a href ='../checkout'><button type='submit' class ='btnProceedCheck'> Proceed to Checkout <image  id = 'rarrow' src = '../../resources/rightarrow.png'> </button></a>";

					echo "</div>";
					
				}else{
					echo "<br />";
					echo "Empty Cart!";
				}
				echo "</div>";
			}else{
				echo "<script>window.location.replace('../../customer/');</script>";
			}
		}else{
			echo "<script>window.location.replace('../../customer/');</script>";
		}
	
		?>
		</div>
	</div>
	
    <?php include '../../header/footer.php';?>
	
</body>
</html>