<?php
session_start();
if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_email']) && isset($_SESSION['customer_name'])){
    if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_email']) && !empty($_SESSION['customer_name'])){
		require_once("../../php/customer.php");
		require_once("../../php/config.php");
		$arr_const_size = unserialize(SIZE);
		$customer = new Customer();
		$cart = $customer -> get_cart($_SESSION['customer_id']);
		//echo "<script type = 'text/javascript' src = '//www.mixlarts.com/MixlArts/customer/cart/mirror_cart.js'></script>";


		echo "<div id = 'cartContainer'>";
		$cart_count = count($cart);
		if($cart_count <= 0)
			$cart_count = "";
		echo "<input type = 'hidden' id = 'cartCountSub' value = '".$cart_count."'>";
		if($cart){
		    $header = array("", "Name", "Description", "Price x Quantity", "Total");
		    echo "<table id = 'tblCart'>";
		    foreach ($header as $h) {
		        echo "<th>$h</th>";
		    }
		    $sum_total_price = 0;
		    $vat_price = 0; $desc="";
		    foreach ($cart as $c) {
		    	
		    	$_SESSION['cart_id'] = $c['cart_id'];
		    	echo "<tr id = 'sc_".$c['sub_cart_id']."'>";
                        $src_front = "";

                        	if (!empty($c['draw_location'])) {

                        		 $src_front = "//www.mixlarts.com/MixlArts/create/custom/".$c['draw_location'].'output/front_shirt.png';
                        		 $desc = $arr_const_size[$c['size']]." ".$c['color']." ".$c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method']. " (CUSTOM)";
                        	        
                        	}  else {

	                        	   $src_front = "//www.mixlarts.com/MixlArts/php/clothing_product_design.php?shirt=../assets/shirt_template/".$c['brand']."/".$c['type']."/".$c['fabric']."/".$c['hex']."/front.png&design=../assets/shirt_design_template/".$c['product_name']."/front.png";

				                     $desc = $arr_const_size[$c['size']]." ".$c['color']." ".$c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method'];
                        	}
		                    
		        echo "<td><img style = 'height:100px;' src = '$src_front'></td>";
		        echo "<td><b>".$c['product_name']."</b></td>";
		        echo "<td>".$desc."</td>";  
		        
		        echo "<td> Php ".number_format($c['price'],2)." x ".$c['quantity']."pc";
		        if($c['quantity'] > 1)
		            echo "s";
		        echo ". </td>";
		        $total_price = $c['price'] * $c['quantity'];
		        echo "<td>Php ".number_format($total_price,2)."</td>";
		        echo "<td><image src = '//www.mixlarts.com/MixlArts/resources/delete.png' cart-id = '".$c['sub_cart_id']."' class = 'delete btn-delete-cart' id = 'btndeleteitem'/></td>";
		        echo "<td style = 'display:none;'><label class = 'price' style = 'display:none;'>".$c['price']."</label>";
		        echo "<label class = 'quantity' style = 'display:none;'>".$c['quantity']."</label></td>";
		        echo "</tr>";
		        
		        $sum_total_price += $total_price;
		    }
		    echo "</table>";
		    $vat = ($sum_total_price * 0.12);
		    $vat_price = $sum_total_price + $vat;
		    $_SESSION['total'] = $vat_price;

		    echo "<div id = 'divtotal'>";
            
            echo "<table>";
                echo "<tr>";
                    echo "<td>Total Price:</td>";
                    echo "<td><label id = 'total'>PHP ".$sum_total_price."</label></td>";
                echo "</tr>";
                
                echo "<tr>";
                    echo "<td>VAT:</td>";
                    echo "<td><label id = 'vat'>PHP ".$vat."</label></td>";
                echo "</tr>";
            
                echo "<tr>";
                    echo "<td>Vatable Price:</td>";
                    echo "<td><label id = 'vatPrice'>PHP ".$vat_price."</label></td>";
                echo "</tr>";
            echo "</table>";

		   	echo "</div>";
		}else{
		    echo "Cart Empty!";
		}
		echo "</div>";
    }
}



?>