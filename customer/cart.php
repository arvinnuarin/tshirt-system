<?php
	session_start();
	require_once('../../php/customer.php');	
	$customer = new Customer();
	$sub_cart_id = $_POST['cartId'];
	$customer_id = $_SESSION['customer_id'];
	$cart_id = $customer -> get_cart_id($customer_id);

	if($sub_cart_id === "" || !is_numeric($sub_cart_id) || $customer -> cart_id_exist($customer_id, $sub_cart_id) === 0){
		echo "Something went wrong!";
	}else{
		if($customer -> delete_sub_cart_from_customer($cart_id[0]['cart_id'], $sub_cart_id)){
			echo "Cart Successfully Deleted!";
		}else{
			echo "Cart Not Successfully Deleted!";
		}
	}
?>