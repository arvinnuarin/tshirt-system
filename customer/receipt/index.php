<?php
session_start();
if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
	if(isset($_GET['id'])){
		require_once("../../php/customer.php");
		require('../../php/pdfreports/mc_table.php');
		require_once('../../php/config.php');
		$customer = new Customer();
		$arr_const_size = unserialize(SIZE);
		$main = $customer -> get_receipt_m($_GET['id']);
		$list = $customer -> get_receipt_list($_GET['id']);
		$pdf=new PDF_MC_Table();
		$header=array('Name','Description','Price x Quantity','Sub Total');
		$pdf->SetFont('Arial','',14);
		$pdf->AddPage();

		//Table with 20 rows and 4 columns
		$w=array(50,60,40,40);
		$pdf->SetWidths($w);

		$pdf->Image('../../resources/receipt_logo.jpg',95, 10,25,15);
		$pdf->SetFont('Arial','',10);
		$pdf->Ln(20);
		$pdf -> Cell(0,0 ,'156 Pag-Asa St. Malhacan, Meycauayan City, Bulacan',0,0,'C');
		$pdf->Ln(5);
		$pdf -> Cell(0,0 ,'sales.mixlarts@gmail.com',0,0,'C');
		$pdf->Ln(5);
		$pdf -> Cell(0,0 ,'0932-668-6538',0,0,'C');
		$pdf->Ln(10);

		$pdf -> Cell(0,0 ,'Transaction ID: TRANS-'. $main[0]['order_history_id'],0,0,'L');
		$pdf -> LN(7);
		$datetime = new DateTime($main[0]['date']);

		$pdf -> Cell(0,0 ,'Transaction Time: '. $datetime->format('F j, Y g:ia'),0,0,'L');
		$pdf -> LN(7);

		$pdf->SetFillColor(34,45,50);
		$pdf->SetTextColor(255);
		$pdf->SetFont('','B');
		$pdf->Row($header, 1);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill= 0;
		for($i=0;$i<count($list);$i++){
			if($fill)
				$pdf->SetFillColor(224,235,255);
			else
				$pdf->SetFillColor(255,255,255);
			$pdf->Row(array($list[$i]['product_name'], $arr_const_size[$list[$i]['size']]." ". $list[$i]['color']." ".$list[$i]['fabric']." ".$list[$i]['brand'].$list[$i]['type']." via ".$list[$i]['print_method'], "Php ".$list[$i]['price']." x ".$list[$i]['quantity']."pc.", ($list[$i]['price']* $list[$i]['quantity'])), 0);
			$fill =! $fill;
		}
		$pdf->SetFont('Arial','B',10);

		$total_sales = $main[0]['total_sales'];
		$gross = $total_sales - 150;
		$vat = ($total_sales*0.12);
		$vatable = (($total_sales*0.12)+$total_sales);
		$overall = ($vatable + 150.00);

		$pdf -> LN(20);
		$pdf -> Cell(0,0 ,'Total Sales: Php '. number_format($total_sales,2) ,0,0,'R');
		$pdf -> LN(7);
		$pdf -> Cell(0,0 ,'Vat: Php '.number_format($vat, 2),0,0,'R');
		$pdf -> LN(7);
		$pdf -> Cell(0,0 ,'Vatable Sales: Php '.number_format($vatable, 2),0,0,'R');
		$pdf -> LN(7);
		$pdf -> Cell(0,0 ,'Shipping Cost: Php 150.00',0,0,'R');
		$pdf -> LN(7);
		$pdf -> Cell(0,0 ,'Overall Cost: Php '.number_format($total_sales,2),0,0,'R');
		$pdf->Output();
	}
}



?>