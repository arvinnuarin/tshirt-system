<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	
	<script type = "text/javascript" src = "../../js/command.js"></script>
	<script type = "text/javascript" src = "../login/login.js"></script>
    
    <link rel="stylesheet" href="success.css" type="text/css">
    <script type = "text/javascript" src = "success.js"></script>

</head>
<body>
<?php	
	session_start();
	require_once('../../php/customer.php');
	require_once("../../php/config.php");	
	$customer = new Customer();
	$arr_const_size = unserialize(SIZE);
	?>
	
    <?php include '../../header/header.php';?>
    
    
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
           <div id = 'divcart'>
                <a id = 'lblcart' class = 'act1' >Cart</a> 
                <label id = 'vline'>|</label>
                <a id = 'lblcart' class = 'act1' > Checkout</a>
                <label id = 'vline'>|</label>
                <a id = 'lblcart' class = 'act' > Success</a>
            </div>	
            <div id = 'divsuccess'>
                <img id = 'successlogo' src="logo2.png">
                <p>Thank You for trusting Mixl Arts<br>Your order  has been placed</p>
                
                <a href ='../../customer'><button type='submit' class ='btnContinue'> Continue Shopping <image  id = 'rarrow' src = '../../resources/rightarrow.png'> </button></a>
            </div>
        </div>
    </div>
   <?php include '../../header/footer.php';?>
	
</body>
</html>