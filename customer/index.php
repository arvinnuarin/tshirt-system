<html>
<head>		
	<title>Welcome to MixlArts!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="customerindex.css" type="text/css">
	<script type = "text/javascript" src = "../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "../js/jssor.slider-21.1.5.mini.js"></script>
	
	<script type = "text/javascript" src = "../js/command.js"></script>
	<script type = "text/javascript" src = "../customer/login/login.js"></script>
	
	<script type = "text/javascript" src = "customerindex.js"></script>
	<script type = "text/javascript">var logInDir = "";</script>
	<script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Opacity:2}
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 3000);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        });
    </script>
</head>

<body>
	<?php	
	require_once('../php/customer.php');	
	$customer = new Customer();
	session_start();
	?>
	 <?php include '../header/header.php';?>
	<div id = "content" class="col-12 col-m-12">
				<?php

					echo "<div id = 'content3a'>";
							//if($img = $customer -> get_home_banner()){
							echo "<div id='jssor_1' style='position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1000px; height: 250px; overflow: hidden; visibility: hidden;'>";
							echo "<div data-u='loading' style='position: absolute; top: 0px; left: 0px;'>";
							echo "<div style='filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;'></div>";
							echo "<div style='position:absolute;display:block;background:url('../pictures/others/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;'></div>";
							echo "</div>";
							echo "<div data-u='slides' style='cursor: default; position: relative; top: 0px; left: 0px; width: 1000px; height: 250px; overflow: hidden;'>";

								for($x = 1; $x <= 5; $x++) {
									echo "<div data-p='112.50' style='display: none;'>";
									echo "<img data-u='image' src = '../cms/home_banner/$x.png'><br />";
									echo "</div>";
								}
							echo "</div>";
							echo "<div data-u='navigator' class='jssorb05' style='bottom:16px;right:16px;' data-autocenter='1'>";
							echo "<div data-u='prototype' style='width:16px;height:16px;'></div>";
							echo "</div>";
							echo "<span data-u='arrowleft' class='jssora12l' style='top:0px;left:0px;width:30px;height:46px;' data-autocenter='2'></span>";
							echo "<span data-u='arrowright' class='jssora12r' style='top:0px;right:0px;width:30px;height:46px;' data-autocenter='2'></span>";
							echo "</div>";

							//}
						echo "</div>";
				?>
		<div id = "wrapper">

			
			
			<div id = "content3">
				<h1 id = 'contentheader'>NEW ARRIVALS</h1>
				<?php
						if($newest = $customer -> get_preview_product_by_newest(0, 4)){
							//print_r($newest);
							echo "<div id = 'prodcontainer'>";
								foreach($newest as $n){
									echo "<a href = 'view/product/".$n['name']."'><div id = 'prod'>";
									echo "<img id = 'prodimg' src = '../php/clothing_product_design.php?shirt=../assets/shirt_template/".$n['clothing']."/front.png&design=../assets/shirt_design_template/".$n['name']."/front.png'><br />";
									echo "<p id = 'pname'>".$n['name']."</p>";
									echo "<p id = 'pdesc'>".$n['description']."</p>";
									echo "<p id = 'pprice'>Php ".number_format($n['price'], 2)."<p/>";
									echo "</div></a>";
								}
							
							echo"</div>";
						}
				
						echo"<div id = 'divseemore'>";
						echo "<button id = 'btnSeeMore'><a id = 'lblcart'href = 'shop/brand_new'>SEE MORE</a></button>";
						echo "</div>";
				?>		
			</div>
			<div id = "content4">
				<h1 id = 'contentheader'>BEST SELLER</h1>
				<?php					
					if($best = $customer -> get_preview_product_by_best_selling(0, 4)){
						//print_r($newest);
						echo "<div id = 'prodcontainer'>";
							foreach($best as $b){
								echo "<a href = 'view/product/".$b['name']."'><div id = 'prod'>";
								echo "<img id = 'prodimg' src = '../php/clothing_product_design.php?shirt=../assets/shirt_template/".$b['clothing']."/front.png&design=../assets/shirt_design_template/".$b['name']."/front.png'><br />";
								echo "<p id = 'pname'>".$b['name']."</p>";
								echo "<p id = 'pdesc'>".$b['description']."</p>";
								echo "<b><p id = 'pprice'>Php ".number_format($b['price'], 2)."</p></b>";
								echo "</div></a>";
							}
						echo "</div>";
					}
				
					echo"<div id = 'divseemore'>";
					echo "<button id = 'btnSeeMore'><a id = 'lblcart'href = 'shop/popular'>SEE MORE</a></button>";
					echo "</div>";
				
				?>
			</div>
		</div>
	</div>
	
    <?php include '../header/footer.php';?>
	
</body>
</html>