$(document).ready(function(){
    $('.btnContinue').hover( function() {
		$('.btnContinue').fadeIn(300);
		$('.btnContinue').css("background-color","white");
		$('.btnContinue').css("color","black");
		$('#rarrow').css("left","35px");

	}, function() {
		$('.btnContinue').fadeIn(300);
		$('.btnContinue').css("background-color"," #222d32");
		$('.btnContinue').css("color","white");
		$('.btnContinue').css("font-weight","bold");
		$('#rarrow').css("left","13px");
	});
});