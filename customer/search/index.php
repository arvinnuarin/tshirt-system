<!DOCTYPE html>
<html>
<head>		
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="//www.mixlarts.com/MixlArts/customer/search/">
	<link rel="stylesheet" href="search.css" type="text/css">
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript">var logInDir = "../";</script>
    <script type = "text/javascript" src = "../login/login.js"></script>	
	<script type = "text/javascript" src = "../../js/command.js"></script>
	<script type = "text/javascript" src = "search.js"></script>	
</head>

<body>
	<?php	
	require_once('../../php/customer.php');	
	$customer = new Customer();
	session_start();
	?>
	
	 <?php include '../../header/header.php';?>
	
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
		<?php
		if(isset($_GET['search'])){
			if(!empty($_GET['search'])){
				$search = $_GET['search'];
				$s_count = $customer -> search_product_design_count($search);
				
				if(count($s_count)){
					$record_count = $s_count[0]['count'];
				}else{
					$record_count = 0;
				}

				echo "<p id = 'lblresult'>";
				echo $record_count." search result/s for \" ".$search." \"";
				echo "</p>";
				if(isset($_GET['start'])){
					if(!empty($_GET['start'])){
						if(is_numeric($_GET['start'])){
							$start = intval($_GET['start']);
						}else{
							$start = 0;
						}
					}else{
						$start = 0;
					}
				}else{
					$start = 0;
				}
				$per_page = 6;
				$prev = $start - $per_page;
				
				if($searches = $customer -> search_product_design($search, $start, $per_page)){				
					echo "<div id = 'prodcontainer'>";
					foreach($searches as $s){
						echo "<a href = '../view/product/".$s['product_design_id']."'><div id = 'prod'>";
						echo "<img id = 'prodimg' src = '../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$s['clothing']."/front.png&design=../assets/shirt_design_template/".$s['name']."/front.png'><br />";
						echo "<p id = 'pname'>".$s['name']."</p>";
						echo "<p id = 'pdesc'>".$s['description']."</p>";
						echo "<p id = 'pprice'>Php ".number_format($s['price'], 2)." </p>";
						echo "</div></a>";
					}
					echo"</div>";
					
					echo "<div id = 'numcontainer'>";
					if($start > 0){
						echo "<a id = 'num' href = '../search/?start=$prev&search=$search'>Prev</a>";
					}

					$i = 1;
					for($x = 0; $x < $record_count; $x = $x + $per_page){
						if($start != $x){
							echo " <a id = 'num' href = '../search/?start=$x&search=$search'><p>$i</p></a> ";
						}else{
							echo " <a id = 'num' href = '../search/?start=$x&search=$search'><b>$i</b></a> ";
						}
						$i++;
					}

					$next = $start + $per_page;
					if($start < $record_count - $per_page ){
						echo " <a id = 'num' href = '../search/?start=$next&search=$search'>Next</a>";
					}
					echo "</div>";
				}
			}
		}
		

		?>
		</div>
	</div>
	
	   <?php include '../../header/footer.php';?>
	
</body>
</html>