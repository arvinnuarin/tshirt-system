<!DOCTYPE html>
<html>
<head>		
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../main.css" type="text/css">
	<link rel="stylesheet" href="transactions.css" type="text/css">

	
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	
	<script type = "text/javascript" src = "../../js/command.js"></script>
	<script type = "text/javascript" src = "../login/login.js"></script>
	
	<script type = "text/javascript" src = "../js/main.js"></script>
	<script type = "text/javascript" src = "transactions.js"></script>
	
	<script type = "text/javascript" src = "/MixlArts/js/c_search.js"></script>
	
</head>

<body>
	<?php	
	require_once('../../php/customer.php');	
	$customer = new Customer();
	session_start();
	?>
	
    <?php include '../header2.php';?>
	
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
            
        <h2>Previous Transactions</h2>
		<?php

			if(isset($_SESSION['customer_id'])){
				$customer_id = $_SESSION['customer_id'];
				if($order_history = $customer -> get_order_history($customer_id)){

						foreach ($order_history as $oh) {
							echo "<div id = 'divorderhistory'>";
								echo "<div id = 'divtransact' class = 'divtransact'>";
										echo "<div id = 'divtbl'>";
										echo "<table>";
											echo "<tr><td>Transaction ID: <a href = ''>".$oh['order_history_id']."</td>";
											echo "<td>Sales: PHP ".$oh['total_sales'].".00</td>";
											echo "<td>Total Sales: PHP ".$oh['vatable_sales'].".00</td></tr>";
                                            echo "<tr><td>Placed on ".$oh['date']."</td><td>VAT: PHP ".$oh['net'].".00</td>";
											echo "<td id = 'tdfulltransact'><a href = 'full_transactions.php?transaction_id=".$oh['order_history_id']."'>View Full Transaction</a></td></tr>";
										echo "</table>";
										echo "</div>";
								echo "</div>";
							
							echo "</div>";
							
							
						}
						
					
				}
			}
		?>
		</div>
	</div>
	
    <?php include '../footer2.php';?>
	
</body>
</html>