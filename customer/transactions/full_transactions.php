<!DOCTYPE html>
<html>
<head>
    <title></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../main.css" type="text/css">
    <link rel="stylesheet" href="full_transactions.css" type="text/css">
    
    <script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
    
    <script type = "text/javascript" src = "../../js/command.js"></script>
    <script type = "text/javascript" src = "../login/login.js"></script>
    
    <script type = "text/javascript" src = "../js/main.js"></script>
    <!--<script type = "text/javascript" src = "full_transactions.js"></script>-->
    
    <script type = "text/javascript" src = "/MixlArts/js/c_search.js"></script>
    
</head>
<body>
    <?php   
    require_once('../../php/customer.php'); 
    require_once('../../php/config.php');
    $customer = new Customer();
    $arr_const_size = unserialize(SIZE);
    session_start();
    ?>
    <?php include '../header2.php';?>
    
    <div id = "content" class="col-12 col-m-12">
        <div id = "wrapper">
             <h2>Transaction Details</h2>
        <?php
            if(isset($_GET['transaction_id'])){
                $transaction_id = $_GET['transaction_id'];
                $customer_id = $_SESSION['customer_id'];

                if($order = $customer -> get_one_order_history($customer_id, $transaction_id)){
                    if($sub_order = $customer -> get_sub_order_history($customer_id, $transaction_id)){
                       /* echo "<div id = 'divdetails'>";
                        echo "<label id = 'lbltransid'><span id = 'lbl1'>TRANSACTION ID:</span> ".$order[0]['order_history_id']."</label>";
                        echo "<label id = 'lblcustid'><span id = 'lbl1'>Customer ID:</span> ".$order[0]['customer_id']."</label>";
                        echo "<label id = 'lbldate'><span id = 'lbl1'>Placed on</span> ".$order[0]['date']."</label>";
                        echo "</div>";*/
                        
                        echo "<div id = 'divdetails'>";
                        echo "<table id = 'tbldetails'>";
                        echo "<tr>";
                        echo "<td id = 'lbltransid'><span id = 'lbl1'>TRANSACTION ID:</span> ".$order[0]['order_history_id']."</td>";
                        echo "<td id = 'lblcustid'><span id = 'lbl1'>Customer ID:</span> ".$order[0]['customer_id']."<td>";
                        echo "<td id = 'lbldate'><span id = 'lbl1'>Placed on</span> ".$order[0]['date']."</td>";
                        echo "</tr>";
                        echo "</table>";
                        echo "</div>";
                      
                        echo "<div id = 'divdetails2'>";
                        echo "<table id = 'tbldetails2'>";
                        echo "<th>Payment Method</th>";
                        echo "<th>Payment Status</th>";
                        echo "</table>";
                        echo "</div>";

                     
                        echo "<div id = 'divorder'>";
                            $header = array("", "Name", "Description", "Color", "Size", "Unit Price", "Quantity", "Sub Total");
                            echo "<table id = 'tblproducts'>";
                                foreach ($header as $h) {
                                    echo "<th style='text-align:left'>$h</th>";
                                }
                                
                                foreach ($sub_order as $so) {
                                    echo "<tr id = 'tprod'>";
                                    $src_front = "";
                                    if(file_exists("../../assets/shirt_design_template/".$so['product_name']."/Front.png")){
                                        $src_front = "../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$so['brand']."/".$so['type']."/".$so['fabric']."/".$so['hex']."/Front.png&design=../assets/shirt_design_template/".$so['product_name']."/Front.png";
                                    }else{
                                        $src_front = "../../assets/shirt_template/".$so['brand']."/".$so['type']."/".$so['fabric']."/".$so['hex']."/Front.png";
                                    }
                                    
                                    echo "<td><img src = '$src_front'></td>";
                                    echo "<td>".$so['product_name']."</td>";
                                    echo "<td>".$so['fabric']." ".$so['brand']." ".$so['type']." via ".$so['print_method']."</td>";
                                    echo "<td>".$so['color']."</td>";
                                    echo "<td style='text-align:center'>".$arr_const_size[$so['size']]."</td>";
                                    echo "<td style='text-align:right'>PHP ".$so['price'].".00</td>";
                                    echo "<td style='text-align:center'>".$so['quantity']."</td>";
                                    echo "<td id = 'td1'>PHP ".($so['price']*$so['quantity']).".00</td>";
                                    echo "</tr>";
                                }
                            echo "</table>";
                        echo "</div>";
                        
                        echo "<div id = 'divtotal'>";
                        echo "<table id = 'tbltotal'>";
                        echo "<col width='290'>";
                        echo "<col width='80'>";
                        echo "<tr><td id = 'td1'>Sales</td><td id = 'td1'>PHP ".$order[0]['total_sales'].".00</td></tr>";
                        echo "<tr><td id = 'td1'>VAT</td><td id = 'td1'>PHP ".$order[0]['net'].".00</td></tr>";
                        echo "<tr id = 'tr2'><td id = 'td2' >Total Sales</td><td id = 'td2' >PHP ".$order[0]['vatable_sales'].".00</td></tr>";
                        echo "</table>";
                        echo "</div>";
                        
                        echo "<div id = 'divback'>";
                        echo "<a href = '../transactions'>Back to my orders</a>";
                        echo "</div>";
                    }
                }

            }
        ?>
        </div>
    </div>
    
    <?php include '../footer2.php';?>
</body>
</html>