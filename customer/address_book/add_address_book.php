<?php
    require_once('../../php/customer.php');	
    session_start(); 
	$customer = new Customer();

	if(!isset($_SESSION['customer_id']) && !isset($_SESSION['customer_name']) && !isset($_SESSION['customer_email'])){
		header("Location:../../customer/");
	}else{
		$customer_id = $_SESSION['customer_id'];
	}

	if(!isset($_POST['txtFN']) || !isset($_POST['txtLN']) || !isset($_POST['txtCN']) || !isset($_POST['txtSt']) || !isset($_POST['selBrgy']) || !isset($_POST['selTown']) || !isset($_POST['selProv'])){
		echo "Some values are not set";	
	}else{
		if(empty($_POST['txtFN']) || empty($_POST['txtLN']) || empty($_POST['txtCN']) || empty($_POST['txtSt']) || empty($_POST['selBrgy']) || empty($_POST['selTown']) || empty($_POST['selProv'])){
			echo "Some values are empty";
		}else{
			$txtFN = $_POST['txtFN']; 
			$txtLN = $_POST['txtLN']; 
			$txtCN = $_POST['txtCN'];
			$txtSt = $_POST['txtSt'];
			$selBrgy = $_POST['selBrgy'];
			$selTown = $_POST['selTown'];
			$selProv = $_POST['selProv'];
			$ab_count = $customer -> address_book_count($customer_id);
			if($ab_count[0]['count'] <= 2){
				if($customer -> is_address_exist($customer_id, $txtSt, $selBrgy, $selTown, $selProv)){
					echo "Address already exists";
				}else {
					if($ab_id = $customer -> add_address_book($txtFN, $txtLN , $txtCN, $txtSt, $selBrgy, $selTown, $selProv, $customer_id)){
						if($ab_count[0]['count'] == 0){
							if($customer -> update_address_book_stat2($ab_id)){
								echo "Success";
							}else
								echo "Failed to update status";
						}else
							echo "Success";
						
					}else{
						echo "Failed to add";
					}					
				}
			}else{
				echo "3 address";
			}
		}
	}

?>
