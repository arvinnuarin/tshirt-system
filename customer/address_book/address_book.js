$(document).ready(function(){
	$("#btnSetPrimary").click(function(){
		var checkAdd = $('input[name=address]:checked').val();
		if(checkAdd != ""){
			$.post("address_book.php", {address:checkAdd}, function(data){
				if(data == "Success"){
					alert("Primary address successfully set!");
					window.location.replace("../address_book");
				}else if(data == "Failed to set"){
					alert("Primary Address setting failed!");
				}else{
					alert("Something Went Wrong!");
					window.location.replace("../address_book");
				}
			});
		}else{
			alert("Something Went Wrong!");
			window.location.replace("../address_book");
		}
	});

	$(".delete-address").click(function(){
		var abId = $(this).attr("ab_id");
		if(abId != ""){
			$.post("delete_address_book.php",{address:abId}, function(data){
				if(data == "Success"){
					alert("Primary address successfully deleted!");
					window.location.replace("../address_book");
				}else if(data == "Failed to delete"){
					alert("Primary address deleting failed!");
				}else{
					alert("Something Went Wrong!");
					window.location.replace("../address_book");
				}
			});
		}else{
			alert("Something Went Wrong!");
			window.location.replace("../address_book");
		}
	});
    
    $(document).on("mouseover", "#btnSetPrimary", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).find('#lbllabel').css("color","black");
	});
	
	$(document).on("mouseout", "#btnSetPrimary", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).find('#lbllabel').css("color","white");
	});
    
    $(document).on("mouseover", "#btnaddbook", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).find('#lbllabel').css("color","black");
	});
	
	$(document).on("mouseout", "#btnaddbook", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).find('#lbllabel').css("color","white");
	});  
    
    $(document).on("mouseover", ".delete-address", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).find('#lbllabel').css("color","black");
	});
	
	$(document).on("mouseout", ".delete-address", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).find('#lbllabel').css("color","white");
	});
});