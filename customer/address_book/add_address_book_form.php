<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		require_once("../../php/mixlarts.php");
		require_once("../../php/customer.php");
		$ma = new MixlArts();
		$customer = new Customer();
		echo $ma -> user_address();	
	?>
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "../../js/mixlarts.js"></script>
	<script type = "text/javascript" src = "add_address_book.js"></script>
    
    <link rel="stylesheet" href="add_address_book_form.css" type="text/css">
    
	<title></title>
</head>
<body>
    
     <?php include '../../header/header.php';?>
    
    <div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
<?php
	if(!isset($_SESSION['customer_id']) && !isset($_SESSION['customer_name']) && !isset($_SESSION['customer_email'])){
		echo "<script>window.location.replace('../address_book')</script>";
	}else{
		$customer_id = $_SESSION['customer_id'];
		$ab_count = $customer -> address_book_count($customer_id);
		if($ab_count[0]['count'] < 3){
?>

	<h1>Add Address</h1>
 
        <div id = "divreg">
            <fieldset>
            <legend>Personal Information</legend>
                <div id = "divreg2">
                    <div id = "divreg1" class = "divreg1">
                        First Name<span id = "req">*</span><br><input type = 'text' id = 'txtFN' /> <br>
                        <label id = "lblWarnFN" class = "errors"></label>
                    </div >
                    <div id = "divreg1" class = "divreg1">
                        Last Name<span id = "req">*</span><br><input type = 'text' id = 'txtLN' /> <br>
                        <label id = "lblWarnLN" class = "errors"></label> 
                    </div>

                </div>

                <div class = "divreg1">
                    Contact Number<span id = "req">*</span><br>+63 <input type = 'text' id = 'txtCN' value = ''/><br>
                    <label id = "lblWarnCN" class = "errors"></label>
                </div>

                <div id = "divaddress">
                    Address<span id = "req">*</span><br>
                    <input type = 'text' id = 'txtSt' placeholder='Home Address (Street, Sitio etc.)' /><br><br>
                    <select id = 'selProv'><option>SELECT PROVINCE</option></select>
                    <select id = 'selTown'><option>SELECT TOWN/CITY</option></select>
                    <select id = 'selBrgy'><option>SELECT BARANGAY</option></select><br>
                    <label id = 'lblWarnAddress' class = "errors"></label>
                </div>
            </fieldset>
        </div>
            
        <div id = "Reg">
            <label id = "req">* Required Fields</label></br>
            <input type = 'button' value = 'Add Address' id = 'btnAddAdress' />
        </div>			
        

<?php
		}else{
			echo "<script>window.location.replace('../address_book')</script>";
		}
	}
?>
            
         </div>
    </div>
    
    <?php include '../../header/footer.php';?>
</body>
</html>