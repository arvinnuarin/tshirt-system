$(document).ready(function(){
	var set_prov = [];
    for(var i = 0; i < set_address.length; i++){
    	if(jQuery.inArray(set_address[i][0], set_prov) == -1){
    		set_prov.push(set_address[i][0]);
    	}
    }
    var opt_prov = "";
    for(var x = 0; x < set_prov.length; x++){
        opt_prov += "<option>"+set_prov[x]+"</option>"
    }
    $("#selProv").append(opt_prov);

    $("#selProv").on('change',function(){
	    $('#selTown').empty().append('<option>SELECT TOWN/CITY</option>');
	    $('#selBrgy').empty().append('<option>SELECT BARANGAY</option>');
	    var prov = $('#selProv :selected').text();
	    var set_town = [];
	    for(var i = 0; i < set_address.length; i++){
	    	if(jQuery.inArray(set_address[i][1], set_town) == -1 && prov == set_address[i][0]){
	    		set_town.push(set_address[i][1]);
	    	}
	    }
	    var opt_town = "";
	    for(var x = 0; x < set_town.length; x++){
	        opt_town += "<option>"+set_town[x]+"</option>"
	    }
	    $("#selTown").append(opt_town);
	  });

  $("#selTown").on('change', function(){
    $('#selBrgy').empty().append('<option>SELECT BARANGAY</option>');
    var town = $('#selTown :selected').text();
    var set_brgy = [];
    for(var i = 0; i < set_address.length; i++){
    	if(jQuery.inArray(set_address[i][2], set_brgy) == -1 && town == set_address[i][1]){
    		set_brgy.push(set_address[i][2]);
    	}
    }
    var opt_brgy = "";
    for(var x = 0; x < set_brgy.length; x++){
        opt_brgy += "<option>"+set_brgy[x]+"</option>"
    }
    $("#selBrgy").append(opt_brgy);
  });

  $('#txtSt').on("input propertychange", function(){
  	$('#txtSt').val(titleCase($('#txtSt').val()));
  });

  $('#txtFN').on("input propertychange", function(){
  	$('#txtFN').val(titleCase(removeDigits($('#txtFN').val())));
  });

  $('#txtLN').on("input propertychange", function(){
  	$('#txtLN').val(titleCase(removeDigits($('#txtLN').val())));
  });

  $('#txtCN').on("input propertychange", function(){
  	$('#txtCN').val(removeNonDigits($('#txtCN').val()));
  	$('#txtCN').val(cpChecker($('#txtCN').val()));
  });

  $("#btnAddAdress").click(function(){
  	var ctr = 0;
  	$("label#lblWarnFN").html("");
    $("label#lblWarnLN").html("");
    $("label#lblWarnCN").html("");
    $("#lblWarnAddress").html("");

    if ($('#txtFN').val() == "") {
 		$("label#lblWarnFN").html("(This is a Required Field)");
		$('#txtFN').css("border","1px dashed red");
		$('#txtFN').css("background-color","#ffc0cb");
  	} else {
      $("label#lblWarnFN").html("");
	  $('#txtFN').css("border","1px solid black");
	  $('#txtFN').css("background-color","white");
      ctr += 1;
  	}

    if ($('#txtLN').val() == "") {
	  $("label#lblWarnLN").html("(This is a Required Field)");
	  $('#txtLN').css("border","1px dashed red");
	  $('#txtLN').css("background-color","#ffc0cb");
	} else {
	  $("label#lblWarnLN").html("");
	  $('#txtLN').css("border","1px solid black");
	  $('#txtLN').css("background-color","white");
	  ctr += 1;
    }

    if ($('#txtCN').val() == "") {
      $("label#lblWarnCN").html("(This is a Required Field)");
	  $('#txtCN').css("border","1px dashed red");
	  $('#txtCN').css("background-color","#ffc0cb");
    } else {
      if($('#txtCN').val.length == 10){
        $("label#lblWarnCN").html("(Invalid Cellphone Number)");
		$('#txtCN').css("border","1px dashed red");
        $('#txtCN').css("background-color","#ffc0cb");
      }else{
        $("label#lblWarnCN").html("");
		$('#txtCN').css("border","1px solid black");
	    $('#txtCN').css("background-color","white");
        ctr += 1;
      }
    }

    if ($("#txtSt").val() == "" || $("#selProv").val() == "SELECT PROVINCE" || $("#selTown").val() == "SELECT TOWN/CITY" || $("#selBrgy").val() == "SELECT BARANGAY"){
        $("#lblWarnAddress").html("Please Complete the Address");
    }else{
        ctr += 1;
        $("#lblWarnAddress").html("");
    }

    if($("#txtSt").val() == ""){
        $('#txtSt').css("border","1px dashed red");
        $('#txtSt').css("background-color","#ffc0cb");	
    }else{
        $('#txtSt').css("border","1px solid black");
	    $('#txtSt').css("background-color","white");
    }
    //prov
    if ($("#selProv").val() == "SELECT PROVINCE"){
        $('#selProv').css("border","1px dashed red");
        $('#selProv').css("background-color","#ffc0cb");
    }
    else{
        $('#selProv').css("border","1px solid black");
	    $('#selProv').css("background-color","white");
    }
    //town
    if ($("#selTown").val() == "SELECT TOWN/CITY"){
        $('#selTown').css("border","1px dashed red");
        $('#selTown').css("background-color","#ffc0cb");
    }
    else{
        $('#selTown').css("border","1px solid black");
	    $('#selTown').css("background-color","white");
    }
    //brgy
    if ($("#selBrgy").val() == "SELECT BARANGAY" ){
        $('#selBrgy').css("border","1px dashed red");
        $('#selBrgy').css("background-color","#ffc0cb");
    }
    else{
        $('#selBrgy').css("border","1px solid black");
	    $('#selBrgy').css("background-color","white");
    }
    if(ctr === 4){
    	$.post("add_address_book.php",{txtFN:$.trim($('#txtFN').val()), txtLN:$.trim($('#txtLN').val()), txtCN:$.trim($('#txtCN').val()), txtSt:$.trim($('#txtSt').val()), selBrgy:$.trim($('#selBrgy').val()), selTown:$.trim($('#selTown').val()), 
	  			selProv:$.trim($('#selProv').val())}, function(data){
	  		if(data == "Address already exists"){
	  			$("#lblWarnAddress").html("Address already exists!");
	  		}else if(data === "Success"){
	  			alert("Address book successfully added!");
	  			window.location.replace("../address_book/");
	  		}else if(data === "Failed to add"){
	  			alert("Address adding failed!");
	  		}else if(data === "3 Address"){
	  			alert("You must only have 3 address!");
	  		}else{
	  			alert("Something went wrong!");
	  		}
    	});
    }

  });
    
    $(document).on("mouseover", "#btnAddAdress", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnAddAdress", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});


});