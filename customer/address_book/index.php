<!DOCTYPE html>
<html>
<head>
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "address_book.js"></script>
    
    <link rel="stylesheet" href="address_book.css" type="text/css">
	<title></title>
</head>
<body>
<?php
    require_once('../../php/customer.php');	
    session_start(); ?>
    
    <?php include '../../header/header.php';?>
    
    <div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
	
    <?php
	if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
		if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
            $customer = new Customer();
            $customer_id = $_SESSION['customer_id'];
			echo "<label id = 'lbladdbook'>List of Recorded Address</label>";

			echo "<br />";

		
			$address_book = $customer -> get_address_book($customer_id);

			

			if($address_book){
				foreach ($address_book as $ab) {
                    echo "<div id = 'divaddress'>";
					echo "<input type = 'radio' name = 'address' ";
					if($ab['status'] == "1"){
						echo " checked ";
					}
					echo " value = '".$ab['ab_id']."' />";
					echo "Name: ".$ab['uname']."<br />";
					echo "Address: ".$ab['uaddress']."<br />";
					echo "Contact Number: ".$ab['contact_number']."<br />";
//					echo "<input type = 'button' value = 'Delete Address Book' class = 'delete-address' ab_id = '".$ab['ab_id']."'>";
				    echo "<button class = 'delete-address' ab_id = '".$ab['ab_id']."'><a id = 'lbllabel'>Delete Address Book</a></button>";
                    echo "</div>";
				}
				
			}else{
				echo "No Address Listed!";
			}
//            echo "<input type = 'button' value = 'Set as Primary Address' id = 'btnSetPrimary' />";
            echo "<button  id = 'btnSetPrimary'><a id = 'lbllabel'>Set as Primary Address</a></button>";
            if(count($address_book) < 3){
              
                echo "<button id = 'btnaddbook'><a id = 'lbllabel'href = 'add_address_book_form.php'>Add Address</a></button>";
                
			}
		}
	}else{
		echo "<script>window.location.replace('../../customer/');</script>";
	}
    ?>
        </div>
    </div>
    
    <?php include '../../header/footer.php';?>
</body>
</html>