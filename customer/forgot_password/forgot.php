<?php
	if(isset($_POST['email'])){
		if(!empty($_POST['email'])){
			require_once('../../php/send_message.php');
			require_once("../../php/customer.php");
			require_once('../../php/mixlarts.php');
			$customer = new Customer();
			$mixlarts = new MixlArts();
			$email = $_POST['email'];

			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				echo "Invalid Email";
			}else if($customer->is_email_exist($email)){
				$x = $customer -> get_customer_id($email);
				$verId = $x[0]['customer_id'];
				$verCode = md5(uniqid(rand(), true));
				$send_email = new SendEmail();
				$user = "customer";
				if($mixlarts -> update_ver_email_code($verId, $verCode, $user)){
					$send_email = new SendEmail();
					if($send_email -> send_em("noreply", $email, "Verify Email", "Click this to activate your account www.mixlarts.com/MixlArts/customer/forgot_password/enter_pass.php?verId=$verId&verCode=$verCode")){
						echo "Success";
					}else{
						echo "Email sending failed";
					}
				}else{
					echo "Update code failed";
				}				
			}else{
				echo "Email doesn't exist!";
			}
		}else{
			echo "Some data are empty";
		}
	}else{
		echo "Some data are not set";
	}
?>