$(document).ready(function(){
	$("#btnReset").click(function(){
		if ($('#txtEmail').val() == "") {
			  $("label#lblWarnEm").html("(This is a Required Field)");
			  $('#txtEmail').css("border","1px dashed red");
			  $('#txtEmail').css("background-color","#ffc0cb");
		}else if ($('#txtEmail').val() != ""){
			if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#txtEmail').val())){
				$("label#lblWarnEm").html("(Invalid Email)");
				$('#txtEmail').css("border","1px dashed red");
				$('#txtEmail').css("background-color","#ffc0cb");
			}else{
				$("label#lblWarnEm").html("");
				$('#txtEmail').css("border","1px solid black");
				$('#txtEmail').css("background-color","white");
				$.post("forgot.php", {email:$("#txtEmail").val()},function(data){
					if(data == "Success"){
						alert("Recovery Message Successfully Sent to your email.");
						window.location.replace("//www.mixlarts.com/MixlArts/customer/forgot_password/forgot_pass_send_email_success.php?email="+$("#txtEmail").val());
					}else if(data == "Email doesn't exist"){
						alert("Email doesn't exist!");
					}else if(data == "Update code failed"){
						alert("Failed to get recovery code!");
					}else if(data.indexOf("Email sending failed") != -1){
						alert("Email Failed to Send!");
					}else if(data == "Invalid Email"){
						$("label#lblWarnEm").html("(Invalid Email)");
						$('#txtEmail').css("border","1px dashed red");
						$('#txtEmail').css("background-color","#ffc0cb");
						alert("Your Email is Invalid!");
					}else{
						alert("Something went wrong!");
					}
				});
			}
	    }
	});


});