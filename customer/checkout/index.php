<!DOCTYPE html>
<html>
<head>		
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="checkout.css" type="text/css">
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	
	<script type = "text/javascript" src = "../../js/command.js"></script>
	<script type = "text/javascript" src = "../login/login.js"></script>
	<script type = "text/javascript" src = "checkout.js"></script>
	<title>MixlArts: Checkout</title>
	
</head>

<body>
	<?php	
	session_start();
	require_once('../../php/customer.php');
	require_once("../../php/config.php");	
	$customer = new Customer();
	$arr_const_size = unserialize(SIZE);
	?>
	
    <?php include '../../header/header.php';?>
	
	
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
			
<?php				
	echo "<div id = 'divcart'>";
		echo "<a id = 'lblcart' class = 'act1'>Cart</a> ";
		echo "<label id = 'vline'>|</label>";
		echo "<a id = 'lblcart' class = 'act'> Checkout</a>";
	echo "</div>";	

	
	if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){

		if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
			$customer_id = $_SESSION['customer_id'];
			require_once('../../php/customer.php');	
			$customer = new Customer();

			$address_book = $customer -> get_address_book($customer_id);
            
            echo "<div id = 'divaddbook'>";
			echo "<h2>Address Book</h2>";
			if($address_book){
				foreach ($address_book as $ab) {
                    echo "<div>";
					echo "<input type = 'radio' name = 'address'";
					if($ab['status'] == "1"){
						echo " checked ";
					}
					echo " value = '".$ab['ab_id']."' />";
					
					echo "Name: ".$ab['uname']."<br />";
					echo "Address: ".$ab['uaddress']."<br />";
					echo "Contact Number: ".$ab['contact_number']."<br />";
					echo "</div>";

					$_SESSION['address']= $ab['ab_id'];
				}
			}else{
				echo "No Address Listed!";
				echo "<br />";
				echo "<a href = '../address_book/add_address_book_form.php'>Add Address</a>";
			}
            
            echo "</div>";
		
			$cart_data = array();

			if($cart = $customer -> get_cart($customer_id)){
					$header = array("Product Name", "Description", "Unit Price", "Quantity", "Price","");
					echo "<table id = 'tblhcheckprod'>";
					foreach ($header as $h) {
						if($h == "Clothing"){
							echo "<th colspan = '2'>";
						}else{
							echo "<th>";
						}
						echo "$h</th>";
					}
					$sum_total_price = 0;
					$vat_price = 0;
					foreach ($cart as $c) {

						$desc = $arr_const_size[$c['size']]." ".$c['color']. " ".$c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method'];

						echo "<tr>";
						echo "<td>".$c['product_name']."</td>";
						//echo "<td>".$c['thickness']."</td>";
						echo "<td>".$desc."</td>";
						$total_price = $c['price'] * $c['quantity'];
						echo "<td>".$c['price']."</td>";
						echo "<td>".$c['quantity']."</td>";
						echo "<td>".$total_price."</td>";
						echo "</tr>";
						$sum_total_price += $total_price;

						$temp = array('prodname'=>$c['product_name'], 'price'=>$c['price'], 'desc'=>$desc, 'qty'=>$c['quantity'], 'subtotal'=>$total_price);
						array_push($cart_data, $temp);
					}
					echo "</table>";

					$jsonD = json_encode($cart_data, JSON_FORCE_OBJECT);

					$_SESSION['jsonD'] = $jsonD;

					$vat = ($sum_total_price * 0.12);
					$vat_price = $sum_total_price + $vat;
					echo "<div id = 'divtotal2'>";
						echo "<label id = 'lbls'>Total Price: ".$sum_total_price."</label><br>";
						echo "<label id = 'lbls'>Vat: ".$vat."</label><br>";
						echo "<label id = 'lbls'>Vatable Price: ".$vat_price."</label><br>";

					echo "</div>";
				}else{
					echo "<br />";
					echo "Empty Checkout!";
				}
                
                echo "<div id = 'divpayment'>";
				echo "<h3>Mode of Payment</h3>";
				echo '<form action="">
					  <input id="rdpay" type="radio" name="pay" value="paypal" checked><img src="img/paypal.png" style="width:250px"></img><br>
					  <input id="rdcoin" type="radio" name="pay" value="coins">&nbsp&nbsp&nbsp&nbsp<img src="img/coins.png" style="width:120px">&nbsp&nbsp</img><img src="img/711.jpg" style="width:60px"></img>&nbsp&nbsp</img><img src="img/bdo.png" style="width:60px"></img>&nbsp&nbsp</img><img src="img/mlhuillier.jpg" style="width:120px"></img><br>
					</form>';
                echo "</div>";
                echo "<button id='btn-pay'>PROCEED TO PAYMENT</button>";
			}
	}

?>
		</div>
	</div>
	
    <?php include '../../header/footer.php';?>
	
</body>
</html>