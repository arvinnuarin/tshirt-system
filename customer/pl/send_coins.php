<?php

session_start();
class PayCoins
{
	
	function __construct() {
		# code...
	}
	function requestPayment($email, $amount) {

		$arr_post_body = array(

		"payer_contact_info" => $email,
		"receiving_account" => "f61d2c5754c14c8086160654ed769487",
		"amount" => $amount,
		"message" => "MixlArts Order Payment");

		$curl_handler = curl_init();

		$headers = array("Authorization: Bearer XU1S1L5bUjjl5J1hFDuQY16giAxk8y", "Content-Type: application/json;charset=UTF-8", "Accept: application/json");
		 $URL = "https://coins.ph/api/v3/payment-requests/";

		curl_setopt($curl_handler, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handler, CURLOPT_URL, $URL);
		curl_setopt($curl_handler, CURLOPT_POSTFIELDS, json_encode($arr_post_body));
		curl_setopt($curl_handler, CURLOPT_HEADER, TRUE); 
		curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
		$response= curl_exec($curl_handler);
		$info = curl_getinfo($curl_handler);
		$actualResponseHeaders = (isset($info["header_size"]))?substr($response,0,$info["header_size"]):"";
		$actualResponse = (isset($info["header_size"]))?substr($response,$info["header_size"]):"";
		
		curl_close($curl_handler);
		return $actualResponse;
	}
}

$co = new PayCoins();
echo $co->requestPayment($_SESSION['customer_email'], $_SESSION['total']);
header("Location: //www.mixlarts.com/MixlArts/customer/success/");
?>