<?php 
require('lib/DPayPal.php');
include('../../create/conf/config.php');
session_start();

$paypal = new DPayPal(); //Create an DPayPal object
 
//Making SetExpressCheckout API call
//All available parameters for SetExpressCheckout are available at https://developer.paypal.com/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP/

$requestParams = array(
    'RETURNURL' =>"http://www.mixlarts.com/MixlArts/customer/pl/receive.php", //Enter URL of the page where you want to redirect your user after user enters PayPal login data and confirms order on PayPal page
    'CANCELURL' => "http://www.mixlarts.com/MixlArts/customer/cart"//Page you want to redirect user to, if user press cancel button on PayPal website
);

$subtotal = 0; $total = 0; $shipping = 150; $tax=0; $x=0; $item=array(); $totqty=0;

if ($totqty > 10) {
    $shipping = ($totqty / 10) * 150;
} else {
    $shipping = 150;
}

    $cart_arr = json_decode($_SESSION['jsonD'], true);

    foreach ($cart_arr as $c) {

            $newprice = $c['subtotal'];

            $item = $item + array(
                    'L_PAYMENTREQUEST_0_NAME'.$x => $c['prodname'], //title of the first product
                    'L_PAYMENTREQUEST_0_DESC'.$x => $c['desc'],
                    'L_PAYMENTREQUEST_0_AMT'.$x => $c['price'],
                    'L_PAYMENTREQUEST_0_QTY'.$x => $c['qty']);

                    $x++;
                    $subtotal += $newprice;
                    $totqty+= $c['qty'];
    }

    $tax = $subtotal * 0.12;
    $total = $subtotal + $shipping +$tax;

    //print_r($item);
    //echo $subtotal. ' Total:' .$total. 'Shipping'. $shipping. 'Tax'. $tax;

$_SESSION['subtotal'] = $subtotal;
$_SESSION['total'] = $total;
$_SESSION['shipping'] = $shipping;
$_SESSION['tax'] = $tax;

$orderParams = array(
    'PAYMENTREQUEST_0_PAYMENTACTION'=>'Sale', //becouse we want to sale something
    'PAYMENTREQUEST_0_AMT' => $total, //total amount (items amount+shipping..etc)
    'PAYMENTREQUEST_0_CURRENCYCODE' => 'PHP', //curency code
    'PAYMENTREQUEST_0_ITEMAMT' => $subtotal, //total amount items, without shipping and other taxes
    'PAYMENTREQUEST_0_TAXAMT' =>$tax, //tax
    'PAYMENTREQUEST_0_SHIPPINGAMT' => $shipping, //the shipping cost
    'LOGOIMG' => 'https://www.mixlarts.com/MixlArts/customer/pl/logo.png', //URL of your website logo. This image which will be displayed to the customer on the PayPal checkout page
    'BRANDNAME' => 'MixlArts',
);
 
 
//Now we will call SetExpressCheckout API operation. 
$response = $paypal->SetExpressCheckout($requestParams + $orderParams + $item);
 
//Response is also accessible by calling  $paypal->getLastServerResponse()
 
if (is_array($response) && $response['ACK'] == 'Success') { //Request successful
    //Now we have to redirect user to the PayPal
	//This is the point where user will be redirected to the PayPal page in order to provide Login details
	//After providing Login details, and after he confirms order in PayPal, user will be redirected to the page which you specified in RETURNURL field
    $token = $response['TOKEN'];
 
    header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . urlencode($token));
} else if (is_array($response) && $response['ACK'] == 'Failure') {
    var_dump($response);
    exit;
} 


?>