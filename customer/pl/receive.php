<?php
require('lib/DPayPal.php');
include('../../create/conf/config.php');	
session_start();

$subtotal = $_SESSION['subtotal'];
$total = $_SESSION['total'];
$tax = $_SESSION['tax'];
$shipping = $_SESSION['shipping'];
$cart_id = $_SESSION['cart_id'];
$uID = $_SESSION['customer_id']; 
$ab_id= $_SESSION['address'];

//$courier = $_SESSION['courier'];
//$ship_cost = $_SESSION['ship_cost'];

$token=$_GET["token"];//Returned by paypal, you can save this in SESSION too
$paypal = new DPaypal();
$requestParams = array('TOKEN' => $token);

$response = $paypal->GetExpressCheckoutDetails($requestParams);
$payerId=$response["PAYERID"];//Payer id returned by paypal

//Create request for DoExpressCheckoutPayment
$requestParams=array(
"TOKEN"=>$token,
"PAYERID"=>$payerId,
"PAYMENTREQUEST_0_AMT"=>$total,
"PAYMENTREQUEST_0_CURRENCYCODE"=>"PHP",//Payment currency
"PAYMENTREQUEST_0_ITEMAMT"=>$subtotal,//Item amount
"PAYMENTREQUEST_0_TAXAMT" =>$tax, //tax
"PAYMENTREQUEST_0_SHIPPINGAMT" => $shipping//the shipping cost
);
$transactionResponse=$paypal->DoExpressCheckoutPayment($requestParams);//Execute transaction

if(is_array($transactionResponse) && $transactionResponse["ACK"]=="Success"){//Payment was successfull

	$cr = date("Y-m-d H:i:s"); $status= "PAID"; $orderID= 0;

	$stmt = $dbc->prepare("SELECT cart_id FROM cart WHERE customer_id=?");
	$stmt->bind_param("s", $uID);
	$stmt->execute();
	$stmt->store_result();
	$data = $stmt->num_rows();
	
	if ($data == 1) {

		$stmt = $dbc->prepare("INSERT INTO order_history(customer_id, cart_id, ab_id, date, update_date, total_sales, status) VALUES (?,?,?,?,?,?,?)");
		$stmt->bind_param("iiissds", $uID, $cart_id, $ab_id, $cr, $cr, $total, $status);
		$stmt->execute();
		$orderID = $dbc->insert_id;
/*
		if ($stmt->affected_rows == 1) {
			$stmt = $dbc->prepare("INSERT INTO shipping (order_history_id, courier, ship_cost,tracking_no,date_send)VALUES ($orderID,$courier, $ship_cost, '', '')");
			$stmt->execute(); */

			if ($stmt->affected_rows == 1){

				$stmt = $dbc->prepare("INSERT INTO payment (payment_method, session, order_history_id)VALUES ('PAYPAL','$token', $orderID)");
				$stmt->execute();

				$stmt= $dbc->prepare("select sub_cart.draw_location from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id where cart.customer_id = $uID AND cart.status='PENDING' AND sub_cart.draw_location IS NOT NULL");
				$stmt->execute();
				$stmt->store_result();
				$data = $stmt->num_rows();

				if ($data > 0) {

					$stmt->bind_result($loc);

					while($stmt->fetch()) {

						$stmt = $dbc->prepare("UPDATE draw_build SET status='PAID' WHERE location='".$loc."'");
						$stmt->execute();
					}
				}

				$stmt = $dbc->prepare("UPDATE cart SET status='PAID' WHERE customer_id=".$uID);
				$stmt->execute();

				header("Location: //www.mixlarts.com/MixlArts/customer/success/");
			}
	
	} else {
		echo "An error occured while completing the transaction.";
	}
 
}else{

	echo 'An error occcured while completing the transaction. Please try again.';
}