$(document).ready(function(){
	$("#spanChangeEmail").hide();
	$("#changeEmail").click(function(){
		$(this).hide();
		$("#spanChangeEmail").show();
	});

	$("#cancelEmailChange").click(function(){
		$("#spanChangeEmail").hide();
		$("#changeEmail").show();
		$("#txtChangeEmail").val("");
		$("#lblWarnChangeEmail").html("");
	});
    
    $(document).on("mouseover", "#cancelEmailChange", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#cancelEmailChange", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});
    
    $(document).on("mouseover", "#btnVerifyEmail", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnVerifyEmail", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

	$("#btnVerifyEmail").click(function(){
		if($("#spanChangeEmail").css('display') == 'inline'){
			if($("#txtChangeEmail").val() == ""){
                $('#txtChangeEmail').css("border","1px dashed red");
			    $('#txtChangeEmail').css("background-color","#ffc0cb");
				$("#lblWarnChangeEmail").html("Enter Email Address!");
			}else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#txtChangeEmail').val())){
				$('#txtChangeEmail').css("border","1px dashed red");
			    $('#txtChangeEmail').css("background-color","#ffc0cb");
                $("#lblWarnChangeEmail").html("Invalid Email Address!");
			}
			else{
                $('#txtChangeEmail').css("border","1px solid black");
			    $('#txtChangeEmail').css("background-color","white");
				$.post("acc_auth.php",{txtChangeEmail: $("#txtChangeEmail").val(), txtVerCode:$.trim($('#txtVerCode').val())},
		  		function(data){
	  				if(data == "Email Sent"){
		  				alert("Email Successfully Sent!");
		  				window.location.replace("../acc_em_auth");
		  			}else if(data == "Email sending failed"){
		  				alert("Email Failed to to Send!");
		  			}else{
		  				alert("Something went wrong!");
		  			}
		  		});
			}

		}else if($("#spanChangeEmail").css('display') == 'none'){
			$.post("acc_auth.php",{txtVerCode:$.trim($('#txtVerCode').val())},
	  		function(data){
  				if(data == "Email Sent"){
	  				alert("Email Successfully Sent!");
	  				window.location.replace("../acc_em_auth");
	  			}else if(data == "Email sending failed"){
	  				alert("Email Failed to to Send!");
	  			}else{
	  				alert("Something went wrong!");
	  			}
	  		});
		}

	});
});



