<html>
	<head>
		<link rel = 'stylesheet' href = "../../css/popup.css" type="text/css">
		<link rel = 'stylesheet' href = "acc_auth.css" type="text/css">
        <link rel="stylesheet" href="../main.css" type="text/css">
		<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
		<script type = "text/javascript" src = "../../js/mixlarts.js"></script>
		<script type = "text/javascript" src = "acc_auth.js"></script>
        <script type = "text/javascript" src = "../../js/command.js"></script>
        <script type = "text/javascript">var logInDir = "../";</script>
        <script type = "text/javascript" src = "../login/login.js"></script>
        <script type = "text/javascript" src = "../js/main.js"></script>
        
	</head>
	<body>
        <?php
		session_start();
		require_once("../../php/mixlarts.php");
		require_once('../../php/customer.php');	
		$customer = new Customer();
	    ?>
        <?php include '../../header/header.php';?>
	<div id = "content" class="col-12 col-m-12">
            <div id = "wrapper">
                <div id = "divem-auth">
                    <div id = 'div-auth1'>
                        <h1>Email Verification</h1>
                        
                        <div id = 'div-auth-container1'>
                        <div id = 'div-auth-container2'>
                        <?php

                            if(isset($_SESSION['customer_id'])){
                                $mixlarts = new MixlArts();
                                $user = "customer";
                                if($mixlarts -> get_user_stat($_SESSION['customer_id'], "Email Send", $user) || $mixlarts -> get_user_stat($_SESSION['customer_id'], "Email Verified", $user)){
                                    $btnVerifyEmail = "Resend Verification Email";
                                    echo "<label id = 'lblveremail'>Your Message was already Sent. Check our message to your Email for more info.</label><br />";
                                }else if($mixlarts -> get_user_stat($_SESSION['customer_id'], "Fully Verified", $user)){
                                    echo "<script>window.location.replace('../../customer')</script>";
                                }else{
                                    $btnVerifyEmail = "Verify via Email";
                                    echo "<label id = 'lblveremail'>In order to use your account, you must verify via Email provided.</label><br />";
                                }

                                echo "<label id = 'lblveremail'>Your Saved Email is: </label>";  
                                if ($email = $mixlarts -> get_email($_SESSION['customer_id'], $user)){
                                    echo "<label id = 'lblveremail2'>".$email[0]['email']."</label>";
                                }else
                                    echo "<script>window.location.replace('../../customer/');</script>";
                                echo "<a id = 'changeEmail'>Change your email</a><br><br>";
                                echo "<span id = 'spanChangeEmail'>Change Email: <input type = 'text' id = 'txtChangeEmail'><button id = 'cancelEmailChange'>Cancel</button></span><br>";
                                echo "<div id = 'div-email'>";
                                echo "<label id = 'lblWarnChangeEmail'></label><br>";
                                echo "<button id = 'btnVerifyEmail'>$btnVerifyEmail</button><br />";
                                echo "</div>";
                            }else{
                                echo "<script>window.location.replace('../../customer/');</script>";
                            }

                        ?>
                        </div>
                    </div>
                    </div>
                
                </div>
            </div>
	</div>
    <?php include '../footer2.php';?>

	</body>
</html>