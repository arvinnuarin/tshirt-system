<?php
	session_start();
	require_once('../../php/mixlarts.php');
	require_once('../../php/send_message.php');

	$user = "customer";
	$mixlarts = new MixlArts();
	$verId = $_SESSION['customer_id'];	
	$verCode = md5(uniqid(rand(), true));
	if(isset($_POST['txtChangeEmail'])){
		if(!empty($_POST['txtChangeEmail'])){
			$txtChangeEmail = $_POST['txtChangeEmail'];
			$mixlarts -> update_email($_SESSION['customer_id'], $txtChangeEmail, $user);
		}
	}else{
		$txtChangeEmail = $_SESSION['customer_email'];
	}
	if($mixlarts -> update_ver_email_code($_SESSION['customer_id'], $verCode, $user)){
		$send_email = new SendEmail();
		if($send_email -> send_em("noreply", $txtChangeEmail, "Verify Email", "Click this to activate your account www.mixlarts.com/MixlArts/customer/verify_email/verify_email_form.php?verId=$verId&verCode=$verCode")){
			if($mixlarts -> update_user_status($verId, "Email Send", $user)){
				echo "Email Sent";
			}else{
				echo "Update user status failed";
			}
		}else{
			echo "Email sending failed";
		}
	}else{
		echo "Update code failed";
	}


?>