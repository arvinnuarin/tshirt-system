$(document).ready(function(){
	$(".view-order").click(function(e){
		$.post("orders.php", {oId:$(this).attr("o-id"), stat:$("#stat").val()}, function(data){
			if(data){
				if(data.indexOf("table") || data == "No Transaction to Show!"){
					$("#popup-inner-sub").html("<a href = '#' class = 'close'>x</a>"+data);
					$(".popup-ci").fadeIn(500);
					$(".close").click(function(e){
						$(".popup-ci").fadeOut(500);
						e.preventDefault();
					});
				}else{
					alert("Something went wrong!");
				}
			}
		});
		e.preventDefault();		
	});
});