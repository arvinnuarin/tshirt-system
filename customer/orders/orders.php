<?php
	session_start();
	if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
		if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
			if(isset($_POST['oId']) && isset($_POST['stat'])){
				if(!empty($_POST['oId']) && !empty($_POST['stat'])){
					require_once('../../php/customer.php');
					$o_id = $_POST['oId'];
					$stat = $_POST['stat'];
					$customer = new Customer();
					if($o = $customer -> one_order($o_id, $stat)){
						echo "<table style = 'width:100%;'>";
						echo "<tr>";
						echo "<td>Transaction ID: ".$o[0]['order_history_id']."</td>";
						
						echo "<td>Status: ".$o[0]['status']."</td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td>Date: ".$o[0]['date']."</td>";
						echo "<td>Payment Method: ".$o[0]['payment_method']."</td>";
						echo "</tr>";
						if($o[0]['status'] == "SHIPPED" || $o[0]['status'] == "DELIVERED"){
							echo "<tr>";					
							echo "<td>Courier: ".$o[0]['courier']."</td>";
							echo "<td>Tracking Number: ".$o[0]['tracking_no']."</td>";
							echo "</tr>";
							echo "<tr>";
						}
						echo "<td>Receiver Name: ".$o[0]['fname']." ".$o[0]['lname']."</td>";
						echo "<td>Address: ".$o[0]['home_add']." ".$o[0]['brgy']." ".$o[0]['town']." ".$o[0]['province']."</td>";
						echo "</tr>";
						echo "</table>";

						$desc="";
						$arr_const_size = unserialize(SIZE);
						$sub_order = $customer -> sub_orders($o[0]['order_history_id'], $stat);
						$header = array("", "Name", "Description", "Color", "Size", "Quantity", "Unit Price", "Price","");
						echo "<table id = 'tblprodcart' style = 'width:100%;'>";
						foreach ($header as $h) {
							if($h == "Clothing"){
								echo "<th colspan = '2'>";
							}else{
								echo "<th>";
							}
							echo "$h</th>";
						}

						foreach ($sub_order as $c) {
							echo "<tr id = 'sc_".$c['sub_cart_id']."'>";
	                        $src_front = "";
	                    	if (!empty($c['draw_location'])) {
	                    		$src_front = "../../create/custom/".$c['draw_location']."/output/front_shirt.png";
	                    		$desc = $c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method']. " (CUSTOM)";

	                    	} else {
	                	       if(file_exists("../../assets/shirt_design_template/".$c['product_name']."/Front.png")){
		                        $src_front = "../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$c['brand']."/".$c['type']."/".$c['fabric']."/".$c['hex']."/front.png&design=../assets/shirt_design_template/".$c['product_name']."/Front.png";
			            		}else{
		                            $src_front = "../../assets/shirt_template/".$c['brand']."/".$c['type']."/".$c['fabric']."/".$c['hex']."/front.png";
		                     	}
		                     	$desc = $c['fabric']." ".$c['brand']." ".$c['type']." via ".$c['print_method'];
	                 		}

		                    echo "<td><img style = 'height:100px;' src = '$src_front'></td>";
		                    echo "<td><b>".$c['product_name']."</b></td>";
		                    echo "<td>".$desc."</td>";
		                    echo "<td>".$c['color']."</td>";
		                    echo "<td>".$arr_const_size[$c['size']]."</td>";  
							
							echo "<td>".$c['quantity']." pc";
							if($c['quantity'] > 0)
								echo "s";
							echo ". </td>";
							$total_price = $c['price'] * $c['quantity'];
							echo "<td>Php ".number_format($c['price'],2)."</td>";
							echo "<td>Php ".number_format($total_price,2)."</td>";
							echo "</tr>";
						}
						$total_sales = $o[0]['total_sales'];
						$vat = ($total_sales*0.12);
						$vatable = (($total_sales*0.12)+$total_sales);
						$overall = ($vatable + 150.00);
						echo "<tr><td>Total Sales: $total_sales</td></tr>";
						echo "<tr><td>Vat: $vat</td></tr>";
						echo "<tr><td>Vatable Sales: $vatable</td></tr>";
						echo "<tr><td>Shipping Cost: Php 150.00</td></tr>";
						echo "<tr><td>Overall Cost: $overall</td></tr>";
						echo "</table>";
					}else{
						echo "No Transaction to Show!";
					}
				}else{
					echo "Some data are empty";
				}
			}else{
				echo "Some data are not set";
			}
		}else{
			echo "User is empty";
		}
	}else{
		echo "User not set";
	}
?>