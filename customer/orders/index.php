<!DOCTYPE html>
<html>
<head>	
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel = "stylesheet" type="text/css" href = "index.css">
    <script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
    <script type = "text/javascript" src = "orders.js"></script>
</head>

<body>
    <?php include '../../header/header.php';
    require_once('../../php/customer.php');
    ?>
        
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
		  <?php
            
            if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
                if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
                    
                    $customer = new Customer();
                    $customer_id = $_SESSION['customer_id'];

                    $status = "";
                    if(isset($_GET['status'])){
                        if(!empty($_GET['status'])){
                            $status = $_GET['status'];
                        }else{
                            $status = "none";
                        }
                    }else{
                        $status = "none";
                    }

                    if($orders = $customer -> orders($customer_id, $status)){
                        echo "<table id = 'tblorderhistory'>";
                        $header = array("Order ID", "Date", "Total Sales", "Payment Method", "Courier", "Status", "Track Order", "View Orders", "RECEIPT");
                        foreach ($header as $h) {
                            echo "<th>$h</th>";
                        }
                        foreach ($orders as $o) {
                            echo "<tr>";
                            echo "<td>".$o['order_history_id']."</td>";
                            echo "<td>".$o['date']."</td>";
                            echo "<td>PHP ".$o['total_sales']."</td>";
                            echo "<td>".$o['payment_method']."</td>";
                            echo "<td>".$o['courier']."</td>";
                            echo "<td>".$o['status']."</td>";
                            if ($o['status'] == 'DELIVERED') {
                                if ($o['courier'] == "LBC") {
                                    echo "<td><a href='http://www.lbcexpress.com/track/?tracking_no=".$o['tracking_no']. "'>Track</a></td> ";
                                } 
                                else if ($o['courier'] == "XEND") {
                                    echo "<td><a href='http://tracker.xend.com.ph/?waybill=".$o['tracking_no']. "'>Track</a></td> ";
                                }
                            }else{
                                echo "<td>N/A</td>";
                            }
                            echo "<td><a href = '#' class = 'view-order'  o-id = '".$o['order_history_id']."'>View Orders</a></td>";
                            echo "<td><a href = '../receipt/?id=".$o['order_history_id']."'>VIEW RECEIPT</a></td>";
                            echo "</tr>";
                        }
                        echo "</table>";

                        echo "<input type = 'hidden' id = 'stat' value = '$status'>";
                        echo "<div class='popup-ci'><div id = 'popup-inner-sub' style = 'background-color:white;position:absolute;top:5%;width:90%;left:5%;'></div></div>";
                    }else{
                        echo "No Current Order!";
                    }

                }else{
                    echo "<script>window.location.replace('../../customer/');</script>";
                }
            }else{
                echo "<script>window.location.replace('../../customer/');</script>";
            }
        ?>
		</div>
	</div>
	
    <?php include '../../header/footer.php';?>
	
</body>
</html>