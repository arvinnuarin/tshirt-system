<?php
session_start();
	require_once("../../php/customer.php");
	require_once("../../php/config.php");
	$arr_const_size = unserialize(SIZE);
	$customer = new Customer();
	if(isset($_POST['status']) && isset($_POST['prodId'])){
		if(!empty($_POST['status']) && !empty($_POST['prodId'])){
			$status = $_POST['status'];
			$product_design_id = $_POST['prodId'];
			if($status == "refresh_brand"){
				if(($clothing = $customer -> get_product_by_clothing($product_design_id)) && ($print_method = $customer -> get_product_by_pm($product_design_id))){
					foreach ($print_method as $pm) {
						echo "<option price-inc = '".$pm['price_increment']."' def-price = '".$pm['default_price']."' value = '".$pm['pm_id']."'>".$pm['name']."</option>";
					}	
					echo "#";
					foreach ($clothing as $c) {
						echo "<option value = '".$c['clothing_id']."'>".$c['clothing']."</option>";
					}			
				}
			}else if($status == "refresh_color" && isset($_POST['clothId'])){
				if(!empty($_POST['clothId'])){
					$clothing_id = $_POST['clothId'];
					if($color = $customer -> get_product_by_color($product_design_id, $clothing_id)){
						foreach ($color as $c) {
							echo "<option dir = '".$c['dir']."' value = '".$c['sc_id']."'>".$c['color']."</option>";
						}			
					}
				}
			}else if($status == "refresh_size" && isset($_POST['colorId'])){
				if(!empty($_POST['colorId'])){
					$color_id = $_POST['colorId'];
					if($size = $customer -> get_product_by_size($product_design_id, $color_id)){
						foreach ($size as $s) {
							echo "<option cart-quantity = '".$s['quantity']."' price = '".$s['price']."' size = '".$s['size']."' stock = '".$s['stock']."' value = '".$s['ssd_id']."'>".$arr_const_size[$s['size']]."</option>";
						}			
					}
				}
			}
			else if ($status == "refresh_comment" && isset($_POST['prodId'])){
				$prodId = $_POST['prodId'];
				if ($comment = $customer->get_product_comment($prodId)) {
					foreach ($comment as $com) {
						
						echo "<tr><td>".$com['rating']." Stars</td><td>".$com['comment_date']."</td><tr/><tr><td>".$com['cusname']."</td></tr><tr class = 'trlast'><td>".$com['comment']."</td><td></td></tr>"; 
					}
				} 
			}  else if ($status == "add_comment" && isset($_POST['prodId'])  && isset($_POST['rating']) && isset($_POST['comtxt'])   ){
				
					$rating = $_POST['rating']; $comtxt = $_POST['comtxt']; $prodId = $_POST['prodId'];
					$custID = $_SESSION['customer_id']; 
					$comment_date  = date("Y-m-d H:i:s");

				if ($addcom = $customer->insert_comment($rating, $comtxt, $prodId, $custID, $comment_date)) {
					echo $addcom;
				}
			}
		}
	}
	
	
?>