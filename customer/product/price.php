<?php
include('../../php/config.php');

$stmt = $dbc->prepare("SELECT
  IF(
    clothing_size_desc.size >= 0 AND clothing_size_desc.size <= 4,
    (
      product_design_print.default_price +(
      SELECT
        price
      FROM
        clothing_size_desc
      WHERE clothing_size_desc.size = 4 and clothing_size_desc.clothing_id = ?
    )
    ),
    (
      clothing_size_desc.price +(
        product_design_print.default_price +(
          (
            (clothing_size_desc.size +1) * product_design_print.price_increment
          )
        )
      )
    )
  ) AS price
FROM
  clothing
INNER JOIN
  clothing_size_desc
ON
  clothing_size_desc.clothing_id = clothing.clothing_id
INNER JOIN
  clothing_color
ON
  clothing_color.clothing_id = clothing.clothing_id
INNER JOIN
  product
ON
  product.clothing_color_id = clothing_color.sc_id
INNER JOIN
  product_design
ON
  product_design.pd_id = product.pd_id
INNER JOIN
  product_design_print
ON
  product_design_print.pd_id = product_design.pd_id
INNER JOIN
  print_method
ON
  print_method.pm_id = product_design_print.pm_id
WHERE
  clothing_size_desc.ssd_id =? AND product_design.pd_id = ? AND print_method.pm_id = ?
limit 1");
$stmt->bind_param();
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($a);

while ($stmt->fetch()) {
	echo $a;

	$stmt = $dbc->prepare("insert into sub_cart (product_id, quantity, cart_id, size_id, pm_id, price) values((select distinct product_id from product inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id where pd_id = ? and clothing_color_id = ? and clothing.clothing_id = ?), ?, ?, ?, ?, ?");
	$stmt->bind_param();
	$stmt->execute();

	if ($stmt->affected_rows == 1) {


	}


}


?>