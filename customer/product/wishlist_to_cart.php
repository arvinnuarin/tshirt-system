<?php
	session_start();
	if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
		if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
			if(isset($_POST['wishlistId']) && isset($_POST['qty']) && isset($_POST['date'])){
				if(!empty($_POST['wishlistId']) && !empty($_POST['qty']) && isset($_POST['date'])){
					$wishlist_id = $_POST['wishlistId'];
					$quantity = $_POST['qty'];
					$date = $_POST['date'];
					$customer_id = $_SESSION['customer_id'];
					if(!is_numeric($wishlist_id)){
						echo "Id is not numeric";
					}else{
						require_once("../../php/customer.php");
						$customer = new Customer();
						$info = $customer -> get_wishlist_info($customer_id, $wishlist_id); 
						if($info){
							if($info[0]['stock'] < $quantity){
								echo "Quantity is greater than stock";
							}else{
								$cart_id = $customer -> get_cart_id($customer_id);
								if(count($cart_id)){
									$sub_cart_id = $customer -> get_sub_cart2($customer_id, $info[0]['size_id'], $info[0]['pm_id'], $info[0]['product_id'], $cart_id[0]['cart_id']);
									if($sub_cart_id){
										if($customer -> update_quantity_cart($sub_cart_id[0]['sub_cart_id'], ($quantity+$sub_cart_id[0]['quantity']))){
											echo "Successfully Updated";
										}else{
											echo "Failed to Update";
										}
									}else if($customer -> wishlist_to_sub_cart($quantity, $info[0]['size_id'], $info[0]['product_id'], $info[0]['pm_id'], $cart_id[0]['cart_id'])){
										echo "Successfully Added";
									}else{
										echo "Failed to Add";
									}
								}else{
									if($customer -> wishlist_to_cart($customer_id, $date, $quantity, $info[0]['size_id'], $info[0]['product_id'])){
										echo "Successfully Added";
									}else{
										echo "Add Cart Failed";
									}
								}
							}
						}
					}
				}else{
					echo "Some values are empty";
				}
			}else{
				echo "Some values are not set";
			}
		}else{
			echo "User is empty";
		}
	}else{
		echo "User not set!";
	}

?>