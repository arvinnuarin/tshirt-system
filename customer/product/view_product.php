<!DOCTYPE html>
<html>
<head>
	<base href="//www.mixlarts.com/MixlArts/customer/product/">
	<title>MixlArts-View Tshirt Design</title>
	<link rel = 'stylesheet' href = "vp.css">

	<?php
		session_start();
		require_once('../../php/customer.php');	
		require_once('../../php/awp.php');	
		$customer = new Customer();
		$awp = new Awp();
		if(isset($_GET['product_id'])){
			if(!empty($_GET['product_id'])){
				$product_design_id = $_GET['product_id'];
				if(isset($_SESSION['customer_id'])){
					if(!empty($_SESSION['customer_id'])){
						$customer_id = $_SESSION['customer_id'];
					}else{
						$customer_id = 0;
					}	
				}else{
					$customer_id = 0;
				}
			}
		}
	?>
	
	<script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
	<script type = "text/javascript" src = "vp.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/command.js"></script>

	<script type = "text/javascript" src = "/MixlArts/js/mixlarts.js"></script>
	<script type = "text/javascript" src = "../../js/command.js"></script>
	
	<title></title>
</head>
<body>
	
	<?php include '../../header/header.php';
    
	echo "<div id = 'content' class='col-12 col-m-12'>
		<div id = 'wrapper'>";
			$sd = $customer -> get_simple_product_data($product_design_id);
			echo "<div id = 'vpwrapper'>
            <div id='damit'> 
				<div id = 'zoom'></div>
				<div id = 'prodPic'>
				</div>
			</div>
								
			<div id= 'lbls'>
					
			<p id = 'pname1'>".$sd[0]['name']."<br>
			<label id = 'lblPrice1'>Php <label id = 'lblPrice'></label></label></p>
					
				<div id='divLabel'>
					
					<div id = 'divlabel2'>
						<div id = 'divl2'>Printing Method: <div id='divLabel1'><select id = 'selPM'></select></div></div>

						<div id = 'divl2'>Clothing: <div id='divLabel1'><select id = 'selBrand'></select><label id = 'loadingBrand'>Loading Brands...</label></div></div>
						<div id = 'divl2'>Color: <div id='divLabel1'><select id = 'selColor'>
						</select><label id = 'loadingColor'>Loading Colors...</label></div></div>
						
						<div id = 'divl2'>Size: <div id='divLabel1'> <select id = 'selSize'></select><label id = 'loadingSize'>Loading Sizes...</label></div></div>
								
						<div id = 'divQuantity'>
							<div id = 'divl2'>Quantity: <div id='divLabel1'> <input type = 'number' placeholder = 'QTY' id = 'txtQuantity' value='1'><label id = 'loadingStocks'>Loading Available Stocks...</label></div></div>
							<div id = 'divl2'><div id='divLabel1'> <label id = 'lblQuantity'></label></div>	</div>											
						</div>
						<br>
						<div id = 'divLabelquan'>
							<div id = 'divl2'><div id='divLabel1'><label id = 'lblWarnQuantity'></label></div></div>
						</div>
					</div>			
					<br>
					<div id = 'divbtnadd'>
						<button id = 'btnAddToCart' type = 'submit' ><label id = 'lblcart'>ADD TO CART</label><image  id = 'rarrow' src = '//www.mixlarts.com/MixlArts/resources/rightarrow.png'></button><br>
                        
                        <button id = 'btnAddWishList'><label id = 'lblcart'>ADD TO WISHLIST</label><image  id = 'rarrow' src = '//www.mixlarts.com/MixlArts/resources/wishlist2.png'></button><br>
                        
                        <button id='btnCustomize'><label id = 'lblcart'>CUSTOMIZE</label><image  id = 'rarrow' src = '//www.mixlarts.com/MixlArts/resources/customize2.png'></button>
					</div>
					
					</div>

					<hr>
					
                    <div id= 'divlabel3'>
                        <p id = 'prodinfo'>About Product</p>
                        <div id = 'proddesc' style = 'min-height: 60px;overflow-y: auto;word-wrap:break-word'>
                            ".$sd[0]['description']."
                        </div>

                        <hr>
                        <p id = 'prodinfo'>Size Chart</p>
                        <div id = 'sizeDesc'>Loading Dimensions...</div>
                    </div>
				</div>
			</div>
			 <hr>
       
            <div id='comment'>
                <div id = 'comdiv'><label id = 'lblratings' >Rating and Reviews of ".$sd[0]['name']."</label>
                <button id='btnWriteReview'>Write Review</button>
                </div>
                
                <div id='cominput'>
                    <div><label id = 'lblthoughts'>Share us your thoughts</label></div>
                    <div>Rate this product(1-5)<br>
                    <input type='number' placeholder='Rating' min='1' max='5' id='comrating'><br></div>
                    <div>Review Description<br>
                    <textarea spellcheck='false' type='text' id='comtxt' placeholder='Type Review here'></textarea><br></div>
                    <button id='btnAddCom'>ADD COMMENT</button>
                </div>
                
                <div id = 'divcusrev'>Customers Reviews</div>
                <div id = 'divtblcomments'>
                <table id='tblcomment'></table>
                </div>
            </div>
        
		</div>
        
       
	</div>";
		include '../../header/footer.php';
		echo "<input type = 'hidden' value = '".$sd[0]['pd_id']."' id = 'prodId'>";
		if(file_exists("../../assets/shirt_design_template/".$sd[0]['name']."/front.png")){
			echo "<input type = 'hidden' id = 'frontStat'>";
		}
		if(file_exists("../../assets/shirt_design_template/".$sd[0]['name']."/back.png")){
			echo "<input type = 'hidden' id = 'backStat'>";
			
		}
		echo "<input type = 'hidden' value = '".$sd[0]['name']."' id = 'prodName'>";
	?>

	<div id = 'pop-up-log-in' style = 'visibility: hidden;'>
		You must Log In First Before Adding to Cart!
		<input type = 'button' value = 'Log In' id = 'btn-log-in' />
		<input type = 'button' value = 'Cancel' id = 'btn-cancel' />
	</div>

	<div id = 'pop-up-warning' style = 'visibility: hidden; display:none;'>
		Something went wrong!
		<input type = 'button' value = 'OK' >
	</div>

</body>
</html>