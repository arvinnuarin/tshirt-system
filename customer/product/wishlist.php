<?php
	session_start();
	if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
		if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
			if(isset($_POST['product']) && isset($_POST['brand']) && isset($_POST['color']) && isset($_POST['size']) && isset($_POST['pm'])){
				if(!empty($_POST['product']) && !empty($_POST['brand']) && !empty($_POST['color']) && !empty($_POST['size']) && !empty($_POST['pm'])){
					require_once('../../php/customer.php');	
					$customer = new Customer();
					$product_design_id = $_POST['product'];
					$clothing_id = $_POST['brand'];
					$color_id = $_POST['color'];
					$size_id = $_POST['size'];
					$pm_id = $_POST['pm'];
					$customer_id = $_SESSION['customer_id'];
					$exists = $customer -> product_comb_exist($product_design_id,  $clothing_id,  $color_id, $size_id, $pm_id);
					if($exists[0]['count'] === 0){
						echo "Unable to find the product!";
					}else if($customer -> is_wishlist_exist($product_design_id, $clothing_id, $color_id, $size_id, $pm_id, $customer_id)){
						echo "Already added";
					}else{
						if($customer -> add_to_wishlist($product_design_id, $clothing_id, $color_id, $size_id, $pm_id, $customer_id))
							echo "Success";
						else
							echo "Failed";
					}
				}else{
					echo "Some values are empty";
				}
			}else{
				echo "Some values are not set";
			}
		}else{
			echo "Log In First";
		}
	}else{
		echo "Log In First";
	}
?>