$(document).ready(function(){
	$(".wishlist").click(function(){
		var ctr = 0;
		var wishlistId = $(this).attr("wid");
		var qty = $(this).parent().parent().find(".qty").val();
		if(wishlistId !== "" && typeof wishlistId !== "undefined" && !isNaN(parseFloat(wishlistId)) && isFinite(wishlistId)){
			ctr += 1;
		}else{
			alert("Something went wrong!");
		}

		if(typeof qty !== "undefined" && !isNaN(parseFloat(wishlistId)) && isFinite(wishlistId)){
			ctr += 1;
		}else{
			alert("Quantity must be numeric!");
		}

		if(ctr == 2){
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("wishlist_to_cart.php", {wishlistId:wishlistId, date:currentDate, qty:qty}, function(data){
				if(data == "Successfully Added"){
					alert("Wishlist is now Added to Cart!");
					window.location.replace("");
				}else if(data == "Successfully Updated"){
					alert("Cart Successfully Updated!");
					window.location.replace("");
				}else{
					alert("Something went wrong!");
				}
			});
		}else{
			alert("Something went wrong!");
		}
	});

	$(".delete").click(function(){
		var wishlistId = $(this).attr("wid");
		if(wishlistId !== "" && typeof wishlistId !== "undefined" && !isNaN(parseFloat(wishlistId)) && isFinite(wishlistId)){
			var datetime = new Date();
			var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
			$.post("remove_wishlist.php", {wishlistId:wishlistId}, function(data){
				if(data == "Success"){
					alert("Successfully Deleted!");
					window.location.replace("");
				}else if(data == "Failed"){
					alert("Failed to Delete!");
				}
			});
		}else{
			alert("Something went wrong!");
		}		
	});

	$('.txtQuantity').on("input propertychange", function(){
		$('#txtQuantity').val(removeNonDigits($('#txtQuantity').val()));
	});
});