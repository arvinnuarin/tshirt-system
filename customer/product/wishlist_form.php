<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src = '//www.mixlarts.com/MixlArts/js/jquery-2.2.3.js'></script>
	<script type="text/javascript" src = 'wishlist.js'></script>
    
    <link rel="stylesheet" href="wishlist.css" type="text/css">
	<title></title>
</head>
<body>
<?php
	if (session_status() == PHP_SESSION_NONE) {
	    session_start();
	}
	require_once("../../php/customer.php");
	require_once("../../php/config.php");
	include '../../header/header.php';
	$arr_const_size = unserialize(SIZE);
	$customer = new Customer();?>
    
    <div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
    
        <?php
            if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_email']) && isset($_SESSION['customer_name'])){
                if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_email']) && !empty($_SESSION['customer_name'])){
                    $customer_id = $_SESSION['customer_id'];
                    $wishlists = $customer -> get_wishlist($customer_id);
                    if($wishlists){
                        $header = array("", "Name", "Description", "Price", "Added to Cart", "Purchasable Stocks", "Options","");
                        echo "<h1>Your Wishlists</h1>";
                        echo "<table id = 'tblwishlist'>";
                        foreach ($header as $h) {
                            echo "<th ";
                            if($h == "Options")
                                echo " colspan = '2' ";
                            echo " >$h</th>";
                        }
                        foreach ($wishlists as $w) {
                            echo "<tr>";
                            $src_front = "";
                            $src_front = "../../php/clothing_product_design.php?shirt=../assets/shirt_template/".$w['brand']."/".$w['type']."/".$w['fabric']."/".$w['hex']."/Front.png&design=../assets/shirt_design_template/".$w['product_name']."/Front.png";

                            echo "<td><img style = 'height:100px;' src = '$src_front'></td>";
                            echo "<td><b>".$w['product_name']."</b></td>";
                            echo "<td>".$arr_const_size[$w['size']]." ".$w['color']." ".$w['fabric']." ".$w['brand']." ".$w['type']." via ".$w['print_method']."</td>";  

                            echo "<td> Php ".number_format($w['price'],2)."</td>";
                            echo "<td>";
                            if(!$w['quantity'])
                                echo "0";
                            else{
                                echo $w['quantity']."pc";
                                if($w['quantity'] > 1)
                                    echo "s";
                                echo ".";
                            }
                            echo "</td>";
                            echo "<td>".($w['stock']-$w['quantity'])."pc";
                            if($w['stock'] > 0)
                                echo "s";
                            echo ". left </td>";
                            echo "<td><input type = 'number' value = '1' min = '1' placeholder = 'QTY' class = 'qty'/></td>";
                            echo "<td><a class = 'wishlist' wid = '".$w['wishlist_id']."' href = #><img id = 'imgdelwish' src = '//www.mixlarts.com/MixlArts/header/resources/cart2.png'></a></td>";
                            echo "<td><a class = 'delete' wid = '".$w['wishlist_id']."' href = '#'><img id = 'imgdelwish' src = '//www.mixlarts.com/MixlArts/header/resources/delete.png'></a></td>";
                            echo "</tr>";
                        }
                        echo "</table>";
                    }
                }
            }
        ?>
        </div>
    </div>
</body>
</html>