<?php
	session_start();

	require_once('../../php/customer.php');	
	$customer = new Customer();
	$product_design_id = $_POST['product'];
	$clothing_id = $_POST['brand'];
	$color_id = $_POST['color'];
	$size_id = $_POST['size'];
	$quantity = $_POST['quantity'];
	$pm_id = $_POST['pm'];
	$date = $_POST['date'];
	if(isset($_SESSION['customer_id'])){
		$customer_id = $_SESSION['customer_id'];
		$cart_id = $customer -> get_cart_id($customer_id);
		$stocks = $customer -> product_quantity($product_design_id, $clothing_id, $color_id, $size_id, $pm_id);

		//include('../../php/config.php');

$stmt = $dbc->prepare("SELECT
  IF(
    clothing_size_desc.size >= 0 AND clothing_size_desc.size <= 4,
    (
      product_design_print.default_price +(
      SELECT
        price
      FROM
        clothing_size_desc
      WHERE clothing_size_desc.size = 4 and clothing_size_desc.clothing_id = ?
    )
    ),
    (
      clothing_size_desc.price +(
        product_design_print.default_price +(
          (
            (clothing_size_desc.size +1) * product_design_print.price_increment
          )
        )
      )
    )
  ) AS price
FROM
  clothing
INNER JOIN
  clothing_size_desc
ON
  clothing_size_desc.clothing_id = clothing.clothing_id
INNER JOIN
  clothing_color
ON
  clothing_color.clothing_id = clothing.clothing_id
INNER JOIN
  product
ON
  product.clothing_color_id = clothing_color.sc_id
INNER JOIN
  product_design
ON
  product_design.pd_id = product.pd_id
INNER JOIN
  product_design_print
ON
  product_design_print.pd_id = product_design.pd_id
INNER JOIN
  print_method
ON
  print_method.pm_id = product_design_print.pm_id
WHERE
  clothing_size_desc.ssd_id =? AND product_design.pd_id = ? AND print_method.pm_id = ?
limit 1");
$stmt->bind_param("iiii", $clothing_id, $size_id, $product_design_id, $pm_id);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($a);
$price=0;
while ($stmt->fetch()) {
	$price =  $a;




}



		if(empty($date)){
			echo "Empty Date!";
		}else if($quantity === ""){
			echo "Empty Quantity!";
		}else if(!is_numeric($quantity)){
			echo "Quantity is not a number!";
		}else if ($quantity <= 0){
			echo "Quantity must be greater than 0!";
		}else if(count($stocks) == 0){
			echo "Unable to find the product!";
		}else if($quantity > $stocks[0]['stock']){
			echo "Quantity is greater than stocks!";
		}else if(count($cart_id)){
			if($sub_cart = $customer -> get_sub_cart($cart_id[0]['cart_id'], $product_design_id, $clothing_id, $color_id, $size_id, $pm_id)){
				if(($quantity + $sub_cart[0]['quantity']) > $stocks[0]['stock']){
					echo "Maximum stocks reached by your cart!";
				}else if($customer -> update_quantity_cart($sub_cart[0]['sub_cart_id'], ($quantity + $sub_cart[0]['quantity']), $size_id, $product_design_id, $pm_id)){
					echo "Successfully Updated!";
				}else{
					echo "Failed";
				}
			}else if($customer -> add_to_sub_cart($cart_id[0]['cart_id'], $product_design_id, $clothing_id, $color_id, $size_id, $quantity, $pm_id, $price)){
				echo "Successfully Added!";
			}
		}else{
			if($customer -> add_to_cart($customer_id, $product_design_id, $clothing_id, $color_id, $size_id, $quantity, $date, $pm_id, $price)){
				echo "Successfully Added!";
			}
		}
	}else{
		echo "Log In First";
	}

	
	
?>
