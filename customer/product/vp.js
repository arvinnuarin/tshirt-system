$(document).ready(function(){
	$("#selBrand").css("display", "none");
	$("#loadingBrand").css("display", "inline");
	$("#selColor").css("display", "none");
	$("#loadingColor").css("display", "inline");
	$("#selSize").css("display", "none");
	$("#loadingSize").css("display", "inline");
	$("#loadingStocks").css("display", "inline");
	$("#txtQuantity").css("display", "none");
	$("#lblQuantity").css("display", "none");
	$("#btnAddToCart").css("display", "none");

	$.post("refresh.php", {status:"refresh_brand", prodId:$("#prodId").val()}, function(data){
		if(data){
			$("#loadingBrand").css("display", "none");
			$("#selBrand").css("display", "inline");
			var x = data.split("#");
			$("#selPM").html($.parseHTML(x[0]));
			$("#selBrand").html($.parseHTML(x[1]));

			$.post("refresh.php", {status:"refresh_color", prodId:$("#prodId").val(), clothId:$("#selBrand").val()}, function(data){
				if(data){
					$("#loadingColor").css("display", "none");
					$("#selColor").css("display", "inline");
					$("#selColor").html($.parseHTML(data));
					$.post("refresh.php", {status:"refresh_size", prodId:$("#prodId").val(), colorId:$("#selColor").val()}, function(data){
						if(data){
							$("#selSize").css("display", "inline");
							$("#loadingSize").css("display", "none");
							$("#txtQuantity").css("display", "inline");
							$("#loadingStocks").css("display", "none");
							$("#lblQuantity").css("display", "inline");
							$("#btnAddToCart").css("display", "inline");
							$("#selSize").html($.parseHTML(data));
							picSetter();
							quantityMonitorring();
							priceManipulator();
							$(".thumbs:nth-child(2)").attr("id","thumbs");
						    	$(".thumbs").click(function(){
								$(".thumbs").removeAttr("id");
								$(this).attr('id', 'thumbs');
								$("#zoom").html("<img  style = 'width:550px;' src = '"+$(this).attr('src')+"'>");
							});
							$("#sizeDesc").html("Loading Dimensions...");
							$("#sizeDesc").load("size_description.php?clothingId="+$("#selBrand").val(), function(data){
								if(data){
									$("sizeDesc").html(data);
								}
							});
						}
					});
				}
			});
		}
	});

	$.post("refresh.php", {status:"refresh_comment", prodId:$("#prodId").val() }, function(data){

		$('#tblcomment').html($.parseHTML( data));
	});

	$('#btnAddCom').click(function(){
		var rating = $('#comrating').val() , comtxt = $('#comtxt').val(), prodID = $("#prodId").val();

		$.post('refresh.php', {status: "add_comment", rating: rating, comtxt: comtxt, prodId: prodID}, function(data){
			if (data == 'success'){
				$.post("refresh.php", {status:"refresh_comment", prodId:$("#prodId").val() }, function(data){

					var tblhead = "<tr><th>Ratings</th><th>Customer Name</th><th>Date Posted</th><th>Comment</th></tr>";
					$('#tblcomment').html($.parseHTML(tblhead + data));
				});
			} else {
				alert(data);
			}
		});

	});

	$("#selPM").on('change', function(){
		var price = 0;
		if($("#selSize:selected").val() <= 4 && $("#selSize:selected").val() >= 0){
			price = $("#selSize:selected").attr('price') + $("#selPM :selected").attr("def-price");
		}else{
			price = $("#selSize:selected").attr('price') + (($("#selPM :selected").attr("def-price")+$("#selPM :selected").attr("price-inc"))*($("#selSize").attr("size")+eval(1)));
		}
		quantityMonitorring();
		priceManipulator();
	});

	
	$("#selBrand").on('change', function(){
		$("#loadingColor").css("display", "inline");
		$("#selColor").css("display", "none");
		$("#loadingSize").css("display", "inline");
		$("#selSize").css("display", "none");
		$("#loadingStocks").css("display", "inline");
		$("#txtQuantity").css("display", "none");
		$("#lblQuantity").css("display", "none");
		$("#btnAddToCart").css("display", "none");
		$.post("refresh.php", {status:"refresh_color", prodId:$("#prodId").val(), clothId:$("#selBrand").val()}, function(data){
			if(data){
				$("#loadingColor").css("display", "none");
				$("#selColor").css("display", "inline");
				$("#selColor").html($.parseHTML(data));
				$.post("refresh.php", {status:"refresh_size", prodId:$("#prodId").val(), colorId:$("#selColor").val()}, function(data){
					if(data){
						$("#loadingSize").css("display", "none");
						$("#selSize").css("display", "inline");
						$("#loadingStocks").css("display", "none");
						$("#txtQuantity").css("display", "inline");
						$("#lblQuantity").css("display", "inline");
						$("#btnAddToCart").css("display", "inline");
						$("#selSize").html($.parseHTML(data));
						$("#prodPic").empty().append($.parseHTML("<div id = 'zoom'></div>"));
					    picSetter();
					    quantityMonitorring();
						priceManipulator();

						/*$(".thumbs:nth-child(2)").attr("id","thumbs");
					    $(".thumbs").click(function(){
							$(".thumbs").removeAttr("id");
							$(this).attr('id', 'thumbs');
							$("#zoom").html("<img  style = 'width:550px;' src = '"+$(this).attr('src')+"'>");
						});*/
						
						$("#sizeDesc").html("Loading Size Chart...");
						$("#sizeDesc").load("size_description.php?clothingId="+$("#selBrand").val(), function(data){
							if(data){
								$("sizeDesc").html(data);
							}
						});
					}
				});
			}
		});
	});

	$("#selColor").on('change', function(data){
		sizeRefresher();
	});

	$("#selSize").on("change", function(data){
		picSetter();
	    quantityMonitorring();
		priceManipulator();
	});

	function quantityMonitorring(){
		$("#lblWarnQuantity").html("");
		$("#lblQuantity").html("");
	    if($("#selSize :selected").attr("cart-quantity")){
	    	var quantity = $("#selSize :selected").attr("stock") - $("#selSize :selected").attr("cart-quantity");
	    	if(quantity <= 0){
	    		$("#divQuantity").css("display", "none");
	    		$("#btnAddToCart").css("display", "none");
	    		$("#lblWarnQuantity").html("Maximum stocks reached by your cart!");
	    	}
	    	else{
	    		$("#divQuantity").css("display", "inline");
	    		$("#btnAddToCart").css("display", "inline");
	    		$("#lblQuantity").html(quantity + " pcs. left");
	    	}	
		}else{
			$("#divQuantity").css("display", "inline");
	    	$("#btnAddToCart").css("display", "inline");
			$("#lblQuantity").html($("#selSize :selected").attr("stock") + " pcs. left");
		}
	}



	function sizeRefresher(){
		$("#loadingSize").css("display", "inline");
		$("#selSize").css("display", "none");
		$("#loadingStocks").css("display", "inline");
		$("#txtQuantity").css("display", "none");
		$("#lblQuantity").css("display", "none");
		$("#btnAddToCart").css("display", "none");
		$.post("refresh.php", {status:"refresh_size", prodId:$("#prodId").val(), colorId:$("#selColor").val()}, function(data){
			if(data){
				$("#loadingSize").css("display", "none");
				$("#selSize").css("display", "inline");
				$("#loadingStocks").css("display", "none");
				$("#txtQuantity").css("display", "inline");
				$("#lblQuantity").css("display", "inline");
				$("#btnAddToCart").css("display", "inline");
				$("#selSize").html($.parseHTML(data));
				$("#prodPic").empty().append($.parseHTML("<div id = 'zoom'></div>"));
				picSetter();
			    quantityMonitorring();
				priceManipulator();
				$(".thumbs:nth-child(2)").attr("id","thumbs");
				$(".thumbs").click(function(){
					$(".thumbs").removeAttr("id");
					$(this).attr('id', 'thumbs');
					$("#zoom").html("<img  style = 'width:550px;' src = '"+$(this).attr('src')+"'>");
				});
			}
		});
	}

	function priceManipulator(){
		var tot_price = 0;
		var price = eval($("#selSize :selected").attr('price'));
		var defPrice = eval($("#selPM :selected").attr("def-price"));
		var priceInc = eval($("#selPM :selected").attr("price-inc"));
		var size = eval($("#selSize :selected").attr("size"));
		if($("#selSize :selected").attr('size') <= 4 && $("#selSize :selected").attr('size') >= 0){
			tot_price = eval(price) + eval(defPrice);
		}else{
			tot_price = (defPrice + (priceInc*(size+1)))+price;	
		}
		$("#lblPrice").html(tot_price.toFixed(2));
	}

	function picSetter(){
		$("#prodPic").empty().append($.parseHTML("<div id = 'zoom'></div>"));
		var front = "";
		var back = "";
		if($("#frontStat").length)
			front = "../../php/clothing_product_design.php?shirt=../assets/shirt_template/"+$("#selColor :selected").attr("dir")+"/front.png&design=../assets/shirt_design_template/"+$("#prodName").val()+"/front.png";
		else
			front = "../../assets/shirt_template/"+$("#selColor :selected").attr("dir")+"/front.png";
		if($("#backStat").length)
			back = "../../php/clothing_product_design.php?shirt=../assets/shirt_template/"+$("#selColor :selected").attr("dir")+"/back.png&design=../assets/shirt_design_template/"+$("#prodName").val()+"/back.png";
		else
			back = "../../assets/shirt_template/"+$("#selColor :selected").attr("dir")+"/back.png";

		$("#zoom").html("<img style = 'width:550px;' src = '"+front+"'/>");
		$("#prodPic").append("<img style = 'height:100px;' class = 'thumbs' src = '"+front+"'/>");
		$("#prodPic").append($.parseHTML("<img style = 'height:100px;' class = 'thumbs' src = '"+back+"'/>"));
	}
	
	$('#btnAddToCart').hover( function() {
		$('#btnAddToCart').fadeIn(300);
		$('#btnAddToCart').css("background-color","white");
		$('#btnAddToCart').css("color","black");
		$('#rarrow').css("left","35px");

	}, function() {
		$('#btnAddToCart').fadeIn(300);
		$('#btnAddToCart').css("background-color","#222d32");
		$('#btnAddToCart').css("color","white");
		$('#btnAddToCart').css("font-weight","bold");
		$('#rarrow').css("left","13px");
	});
    
    $(document).on("mouseover", "#btnAddWishList", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnAddWishList", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});
    
    $(document).on("mouseover", "#btnCustomize", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnCustomize", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

	
	
	$('#txtQuantity').on("input propertychange", function(){
		$('#txtQuantity').val(removeNonDigits($('#txtQuantity').val()));
	});
	
	$("#pop-up-log-in").hide();

	$("#btnAddToCart").click(function(){
		
		if($("#divQuantity").is(':visible')){

			var cart_quantity = eval($("#selSize :selected").attr("stock") - $("#selSize :selected").attr("cart-quantity"));
			var quantity = eval($("#txtQuantity").val());
			var stock = eval($("#selSize :selected").attr("stock"));
			$("#lblWarnQuantity").html("");
			$("#lblWarnQuantity").css("color","#d50101");

			if(quantity == ""){
				$("#lblWarnQuantity").html("Enter Quantity");
			}else if(quantity == 0){
				$("#lblWarnQuantity").html("Quantity must be greater than 0!");
			}else if(cart_quantity <= 0){
				$("#lblWarnQuantity").html("Maximum stocks reached by your cart!");
			}else if(quantity > stock) {
			    $("#lblWarnQuantity").html("Quantity is greater than stocks left!");
			}else{

				var datetime = new Date();
				var currentDate = datetime.getFullYear()+"-"+(datetime.getMonth()+eval(1))+"-"+datetime.getDate()+" "+datetime.getHours()+":"+datetime.getMinutes()+":"+datetime.getSeconds();
				$("#btnAddToCart").prop('disabled', true);
				
				$.post("vp.php", {product:$("#prodId").val(), brand:$("#selBrand").val(), color:$("#selColor").val(), size:$("#selSize").val(), quantity:$("#txtQuantity").val(), pm:$("#selPM").val(),date:currentDate}, function(data){
					if(data){
						$("#btnAddToCart").prop('disabled', false);
						if(data == "Maximum stocks reached by your cart!" || data == "Empty Quantity!" || data == "Quantity must be greater than 0!" || data == "Quantity is not a number!" || data == "Quantity is greater than stocks!" || data == "Quantity is not a valid integer!"){
							$("#lblWarnQuantity").html(data);
						}else if(data == "Something Went Wrong!"){
							window.location.replace('');
						}else if(data == "Successfully Added!" || data == "Successfully Updated!"){
							sizeRefresher();
							if($("#cartCount") && $("#cartContainer")){
								$("#cartContainer").load("//www.mixlarts.com/MixlArts/customer/cart/mirror_cart.php", function(data){
									$("#cartCount").html($("body").find("#cartCountSub").val());
								});
			                }
						alert(data);
						}else if(data == "Log In First"){
							$("#pop-up-log-in").show();
						}else{
							$("#pop-up-warning").show();
						}
					}
				});		
			}
		}
	});


	$("#btn-log-in").click(function(){
		$(".login-content").slideToggle(500);
		$(".help-content").slideUp(500);
		$("#pop-up-log-in").hide();
	});
	$("#btn-cancel").click(function(){
		$("#pop-up-log-in").hide();
	});
	
	$('#imgsearch').click(function(){
    if($("#txt-search").val() !== ""){
      window.location.href = "../search/?search="+$("#txt-search").val();
    }
	});
    $("#txt-search").bind('keypress',function(e){
	if ($("#txt-search").val()!= "")
	  if(e.which==13){
		  	window.location.href = "../search/?search="+$("#txt-search").val();
		
	  }	
	});



	$("#btnAddWishList").click(function(data){
		if($("#divQuantity").is(':visible')){
			var datetime = new Date();
			$.post("wishlist.php", {product:$("#prodId").val(), brand:$("#selBrand").val(), color:$("#selColor").val(), size:$("#selSize").val(), pm:$("#selPM").val()}, function(data){
				if(data){
					if(data == "Success"){
						var count = eval($("#count-wishlist").html());
						if ($("#count-wishlist").html() == "")
							count = 1;
						else if(count > 0)
							count += eval(1);
						$("#count-wishlist").html(count);
						alert("Successfully Added to Wishlist!");
					}else if(data == "Already added"){
						alert("Product Already Added to Wishlist!");
					}else if(data == "Log In First"){
						$("#pop-up-log-in").show();
					}else{
						$("#pop-up-warning").show();
					}
				}			
			});		
		}
	});

    //customize
    $('#btnCustomize').click(function(){

    	var proname = $("#prodName").val(), 
    	size = $('#selSize :selected').val(), qty = $('#txtQuantity').val(), prt = $("#selPM :selected").val(),
    	budget = $('#lblPrice').text(), note = 'Customize predefined Tshirt design.', color= $('#selColor').val();

    	var imgSrc = '//www.mixlarts.com/MixlArts/assets/shirt_design_template/'+$("#prodName").val()+'/Front.png';
    	var temploc = $("#selColor :selected").attr("dir");
    	
    	$.post('../../create/custom/php/createPredefined.php', {prname: proname, shirtQty: qty, shirtSize: size, shirtColor: color, shirtBgt: budget, shirtPrint: prt, shirtNote: note, imageSrc: imgSrc, temploc: temploc}, function(data){

    		var x= data.split('*');
		if (x[0] == 'created'){
			window.location.href = '../../create/mixl/project/' + x[1];
		} else {
			alert(data);
		}
    	}); 
    });
    
     $('#cominput').hide();
     $('#btnWriteReview').click(function(){
         $('#cominput').toggle(500);
     });

});
