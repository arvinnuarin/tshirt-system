<!DOCTYPE html>
<html>
<head>
    <base href="//www.mixlarts.com/MixlArts/customer/product/">
    <script type = "text/javascript" src = "/MixlArts/js/jquery-2.2.3.js"></script>
    <link rel = 'stylesheet' href = "index.css">
    <script type = "text/javascript" src = "/MixlArts/js/command.js"></script>
	<script type = "text/javascript" src = "index.js"></script>
    <script type = "text/javascript">var logInDir = "../";</script>
    <script type = "text/javascript" src = "../login/login.js"></script>
	<script type = "text/javascript" src = "/MixlArts/js/command.js"></script>

</head>
<body>
    <?php	
	require_once('../../php/customer.php');	
	$customer = new Customer();
	session_start();
	?>
	<?php include '../../header/header.php';?>
    
	   <div id = 'content' class='col-12 col-m-12'>
			<?php
                $customer = new Customer();
                if(isset($_GET['cat_id'])){
                    if(!empty($_GET['cat_id'])){
                        $cat_id = $_GET['cat_id'];
                        if($sub_cat = $customer -> get_one_scategory($cat_id)){					
                            if(isset($_GET['start'])){
                                if(!empty($_GET['start'])){
                                    if(is_numeric($_GET['start'])){
                                        $start = intval($_GET['start']);
                                    }else{
                                        $start = 0;
                                    }
                                }else{
                                    $start = 0;
                                }
                            }else{
                                $start = 0;
                            }
                            $per_page = 6;

                            echo "<title>MixlArts  ".$_GET['cat_id']." Tshirt</title>";
                            
                            echo "<img id = 'imgsubbanner' src = '../../cms/category_banner/".$sub_cat[0]['sub_cat_name'].".png'>";
               
                            echo" <div id = 'wrapper'>";
                            if($preview_products = $customer -> get_preview_product_by_category($cat_id, $start, $per_page)){
                                
                                echo "<div id = 'prodcontainer'>";
                               
                               foreach($preview_products as $pp){
                                    echo "<a href = '../view/product/".$pp['name']."'>";
                                    echo "<div id = 'prod'>";
                                    echo "<img id = 'prodimg' src = '//www.mixlarts.com/MixlArts/php/clothing_product_design.php?shirt=../assets/shirt_template/".$pp['clothing']."/front.png&design=../assets/shirt_design_template/".$pp['name']."/front.png'><br />";
                                    echo "<p id = 'pname'>".$pp['name']."</p>";
                                    echo "<p id = 'pdesc'>".$pp['description']."</p>";
                                    echo "<p id = 'pprice'>Php ".number_format($pp['price'], 2)."</p>";
                                    echo "</div></a>";
                                }
                                
                                echo"</div>";
                                
                                echo "<div id = 'numcontainer'>";
                                $record_count = $customer ->get_preview_product_by_category_count($cat_id);

                                $prev = $start - $per_page;
                                if($start > 0){
                                    echo "<a id = 'num' href = '../product/?start=$prev&cat_id=$cat_id'>Prev</a>";
                                }

                                $i = 1;
                                for($x = 0; $x < $record_count[0]['count']; $x = $x + $per_page){
                                    if($start != $x){
                                        echo " <a id = 'num'href = '../product/?start=$x&cat_id=$cat_id'>$i</a> ";
                                    }else{
                                        echo " <a id = 'num' href = '../product/?start=$x&cat_id=$cat_id'><b>$i</b></a> ";
                                    }
                                    $i++;
                                }

                                $next = $start + $per_page;
                                if($start < $record_count[0]['count'] - $per_page ){
                                    echo " <a id = 'num' href = '../product/?start=$next&cat_id=$cat_id'>Next</a>";
                                }
                                echo "</div>";
                            }
                        }
                    }
                }

            ?>
		</div>
	</div>
	
	  <?php include '../footer2.php';?>
	
</body>
</html>