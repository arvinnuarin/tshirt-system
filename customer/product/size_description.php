<?php
if(isset($_GET['clothingId'])){
	if(!empty($_GET['clothingId'])){
		require_once("../../php/customer.php");
		require_once("../../php/config.php");
		$arr_const_size = unserialize(SIZE);
		$customer = new Customer();
		$clothing_id = $_GET['clothingId'];
		$size_dimensions = $customer -> get_size($clothing_id);
		echo "<table id = 'tblsizechart'>";
		echo "<th>Size</th><th>Height</th><th>Width</th>";
		foreach ($size_dimensions as $sd) {
			echo "<tr>";
			echo "<td>".$arr_const_size[$sd['size']]."</td>";
			echo "<td>".$sd['height']."</td>";
			echo "<td>".$sd['width']."</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
}
?>