<?php
	session_start();
	if(isset($_SESSION['customer_id']) && isset($_SESSION['customer_name']) && isset($_SESSION['customer_email'])){
		if(!empty($_SESSION['customer_id']) && !empty($_SESSION['customer_name']) && !empty($_SESSION['customer_email'])){
			
			if(isset($_POST['wishlistId'])){
				if(!empty($_POST['wishlistId'])){
					$wishlist_id = $_POST['wishlistId'];

					if(!is_numeric($wishlist_id)){
						echo "Id is not numeric";
					}else{
						require_once("../../php/customer.php");
						$customer = new Customer();
						if($customer -> delete_wishlist($wishlist_id)){
							echo "Success";
						}else{
							echo "Failed";
						}
					}
				}else{
					echo "Some values are empty";
				}
			}else{
				echo "Some values are not set";
			}
		}else{
			echo "User is empty";
		}
	}else{
		echo "User not set!";
	}

?>