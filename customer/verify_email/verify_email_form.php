<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="verify_email.css">
    <link rel="stylesheet" href="../main.css" type="text/css">
	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	<script type="text/javascript" src = "verify_email.js"></script>
    <script type = "text/javascript" src = "../../js/command.js"></script>
    <script type = "text/javascript">var logInDir = "../";</script>
    <script type = "text/javascript" src = "../login/login.js"></script>
    <script type = "text/javascript" src = "../js/main.js"></script>
</head>
<body>
    
    <?php
    session_start();
    require_once('../../php/customer.php');	
    $customer = new Customer();
    //$customer -> get_log_in_credential();
    if(isset($_GET['verId']) && isset($_GET['verCode'])){
        require_once('../../php/mixlarts.php');

        $user = "customer";
        $ver_id = $_GET['verId'];
        $ver_code = $_GET['verCode'];
        $_SESSION['customer_id'] = $ver_id;
        $_SESSION['customer_code'] = $ver_code;
        $mixlarts = new MixlArts();

        $stat = "";
        if($mixlarts -> is_email_code_exist($ver_id, $ver_code, $user)){
            if(!$mixlarts -> get_user_stat($ver_id, "Fully Verified", $user)){
                if($mixlarts -> ver_em($ver_id, $ver_code, $user)){
                    $stat = "Verification Success";
                    $result = $customer -> get_log_in_credential($ver_id);
                    $_SESSION['customer_name'] = $result[0]['uname'];
                    $_SESSION['customer_email'] = $result[0]['email'];
                    $_SESSION['customer_id'] = $result[0]['customer_id'];
                }else{
                    $stat = "Verification Failed";
                }
            }else{
                $stat = "Account is already Verified";
            }
        }else{
            $stat = "Doesn't Match";
        }

    }
    ?>
    
    
    <?php include '../../header/header.php';?>
    
    <div id = "content" class="col-12 col-m-12">
            <div id = "wrapper">
                <div id = 'div-verify'>
                <?php
                    if($stat == "Verification Success"){
                        echo "<h1>Email Successfully Verified!</h1>";
                        echo "<img id = 'imgver' src = '../../resources/veremail.png'><br>";
                                //echo "<button id = 'verifyCP'>Verify Using Your Cellphone Number</button>";
                        echo "<input id = 'btnGoTo' type = 'button' value = 'Go to Homepage'>";
                    }else if($stat == "Verification Failed"){
                        echo "<h1>Verification Failed!</h1>";
                    }else if($stat == "Account is already Verified"){
                        echo "<h1>Account is already Verified!</h1>";
                    }else if($stat == "Doesn't Match"){
                        echo "Verification code doesn't exist! If you are having a problem regarding this, please \"Log In\" again your account and click \"RESEND VERIFICATION EMAIL\".";
                    }

                ?>
                </div>
            </div>
	</div>
    <?php include '../footer2.php';?>


</body>
</html>



	