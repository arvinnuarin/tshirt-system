<?php
	require_once('../../php/customer.php');
	
	$customer = new Customer();

	session_start();

	if(isset($_POST['refresh']) && isset($_POST['place'])){
		if(!empty($_POST['refresh']) && !empty($_POST['place'])){
			$refresh = $_POST['refresh'];
			$place = $_POST['place'];
			if($refresh == "city"){
				$city = $customer -> get_town($place);
				foreach ($city as $c) {
					echo "<option>$c</option>";
				}
			}else if($refresh == "brgy"){
				$brgy = $customer -> get_brgy($place);
				foreach ($brgy as $b) {
					echo "<option>$b</option>";
				}
			}
		}
	}

	if(isset($_POST['txtFN']) && isset($_POST['txtLN']) && isset($_POST['txtEm']) && isset($_POST['txtPassword']) && isset($_POST['txtCN']) && isset($_POST['captcha']) && isset($_POST['txtSt']) && isset($_POST['selBrgy']) && isset($_POST['selTown']) && isset($_POST['selProv'])){
		$txtFN = $_POST['txtFN']; 
		$txtLN = $_POST['txtLN']; 
		$txtEm = $_POST['txtEm']; 
		$txtPw = md5($_POST['txtPassword']); 
		$txtCN = $_POST['txtCN'];
		$captcha = $_POST['captcha'];
		$status = "Unverified";
		$txtSt = $_POST['txtSt'];
		$selBrgy = $_POST['selBrgy'];
		$selTown = $_POST['selTown'];
		$selProv = $_POST['selProv'];
		$email_ver = md5(uniqid(rand(), true));

		if($customer->is_email_exist($txtEm)){
			echo "Email Already Exist!";
		}else if(!$captcha){
	        echo 'Please check the the captcha form.';
	    }else{
		    $secretKey = "6Ld1TQwUAAAAACFMmIJv7SrIU-jGzRVN508ygv_m";
		    $ip = $_SERVER['REMOTE_ADDR'];
		    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
		    $responseKeys = json_decode($response,true);
			if(intval($responseKeys["success"]) !== 1) {
		        echo 'Invalid captcha';
		    }else{
				if($customer -> register($txtFN, $txtLN, $txtEm, $txtPw, $txtCN, $status, $email_ver, $txtSt, $selBrgy, $selTown, $selProv)){
					if($result = $customer -> log_in($txtEm, $_POST['txtPassword'])){
						if ($result[0] == "Account not already Verified!"){
							$_SESSION['customer_id'] = $result[1];
							$_SESSION['customer_email'] = $result[2];
							echo "Registered";
						}else{
							echo "Failed";
						}
					}
				}
				/*if ($res){
					require("../../PHPMailer/class.phpmailer.php");
					$verId = $awp -> get_id($txtEm);				
					$mailer = new PHPMailer();
					$mailer->IsSMTP();
					$mailer->Host = 'ssl://smtp.gmail.com:465';
					$mailer->SMTPAuth = TRUE;
					$mailer->Username = 'bjmaclang@gmail.com';
					$mailer->Password = 'Gwapoako123';
					$mailer->From = 'bjmaclang@gmail.com';
					$mailer->FromName = 'MixlArts';
					$mailer->Body = "Click this to activate your account https://localhost/MixlArts/wp/verify/verifyForm.php?verId=$verId&verCode=$email_ver";
					$mailer->Subject = 'Verification of Account';
					$mailer->AddAddress($txtEm);
					if (!$mailer->Send()) {
						echo "Message was not sent. ";
						echo "Mailer Error: " . $mailer->ErrorInfo;
					} else {
						echo "Successfully Registered! Please verify your account using the email you provided.";
					}
				}*/
			}

		}

	}
?>