<html>
<head>		
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="register.css" type="text/css">
	<link rel="stylesheet" href="../main.css" type="text/css">

	<script type = "text/javascript" src = "../../js/jquery-2.2.3.js"></script>
	
	<script type = "text/javascript" src = "../../js/command.js"></script>
	
	
	<script type = "text/javascript" src = "register.js"></script>
	<script type = "text/javascript" src = "../../js/mixlarts.js"></script>
	<script src='https://www.google.com/recaptcha/api.js' ></script>
	<script type = "text/javascript">var logInDir = "../";</script>
	<script type = "text/javascript" src = "../login/login.js"></script>
	<?php
		session_start();
		require_once('../../php/customer.php');	
		$customer = new Customer();	
	?>
</head>

<body>

	<?php include '../../header/header.php';?>
	<div id = "content" class="col-12 col-m-12">
		<div id = "wrapper">
				<div id = "divreg">
				<h1>CREATE AN ACCOUNT</h1>
						<div id = "divreg2wrapper">
						<fieldset>
							<legend>Personal Information</legend>
							<div id = "divreg2">
								<div id = "divreg1" class = "divreg1">
									First Name<span id = "req">*</span><br><input type = 'text' id = 'txtFN' /> <br>
									<label id = "lblWarnFN" class = "errors"></label>
								</div >
								<div id = "divreg1" class = "divreg1">
									Last Name<span id = "req">*</span><br><input type = 'text' id = 'txtLN' /> <br>
									<label id = "lblWarnLN" class = "errors"></label> 
								</div>
							
							</div>
						
							<div class = "divreg1">
								Email<span id = "req">*</span> <br><input type = 'text' id = 'txtEmail' /><br>
								<label id = "lblWarnEm" class = "errors"></label>
							</div>
							
							<div class = "divreg1">
								Contact Number<span id = "req">*</span><br>+63 <input type = 'text' id = 'txtCN' value = ''/><br>
								<label id = "lblWarnCN" class = "errors"></label>
							</div>
                            
                            <div id = "divaddress">
                                Address<span id = "req">*</span><br>
                                <input type = 'text' id = 'txtSt' placeholder='Home Address (Street, Sitio etc.)' /><br><br>
                                <select id = 'selProv'><option>SELECT PROVINCE</option>
                                <?php
                                	$province = $customer -> get_province();
                                	foreach ($province as $p) {
                                		echo "<option>$p</option>";
                                	}

                                ?>
                                </select>
                                <label id = 'loadingTown' style = 'display: none;'>Loading Town/City...</label>
                                <select id = 'selTown'><option>SELECT TOWN/CITY</option></select>
                                <label id = 'loadingBrgy' style = 'display: none;'>Loading Brgy...</label>
                                <select id = 'selBrgy'><option>SELECT BARANGAY</option></select><br>
                                <label id = 'lblWarnAddress' class = "errors"></label>
							</div>
                            
							
							
						
						</fieldset>
						</div>
						
						<div id = "divreg3">
							<fieldset id = "f2">
							<legend>Log in Information</legend>
							<div id = "divreg1" class = "divreg1">
								Password<span id = "req">*</span><br><input type = 'password' id = 'txtPassword' /> <span id = "spw"> <br>(8 characters and Alphanumeric)</span> <br>
								<label id = "lblWarnPw" class = "errors"></label> 
							</div>
							<div id = "divreg1" class = "divreg1">
								Confirm Password<span id = "req">*</span><br> <input type = 'password' id = 'txtCP' /> <br>
								<label id = "lblWarnCP" class = "errors"></label>
							</div>
							</fieldset>
						</div>
						
						
						<div class="g-recaptcha" data-sitekey="6Ld1TQwUAAAAAAt_lMdhQGN0rAjIzj63StmoTW-g"></div>
						
						<div id = "Reg">
							<label id = "req">* Required Fields</label></br>
							<input id = 'btnRegister1' type = "button" value = "SUBMIT"/>
						</div>				
					</div>
		</div>
	</div>

	  <?php include '../footer2.php';?>
	
</body>
</html>