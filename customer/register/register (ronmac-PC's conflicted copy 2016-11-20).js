$(document).ready(function(){


	$('#txtPassword').bind("cut copy paste",function(e) {
	    e.preventDefault();
	});

	$('#txtCP').bind("cut copy paste",function(e) {
	    e.preventDefault();
	});

	$("#txtCP").prop( "disabled", true );
	
	$('#txtPassword').on("input propertychange", function(){
  	if($('#txtPassword').val().length >= 8){
  		$("#txtCP").prop("disabled", false);
		$('#txtCP').css("background-color","gray");
  	}
	else{
		$("#txtCP").prop("disabled", true);
		$('#txtCP').css("background-color","white");
	}
	});


	/*var set_prov = [];
    for(var i = 0; i < set_address.length; i++){
    	if(jQuery.inArray(set_address[i][0], set_prov) == -1){
    		set_prov.push(set_address[i][0]);
    	}
    }
    var opt_prov = "";
    for(var x = 0; x < set_prov.length; x++){
        opt_prov += "<option>"+set_prov[x]+"</option>"
    }
    $("#selProv").append(opt_prov);

    $("#selProv").on('change',function(){
	    $('#selTown').empty().append('<option>SELECT TOWN/CITY</option>');
	    $('#selBrgy').empty().append('<option>SELECT BARANGAY</option>');
	    var prov = $('#selProv :selected').text();
	    var set_town = [];
	    for(var i = 0; i < set_address.length; i++){
	    	if(jQuery.inArray(set_address[i][1], set_town) == -1 && prov == set_address[i][0]){
	    		set_town.push(set_address[i][1]);
	    	}
	    }
	    var opt_town = "";
	    for(var x = 0; x < set_town.length; x++){
	        opt_town += "<option>"+set_town[x]+"</option>"
	    }
	    $("#selTown").append(opt_town);
	  });

	  $("#selTown").on('change', function(){
	    $('#selBrgy').empty().append('<option>SELECT BARANGAY</option>');
	    var town = $('#selTown :selected').text();
	    var set_brgy = [];
	    for(var i = 0; i < set_address.length; i++){
	    	if(jQuery.inArray(set_address[i][2], set_brgy) == -1 && town == set_address[i][1]){
	    		set_brgy.push(set_address[i][2]);
	    	}
	    }
	    var opt_brgy = "";
	    for(var x = 0; x < set_brgy.length; x++){
	        opt_brgy += "<option>"+set_brgy[x]+"</option>"
	    }
	    $("#selBrgy").append(opt_brgy);
	  });

	  $('#txtPassword').keyup(function(){
	  	if($('#txtPassword').val().length >= 8){
	  		$("#txtCP").prop("disabled", false);
	  	}else{
	  		$("#txtCP").prop("disabled", true);
	  	}
	  });*/

	$("#selProv").on('change', function(){
		refresh_town();
		refresh_brgy();
	}); 

	$("#selTown").on('change', function(){
		refresh_brgy();
	}); 

	function refresh_town(){
		$("#loadingTown").css("display", "inline");
		$("#selTown").css("display", "none");
		$("#selTown").html("").append("<option>SELECT TOWN/CITY</option>");
		if($("#selProv :selected").html() != "SELECT PROVINCE"){
			$.post("register.php",{refresh:"city", place: $("#selProv :selected").html()}, function(data){
				$("#selTown").append(data);
				$("#loadingTown").css("display", "none");
				$("#selTown").css("display", "inline");
			});
		}else{
			$("#loadingTown").css("display", "none");
			$("#selTown").css("display", "inline");			
		}
	}	

	function refresh_brgy(){
		$("#loadingBrgy").css("display", "inline");
		$("#selBrgy").css("display", "none");
		$("#selBrgy").html("").append("<option>SELECT BARANGAY</option>");
		
		if($("#selTown :selected").html() != "SELECT TOWN/CITY"){
			$.post("register.php",{refresh:"brgy", place: $("#selTown :selected").html()}, function(data){
				$("#selBrgy").append(data);
				$("#loadingBrgy").css("display", "none");
				$("#selBrgy").css("display", "inline");
			});	
		}else{
			$("#loadingBrgy").css("display", "none");
			$("#selBrgy").css("display", "inline");		
		}
	} 

	$("#btnRegister1").click(function(){
	    var ctr = 0;
	    var captcha = $("#g-recaptcha-response").val();
	    $("label#lblWarnFN").html("");
	    $("label#lblWarnLN").html("");
	    $("label#lblWarnEm").html("");
	    $("label#lblWarnPw").html("");
	    $("label#lblWarnCP").html("");
	    $("label#lblWarnCN").html("");
	    $("#lblWarnAddress").html("");

		if ($('#txtFN').val() == "") {
	 		$("label#lblWarnFN").html("(This is a Required Field)");
			$('#txtFN').css("border","1px dashed red");
			$('#txtFN').css("background-color","#ffc0cb");
	  	} else {
	      $("label#lblWarnFN").html("");
		  $('#txtFN').css("border","1px solid black");
		  $('#txtFN').css("background-color","white");
	      ctr += 1;
	  	}
	    if ($('#txtLN').val() == "") {
			  $("label#lblWarnLN").html("(This is a Required Field)");
			  $('#txtLN').css("border","1px dashed red");
			  $('#txtLN').css("background-color","#ffc0cb");
			} else {
			  $("label#lblWarnLN").html("");
			  $('#txtLN').css("border","1px solid black");
			  $('#txtLN').css("background-color","white");
			  ctr += 1;
	    }
	    if ($('#txtEmail').val() == "") {
			  $("label#lblWarnEm").html("(This is a Required Field)");
			  $('#txtEmail').css("border","1px dashed red");
			  $('#txtEmail').css("background-color","#ffc0cb");
			} else if ($('#txtEmail').val() != ""){
			  if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#txtEmail').val())){
				$("label#lblWarnEm").html("(Invalid Email)");
				$('#txtEmail').css("border","1px dashed red");
			    $('#txtEmail').css("background-color","#ffc0cb");
			  }else{
				$("label#lblWarnEm").html("");
				$('#txtEmail').css("border","1px solid black");
			    $('#txtEmail').css("background-color","white");
				ctr += 1;
			  }
	    }
	    if ($('#txtPassword').val() == "") {
			  $("label#lblWarnPw").html("(This is a Required Field)");
			  $('#txtPassword').css("border","1px dashed red");
			  $('#txtPassword').css("background-color","#ffc0cb");
			}
			else if ($('#txtPassword').val().length < 8){
			  $("label#lblWarnPw").html("(Password must be 8 or more characters long..)");
			}
			else if($('#txtPassword').val().match(/[^0-9a-z]/i)){
				$("label#lblWarnPw").html("(Only letters and digits allowed)");
				$('#txtPassword').css("border","1px dashed red");
				$('#txtPassword').css("background-color","#ffc0cb");
			}
			else if(!$('#txtPassword').val().match(/\d/)){
				$("label#lblWarnPw").html("(At least one digit required)");
				$('#txtPassword').css("border","1px dashed red");
				$('#txtPassword').css("background-color","#ffc0cb");
			}
				
			else if(!$('#txtPassword').val().match(/[a-z]/i)){
				$("label#lblWarnPw").html("(At least one letter required)");
				$('#txtPassword').css("border","1px dashed red");
				$('#txtPassword').css("background-color","#ffc0cb");
			}
				
			else {
				$("label#lblWarnPw").html("");
				$('#txtPassword').css("border","1px solid black");
				$('#txtPassword').css("background-color","white");
				ctr += 1;
			}
			
	    if ($('#txtCP').val() == "") {
	      $("label#lblWarnCP").html("(This is a Required Field)");
		  $('#txtCP').css("border","1px dashed red");
		  $('#txtCP').css("background-color","#ffc0cb");
	    } else if($('#txtCP').val() != $('#txtPassword').val()){
	      $("label#lblWarnCP").html("(Mismatched Password)");
		  $('#txtCP').css("border","1px dashed red");
		  $('#txtCP').css("background-color","#ffc0cb");
	    }else {
	      $("label#lblWarnCP").html("");
		  $('#txtCP').css("border","1px solid black");
		  $('#txtCP').css("background-color","white");
	      ctr += 1;
	    }
	    if ($('#txtCN').val() == "") {
	      $("label#lblWarnCN").html("(This is a Required Field)");
		  $('#txtCN').css("border","1px dashed red");
		  $('#txtCN').css("background-color","#ffc0cb");
	    } else {
	      if($('#txtCN').val.length == 10){
	        $("label#lblWarnCN").html("(Invalid Cellphone Number)");
			$('#txtCN').css("border","1px dashed red");
            $('#txtCN').css("background-color","#ffc0cb");
	      }else{
	        $("label#lblWarnCN").html("");
			$('#txtCN').css("border","1px solid black");
		    $('#txtCN').css("background-color","white");
	        ctr += 1;
	      }
	    }
        
        if ($("#txtSt").val() == "" || $("#selProv").val() == "SELECT PROVINCE" || $("#selTown").val() == "SELECT TOWN/CITY" || $("#selBrgy").val() == "SELECT BARANGAY"){
            $("#lblWarnAddress").html("Please Complete the Address");
        }
        else{
            ctr += 1;
            $("#lblWarnAddress").html("");
        }
        //street
	    if($("#txtSt").val() == ""){
            $('#txtSt').css("border","1px dashed red");
            $('#txtSt').css("background-color","#ffc0cb");	
	    }else{
            $('#txtSt').css("border","1px solid black");
		    $('#txtSt').css("background-color","white");
	    }
        //prov
        if ($("#selProv").val() == "SELECT PROVINCE"){
            $('#selProv').css("border","1px dashed red");
            $('#selProv').css("background-color","#ffc0cb");
        }
        else{
            $('#selProv').css("border","1px solid black");
		    $('#selProv').css("background-color","white");
	    }
        //town
        if ($("#selTown").val() == "SELECT TOWN/CITY"){
            $('#selTown').css("border","1px dashed red");
            $('#selTown').css("background-color","#ffc0cb");
        }
        else{
            $('#selTown').css("border","1px solid black");
		    $('#selTown').css("background-color","white");
	    }
        //brgy
        if ($("#selBrgy").val() == "SELECT BARANGAY" ){
            $('#selBrgy').css("border","1px dashed red");
            $('#selBrgy').css("background-color","#ffc0cb");
        }
        else{
            $('#selBrgy').css("border","1px solid black");
		    $('#selBrgy').css("background-color","white");
	    }

	  	if (ctr == 7) {
	  		$.post("../register/register.php",{txtFN:$.trim($('#txtFN').val()), txtLN:$.trim($('#txtLN').val()), txtEm:$.trim($('#txtEmail').val()), 
	  			txtPassword:$.trim($('#txtPassword').val()), txtCN:$.trim($('#txtCN').val()), txtSt:$.trim($('#txtSt').val()), selBrgy:$.trim($('#selBrgy').val()), selTown:$.trim($('#selTown').val()), 
	  			selProv:$.trim($('#selProv').val()), captcha:captcha},
	  		function(data){
		        if(data == "Registered"){
		        	alert("Your are now Successfully Registered!");
		        	window.location.replace('../acc_em_auth');
		        }else if(data == "Failed"){
		        	alert(data);
		        }else if(data == "Invalid captcha"){
		        	grecaptcha.reset();
		        }
		        alert(data);
	  		});
	  	}
	});
	
$('#txtSt').on("input propertychange", function(){
  	$('#txtSt').val(titleCase($('#txtSt').val()));
  });

  $('#txtFN').on("input propertychange", function(){
  	$('#txtFN').val(titleCase(removeDigits($('#txtFN').val())));
  });

  $('#txtLN').on("input propertychange", function(){
  	$('#txtLN').val(titleCase(removeDigits($('#txtLN').val())));
  });

  $('#txtCN').on("input propertychange", function(){
  	$('#txtCN').val(removeNonDigits($('#txtCN').val()));
  	$('#txtCN').val(cpChecker($('#txtCN').val()));
  });

 $('#imgsearch').click(function(){
    if($("#txt-search").val() !== ""){
      window.location.href = "../search/?search="+$("#txt-search").val();
    }
  });
    $("#txt-search").bind('keypress',function(e){
	if ($("#txt-search").val()!= "")
	  if(e.which==13){
		  	window.location.href = "../search/?search="+$("#txt-search").val();
		
	  }	
  });
  
  $(document).on("mouseover", "#btnRegister1", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","white");
		$(this).css("color","black")
		$(this).css("font-weight","bold");
	});
	
	$(document).on("mouseout", "#btnRegister1", function() {
		$(this).fadeIn(300);
		$(this).css("background-color","#222d32");
		$(this).css("color","white");
		$(this).css("font-weight","bold");
	});

});



