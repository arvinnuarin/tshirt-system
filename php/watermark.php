<?php
require_once('mixlarts.php');
$ma = new MixlArts();

if(isset($_GET['shirt']) && isset($_GET['design'])){
	$shirt = $_GET['shirt'];
	$design = $_GET['design'];
	if(!empty($shirt) && !empty($design)){
		$shirt_id = $ma -> get_shirt_pic($shirt);
		$design_id = $ma -> get_design_pic($design);

		$shirt_img = '../'.$shirt_id[0]['pic'];
		$design_img = '../'.$design_id[0]['design'];
		//echo $shirt_img;
		$stamp = imagecreatefrompng($design_img);

		$im = imagecreatefrompng($shirt_img);

		$marge_right = 0;
		$marge_bottom = 0;
		$sx = imagesx($stamp);
		$sy = imagesy($stamp);


		imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

		header('Content-type: image/png');
		imagepng($im);
		imagedestroy($im);
	}
}

?>