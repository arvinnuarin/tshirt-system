<?php
	require_once("db.php");

	class Customer{
		private $db;

		function __construct(){
			$this -> db = new DB();
		}

		function register($fn, $ln, $em, $pw, $cn, $status, $email_ver, $st, $brgy, $town, $prov){
			
			$type = "mixl";
			if($this -> db -> prep_query("INSERT into customer (fname, lname, email, password, status, type, email_ver) VALUES(?, ?, ?, ?, ? ,?, ?)", array($fn, $ln, $em, $pw, $status, $type, $email_ver), "sssssss")){
				$customer_id = $this -> db -> getLastId();
				return $this -> db -> prep_query("insert into address_book (fname, lname, contact_number, home_add, brgy, town, province, customer_id, status) values(?, ?, ?, ?, ?, ?, ?, ?, ?)", array($fn, $ln, $cn, $st, $brgy, $town, $prov, $customer_id, 'PRIMARY'), "sssssssis" );
			}
		}

		function is_email_exist($email){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select email from customer where email = ?", array($email), "s"));
		}

		function log_in($email, $password){
			$data = $this -> db -> prep_query("SELECT customer_id, concat(fname, ' ', lname) as uname, password, status FROM customer where email = ? and password <> ?", array($email, ""), "ss");

			if (!$data){
				return array("Email Doesn't Exist");
			}else{
				foreach($data as $d){
					if($d['password'] == md5($password)){
						if($d['status'] == "Unverified"){
							return array("Account not already Verified!", $d['customer_id'], $email);
						}else if($d['status'] == "Email Send"){
							return array("Verification Email Already Sent!", $d['customer_id'], $email);
						}else{
							return array("Success", $d['customer_id'], $d['uname'], $email);
						}
					}
					else{
						return array("Incorrect Password");
					}
				}
			}
		}

		function get_log_in_credential($id){
			return $this -> db -> prep_query("SELECT customer_id, concat(fname, ' ', lname) as uname, email FROM customer where customer_id = ?", array($id), "i");
		}

		function sub_log_in($id, $code){
			return $this -> db -> prep_query("SELECT customer_id, concat(fname, ' ', lname) as uname, email, customer_id FROM customer where customer_id = ? and email_ver = ?", array($id, $code), "is");
		}
		/*
		function get_customer_stat($id, $status){
			return $this -> db -> prep_query("SELECT status from customer where status = ? and customer_id = ?", array($status, $id), "si");
		}

		function update_email($id, $email){
			return $this -> db -> prep_query("UPDATE customer set email = ? where customer_id = ?", array($email, $id), "si");
		}

		function update_ver_email_code($id, $email_ver_code){
			return $this -> db -> prep_query("UPDATE customer set email_ver = ? where customer_id = ?", array($email_ver_code, $id), "si");
		}

		function update_customer_status($id, $status){
			return $this -> db -> prep_query("UPDATE customer set status = ? where customer_id = ?", array($status, $id), "si");
		}

		function is_email_code_exist($id, $code){
			return $this -> db -> prep_query("SELECT email_ver FROM customer where customer_id = ? and email_ver = ?", array($id, $code), "is");
		}

		function ver_em($ver_id, $ver_email){
			return $this -> db -> prep_query("UPDATE customer SET status = ? WHERE customer_id = ? and email_ver = ?", array('Email Verified', $ver_id, $ver_email), "sss");
		}

		function get_cp($id){
			return $this -> db -> prep_query("SELECT contact_no from customer where customer_id = ?", array($id), "i");
		}

		function send_code($id){
			$con_ver = substr(md5(uniqid(rand(), true)), rand(0, 26), 6);
			return $this -> db -> prep_query("UPDATE customer SET contact_no_ver = ? WHERE customer_id = ?", array($con_ver, $id), "si");
		}

		function get_cp_code($id){
			return $this -> db -> prep_query("SELECT contact_no_ver FROM customer where customer_id = ?", array($id), "i");
		}
		*/


		/*function get_4_discounted(){
			return $this -> db -> prep_query("select product.product_id, product.name, product.discount_percent, product.price, product_pic.front_side from product left outer join product_stocks on product_stocks.product_id = product.product_id left outer join product_pic on product_pic.product_id = product.product_id where product.discount_percent > 0 order by product.discount_percent DESC limit 0, 4");
		}

		function get_4_popular(){
			return $this -> db -> prep_query("select product.product_id, product.name, (COALESCE(products_sold.xxs,0) + COALESCE(products_sold.xs,0) + COALESCE(products_sold.small,0) + COALESCE(products_sold.medium,0) + COALESCE(products_sold.large,0) + COALESCE(products_sold.xl,0) + COALESCE(products_sold.xxl,0)) as total_sales, product.price, product_pic.front_side from product left outer join products_sold on products_sold.product_id = product.product_id left outer join product_pic on product_pic.product_id = product.product_id ORDER by total_sales desc limit 0, 4
");
		}*/

		/*function send_code($email){
			$con_ver = substr(md5(uniqid(rand(), true)), rand(0, 26), 6);
			return $this -> db -> prep_query("UPDATE customer SET contact_no_ver = ? WHERE email = ?", array($con_ver, $email), "si");
		}*/


		function get_cp($id){
			return $this -> db -> prep_query("SELECT contact_number from address_book where customer_id = ? limit 1", array($id), "i");
		}

		function get_brand_new(){
			$new_prod_logs = $this -> db -> prep_query("select entity_id from employee_logs where tbl_name = ? ORDER by time desc ", array("product_design"), "s");
			$arr_prod = array();
			foreach ($new_prod_logs as $npl) {
				$prod = $this -> db -> prep_query("select product_design.pd_id, product_design.name, product_design_template.design from product_design LEFT outer join product on product_design.pd_id = product.pd_id left outer join product_design_template on product_design.pd_id = product_design_template.product_id where product_design_template.side = ? and product_design.pd_id = ?", array("Preview", $npl['entity_id']), "si");
				if($prod){
					array_push($arr_prod, $prod);
				}
			}
			return $arr_prod;
			//return $this -> db -> prep_query("select product_design.pd_id, product_design.name, product_design_template.design from product_design LEFT outer join product on product_design.pd_id = product.pd_id left outer join product_design_template on product_design.pd_id = product_design_template.product_id where product_design_template.side = ?", array("Preview"), "s");
		}

		function get_home_banner(){
			return $this -> db -> prep_query("select img_banner from home_banner");
		}

		function get_scategory(){
			return $this -> db -> prep_query("select category_id, sub_cat_name, cat_name from category");
		}

		function get_one_scategory($category_id){
			return $this -> db -> prep_query("select * from category where sub_cat_name = ?", array($category_id), "s");
		}

		function get_preview_product_by_category($category_id, $start, $per_page){
			return $this -> db -> prep_query("select distinct category.sub_cat_name, product_design.description, product_design.pd_id as product_design_id, product_design.name, (clothing_size_desc.price + product_design_print.default_price) as price, concat(clothing_brand.brand_name, '/', clothing_type.type, '/', clothing_fabric.name, '/', clothing_color.hex) as clothing from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id inner join category on category.category_id = product_design.category_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ? and category.sub_cat_name = ? GROUP by product_design.name limit ?, ?", array(4, 0, "Available", $category_id, $start, $per_page), "iissii");
		}


		function get_preview_product_by_category_count($category_id){
			return $this -> db -> prep_query("select count(distinct product_design.name) as count from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id inner join category on category.category_id = product_design.category_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ? and category.sub_cat_name = ? GROUP by product_design.name ", array(4, 0, "Available", $category_id), "iiss");
		}

		function get_preview_product_by_newest($start, $limit){
			return $this -> db -> prep_query("select distinct product_design.pd_id, product_design.name, product_design.description, (clothing_size_desc.price + product_design_print.default_price) as price, concat(clothing_brand.brand_name, '/', clothing_type.type, '/', clothing_fabric.name, '/', clothing_color.hex) as clothing from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join employee_logs on employee_logs.entity_id = product.pd_id where clothing_size_desc.size = ? and clothing_stock.stock > ? and employee_logs.tbl_name = ? and print_method.availability = ? and employee_logs.description LIKE CONCAT('%',?, '%') GROUP by product_design.name ORDER BY TIME desc limit ?, ?", array(4, 0, "product_design", "Available", "Add Clothing Design", $start, $limit), "iisssii");
		}

		function get_preview_product_by_newest_count(){
			return $this -> db -> prep_query("select count(distinct product_design.pd_id) as count from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join employee_logs on employee_logs.entity_id = product.pd_id where clothing_size_desc.size = ? and clothing_stock.stock > ? and employee_logs.tbl_name = ? and print_method.availability = ? and employee_logs.description LIKE CONCAT('%',?, '%') GROUP by product_design.name ", array(4, 0, "product_design", "Available", "Add Clothing Design"), "iisss");
		}		

		function get_preview_product_by_best_selling($start, $per_page){
			return $this -> db -> prep_query("select distinct product_design.pd_id as product_design_id, product_design.name, product_design.description, (clothing_size_desc.price + product_design_print.default_price) as price, concat(clothing_brand.brand_name, '/', clothing_type.type, '/', clothing_fabric.name, '/', clothing_color.hex) as clothing,  (select sum(quantity) from sub_cart inner join product on product.product_id = sub_cart.product_id where product.pd_id = product_design_id) as quantity from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ?  GROUP by product_design.name ORDER by quantity desc limit ?, ?", array(4, 0, "Available", $start, $per_page), "iisii");
				
		}

		function get_preview_product_by_best_selling_count(){
			return $this -> db -> prep_query("select count(distinct product_design.pd_id) as count from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ?  GROUP by product_design.name", array(4, 0, "Available"), "iis");
				
		}

		function get_clothing_assoc($product_design_id){
			return $this -> db -> prep_query("select distinct clothing_type.ct_id, clothing_type.type from clothing_type inner join clothing on clothing.ct_id = clothing_type.ct_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id  inner join product on product.clothing_color_id = clothing_color.sc_id where product.pd_id = ?", array($product_design_id), "i");
		}

		function is_shirt_exist_in_preview($product_design_id){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select clothing_type.type from product_design LEFT outer join product on product_design.pd_id = product.pd_id left outer join clothing_color on product.clothing_color_id = clothing_color.sc_id left outer join clothing on clothing_color.clothing_id = clothing.clothing_id left OUTER join category on product_design.category_id = category.category_id left outer join clothing_brand on clothing_brand.cb_id = clothing.cb_id left outer join clothing_type on clothing.ct_id = clothing_type.ct_id where clothing_type.type = ? and product_design.pd_id = ?", array("T Shirt", $product_design_id), "si"));
		}

		function product_quantity($product_design_id,  $clothing_id,  $clothing_color_id,$clothing_size_id, $pm_id){
			return $this -> db -> prep_query("select distinct clothing_stock.stock from product_design left outer join  product on product_design.pd_id = product.pd_id inner join clothing_color on product.clothing_color_id = clothing_color.sc_id inner join clothing on clothing_color.clothing_id = clothing.clothing_id  inner join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join clothing_size_desc on clothing_size_desc.ssd_id = clothing_stock.size inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing.ct_id = clothing_type.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id where product_design.pd_id = ? and clothing_color.sc_id = ? and clothing_size_desc.ssd_id = ? and clothing.clothing_id = ? and product_design_print.pm_id = ? and clothing_stock.stock > 0", array($product_design_id, $clothing_color_id, $clothing_size_id, $clothing_id, $pm_id),"iiiii");
		}

		function product_comb_exist($product_design_id,  $clothing_id,  $clothing_color_id,$clothing_size_id, $pm_id){
			return $this -> db -> prep_query("select distinct count(clothing_stock.stock) as count from product_design left outer join  product on product_design.pd_id = product.pd_id inner join clothing_color on product.clothing_color_id = clothing_color.sc_id inner join clothing on clothing_color.clothing_id = clothing.clothing_id  inner join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join clothing_size_desc on clothing_size_desc.ssd_id = clothing_stock.size inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing.ct_id = clothing_type.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id where product_design.pd_id = ? and clothing_color.sc_id = ? and clothing_size_desc.ssd_id = ? and clothing.clothing_id = ? and product_design_print.pm_id = ?", array($product_design_id, $clothing_color_id, $clothing_size_id, $clothing_id, $pm_id),"iiiii");
		}

		function add_to_cart($customer_id, $product_design_id, $clothing_id, $clothing_color_id, $size_id, $quantity, $date, $pm_id, $price){
			if($this -> db -> prep_query("insert into cart (customer_id, date, status) values(?, ?, ?)", array($customer_id, $date, "PENDING"), "iss")){
				return $this -> add_to_sub_cart($this -> db -> getLastId(), $product_design_id, $clothing_id, $clothing_color_id, $size_id, $quantity, $pm_id);
			}
		}

		function add_to_sub_cart($cart_id,  $product_design_id, $clothing_id, $clothing_color_id, $size_id, $quantity, $pm_id, $price){
			return $this -> db -> prep_query("insert into sub_cart (product_id, quantity, cart_id, size_id, pm_id, price) values((select distinct product_id from product inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id where pd_id = ? and clothing_color_id = ? and clothing.clothing_id = ?), ?, ?, ?, ?, ?)", array($product_design_id, $clothing_color_id,  $clothing_id, $quantity, $cart_id, $size_id, $pm_id, $price), "iiiiiiid");
		}

		function add_to_wishlist($product_design_id, $clothing_id, $clothing_color_id, $size_id, $pm_id, $customer_id){
			return $this -> db -> prep_query("insert into wishlist (product_id, size_id, pm_id, customer_id) values((select distinct product_id from product inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id where pd_id = ? and clothing_color_id = ? and clothing.clothing_id = ?), ?, ?, ?)", array($product_design_id, $clothing_color_id,  $clothing_id, $size_id, $pm_id, $customer_id), "iiiiii");
		}


		function get_cart_id($customer_id){
			return $this -> db -> prep_query("select cart_id from cart where customer_id = ? and status=?", array($customer_id,"PENDING"), "is");
		}

		function get_sub_cart($cart_id, $product_design_id, $clothing_id, $clothing_color_id, $size_id, $pm_id){
			return $this -> db -> prep_query("select sub_cart_id, quantity from sub_cart where size_id = ? and cart_id = ? and product_id = (select product.product_id from product inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id  where product.clothing_color_id = ? and clothing.clothing_id = ? and product.pd_id = ?) and sub_cart.pm_id = ?", array($size_id, $cart_id, $clothing_color_id, $clothing_id, $product_design_id, $pm_id), "iiiiii");

			//return $this -> db -> prep_query("select sub_cart_id, quantity from sub_cart where size_id = ? and cart_id = ? and product_id = (select product_id from product inner join product_design_print on product_design_print.pd_id = product.pd_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on clothing.clothing_id = clothing_color.sc_id where product_design_print.pd_id = ? and clothing_color.sc_id = ? and clothing.clothing_id = ? and product_design_print.pm_id = ?)", array($size_id, $cart_id, $product_design_id, $clothing_color_id,  $clothing_id, $pm_id), "iiiiii");
			//return array($size_id, $cart_id, $product_design_id, $clothing_color_id,  $clothing_id, $pm_id);
		}

		function get_wishlist_count($customer_id){
			return $this -> db -> prep_query("select count(wishlist_id) as count from wishlist where customer_id = ?", array($customer_id), "i");
		}

		function get_customer_wishlist(){
			return $this -> db -> prep_query("select wishlist.");
		}

		function update_quantity_cart($sub_cart_id, $quantity){
			return $this -> db -> prep_query("update sub_cart set quantity = ? where sub_cart_id = ?", array($quantity, $sub_cart_id), "ii");
		}

		function cart_id_exist($customer_id, $sub_cart_id){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select sub_cart.sub_cart_id from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id where sub_cart.sub_cart_id = ? and cart.customer_id = ?", array($sub_cart_id, $customer_id), "ii"));
		}

		function delete_sub_cart_from_customer($sub_cart_id){
			return $this -> db -> prep_query("delete from sub_cart where sub_cart_id = ?", array($sub_cart_id), "i");
		}
		

		function deduct_stock($product_id, $quantity){
			if($stock = $this -> db -> prep_query("select clothing_stock.stock, clothing_stock.scs_id from clothing_stock inner join clothing_size_desc on clothing_size_desc.ssd_id = clothing_stock.size inner join product on product.clothing_size_id = clothing_size_desc.ssd_id where product.product_id = ?", array($product_id), "i")){
				$deduction = $stock[0]['stock'] - $quantity;
				return $this -> db -> prep_query("update clothing_stock set stock = ? where scs_id = ?", array($deduction, $stock[0]['scs_id']), "ii");
			}
		}

		function get_order_history($customer_id){
			return $this -> db -> prep_query("select * from order_history where customer_id = ? order by date desc", array( $customer_id), "i");			
		}

		function get_one_order_history($customer_id, $order_history_id){
			return $this -> db -> prep_query("select * from order_history where customer_id = ? and order_history_id = ?", array( $customer_id, $order_history_id), "ii");	
		}

		function get_sub_order_history($customer_id, $order_history_id){
			return $this -> db -> prep_query("select distinct print_method.name as print_method, product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing_color.color, clothing_color.hex, clothing_size_desc.size, sub_order_history.quantity, sub_order_history.price from sub_order_history inner join product on product.product_id = sub_order_history.product_id inner join product_design on product_design.pd_id = product.pd_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_order_history.size_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join order_history on order_history.order_history_id = sub_order_history.order_history_id inner join clothing on clothing.clothing_id = clothing_size_desc.clothing_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join customer on customer.customer_id = order_history.customer_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id where order_history.order_history_id = ? and customer.customer_id = ?", array($order_history_id, $customer_id), "ii");
		}

		function search_product_design($search, $start, $per_page){
			return $this -> db -> prep_query("select distinct product_design.description, product_design.pd_id as product_design_id, product_design.name, (clothing_size_desc.price + product_design_print.default_price) as price, concat(clothing_brand.brand_name, '/', clothing_type.type, '/', clothing_fabric.name, '/', clothing_color.hex) as clothing from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ? and product_design.name like concat('%', ?, '%') GROUP by product_design.name limit ?, ?", array(4, 0, "Available", $search, $start, $per_page), "iissii");
		}

		function search_product_design_count($search){
			return $this -> db -> prep_query("select count(distinct product_design.name) as count from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ? and product_design.name like concat('%', ?, '%') GROUP by product_design.name ", array(4, 0, "Available", $search), "iiss");
		}

		function view_product_design_template($id){
			return $this -> db -> prep_query("select * from product_design_template where product_design_template.product_id = ? and side <> ?", array($id, "Preview"), "is");
		}

		function get_address_book($customer_id){
			return $this -> db -> prep_query("select ab_id, concat(fname, ' ' ,lname) as uname, contact_number, concat(home_add,' ', brgy, ' ', town, ' ', province) as uaddress, status from address_book where customer_id = ?", array($customer_id), "i");
		}

		function is_address_book_exist($customer_id, $ab_id){
			return $this -> db -> prep_query("select count(ab_id) as count from address_book where ab_id = ? and customer_id = ?", array($ab_id, $customer_id), "ii");
		}

		function update_address_book_stat($customer_id, $ab_id){
			if($this -> db -> prep_query("update address_book set status = '0' where customer_id = ?", array($customer_id), "i"))
				return $this -> db -> prep_query("update address_book set status = '1' where ab_id = ?", array($ab_id), "i");
			else
				return false;
		}

		function update_address_book_stat2($ab_id){
			return $this -> db -> prep_query("update address_book set status = '1' where ab_id = ?", array($ab_id), "i");
		}

		function address_book_count($customer_id){
			return $this -> db -> prep_query("select count(ab_id) as count from address_book where customer_id = ?", array($customer_id), "i");
		}

		function add_address_book($fn, $ln , $cn, $ha, $brgy, $town, $prov, $customer_id){
			if($this -> db -> prep_query("INSERT INTO address_book(fname, lname, contact_number, home_add, brgy, town, province, customer_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", array($fn, $ln , $cn, $ha, $brgy, $town, $prov, $customer_id), "sssssssi")){	
				return $this -> db -> getLastId();
			}else
				return false;
		}

		function is_address_exist($customer_id, $home_add, $brgy, $town, $prov){
			return $this -> db -> prep_query("select ab_id from address_book where customer_id = ? and home_add = ? and brgy = ? and town = ? and province = ?", array($customer_id, $home_add, $brgy, $town, $prov), "issss");
		}

		function delete_address($customer_id, $ab_id){
			return $this -> db -> prep_query("delete from address_book where customer_id = ? and ab_id = ?", array($customer_id, $ab_id), "ii");
		}
		
		function get_product_by_pm($pd_id){
			return $this -> db -> prep_query("select distinct print_method.name, print_method.pm_id, product_design_print.default_price, product_design_print.price_increment from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_stock.stock > ? and print_method.availability = ? and product.pd_id = ? GROUP by print_method.name", array( 0, "Available", $pd_id), "isi");
		}

		function get_product_by_clothing($pd_id){
			return $this -> db -> prep_query("select distinct clothing.clothing_id, concat(clothing_brand.brand_name, '(', clothing_type.type, ' ', clothing_fabric.name, ')') as clothing from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id where clothing_stock.stock > ? and print_method.availability = ? and product.pd_id = ? GROUP by clothing", array(0, "Available", $pd_id), "isi");
		}

		function get_product_by_color($pd_id, $clothing_id){
			return $this -> db -> prep_query("select distinct  concat(clothing_brand.brand_name, '/', clothing_type.type, '/', clothing_fabric.name, '/', clothing_color.hex) as dir, clothing_color.sc_id, clothing_color.color from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_stock.stock > ? and print_method.availability = ? and product.pd_id = ? and clothing.clothing_id = ?  GROUP by clothing_color.color", array(0, "Available", $pd_id, $clothing_id), "isii");
		}

		function get_product_by_size($pd_id, $color_id){
			return $this -> db -> prep_query("select distinct sub_cart.quantity, clothing_size_desc.size, if(clothing_size_desc.size >= 0 and clothing_size_desc.size <= 4, (select price from clothing_size_desc where size = 4 and clothing_id = clothing.clothing_id and ssd_id = clothing_size_desc.ssd_id group by ssd_id), clothing_size_desc.price) as price , clothing_stock.stock, clothing_size_desc.ssd_id, clothing_size_desc.size from clothing inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id inner join clothing_stock on clothing_stock.size = clothing_size_desc.ssd_id and clothing_stock.sc_id = clothing_color.sc_id left outer join sub_cart on sub_cart.product_id = product.product_id and sub_cart.pm_id = print_method.pm_id and sub_cart.size_id = clothing_size_desc.ssd_id where clothing_stock.stock > ? and print_method.availability = ? and product.pd_id = ? and clothing_color.sc_id = ?  group by clothing_size_desc.size", array( 0, "Available", $pd_id, $color_id), "isii");
		}

		function get_simple_product_data($pd_id){
			return $this -> db -> prep_query("select pd_id, name, description from product_design where name = ?", array($pd_id), "s");
		}


		function get_cart($customer_id){
			return $this -> db -> prep_query("select distinct cart.cart_id, sub_cart.sub_cart_id, product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing_color.color, clothing_color.hex, clothing_size_desc.size, clothing_size_desc.ssd_id, sub_cart.quantity, print_method.name as print_method, sub_cart.price, sub_cart.draw_location from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id inner join product on product.product_id = sub_cart.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_cart.size_id inner join print_method on print_method.pm_id = sub_cart.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id where cart.customer_id = ? AND cart.status='PENDING'", array($customer_id), "i");
		}

		function get_wishlist($customer_id){
			return $this -> db -> prep_query("select distinct sub_cart.quantity,  wishlist.wishlist_id, clothing_stock.stock, product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing_color.color, clothing_color.hex, clothing_size_desc.size, clothing_size_desc.ssd_id, print_method.name as print_method, 
				if(clothing_size_desc.size >= 0 and clothing_size_desc.size <= 4, 
					(product_design_print.default_price + 
						(select price from clothing_size_desc where size = 4 and clothing_id = clothing.clothing_id and ssd_id = clothing_size_desc.ssd_id
						)
					)
				, 
				(clothing_size_desc.price + (product_design_print.default_price+
				(
					(
						(clothing_size_desc.size+1)*product_design_print.price_increment
					)
				)
				)))
					 as price from wishlist inner join product on product.product_id = wishlist.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = wishlist.size_id inner join print_method on print_method.pm_id = wishlist.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id  left outer join sub_cart on sub_cart.product_id = product.product_id and sub_cart.pm_id = print_method.pm_id and sub_cart.size_id = clothing_size_desc.ssd_id where wishlist.customer_id = ? group by wishlist.wishlist_id", array($customer_id), "i");
		}

		function checkout($customer_id, $date){
			if($cart = $this -> db -> prep_query("select distinct sub_cart.cart_id, clothing_stock.stock, clothing_size_desc.ssd_id, clothing_color.sc_id, sub_cart.quantity, sub_cart.size_id, sub_cart.pm_id, sub_cart.product_id, sub_cart.cart_id,  clothing_size_desc.size,if(clothing_size_desc.size >= 0 and clothing_size_desc.size <= 4, (product_design_print.default_price + (select price from clothing_size_desc where size = 4 and clothing_id = clothing.clothing_id and ssd_id = clothing_size_desc.ssd_id)), (clothing_size_desc.price + (product_design_print.default_price+(((clothing_size_desc.size+1)*product_design_print.price_increment)))))as price from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id inner join product on product.product_id = sub_cart.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_cart.size_id inner join print_method on print_method.pm_id = sub_cart.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join clothing_stock on clothing_stock.sc_id=clothing_color.sc_id and clothing_stock.size = clothing_size_desc.ssd_id where cart.customer_id = ?", array($customer_id), "i")){
				$sum_total_price = 0;
				$vat_price = 0;
				foreach ($cart as $c) {
					$total_price = $c['price'] * $c['quantity'];
					$sum_total_price += $total_price;
				}
				$vat = $sum_total_price * 0.12;
				$vat_price = $sum_total_price + $vat;
				if($this -> db -> prep_query("insert into order_history (customer_id, date, total_sales, net, vatable_sales) values(?, ?, ?, ?, ?)", array($customer_id, $date, $sum_total_price, $vat, $vat_price), "isddd")){
					$id = $this -> db -> getLastId();
					$sql = "";
					$sql_deduct_stock = "";
					foreach ($cart as $c) {
						$sql .= "insert into sub_order_history (product_id, price, quantity, size_id, pm_id, order_history_id) values (".$c['product_id'].",".$c['price'].",". $c['quantity'].",". $c['size_id'].",". $c['pm_id'].",". $id.");";
						$sql .= "UPDATE clothing_stock SET stock = (".$c['stock']."-".$c['quantity'].") WHERE sc_id = ".$c['sc_id']." and size = ".$c['ssd_id'].";";
					}
					$sql .= "delete from sub_cart where cart_id = ".$cart[0]['cart_id'].";";
					$sql .= "delete from cart where cart_id = ".$cart[0]['cart_id'].";"; 
					if($this -> db -> multi_query($sql))
						return "Checkout Success";
				}
			}
			return false;
		}

		function get_size($clothing_id){
			return $this -> db -> prep_query("select size, height, width from clothing_size_desc where clothing_id = ?", array($clothing_id), "i");
		}

		function cart_count($customer_id){
			return $this -> db -> prep_query("select count(sub_cart.sub_cart_id) as count from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id where cart.customer_id = ?", array($customer_id), "i");
		}

		function get_wishlist_info($customer_id, $wishlist_id){
			return $this -> db -> prep_query("select distinct clothing_stock.stock, wishlist.product_id, wishlist.pm_id, wishlist.size_id from wishlist inner join product on product.product_id = wishlist.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = wishlist.size_id inner join print_method on print_method.pm_id = wishlist.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id  where wishlist.customer_id = ? and wishlist.wishlist_id = ? ", array($customer_id, $wishlist_id), "ii");
		}

		function get_sub_cart2($customer_id, $size_id, $pm_id, $product_id, $cart_id){
			return $this -> db -> prep_query("select sub_cart.sub_cart_id, sub_cart.quantity from  sub_cart inner join cart on cart.cart_id = sub_cart.cart_id where sub_cart.size_id = ? and sub_cart.pm_id = ? and sub_cart.product_id = ? and cart.cart_id = ? and cart.customer_id = ?", array($size_id, $pm_id, $product_id, $cart_id, $customer_id), "iiiii");
		}

		function wishlist_to_cart($customer_id,$date, $quantity, $size_id, $product_id, $pm_id){
			if($this -> db -> prep_query("insert into cart (date, customer_id) values(?, ?) ", array($date, $customer_id), "si")){
				return $this -> wishlist_to_sub_cart($quantity, $size_id, $product_id, $pm_id, $this -> db -> getLastId());
			}else
				return "Insert to cart failed";
		}

		function wishlist_to_sub_cart($quantity, $size_id, $product_id, $pm_id, $cart_id){
			return $this -> db -> prep_query("insert into sub_cart(quantity, size_id, pm_id, product_id, cart_id) values(?, ?, ?, ?, ?)", array($quantity, $size_id, $pm_id, $product_id, $cart_id), "iiiii");
		}

		function delete_wishlist($wishlist_id){
			return $this -> db -> prep_query("delete from wishlist where wishlist_id = ?", array($wishlist_id), "i");
		}

		function is_wishlist_exist($product_design_id, $clothing_id, $clothing_color_id, $clothing_size_id, $pm_id, $customer_id){
			return $this -> db -> prep_query("select distinct wishlist_id from product_design left outer join  product on product_design.pd_id = product.pd_id inner join clothing_color on product.clothing_color_id = clothing_color.sc_id inner join clothing on clothing_color.clothing_id = clothing.clothing_id  inner join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id inner join clothing_size_desc on clothing_size_desc.ssd_id = clothing_stock.size inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing.ct_id = clothing_type.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id inner join wishlist on wishlist.product_id = product.product_id and wishlist.pm_id = print_method.pm_id and wishlist.size_id = clothing_size_desc.ssd_id where product_design.pd_id = ? and clothing_color.sc_id = ? and clothing_size_desc.ssd_id = ? and clothing.clothing_id = ? and product_design_print.pm_id = ? and wishlist.customer_id = ?", array($product_design_id, $clothing_color_id, $clothing_size_id, $clothing_id, $pm_id, $customer_id),"iiiiii");
		}
		function get_product_comment($pd_id) {
			return $this -> db -> prep_query("SELECT DATE_FORMAT(com.comment_date, '%b %d %Y %h:%i %p') AS 'comment_date', com.rating, CONCAT(cus.fname, ' ', cus.lname) AS 'cusname', com.comment FROM product_comment com INNER JOIN customer cus ON com.customer_id=cus.customer_id WHERE com.product_id=? ORDER BY com.comment_date DESC",array($pd_id),"i");
		} 

		function insert_comment($rating, $com, $pd_id, $customer_id, $comment_date){

			if($this -> db -> prep_query("INSERT INTO product_comment(customer_id, product_id, comment_date, rating, comment) VALUES(?,?,?,?,?)", array($customer_id, $pd_id, $comment_date, $rating, $com), "iisis")){	

				return 'success';
			}
		}

		function get_province(){
			$arr_address = array_map('str_getcsv', file('../../address.csv'));
			array_walk($arr_address, function(&$a) use ($arr_address) {$a = array_combine($arr_address[0], $a);});
	    	array_shift($arr_address);
	    	$sub_add = array();
	    	foreach ($arr_address as $a) {
	    		if(!in_array($a['province'], $sub_add)){
	    			array_push($sub_add, $a['province']);
	    		}
	    	}
	    	return $sub_add;
		}

		function get_town($province){
			$arr_address = array_map('str_getcsv', file('../../address.csv'));
			array_walk($arr_address, function(&$a) use ($arr_address) {$a = array_combine($arr_address[0], $a);});
	    	array_shift($arr_address);
	    	$sub_add = array();
	    	foreach ($arr_address as $a) {
	    		if(!in_array($a['city'], $sub_add) && $a['province'] == $province){
	    			array_push($sub_add, $a['city']);
	    		}
	    	}
	    	return $sub_add;
		}

		function get_brgy($city){
			$arr_address = array_map('str_getcsv', file('../../address.csv'));
			array_walk($arr_address, function(&$a) use ($arr_address) {$a = array_combine($arr_address[0], $a);});
	    	array_shift($arr_address);
	    	$sub_add = array();
	    	foreach ($arr_address as $a) {
	    		if(!in_array($a['brgy'], $sub_add) && $a['city'] == $city){
	    			array_push($sub_add, $a['brgy']);
	    		}
	    	}
	    	return $sub_add;
		}

		function current_orders($customer_id){
			return $this -> db -> prep_query("select order_history.order_history_id, concat(customer.fname ,' ', customer.lname) as buyer, order_history.date, order_history.total_sales, order_history.status, payment.payment_method, payment.session, shipping.courier, shipping.tracking_no from order_history inner join customer on customer.customer_id = order_history.customer_id INNER JOIN payment ON payment.order_history_id=order_history.order_history_id LEFT JOIN shipping ON shipping.order_history_id=order_history.order_history_id  WHERE order_history.status <> ? order by date desc", array("DELIVERED"), "s");
		}

		function current_sub_orders($order_history_id){
			return $this -> db -> prep_query("select distinct sub_cart.sub_cart_id, product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing_color.color, clothing_color.hex, clothing_size_desc.size, clothing_size_desc.ssd_id, sub_cart.quantity, print_method.name as print_method, sub_cart.price, sub_cart.draw_location from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id inner join product on product.product_id = sub_cart.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_cart.size_id inner join print_method on print_method.pm_id = sub_cart.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join order_history on order_history.cart_id = cart.cart_id where order_history.order_history_id <> ?", array($order_history_id, "DELIVERED"), "is");
		}

		function orders($customer_id, $status){
			if($status === "PREVIOUS"){
				$sign = " = ";
			}else{
				$sign = " <> ";
			}
			return $this -> db -> prep_query("select order_history.order_history_id, order_history.date, order_history.total_sales, order_history.status,  payment.payment_method, shipping.courier, shipping.tracking_no from order_history inner join customer on customer.customer_id = order_history.customer_id INNER JOIN payment ON payment.order_history_id=order_history.order_history_id LEFT JOIN shipping ON shipping.order_history_id=order_history.order_history_id  WHERE order_history.status $sign ? order by date desc", array("DELIVERED"), "s");
		}

		function sub_orders($order_history_id, $status){
			if($status === "PREVIOUS"){
				$sign = " = ";
			}else{
				$sign = " <> ";
			}
			return $this -> db -> prep_query("select distinct sub_cart.sub_cart_id, product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing_color.color, clothing_color.hex, clothing_size_desc.size, clothing_size_desc.ssd_id, sub_cart.quantity, print_method.name as print_method, sub_cart.price, sub_cart.draw_location from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id inner join product on product.product_id = sub_cart.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_cart.size_id inner join print_method on print_method.pm_id = sub_cart.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join order_history on order_history.cart_id = cart.cart_id where order_history.order_history_id = ? and order_history.status $sign ?", array($order_history_id, "DELIVERED"), "is");
		}

		function one_order($order_history_id, $status){
			if($status === "PREVIOUS"){
				$sign = " = ";
			}else{
				$sign = " <> ";
			}
			return $this -> db -> prep_query("select order_history.order_history_id, order_history.date, order_history.total_sales, order_history.status, payment.payment_method, shipping.courier, shipping.tracking_no, address_book.fname, address_book.lname, address_book.home_add, address_book.brgy, address_book.town, address_book.province from order_history inner join customer on customer.customer_id = order_history.customer_id INNER JOIN payment ON payment.order_history_id=order_history.order_history_id inner join address_book on address_book.ab_id = order_history.ab_id LEFT JOIN shipping ON shipping.order_history_id=order_history.order_history_id  WHERE order_history.status $sign ? and order_history.order_history_id = ?", array("DELIVERED", $order_history_id), "si");
		}

		function search_product_design_se($search){
			return $this -> db -> prep_query("select distinct product_design.name, concat(clothing_brand.brand_name, '/', clothing_type.type, '/', clothing_fabric.name, '/', clothing_color.hex) as clothing from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id INNER join clothing_stock on clothing_stock.sc_id = clothing_color.sc_id  where clothing_size_desc.size = ? and clothing_stock.stock > ? and print_method.availability = ? and product_design.name like concat('%', ?, '%') GROUP by product_design.name limit ?, ?", array(4, 0, "Available", $search, 0, 10), "iissii");
		}

		function get_receipt_list($order_history_id){
			return $this -> db -> prep_query("select distinct product_design.name as product_name, clothing_color.color,  clothing_size_desc.size, clothing_fabric.name as fabric, clothing_brand.brand_name as brand, clothing_type.type, clothing_size_desc.ssd_id, sub_cart.quantity, print_method.name as print_method, sub_cart.price
				 from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id inner join product on product.product_id = sub_cart.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_cart.size_id inner join print_method on print_method.pm_id = sub_cart.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join order_history on order_history.cart_id = cart.cart_id where order_history.order_history_id = ?", array($order_history_id), "i");
		}

		function get_receipt_m($order_history_id){
			return $this -> db -> prep_query("select order_history_id, date, total_sales from order_history where order_history_id = ?", array($order_history_id), "i");
		}
	}

?>
