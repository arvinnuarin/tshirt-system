<?php
	//Database config
	define("DB_SERVER", "localhost");
	define("DB_USER", "root");
	define("DB_PASSWORD", "");
	define("DB_NAME", "dbmixlarts");
	define ("SIZE", serialize (array ("XXS", "XS", "S", "M", "L", "XL", "2XL", "3XL", "4XL", "5XL")));


	$dbc = @mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
	//Set timezone
	date_default_timezone_set('Asia/Manila');
?>
