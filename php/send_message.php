<?php
	class SendEmail{
		function __construct(){}
		function send_em($type, $email, $subject, $body){
			require("PHPMailer/class.phpmailer.php");				
			$mailer = new PHPMailer();
			$mailer->IsSMTP();
			$mailer->SMTPAuth = TRUE;
			$mailer->Host = 'smtp.1and1.com:587';
				
			if ($type == "newsletter") {
				$mailer->Username = 'newsletter@mixlarts.com';
				$mailer->From = 'newsletter@mixlarts.com';
				$mailer->FromName = 'MixlArts Newsletter';

			} else if ($type == "orders") {
				$mailer->Username = 'orders@mixlarts.com';
				$mailer->From = 'orders@mixlarts.com';
				$mailer->FromName = 'MixlArts Orders';
			} else if ($type == "support") {
				$mailer->Username = 'support@mixlarts.com';
				$mailer->From = 'support@mixlarts.com';
				$mailer->FromName = 'MixlArts Help Center';
			}else if ($type == "noreply") {
				$mailer->Username = 'no-reply@mixlarts.com';
				$mailer->From = 'no-reply@mixlarts.com';
				$mailer->FromName = 'MixlArts Notification';
			}

			$mailer->Password = 'mixlarts2016';
			$mailer->Body = $body;
			$mailer->Subject = $subject;
			$mailer->AddAddress($email);
			return $mailer->Send();		
		}
	}

	//$s = new SendEmail();
	//echo $s->send_em('orders','johnarvinnuarin@gmail.com','Test','dadada');
?>