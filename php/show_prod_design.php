<?php
	if($_GET['prod_design']){
		require_once('mixlarts.php');
		$ma = new MixlArts();
		$prod_img = $_GET['prod_design'];
		if(!empty($prod_img)){
			$design = $ma -> get_design_pic($prod_img);
			if($design){
				//image without background
				$img = '../'.$design[0]['design'];
				$bg = '../pictures/others/bg_prod_design.png';
				/*$im = imagecreatefrompng($img);

				header('Content-Type: image/png');
				imagealphablending($im, false);
				imagesavealpha($im, true);
				imagepng($im);
				imagedestroy($im);*/

				//echo $img;
				//echo $bg;
				$stamp = imagecreatefrompng($img);

				$im = imagecreatefrompng($bg);

				$marge_right = 0;
				$marge_bottom = 0;
				$sx = imagesx($stamp);
				$sy = imagesy($stamp);


				imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

				header('Content-type: image/png');
				imagepng($im);
				imagedestroy($im);
			}
		}
	}
?>