<?php
	require_once("db.php");
	
	class MixlArts{
		private $db;
		function __construct(){
			$this -> db = new DB();
		}
		function user_address(){
			$arr_address = array_map('str_getcsv', file('../../address.csv'));
			array_walk($arr_address, function(&$a) use ($arr_address) {$a = array_combine($arr_address[0], $a);});
	    	array_shift($arr_address);
	    	$sub_add = array();
	    	$arr_js_add = "<script type = 'text/javascript'>var set_address = [";
	    	foreach ($arr_address as $a) {
	    		$arr_js_add .= "['".$a['province']."','". $a['city']."','". $a['brgy']."'],";
	    		if(!in_array($a['province'], $sub_add)){
	    			array_push($sub_add, $a['province']);
	    		}
	    	}
	    	$arr_js_add = trim($arr_js_add, ",");
	    	$arr_js_add .= "]</script>";
	    	
	    	return $arr_js_add;
		}

		function get_user_stat($id, $status, $user){
			return $this -> db -> prep_query("SELECT status from $user where status = ? and ".$user."_id = ?", array($status, $id), "si");
		}

		function update_email($id, $email, $user){
			return $this -> db -> prep_query("UPDATE $user set email = ? where ".$user."_id = ?", array($email, $id), "si");
		}

		function update_ver_email_code($id, $email_ver_code, $user){
			return $this -> db -> prep_query("UPDATE $user set email_ver = ? where ".$user."_id = ?", array($email_ver_code, $id), "si");
		}

		function update_user_status($id, $status, $user){
			return $this -> db -> prep_query("UPDATE $user set status = ? where ".$user."_id = ?", array($status, $id), "si");
		}

		function is_email_code_exist($id, $code, $user){
			return $this -> db -> prep_query("SELECT email_ver FROM $user where ".$user."_id = ? and email_ver = ?", array($id, $code), "is");
		}

		function ver_em($ver_id, $ver_email, $user){
			return $this -> db -> prep_query("UPDATE $user SET status = ? WHERE ".$user."_id = ? and email_ver = ?", array('Fully Verified', $ver_id, $ver_email), "sss");
		}

		function get_cp($id, $user){
			return $this -> db -> prep_query("SELECT contact_no from $user where ".$user."_id = ?", array($id), "i");
		}


		/*function get_cp_code($id, $user){
			return $this -> db -> prep_query("SELECT contact_no_ver FROM $user where ".$user."_id = ?", array($id), "i");
		}

		function update_cn($id, $cn, $user){
			return $this -> db -> prep_query("UPDATE $user set contact_no = ? where ".$user."_id = ?", array($cn, $id), "si");
		}*/

		function get_email($id, $user){
			return $this -> db -> prep_query("select email from $user where ".$user."_id = ?", array($id), "i");
		}

		function get_shirt_pic($id){
			return $this -> db -> prep_query("select pic from clothing_color_template where sct_id = ?", array($id), "i");
		}

		function get_design_pic($id){
			return $this -> db -> prep_query("select design from product_design_template where pd_template_id = ?", array($id), "i");
		}

	}
?>