<?php
require_once('config.php');
if ( !class_exists( 'DB' ) ) {
	class DB {
		private $db;
		public function __construct() {
			$this -> db = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
		}

		public function getLastId(){
			return mysqli_insert_id($this -> db);
		}

		public function prep_query($query, $data=array(), $format='') {
			if($stmt = $this -> db->prepare($query)){
				if(count($data) > 0 && $format != ""){
					array_unshift($data, $format);
					call_user_func_array(array( $stmt, 'bind_param'), $this->ref_values($data));
				}
				if($stmt->execute()){
					$s_query = explode(' ', $query);
					if(strtoupper($s_query[0]) == "SELECT"){
						$result = $stmt->get_result();
						$results = array();
						while ($row = $result->fetch_assoc()) {
							$results[] = $row;
						}
						return $results;
					}else{
						return true;
					}
				$stmt -> close();	
				} else{ echo $this -> db->error; }
			} else{ echo $this -> db->error; }
			
			
			return false;
		}

		public function prep_num_rows($result_set){
			 return count($result_set);
		}

		private function ref_values($array) {
			$refs = array();
			foreach ($array as $key => $value) {
				$refs[$key] = &$array[$key]; 
			}
			return $refs; 
		}
	}
}
/*$db = new DB();
$x = $db -> prep_query("insert into dbmixlarts2.customer (email) values('HEY');insert into dbmixlarts2.EMPLOYEE (fname) values('HEY');");
print_r($x);*/