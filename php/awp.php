<?php
	require_once("db.php");
	require_once("config.php");

	class Awp{
		private $db;
		private $arr_const_size;
		function __construct(){
			$this -> db = new DB();
			$this -> arr_const_size = unserialize (SIZE);
		}
		
		function log_in($email, $password){
			$data = $this -> db -> prep_query("SELECT employee_id, concat(fname, ' ', lname) as uname, password, status, position FROM employee where email = ?", array($email), "s");

			if ($this -> db -> prep_num_rows($data) == 0){
				return array("Email doesn't match any account in our website!");
			}
			foreach($data as $d){
				if($d['password'] == md5($password)){
					if($d['status'] == "Unverified"){
						return array("Account not already Verified!", $d['employee_id'], $email);
					}else if($d['status'] == "Email Send"){
						return array("Verification Email Already Sent!", $d['employee_id'], $email);
					}else if($d['status'] == "Email Verified"){
						return array("Email is only Verified!", $d['employee_id'], $email, $password);
					}else{
						return array("Success", $d['employee_id'], $d['uname'], $email, $d['position']);
					}
				}
				else{
					return array("Your Password is incorrect!");
				}
			}
		}


		
		function sub_log_in($id, $code){
			return $this -> db -> prep_query("SELECT employee_id, concat(fname, ' ', mname, ' ', lname) as uname, email, status, position FROM employee where employee_id = ? and email_ver = ?", array($id, $code), "is");
		}

		function add_category($sub_cat_name, $cat_name){
			return $this -> db -> prep_query("INSERT INTO category(sub_cat_name, cat_name) VALUES(?, ?)", array($sub_cat_name, $cat_name), "ss");
		}

		function is_category_exist($sub_cat_name){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("SELECT sub_cat_name FROM category where sub_cat_name = ?", array($sub_cat_name), "s"));
		}

		function get_all_category(){
			return $this -> db -> prep_query("SELECT * FROM category where sub_cat_name <> ?", array("Custom"), "s");
		}

		function get_one_category($cat_id){
			return $this -> db -> prep_query("SELECT sub_cat_name, cat_name FROM category where category_id = ?", array($cat_id), "i");
		}

		function get_clothing_by_id($clothing_id){
			return $this -> db -> prep_query("select concat(clothing_brand.brand_name,'(', clothing_fabric.name, ' ', clothing_type.type, ')') as clothing from clothing inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing.ct_id = clothing_type.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id where clothing.clothing_id = ?", array($clothing_id), "i");
		}

		function add_product($prod_name, $prod_desc, $cat_id, $date, $employee_id){
			if ($this -> db -> prep_query("INSERT INTO product_design(name, description, category_id) VALUES(?, ?, ?)", array($prod_name, $prod_desc, $cat_id), "ssi")){
				$pd_id = $this -> db -> getLastId();
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "product_design", "Add Clothing Design \"$prod_name\"", $employee_id, $pd_id), "sssii"))
					return "Success";
				else
					return "Product design logs adding failed";
			}else
				return "Product design adding failed";			
		}

		function add_prod($pd_id, $color_id, $employee_id, $date){
			if($this -> db -> prep_query("insert into product (pd_id, clothing_color_id) values(?, ?)", array($pd_id, $color_id), "ii")){
				$product = $this -> db -> prep_query("select product.product_id, product_design.name, clothing_color.color, concat(clothing_brand.brand_name,'(',clothing_type.type, ' ', clothing_fabric.name, ')') as cloth_name  from product inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join product_design on product_design.pd_id = product.pd_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing.cb_id = clothing_brand.cb_id inner join clothing_type on clothing.ct_id = clothing_type.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id where product.pd_id = ?", array($pd_id), "i");

				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "product", "Add Product with Product Design \"".$product[0]['name']."\" with Clothing \"".$product[0]['cloth_name']."\" color \"".$product[0]['color']."\"", $employee_id, $product[0]['product_id']), "sssii")){
					return "Success";
				}else
					return "Product logs adding failed";
			}else
				return "Product adding failed";
		}

		function get_clothing_size_id($clothing_color_id){
			return $this -> db -> prep_query("select clothing_size_desc.ssd_id from clothing_color inner join clothing on clothing.clothing_id = clothing_color.clothing_id inner join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id where clothing_color.sc_id = ?", array($clothing_color_id), "i");
		}

		function update_product($prod_id, $price_normal, $price_xl, $price_xxl, $color, $name, $date, $employee_id){
			$success_shirt = false;
			$success_prod = false;
			$prices = array(array($price_normal, "price_normal", "Normal Price"), array($price_xl, "price_xl", "XL Price"), array($price_xxl, "price_xxl", "XXL Price"));
			$price_change = array();
			$str_prices = "";
			$str_dt_prices = "";
			$price_log = "";
			foreach ($prices as $p) {
				if($p[0] !== "null"){
					array_push($price_change, $p[0]);
					$str_prices .= $p[1]."=?,";
					$str_dt_prices .= "d";	
					$price_log .= $p[2]." to ".$p[0].", ";			
				}
			}
			$str_dt_prices.= "i";
			array_push($price_change, $prod_id);
			$str_prices = trim($str_prices,",");
			$price_log = trim($price_log, ", ");
			//print_r($price_change);
			if($str_prices != ""){
				//echo "update product_design set $str_prices where pd_id = ?";
				//print_r($price_change);
				//echo $str_dt_prices;
				if($this -> db -> prep_query("update product_design set $str_prices where pd_id = ?", $price_change, $str_dt_prices)){
					$id = $this -> db -> getLastId();
					if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "product_design", "Update ".$name."'s $price_log ", $employee_id, $prod_id), "sssii")){
						$success_shirt = true;
					}
				}else
					return false;
			}
			if($color == 0){
				$success_prod = true;
			}else{
				$ctr = 0;
				$ctr_2 = 0;
				foreach($color as $c){
					$clothing_size_id = $this -> get_clothing_size_id($c);
					foreach ($clothing_size_id as $csi) {
						if($this -> db -> prep_query("insert into product(pd_id, clothing_color_id, clothing_size_id) values(?, ?, ?)", array($prod_id, $c, $csi['ssd_id']), "iii")){
							$ctr ++;
						}
						$ctr_2 ++;
					}
				}
				if($ctr == $ctr_2){
					$success_prod = true;
				}else
					return false;
			}
			if($success_prod || $success_shirt){
				return true;
			}

		}

		function get_brand($color_id){
			return $this -> db -> prep_query("select clothing_color.color, clothing.brand from clothing_color left outer join clothing on clothing_color.clothing_id = clothing.clothing_id where clothing_color.sc_id = ?", array($color_id), "i");
		}

		function get_shirt_template($id){
			return $this -> db -> prep_query("select * from clothing_color_template where sc_id = ?", array($id), "i");
		}

		function view_product_design($start, $limit, $filter, $search, $col_order, $order, $from, $to, $from_sales, $to_sales){
			$sql = "select product_design.pd_id as pi, product_design.name, product_design.description, category.sub_cat_name from product_design left outer join category on product_design.category_id = category.category_id where sub_cat_name <> ? and $filter like concat('%', ?, '%') order by $col_order $order limit ?, ?";
			$sql_val = array("Custom");
			$sql_type = "s";
			array_push($sql_val, $search, $start, $limit);
			$sql_type .= "sii";
			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
			//return $sql_val;
		}

		function get_all_product_design_count($filter, $search){
			return $this -> db -> prep_query("select count(product_design.pd_id) as count from product_design left outer join category on product_design.category_id = category.category_id where $filter like concat('%', ?, '%') and sub_cat_name <> ? ", array($search, "Custom"), "ss");
		}


		function is_product_exist($col, $col_val){
			if($col == "name"){
				return $this -> db -> prep_num_rows($this -> db -> prep_query("select name from product_design where name = ?", array($col_val), "s"));
			}else if($col == "id"){
				return $this -> db -> prep_num_rows($this -> db -> prep_query("select name from product_design where pd_id = ?", array($col_val), "i"));
			}

		}

		function view_one_product_design($id){
			return $this -> db -> prep_query("select pd_id, name,  price_normal, price_xl, price_xxl from product_design where pd_id = ?", array($id), "i");
		}

		function get_product_design_name_by_id($id){
			return $this -> db -> prep_query("select name from product_design where pd_id = ? and category_id <> ?", array($id, 4), "ii");
		}

		function get_pd_name_with_pm($pd_id, $pm_id){
			$sql = "select product_design.name as design ";
			$sql_val = array();
			$sql_type = "";
			if($pm_id > 0)
				$sql .= ", print_method.name as print ";
			$sql .= " from product_design ";
			if($pm_id > 0)
				$sql .= " inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join print_method on print_method.pm_id = product_design_print.pm_id ";
			$sql .= " where product_design.pd_id = ? and product_design.category_id <> ? ";
			array_push($sql_val, $pd_id, 4);
			$sql_type .= "ii";
			if($pm_id > 0){
				$sql .= " and product_design_print.pm_id = ? ";
				array_push($sql_val, $pm_id);
				$sql_type .= "i";
			}
			return $this -> db -> prep_query($sql, $sql_val, $sql_type); 
			//return $sql;
		}

		function get_clothing_name($clothing_id){
			return $this -> db -> prep_query("select clothing_brand.brand_name, clothing_type.type, clothing_fabric.name from clothing inner join clothing_brand on clothing.cb_id = clothing_brand.cb_id inner join clothing_type on clothing.ct_id = clothing_type.ct_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id where clothing.clothing_id = ?", array($clothing_id), "i");
		}
		

		function add_home_banner($home_banner){
			return $this -> db -> prep_query("insert into home_banner(img_banner) values (?)", array($home_banner),"s");
		}

		function get_home_banner_count(){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select hb_id from home_banner"));
		}

		function get_home_banner_possible_id(){
			return $this -> db -> prep_query("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE table_name = 'home_banner' AND table_schema = 'dbmixlarts'");
		}

		function get_all_home_banner(){
			return $this -> db -> prep_query("select * from home_banner");
		}

		function get_one_home_banner($id){
			return $this -> db -> prep_query("select * from home_banner where hb_id = ?", array($id), "i");
		}
		/*function get_home_banner(){
			return $this -> db -> prep_query("select hb_id from home_banner");
		}*/

		function is_brand_exist($brand, $type, $fabric){
			return $this -> db -> prep_query("select cb_id from clothing where cb_id = ? and ct_id = ? and cf_id = ? ", array($brand, $type, $fabric), "iii");
		}

		function add_shirt($brand, $type, $fabric, $size_info, $date, $employee_id){
			if($this -> db -> prep_query("insert into clothing(cb_id, ct_id, cf_id) values(?, ?, ?)", array($brand, $type, $fabric), "iii")){
				$shirt_id = $this -> db -> getLastId();
				$clothing = $this -> db -> prep_query("select distinct clothing_brand.brand_name, clothing_type.type, clothing_fabric.name from clothing LEFT outer join clothing_color on clothing_color.clothing_id = clothing.clothing_id left outer join clothing_brand on clothing.cb_id = clothing_brand.cb_id left outer join clothing_type on clothing.ct_id = clothing_type.ct_id left outer join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id where clothing.clothing_id = ?", array($shirt_id), "i");
				$clothing_str = $clothing[0]['brand_name']."(".$clothing[0]['type']." ".$clothing[0]['name'].")";
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing", "Add clothing information for brand ".$clothing_str, $employee_id, $shirt_id), "sssii"))
				{
					$sql_csd = "insert into clothing_size_desc(size, height, width, price, clothing_id) values ";
					$sql_val_csd = array();
					$sql_type_csd = "";
					for($x = 0; $x < count($size_info); $x++){
						$sql_csd .= "(?, ?, ?, ?, ?), ";
						array_push($sql_val_csd, $size_info[$x][3],$size_info[$x][0], $size_info[$x][1], $size_info[$x][2], $shirt_id);
						$sql_type_csd .= "sdddi";
					}
					$sql_csd = trim($sql_csd, ", ");
					if($this -> db -> prep_query($sql_csd, $sql_val_csd, $sql_type_csd)){
						$sizes = $this -> db -> prep_query("select ssd_id, size FROM clothing_size_desc where ssd_id >= ?", array($this-> db ->getLastId()), "i");
						$sql_size = "insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values ";
						$sql_val_size = array();
						$sql_type_size = "";
						foreach($sizes as $s){
							$sql_size .= "(?, ?, ?, ?, ?), ";
							array_push($sql_val_size, $date, "clothing_size_desc", "Add size ".$this -> arr_const_size[$s['size']]." Information for clothing ".$clothing_str, $employee_id, $s['ssd_id']);
							$sql_type_size .= "sssii";
						} 
						$sql_size = trim($sql_size, ", ");
						return $this ->db-> prep_query($sql_size, $sql_val_size, $sql_type_size);
					}else{
						return false;
					}	
				}
				
			}
		}

		function add_shirt_size_info($clothing_id, $size_info, $date, $employee_id, $clothing_str){
			$sql_csd = "insert into clothing_size_desc(size, height, width, price, clothing_id) values ";
			$sql_val_csd = array();
			$sql_type_csd = "";
			for($x = 0; $x < count($size_info); $x++){
				$sql_csd .= "(?, ?, ?, ?, ?), ";
				array_push($sql_val_csd, $size_info[$x][0], $size_info[$x][1],$size_info[$x][2], $size_info[$x][3], $clothing_id);
				$sql_type_csd .= "sdddi";
			}
			$sql_csd = trim($sql_csd, ", ");
			if($this -> db -> prep_query($sql_csd, $sql_val_csd, $sql_type_csd)){
				$sizes = $this -> db -> prep_query("select ssd_id, size FROM clothing_size_desc where ssd_id >= ?", array($this-> db ->getLastId()), "i");
				$sql_size = "insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values ";
				$sql_val_size = array();
				$sql_type_size = "";
				foreach($sizes as $s){
					$sql_size .= "(?, ?, ?, ?, ?), ";
					array_push($sql_val_size, $date, "clothing_size_desc", "Add size ".$this -> arr_const_size[$s['size']]." Information for clothing ".$clothing_str, $employee_id, $s['ssd_id']);
					$sql_type_size .= "sssii";
				} 
				$sql_size = trim($sql_size, ", ");
				if($this ->db-> prep_query($sql_size, $sql_val_size, $sql_type_size)){
					return "Success";
				}else{
					return "Clothing size logs query failed";
				}
			}else{
				return "Clothing size query failed";
			}	
		}

		function add_shirt_color($shirt_id, $color_name, $color_hex, $stocks, $date, $employee_id, $side, $clothing_str, $size_id){
			$ctrClothing = 0;
			
			if($this -> db -> prep_query("insert into clothing_color(color, hex, clothing_id) values(?, ?, ?)", array($color_name, $color_hex, $shirt_id), "ssi")){
				$scs_id = $this -> db -> getLastId();
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_color", "Add color ".$color_name." for brand ".$clothing_str, $employee_id, $scs_id), "sssii")){

					/*$sql_template = "insert into clothing_color_template (pic, side, sc_id) values ";
					$sql_template_val = array();
					$sql_template_type = "";
					$sql_template_logs = "insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values ";
					$sql_template_logs_val = array();
					$sql_template_logs_type = "";

					for($z = 0; $z < count($template); $z++){
						$sql_template .= "(?, ?, ?),";
						array_push($sql_template_val, $template[$z], $side[$z], $scs_id);
						$sql_template_type .= "ssi";
					}

					$sql_template = trim($sql_template, ",");

					if($this -> db -> prep_query($sql_template, $sql_template_val, $sql_template_type)){
						$template_id = $this -> db -> prep_query("select sct_id, side from clothing_color_template where sct_id >= ?", array($this-> db ->getLastId()), "i");
						foreach ($template_id as $ti) {
							$sql_template_logs .= "(?, ?, ?, ?, ?),";
							array_push($sql_template_logs_val, $date, "clothing_color", "Add ".$color_name." ".$ti['side']." Face for brand ".$clothing_str, $employee_id, $ti['sct_id']);
							$sql_template_logs_type .= "sssii";	
						}
						$sql_template_logs = trim($sql_template_logs, ",");
						if($this -> db -> prep_query($sql_template_logs, $sql_template_logs_val, $sql_template_logs_type)){
							$ctrClothing += 1;
						}else{
							return "Add clothing color template logs query failed";
						}
					}else
						return "Add clothing color template query failed";*/

					$sql_stocks = "insert into clothing_stock(size, stock, sc_id) values ";
					$sql_stocks_val = array();
					$sql_stocks_type = "";
					$sql_stocks_logs = "insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values ";
					$sql_stocks_logs_val = array();
					$sql_stocks_logs_type = "";

					for($y = 0; $y < count($stocks); $y++){
						$sql_stocks .= "(?, ?, ?),";
						array_push($sql_stocks_val, $size_id[$y], $stocks[$y], $scs_id);
						$sql_stocks_type .= "iii";
					}
					$sql_stocks = trim($sql_stocks, ",");

					if($this -> db -> prep_query($sql_stocks, $sql_stocks_val, $sql_stocks_type)){
						$stocks_id = $this -> db -> prep_query("select distinct clothing_stock.scs_id, clothing_stock.stock, clothing_size_desc.size from clothing_stock inner join clothing_size_desc on clothing_size_desc.ssd_id = clothing_stock.size where scs_id >= ?", array($this-> db ->getLastId()), "i") ;
						foreach ($stocks_id as $si) {
							$sql_stocks_logs .= "(?, ?, ?, ?, ?),";
							array_push($sql_stocks_logs_val, $date, "clothing_stock", "Add ".$si['stock']." Stocks for size ".$this -> arr_const_size[$si['size']]." of brand ".$clothing_str, $employee_id, $si['scs_id']);
							$sql_stocks_logs_type .= "sssii"; 
						}	
						$sql_stocks_logs = trim($sql_stocks_logs, ",");
						if(($this -> db -> prep_query($sql_stocks_logs, $sql_stocks_logs_val, $sql_stocks_logs_type)))
							$ctrClothing += 1;
						else
							return "Clothing stocks logs query failed";
					}else
						return "Clothing stocks query failed";
					$ctrClothing += 1;
				}else
					return "Clothing color logs query failed";
				$ctrClothing += 1;
			}else
				return "Clothing color query failed";
			if($ctrClothing == 3)	
				return "Success";
		}

		function get_clothing_size_id_by_clothing($color, $size, $clothing_id){
			$size_id = array();
			for($x = 0; $x < count($color); $x++){
				for($y = 0; $y < count($size[$x]); $y++){
					$si = $this -> db -> prep_query("select ssd_id from clothing_size_desc where size = ? and clothing_id = ?", array($size[$x][$y], $clothing_id), "ii");
					array_push($size_id, $si[0]['ssd_id']);
				}
			}
			return $size_id;
		}

		function is_color_name_exist($color_name, $shirt_id){
			if($this -> db -> prep_query("select color from clothing_color where clothing_id = ? and color = ?", array($shirt_id, $color_name), "is")){
				return true;
			}
		}

		function is_color_hex_exist($color_hex, $shirt_id){
			if($this -> db -> prep_query("select color from clothing_color where clothing_id = ? and hex = ?", array($shirt_id, $color_hex), "is")){
				return true;
			}
		}
		
		function get_all_shirt_count($filter, $search, $product_design_id, $pm_id){
			$sql = "select count(distinct clothing.clothing_id) as count from clothing  inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id left outer join clothing_color on clothing_color.clothing_id = clothing.clothing_id left outer join product on clothing_color.sc_id= product.clothing_color_id left outer join product_design on product_design.pd_id = product.pd_id where $filter like concat('%', ?, '%')  ";
			$sql_val = array($search);
			$sql_type = "s";

			if($product_design_id > 0){
				$sql .= " and product_design.pd_id = ? ";
				array_push($sql_val, $product_design_id);
				$sql_type .= "i";
			}

			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function get_clothing($clothing_id, $product_design_id, $pm_id){
			$sql = "select distinct clothing.clothing_id as ci, clothing_brand.brand_name, clothing_type.type, clothing_fabric.name";
			$sql_val = array();
			$sql_type = "";
			if($product_design_id > 0){
				$sql .= ", (select name from product_design where pd_id = ?) as pd_name";
				array_push($sql_val,$product_design_id);
				$sql_type .= "i";
			}

			if($pm_id > 0){
				$sql .= ", (select name from print_method where pm_id = ?) as pm_name";
				array_push($sql_val,$pm_id);
				$sql_type .= "i";
			}

			$sql .= " from clothing inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id left outer join clothing_color on clothing_color.clothing_id = clothing.clothing_id left outer join product on product.clothing_color_id = clothing_color.sc_id ";
			if($product_design_id > 0){
				$sql .= " inner join product_design on product_design.pd_id = product.pd_id ";
			}

			if($pm_id > 0){
				$sql .= " inner join product_design_print on product_design_print.pd_id = product_design.pd_id ";

			}
			$sql .= " WHERE clothing.clothing_id = ? ";
			array_push($sql_val, $clothing_id);
			$sql_type .= "i";
			if($product_design_id > 0){
				$sql .= " and product_design.pd_id = ? and product_design.category_id <> ? ";
				array_push($sql_val, $product_design_id, 4);
				$sql_type .= "ii";
			}

			if($pm_id > 0){
				$sql .= " and product_design_print.pm_id = ? ";
				array_push($sql_val, $pm_id);
				$sql_type .= "i";
			}
			
			
			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function view_shirt($start, $limit, $filter, $search, $col_order, $order, $product_design_id, $from, $to, $from_sales, $to_sales, $pm_id){
			$sql = "select distinct clothing.clothing_id as ci, clothing_brand.brand_name, clothing_type.type, clothing_fabric.name, (select sum(clothing_stock.stock) from clothing_stock inner join clothing_color on clothing_color.sc_id = clothing_stock.sc_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id ";
			if($product_design_id > 0 && $pm_id > 0){
				$sql .= " inner join product on product.clothing_color_id = clothing_color.sc_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id ";
			}
			$sql .= " where clothing.clothing_id = ci ";
			$sql_val = array();
			$sql_type = "";
			if($product_design_id > 0 && $pm_id > 0){
				$sql .= " and  product.pd_id = ? and product_design_print.pm_id = ?";			
				array_push($sql_val, $product_design_id, $pm_id);
				$sql_type .= "ii";
			}
			$sql .= ") as total_clothing_stock from clothing  inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id left outer join clothing_color on clothing_color.clothing_id = clothing.clothing_id left outer join product on clothing_color.sc_id= product.clothing_color_id left outer join product_design on product_design.pd_id = product.pd_id where $filter like concat('%', ?, '%') ";
			array_push($sql_val, $search);
			$sql_type .= "s";

			if($product_design_id > 0){
				$sql .= " and product.pd_id = ? ";
				array_push($sql_val, $product_design_id);
				$sql_type .= "i";
			}

			$sql .= " order by $col_order $order limit ?, ? ";
			array_push($sql_val, $start, $limit);
			$sql_type .= "ii";
			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function get_unlisted_clothing($product_design_id){
			$sql = "select distinct clothing.clothing_id, concat(clothing_brand.brand_name, '(' ,clothing_type.type,' ',clothing_fabric.name, ')') as name from clothing  inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join clothing_color on clothing_color.clothing_id = clothing.clothing_id left outer join product on product.clothing_color_id = clothing_color.sc_id ";

			$sql_val = array();
			$sql_type = "";

			if($color_listed = $this -> db -> prep_query("select distinct clothing_color_id from product where pd_id = ?", array($product_design_id), "i")){
				$sql .= " where ";
				foreach ($color_listed as $cl) {
					$sql .= " clothing_color.sc_id <> ? and ";
					array_push($sql_val, $cl['clothing_color_id']);
					$sql_type .= "i";
				}
				$sql = trim($sql, "and ");
			}

			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function get_unlist_colors($product_design_id, $clothing_id){
			$sql = "select distinct clothing_color.sc_id, clothing_color.color from clothing_color left outer join product on product.clothing_color_id = clothing_color.sc_id where clothing_color.clothing_id = ?";
			$sql_val = array($clothing_id);
			$sql_type = "i";

			if($color_listed = $this -> db -> prep_query("select distinct clothing_color_id from product where pd_id = ?", array($product_design_id), "i")){
				$sql .= " and ";
				foreach ($color_listed as $cl) {
					$sql .= " clothing_color.sc_id <> ? and ";
					array_push($sql_val, $cl['clothing_color_id']);
					$sql_type .= "i";
				}
				$sql = trim($sql, "and ");
			}

			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}
		
		function view_available_shirt_size($clothing_id){
			return $this -> db -> prep_query("select ssd_id, size from clothing_size_desc where clothing_id = ? order by size asc", array($clothing_id), "i");
		}


		function view_shirt_size_info($id){
			return $this -> db -> prep_query("select clothing_size_desc.ssd_id,  clothing_size_desc.height, clothing_size_desc.width, clothing_size_desc.price, clothing_size_desc.size from clothing_size_desc  where clothing_size_desc.clothing_id = ? order by clothing_size_desc.size asc", array($id), "i");
		}

		function view_shirt_color_stock($clothing_color_id, $product_design_id, $pm_id){
			$sql = "select distinct clothing_stock.scs_id,clothing_stock.stock,(select time from employee_logs where entity_id = clothing_stock.scs_id and tbl_name = 'clothing_stock' order by time desc limit 1) as time, clothing_color.sc_id as cc_id,  clothing_size_desc.size, clothing_size_desc.ssd_id as csd_id from clothing_color inner join clothing on clothing.clothing_id = clothing_color.clothing_id left outer join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id left outer join clothing_stock on clothing_stock.size = clothing_size_desc.ssd_id and clothing_color.sc_id = clothing_stock.sc_id ";
			$sql_val = array();
			$sql_type = "";
			if($product_design_id > 0 || $pm_id > 0){
				$sql .= "inner join product on product.clothing_color_id = clothing_color.sc_id  inner join product_design on product_design.pd_id = product.pd_id left outer join product_design_print on product_design_print.pd_id = product_design.pd_id ";
			}
			$sql .= " where clothing_color.sc_id = ? "; 
			array_push($sql_val, $clothing_color_id);
			$sql_type .= "i";
			if($product_design_id > 0){
				$sql .= " and product.pd_id = ? ";
				array_push($sql_val, $product_design_id);
				$sql_type .= "i";
			}

			if($pm_id > 0){
				$sql .= " and product_design_print.pm_id = ? ";
				array_push($sql_val, $pm_id);
				$sql_type .= "i";
			}
			$sql .= " order by clothing_size_desc.size asc";

			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
			
		}

		function view_shirt_color($id, $search, $start, $per_page, $col_order, $order, $product_design_id, $pm_id){
			$sql_val = array();
			$sql_type = "";
			$sql = "select distinct clothing.clothing_id, clothing_color.sc_id as sci, clothing_color.color, clothing_color.hex from clothing_color inner join clothing on clothing.clothing_id = clothing_color.clothing_id ";
			if($product_design_id > 0 || $pm_id > 0){
				$sql .= "inner join product on product.clothing_color_id = clothing_color.sc_id  inner join product_design on product_design.pd_id = product.pd_id left outer join product_design_print on product_design_print.pd_id = product_design.pd_id ";
			}
			$sql .= " where clothing.clothing_id = ? and clothing_color.color like concat('%', ?, '%') ";
			array_push($sql_val, $id, $search);
			$sql_type .= "is";

			if($product_design_id > 0){
				$sql .= " and product.pd_id = ? ";
				array_push($sql_val, $product_design_id);
				$sql_type .= "i";
			}

			if($pm_id > 0){
				$sql .= " and product_design_print.pm_id = ? ";
				array_push($sql_val, $pm_id);
				$sql_type .= "i";
			}

			$sql .= " order by $col_order $order limit ?, ? ";
			array_push($sql_val, $start, $per_page);
			$sql_type .= "ii";
			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function view_shirt_color_count($clothing_id, $search, $product_design_id, $pm_id){
			$sql = "select count(distinct clothing_color.sc_id) as count from clothing_color inner join clothing on clothing.clothing_id = clothing_color.clothing_id ";
			if($product_design_id > 0 && $pm_id > 0){
				$sql .= " left outer join product on clothing_color.sc_id= product.clothing_color_id inner join product_design_print on product_design_print.pd_id = product.pd_id ";
			}
			$sql.= " where clothing.clothing_id = ? and clothing_color.color like concat('%', ?, '%') ";
			$sql_val = array($clothing_id, $search);
			$sql_type = "is";
			if($product_design_id > 0 && $pm_id > 0){
				$sql .= " and product.pd_id = ? and product_design_print.pm_id = ? ";
				array_push($sql_val, $product_design_id, $pm_id);
				$sql_type .= "ii";
			}
			return $this -> db -> prep_query($sql, $sql_val, $sql_type);

			//return $product_design_id;
		}

		/*function view_unlist_color($id){
			$color_listed = $this -> db -> prep_query("select distinct clothing_color_id from product where pd_id = ?", array($id), "i");
			$arr_val = array();
			$str_val = "";
			$sql = "select distinct clothing_brand.brand_name, clothing.thickness, clothing_fabric.name, clothing_type.type, clothing_color.sc_id, clothing_color.color, clothing_color.hex from clothing_color left outer join product on product.clothing_color_id = clothing_color.sc_id left outer join clothing on clothing_color.clothing_id = clothing.clothing_id  left outer join clothing_brand on clothing_brand.cb_id = clothing.cb_id left outer join clothing_type on clothing.ct_id = clothing_type.ct_id left outer join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id where ";
			foreach ($color_listed as $cl) {
				$sql .= "clothing_color.sc_id <> ? and ";
				array_push($arr_val, $cl['clothing_color_id']);
				$str_val .= "i";
			}
			$sql = trim($sql, "and ");
			return $this -> db -> prep_query($sql, $arr_val, $str_val);
			//return $sql;
		}*/

		
		/*function view_clothing_color_stock($clothing_color_id){
			return $this -> db -> prep_query("select distinct (select time from employee_logs where entity_id = clothing_stock.scs_id and description like concat('%', 'Add', '%') and tbl_name = 'clothing_stock' order by time asc limit 1) as time, clothing_color.sc_id as ccsc_id, clothing_stock.scs_id, clothing_size_desc.size, clothing_stock.stock, clothing_size_desc.ssd_id as csd_id, (select time from employee_logs where entity_id = clothing_stock.scs_id and tbl_name = 'clothing_stock') as time, (select sum(quantity) from sub_order_history inner join product on product.product_id = sub_order_history.product_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing_size_desc on clothing_size_desc.ssd_id = product.clothing_size_id where clothing_color.sc_id = ccsc_id and clothing_size_desc.ssd_id = csd_id) as used from clothing_stock inner join clothing_color on clothing_color.sc_id = clothing_stock.sc_id inner join clothing_size_desc on clothing_size_desc.ssd_id = clothing_stock.size inner join clothing on clothing.clothing_id = clothing_size_desc.clothing_id  where clothing_color.sc_id = ? order by clothing_size_desc.size asc", array($clothing_color_id), "i");
		}*/

		function get_size_info($id){
			return $this -> db -> prep_query("select ssd_id, size, height, width, price, (select time from employee_logs where entity_id = ssd_id and tbl_name = 'clothing_size_desc' order by time limit 1) as time from clothing_size_desc where clothing_size_desc.clothing_id = ?", array($id), "i");
		}

		function add_clothing_color_stock($size, $clothing_color_id, $size_id, $stock, $clothing, $employee_id, $date, $clothing_id, $pd_id, $pm_id){
			if($this -> db -> prep_query("INSERT INTO clothing_stock (size, stock, sc_id) VALUES (?, ?, ?)", array($size_id, $stock, $clothing_color_id), "iii")){
				$id = $this -> db -> getLastId();
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_stock", "Add ".$stock." Stocks for size ".$this -> arr_const_size[$size]." of brand \"$clothing\"", $employee_id, $id), "sssii")){
					$datetime = new DateTime($date);
					return "Success#".$datetime->format('F j, Y g:ia')."#".$id;
					//return "Success";	
				}else{
					return "Add color clothing stocks logs failed";
				}
			}else{
				return "Add color clothing stocks failed";
			}
		}

		function update_clothing_color_stock($stock_id, $stocks, $size, $clothing, $employee_id, $date, $color_id, $clothing_id, $pd_id, $pm_id){
			if($this -> db -> prep_query("update clothing_stock set stock = ? where scs_id = ?", array($stocks, $stock_id), "ii")){
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_stock", "Update ".$stocks." Stocks for size ".$this -> arr_const_size[$size]." of brand \"$clothing\"", $employee_id, $stock_id), "sssii")){
					$datetime = new DateTime($date);
					return "Success#".$datetime->format('F j, Y g:ia');
				}else{
					return "Update color clothing stocks logs failed";
				}
			}else{
				return "Update color clothing stocks failed";
			}
		}

		function get_clothing_size_id_by_size($size, $clothing_color_id){
			if($clothing_id = $this -> db -> prep_query("select clothing_id from clothing_color where sc_id = ?", array($clothing_color_id), "i")){
				return $this -> db -> prep_query("select ssd_id from clothing_size_desc where size = ? and clothing_id = ?", array($size, $clothing_id[0]['clothing_id']), "ii");
			}
		}

		
		function view_one_shirt($id){
			return $this -> db -> prep_query("select * from shirt where shirt_id = ?", array($id), "i");
		}

		function is_shirt_color_exist($shirt_id, $color, $hex){
			return $this -> db -> prep_query("select color, hex from shirt_color where shirt_id = ? and (color = ? or hex = ?)", array($shirt_id, $color, $hex), "iss");
		}

		function get_shirt_brand(){
			return $this -> db -> prep_query("select clothing_id, clothing_brand.brand_name from clothing left outer join clothing_brand on clothing_brand.cb_id = clothing.cb_id");
		}

		function get_shirt_color_brand($id){
			return $this -> db -> prep_query("select color, hex, sc_id from clothing_color where clothing_id = ?", array($id), "i");
		}

		function get_shirt_color($id){
			return $this -> db -> prep_query("select color, hex, sc_id from shirt_color shirt_id where sc_id = ?", array($id), "i");
		}

		/*function get_next_shirt_color_template_id(){

		}*/

		/*function view_product(){
			return $this -> db -> prep_query("select * from product");
		}*/

		function get_product_design($id){
			return $this -> db -> prep_query("select name, description, ave_rating, price_normal, price_xl, price_xxl from product_design where pd_id = ?", array($id), "i");
		}

		function get_product_design_template($id){
			return $this -> db -> prep_query("select design, side from product_design_template where product_id = ?", array($id), "i");
		}

		function get_shirt_color_template($id){
			return $this -> db -> prep_query();
		}

		/*function get_log_time($table_name, $id){
			return $this -> db -> prep_query("select time from employee_logs where tbl_name = ? and entity_id = ? order by  time desc limit 1", array($table_name, $id), "si");
		}*/

		function get_ass_color($id){
			return $this -> db -> prep_query("select distinct clothing_brand.brand_name, clothing.thickness, clothing_type.type, clothing_fabric.name, clothing_color.sc_id, clothing_color.color, clothing_color.hex from clothing_color left outer join product on clothing_color_id = sc_id left outer join clothing on clothing.clothing_id = clothing_color.clothing_id left outer join clothing_brand on clothing_brand.cb_id = clothing.cb_id left outer join clothing_type on clothing.ct_id = clothing_type.ct_id left outer join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id  where product.pd_id = ?", array($id), "i");
		}

		/*function get_shirt_id_via_scid($id){
			return $this -> db -> prep_query("select shirt_id  from shirt_color where sc_id = ?", array($id), "i");
		}*/

		/*function get_product_by_pd($id){
			return $this -> db -> prep_query("select * from product where pd_id = ?", array($id), "i");
		}

		function get_shirt_desc_by_scid($id){
			return $this -> db -> prep_query("select shirt_id from shirt_color where sc_id = ?", array($id), "i");
		}

		function get_brand_by_shirt_id($id){
			return $this -> db -> prep_query("select brand from shirt where shirt_id = ?", array($id), "i");
		}

		function get_shirt_color_shirt_id($id){
			return $this -> db -> prep_query("select color, hex from shirt_color where sc_id = ?", array($id), "i");
		}*/

		/*function view_all_products($start, $per_page, $filter, $search){
			return $this -> db -> prep_query("select product_design.name as design_name, clothing_color.sc_id, product_design.pd_id, clothing_color.color, clothing_size_desc.size, if(clothing_size_desc.size >= 0 and clothing_size_desc.size <= 4, (product_design.price_normal + clothing_size_desc.price), if(clothing_size_desc.size = 5, (product_design.price_xl + clothing_size_desc.price), if(clothing_size_desc.size = 6, (product_design.price_xxl + clothing_size_desc.price), 0))) as price, clothing_brand.brand_name, clothing_type.type, clothing_fabric.name, clothing.thickness, product_design.ave_rating, category.sub_cat_name from product_design LEFT outer join product on product_design.pd_id = product.pd_id left outer join clothing_color on product.clothing_color_id = clothing_color.sc_id left outer join clothing on clothing_color.clothing_id = clothing.clothing_id left outer join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id left OUTER join category on product_design.category_id = category.category_id left outer join clothing_brand on clothing_brand.cb_id = clothing.cb_id left outer join clothing_type on clothing.ct_id = clothing_type.ct_id left outer join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id  left outer join product_design_template on product_design.pd_id = product_design_template.product_id where $filter like concat('%', ?, '%') limit ?, ?", array($search, $start, $per_page), "sii");
		}

		function get_all_product_count($filter, $search){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select product_design.name as design_name, clothing_color.sc_id, product_design.pd_id, clothing_color.color, clothing_size_desc.size, if(clothing_size_desc.size >= 0 and clothing_size_desc.size <= 4, (product_design.price_normal + clothing_size_desc.price), if(clothing_size_desc.size = 5, (product_design.price_xl + clothing_size_desc.price), if(clothing_size_desc.size = 6, (product_design.price_xxl + clothing_size_desc.price), 0))) as price, clothing_brand.brand_name, clothing_type.type, clothing_fabric.name, clothing.thickness, product_design.ave_rating, category.sub_cat_name from product_design LEFT outer join product on product_design.pd_id = product.pd_id left outer join clothing_color on product.clothing_color_id = clothing_color.sc_id left outer join clothing on clothing_color.clothing_id = clothing.clothing_id left outer join clothing_size_desc on clothing_size_desc.clothing_id = clothing.clothing_id left OUTER join category on product_design.category_id = category.category_id left outer join clothing_brand on clothing_brand.cb_id = clothing.cb_id left outer join clothing_type on clothing.ct_id = clothing_type.ct_id left outer join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id where $filter like concat('%', ?, '%')", array($search), "s"));
		}*/

		function add_clothing_type($ct, $employee_id, $date){
			if($this -> db -> prep_query("insert into clothing_type (type) values(?)", array($ct), "s")){
				$id = $this -> db -> getLastId();
				return $this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_type", "Add clothing type \"$ct\"", $employee_id, $id), "sssii");
			}
		}

		function is_clothing_type_exist($ct){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select type from clothing_type where type = ?", array($ct), "s"));
		}

		function view_clothing_type($start, $per_page, $search, $col_order, $order){
			return $this -> db -> prep_query("SELECT * FROM clothing_type where type like concat('%', ?, '%') order by $col_order $order limit ?, ?", array($search, $start, $per_page), "sii");
		}

		function get_all_clothing_type_count($search, $col_order, $order){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("SELECT * FROM clothing_type where type like concat('%', ?, '%') order by $col_order $order", array($search), "s"));
		}

		function view_clothing_fabric($start, $per_page, $search, $col_order, $order){
			return $this -> db -> prep_query("SELECT * FROM clothing_fabric where name like concat('%', ?, '%') order by $col_order $order limit ?, ?", array($search, $start, $per_page), "sii");
		}

		function get_all_clothing_fabric_count($search, $col_order, $order){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("SELECT * FROM clothing_fabric where name like concat('%', ?, '%') order by $col_order $order", array($search), "s"));
		}

		function add_clothing_fabric($cf, $employee_id, $date){
			if($this -> db -> prep_query("insert into clothing_fabric (name) values(?)", array($cf), "s")){
				$id = $this -> db -> getLastId();
				return $this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_fabric", "Add clothing fabric \"$cf\"", $employee_id, $id), "sssii");
			}
		}

		function is_clothing_fabric_exist($cf){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select name from clothing_fabric where name = ?", array($cf), "s"));
		}

		function view_clothing_brand($start, $per_page, $search, $col_order, $order){
			return $this -> db -> prep_query("SELECT * FROM clothing_brand where brand_name like concat('%', ?, '%') order by $col_order $order limit ?, ?", array($search, $start, $per_page), "sii");
		}

		function get_all_clothing_brand_count($search, $col_order, $order){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("SELECT * FROM clothing_brand where brand_name like concat('%', ?, '%') order by $col_order $order", array($search), "s"));
		}

		function add_clothing_brand($cb, $employee_id, $date){
			if($this -> db -> prep_query("insert into clothing_brand (brand_name) values(?)", array($cb), "s")){
				$id = $this -> db -> getLastId();
				return $this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_brand", "Add clothing brand \"$cb\"", $employee_id, $id), "sssii");
			}
		}

		function is_clothing_brand_exist($cb){
			return $this -> db -> prep_num_rows($this -> db -> prep_query("select brand_name from clothing_brand where brand_name = ?", array($cb), "s"));
		}

		function get_all_clothing_brand(){
			return $this -> db -> prep_query("SELECT * FROM clothing_brand order by brand_name");
		}

		function get_all_clothing_type(){
			return $this -> db -> prep_query("SELECT * FROM clothing_type order by type");
		}

		function get_all_clothing_fabric(){
			return $this -> db -> prep_query("SELECT * FROM clothing_fabric order by name");
		}

		function view_order_history($status){
			return $this -> db -> prep_query("select order_history.order_history_id, order_history.ab_id, customer.customer_id, concat(customer.fname ,' ', customer.lname) as buyer, order_history.date, order_history.total_sales, order_history.status, payment.payment_method, payment.session, shipping.courier, shipping.tracking_no, ship_courier.track_url from order_history inner join customer on customer.customer_id = order_history.customer_id INNER JOIN payment ON payment.order_history_id=order_history.order_history_id LEFT JOIN shipping ON shipping.order_history_id=order_history.order_history_id LEFT JOIN ship_courier ON ship_courier.courier_name=shipping.courier WHERE order_history.status = '$status' order by date desc");
		}

		function get_courier(){
			return $this -> db -> prep_query("SELECT courier_name FROM ship_courier");
		}
		function view_one_sub_order_history($order_history_id){
			return $this -> db -> prep_query("select distinct product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing.thickness, clothing_color.color, clothing_stock.size, sub_order_history.quantity, sub_order_history.price, sub_order_history.price_per_quantity from sub_order_history inner join order_history on order_history.order_history_id = sub_order_history.order_history_id inner join clothing on clothing.clothing_id = sub_order_history.clothing_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = sub_order_history.product_design_id inner join clothing_color on clothing_color.sc_id = sub_order_history.color_id inner join clothing_stock on clothing_stock.scs_id = sub_order_history.size_id where order_history.order_history_id = ?", array($order_history_id), "i");
		}

		function view_one_order_history($order_history_id){
			return $this -> db -> prep_query("select order_history.order_history_id, concat(customer.fname  ,' ', customer.lname) as buyer, order_history.date, order_history.total_sales, order_history.net, order_history.vatable_sales, order_history.payment_method, order_history.payment_status from order_history inner join customer on customer.customer_id = order_history.customer_id where order_history.order_history_id = ?", array($order_history_id), "i");
		}

		
		function view_all_employee_logs($start, $per_page, $filter, $search, $order, $from_date, $to_date){
			$sql = "select employee_logs.log_id, employee_logs.time,  employee_logs.description from employee_logs inner join employee on employee_logs.employee_id = employee.employee_id where description like concat('%',?,'%') and description like concat('%',?,'%') ";
			$sql_val = array($search, $filter);
			$sql_type = "ss";

			if($from_date !== "" && $to_date !== ""){
				$sql .= " and time between ? and ? ";
				array_push($sql_val, $from_date, $to_date);
				$sql_type .= "ss";
			}

			$sql .= " order by time $order limit ?, ? ";
			array_push($sql_val, $start, $per_page);
			$sql_type .= "ii";
			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function get_employee_logs_count($filter, $search, $from_date, $to_date){
			$sql = "select count(employee_logs.log_id) as count from employee_logs inner join employee on employee_logs.employee_id = employee.employee_id where description like concat('%',?,'%') and description like concat('%',?,'%') ";
			$sql_val = array($search, $filter);
			$sql_type = "ss";

			if($from_date !== "" && $to_date !== ""){
				$sql .= " and time between ? and ? ";
				array_push($sql_val, $from_date, $to_date);
				$sql_type .= "ss";
			}

			return $this -> db -> prep_query($sql, $sql_val, $sql_type);
		}

		function get_color_name($clothing_color_id){
			return $this -> db -> prep_query("select color, hex from clothing_color where sc_id = ?", array($clothing_color_id), "i");
		}
        
        function add_clothing_color($clothing_id, $clothing_color_id, $product_design_id, $time, $employee_id){
            if($size = $this -> db -> prep_query("select distinct ssd_id, size from clothing_size_desc where clothing_id = ?", array($clothing_id), "i")){
                $get_color = $this -> db -> prep_query("select color from clothing_color where sc_id = ?", array($clothing_color_id), "i");
                $design = $this -> db -> prep_query("select name from product_design where pd_id = ?", array($product_design_id), "i");
                $sql = "INSERT INTO product(pd_id, clothing_color_id, clothing_size_id) VALUES ";
                $sql_val = array();
                $sql_type = "";
                
                $sql_logs = "INSERT INTO employee_logs(time, employee_id, description, entity_id, tbl_name) values ";
                $sql_logs_val = array();
                $sql_logs_type = "";
                
                foreach($size as $si){
                    $sql .= "(?, ?, ?),";
                    array_push($sql_val, $product_design_id, $clothing_color_id, $si['ssd_id']);
                    $sql_type .= "iii";
                    
                }
                $sql = trim($sql, ",");
                $sql_logs = trim($sql_logs, ",");
                if($this -> db -> prep_query($sql, $sql_val, $sql_type)){
                    $id =  $this -> db -> getLastId();
                    foreach($size as $si){
                        $sql_logs .= "(?, ?, ?, ?, ?),";
                        array_push($sql_logs_val, $time, $employee_id, "Add Clothing Color ".$get_color[0]['color']." Size ".$si['size']." for Design ".$design[0]['name'],$id,"product");
                        $sql_logs_type .= "sisis";
                        $id += 1;
                    }
                    $sql_logs = trim($sql_logs, ",");
                    return $this -> db -> prep_query($sql_logs, $sql_logs_val, $sql_logs_type);
                }
                
            }else{
                return false;
            }
        }

        function add_clothing_design_type($type, $date, $employee_id){
        	if($this -> db -> prep_query("insert into print_method (name, availability) values(?, ?)", array($type, "Available"),"ss")){
        		$id = $this -> db -> getLastId();
        		if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "print_method", "Add Printing Method \"$type\" ", $employee_id, $id), "sssii"))
				{
					return "Success";
        		}else{
        			return "Add print method logs failed";
        		}
        	}else{
        		return "Add print method failed";
        	}
        }

        function update_clothing_design_method($pm_id, $employee_id, $av, $date){
        	if($this -> db -> prep_query("update print_method set availability = ? where pm_id = ?", array($av, $pm_id), "si")){
        		$method = $this -> db -> prep_query("select name from print_method where pm_id = ?", array($pm_id), "i");
        		if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "print_method", "Printing Method \"".$method[0]['name']."\" availability set to \"$av\"", $employee_id, $pm_id), "sssii")){
        			return "Success";
        		}else{
        			return "Update print method logs failed";
        		}
        	}else{
        		return "Update print method availability failed";
        	}
        }

        function get_all_print_method_for_prod_count($search, $product_design_id){
        	return $this -> db -> prep_query("select count(distinct print_method.pm_id) as count from print_method left outer join product_design_print on product_design_print.pm_id = print_method.pm_id where product_design_print.pd_id = ? and print_method.name like concat('%', ?, '%') ", array($product_design_id, $search), "is");
        	//return $sql_type;
        }

        function get_all_print_method_for_prod($search, $col_order, $order, $pd_id, $from, $to, $from_sales, $to_sales){
        	$sql = "select distinct print_method.pm_id as pmi, product_design_print.pd_id, print_method.name, print_method.availability , product_design_print.default_price, product_design_print.price_increment from print_method left outer join product_design_print on product_design_print.pm_id = print_method.pm_id where product_design_print.pd_id = ? and print_method.name like concat('%', ?, '%') order by $col_order $order";
        	$sql_val = array();
        	$sql_type = "";
			array_push($sql_val, $pd_id, $search);
			$sql_type .= "is";

        	return $this -> db -> prep_query($sql, $sql_val, $sql_type);
        	//return $sql_type;
        }

        function get_all_print_method($start, $per_page, $search, $col_order, $order){
        	return $this -> db -> prep_query("select distinct print_method.name, print_method.availability ,print_method.pm_id from print_method left outer join product_design_print on product_design_print.pm_id = print_method.pm_id where print_method.name like concat('%', ?, '%') order by $col_order $order limit ?, ? ", array($search, $start, $per_page), "sii");
        }

        function get_all_print_method_count($search, $col_order, $order){
        	return $this -> db -> prep_query("select count(print_method.pm_id) as count from print_method where print_method.name like concat('%', ?, '%') ", array($search), "s");
        }

        function is_print_method_exist($name){
        	return $this -> db -> prep_query("select name from print_method where name = ?", array($name), "s");
        }

        function get_all_unlist_print_method($product_design_id){
        	$pm_id = $this -> db -> prep_query("select print_method.pm_id, print_method.name from print_method inner join product_design_print on product_design_print.pm_id = print_method.pm_id where product_design_print.pd_id = ?", array($product_design_id), "i");
        	$sql = "";
        	$sql_val= array();
        	$sql_type = "";
        	if(count($pm_id)){
        		$sql .= " where ";
	        	foreach ($pm_id as $pi) {
	        		$sql .= " pm_id <> ? or ";
	        		array_push($sql_val, $pi);
	        		$sql_type .= "i";
	        	}
	        	$sql = trim($sql, "or ");
        	}

        	return $this -> db -> prep_query("select pm_id, name from print_method $sql ", $sql_val, $sql_type);
        	//return $sql;
        }

        function add_print_method_for_product($pd_id, $employee_id, $date, $pm_id, $pm_name, $def_price, $price_inc){
        	$product_design = $this -> get_product_design_name_by_id($pd_id);
        	if($this -> db -> prep_query("insert into product_design_print (pm_id, default_price, price_increment, pd_id) values(?, ?, ?, ?)",array($pm_id, $def_price, $price_inc, $pd_id), "iddi")){
        		$id = $this -> db -> getLastId();
        		$product_design = $this -> get_product_design_name_by_id($pd_id);
        		if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "product_design_print", "Add printing method \"$pm_name\" for product design \"".$product_design[0]['name']."\"", $employee_id, $id), "sssii")){
        			return "Success";
        		}else{
        			return "Update printing method $pm_name for product design ".$product_design[0]['name']." failed";
        		}
        	}else{
        		return "Add printing method $pm_name for product design ".$product_design[0]['name']." failed";
        	}
        }

		function update_clothing_price($price_id, $price, $date, $size, $clothing, $employee_id){
			if($this -> db -> prep_query("update clothing_size_desc set price = ? where ssd_id = ?", array($price, $price_id), "ii")){
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "clothing_size_desc", "Set price to $price of size $size $clothing", $employee_id, $price_id), "sssii")){
        			return "Success";
        		}else{
        			return "Fail to add update price log";
        		}
			}else{
				return "Fail to update price";
			}
		}


		function critical_stocks($start, $per_page, $col_order, $order, $filter, $search){
			return $this -> db -> prep_query("select clothing.clothing_id, clothing_color.sc_id, clothing_stock.scs_id, clothing_color.color, clothing_size_desc.size, clothing_stock.stock, clothing_brand.brand_name, clothing_type.type, clothing_fabric.name as fabric, (select time from employee_logs where entity_id = clothing_stock.scs_id and tbl_name = ? order by time desc limit 1) as time from clothing_stock inner join clothing_color on clothing_color.sc_id = clothing_stock.sc_id  inner join clothing_size_desc on clothing_stock.size = clothing_size_desc.ssd_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id and clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id where clothing_stock.stock <= 5 and $filter like concat('%', ?, '%') group by clothing_stock.scs_id order by $col_order $order  limit ?, ?", array("clothing_stock",$search, $start, $per_page), "ssii");
		}
		
		function critical_stocks_count($filter, $search){
			return $this -> db -> prep_query("select count(clothing_stock.scs_id) as count from clothing_stock inner join clothing_color on clothing_color.sc_id = clothing_stock.sc_id  inner join clothing_size_desc on clothing_stock.size = clothing_size_desc.ssd_id inner join clothing on clothing.clothing_id = clothing_color.clothing_id and clothing_size_desc.clothing_id = clothing.clothing_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id where clothing_stock.stock <= 5 and $filter like concat('%', ?, '%')", array($search), "s");
		}
		
		function sales($from, $to){
			return $this -> db -> prep_query("select concat(customer.fname,' ',customer.lname) as customer_name, sum(sub_order_history.quantity) as orders, sum(sub_order_history.price) as price from order_history inner join sub_order_history on sub_order_history.order_history_id = order_history.order_history_id inner join customer on customer.customer_id = order_history.customer_id where order_history.date BETWEEN ? and ?", array($from, $to), "ss");
		}
		
		function get_raw_materials($start, $per_page, $col_order, $order, $filter, $search, $status){
			$sql = "";
			if($status == "CRITICAL"){
				$sql .= " and stock <= 10 ";
			}
			return $this -> db -> prep_query("select rm_id, name, description, price, stock,(select time from employee_logs where tbl_name = 'raw_material' and entity_id = rm_id order by time desc limit 1) as time from raw_material where $filter like concat('%', ?, '%') $sql order by $col_order $order limit ?, ?", array($search, $start, $per_page), "sii");
		}
		
		function get_raw_materials_count($filter, $search, $status){
			$sql = "";
			if($status == "CRITICAL"){
				$sql .= " and stock <= 10 ";
			}
			return $this -> db -> prep_query("select count(rm_id) as count from raw_material where $filter like concat('%', ?, '%') $sql ", array($search), "s");
		}
		
		function add_raw_material($name, $description, $stocks, $price, $date, $id){
			if($this -> db-> prep_query("insert into raw_material (name, description, price, stock) values(?, ?, ?, ?)", array($name, $description, $stocks, $price), "ssid"))
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "raw_material", "Add Raw Material \"$name\" with stocks $stocks", $id, $this->db-> getLastId()), "sssii")){
					return "Success";
				}else{
					return "Failed to add raw materials log";
				}
			else 
				return "Failed to add raw materials";
		}
		function update_raw_material($id, $stock, $price, $name, $date, $employee_id){
			$sql = "update raw_material set ";
			$sql_val = array();
			$sql_type = "";
			$logs = "Update Raw Materials' ";
			if($stock != ""){
				$sql .= " stock = ? ";
				array_push($sql_val, $stock);
				$sql_type .= "i";
				$logs .= " Stocks to $stock ";
			}
			
			if($price != ""){
				if($stock != "")
					$sql .= ", ";
				$sql .= " price = ? ";
				array_push($sql_val, $price);
				$sql_type .= "i";
				$logs .= " Price to $price ";
			}
			//return $sql;
			$sql .= " where rm_id = ? ";
			array_push($sql_val, $id);
			$sql_type .= "i";
			if($this -> db -> prep_query($sql, $sql_val, $sql_type)){
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "raw_material", $logs, $employee_id, $id), "sssii")){
					
					$datetime = new DateTime($date);
					return "Success#".$datetime->format('F j, Y g:ia');
				}else{
					return "Failed to add raw materials logs";
				}
			}else{
				return "Failed to update raw materials";
			}
		}

		function get_design_price($pd_id, $pm_id){
			return $this -> db -> prep_query("select default_price, price_increment from product_design_print where pm_id = ? and pd_id = ?", array($pm_id, $pd_id), "ii");
		}
	
		function view_courier(){
			return $this -> db -> prep_query("SELECT courier_name, ship_fee, track_url FROM ship_courier");
		}

		function get_customer($start, $per_page, $filter, $search){
			return $this -> db -> prep_query("select customer.customer_id, customer.email, customer.lname, customer.fname, address_book.contact_number, address_book.home_add, address_book.brgy, address_book.town, address_book.province, customer.type from customer left outer join address_book on address_book.customer_id = customer.customer_id where address_book.status = ? and $filter like concat('%',?,'%') limit ?, ?", array("PRIMARY", $search, $start, $per_page), "ssii");
		}

		function get_customer_count($filter, $search){
			return $this -> db -> prep_query("select count(customer_id) as count from customer where $filter like concat('%',?, '%')", array($search), "s");
		}
	
		function update_product_design_print($id, $def_price, $price_inc, $product_design, $print_method, $date, $employee_id){
			$sql = "";
			$sql_val = array();
			$sql_type = "";

			$update_entity = "";

			if($def_price != ""){
				$update_entity .= " Default Price ";
				$sql .= " set default_price = ? ";
				array_push($sql_val, $def_price);
				$sql_type .= "d";
			}

			if($price_inc != ""){
				if($sql == ""){
					$sql .= " set ";
					$update_entity .= " and ";
				}else
					$sql .= ", ";
				$update_entity .= " Price Increment ";
				$sql .= " price_increment = ? ";
				array_push($sql_val, $price_inc);
				$sql_type .= "d";
			}

			$sql = "update product_design_print $sql ";
			if($this -> db -> prep_query($sql, $sql_val, $sql_type)){
				if($this -> db -> prep_query("insert into employee_logs (time, tbl_name, description, employee_id, entity_id) values(?, ?, ?, ?, ?)", array($date, "product_design_print", "Update $update_entity of $product_design($print_method)", $employee_id, $id), "sssii"))
					return "Success";
				else
					return "Product design print logs adding failed";
			}else{
				return "Failed to update product design print";
			}
		}

		function one_order($order_history_id){
			return $this -> db -> prep_query("select order_history.order_history_id, order_history.date, order_history.total_sales, order_history.status, payment.payment_method, shipping.courier, shipping.tracking_no, address_book.fname, address_book.lname, address_book.home_add, address_book.brgy, address_book.town, address_book.province from order_history inner join customer on customer.customer_id = order_history.customer_id INNER JOIN payment ON payment.order_history_id=order_history.order_history_id inner join address_book on address_book.ab_id = order_history.ab_id LEFT JOIN shipping ON shipping.order_history_id=order_history.order_history_id  WHERE order_history.order_history_id = ?", array($order_history_id), "i");
		}

		function sub_orders($order_history_id){
			return $this -> db -> prep_query("select distinct sub_cart.sub_cart_id, product_design.name as product_name, clothing_brand.brand_name as brand, clothing_type.type, clothing_fabric.name as fabric, clothing_color.color, clothing_color.hex, clothing_size_desc.size, clothing_size_desc.ssd_id, sub_cart.quantity, print_method.name as print_method, sub_cart.price, sub_cart.draw_location from sub_cart inner join cart on cart.cart_id = sub_cart.cart_id inner join product on product.product_id = sub_cart.product_id inner join clothing_size_desc on clothing_size_desc.ssd_id = sub_cart.size_id inner join print_method on print_method.pm_id = sub_cart.pm_id inner join clothing_color on clothing_color.sc_id = product.clothing_color_id inner join clothing on  clothing.clothing_id = clothing_color.clothing_id inner join clothing_brand on clothing_brand.cb_id = clothing.cb_id inner join clothing_fabric on clothing_fabric.cf_id = clothing.cf_id inner join clothing_type on clothing_type.ct_id = clothing.ct_id inner join product_design on product_design.pd_id = product.pd_id inner join product_design_print on product_design_print.pd_id = product_design.pd_id inner join order_history on order_history.cart_id = cart.cart_id where order_history.order_history_id = ?", array($order_history_id), "i");
		}
	}
?>
